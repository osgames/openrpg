import wx
import types

#This is a bad import fix it
from orpg.mapper.miniatures_handler import *

from orpg.orpgCore import open_rpg
from orpg.orpgCore import ParserContext
from orpg.pluginhandler import PluginHandler
from orpg.mapper.base_handler import base_layer_handler

# these functions will be added to the OpenRPG miniatures_handler, some override existing methods
def my_on_node_menu_item(self, evt):
    menuid = evt.GetId()
    if menuid >= self.menuid0 and menuid < self.menuid0+100:
        handler = open_rpg.get_component("tree").GetPyData(self.menuId_nodeId_map[menuid])
        text = ''
        if handler:
            text = handler.get_value() # or should it be tohtml()?
        #self.selectAliasName(self.sel_rmin.label)
        open_rpg.get_component("chat").ParsePost(text, True, True, ParserContext(handler))

def my_selectAliasName(self, aliasName):
    names = open_rpg.get_component("chat").alias_bar._alias_list.GetItems()
    index = 0
    for name in names:
        if name == aliasName:
            open_rpg.get_component("chat").alias_bar._alias_list.SetSelection(index)
            return
        index += 1

def my_recursiveAddMenuNode(self, tree_node_id, parent_menu, menuid, top_name):
    if menuid == self.menuid0+100: # max of 100 added menu items
        return
    (child_id, cookie) = open_rpg.get_component("tree").GetFirstChild(tree_node_id)
    if child_id.IsOk():
        # a container means a sub-menu
        menu = wx.Menu()
        myName = top_name
        if myName == None:
            myName = open_rpg.get_component("tree").GetItemText(tree_node_id)
        parent_menu.AppendMenu(menuid, myName, menu)
        menuid += 1
        while child_id.IsOk():
            menuid = self.recursiveAddMenuNode(child_id, menu, menuid, None)
            (child_id, cookie) = open_rpg.get_component("tree").GetNextChild(tree_node_id, cookie)
    else:
        # leaf node means a normal menu item
        parent_menu.Append(menuid, open_rpg.get_component("tree").GetItemText(tree_node_id))
        self.menuId_nodeId_map[menuid] = tree_node_id
        menuid += 1
    return menuid

def my_on_mini_dclick(self, evt, mini):
    """overrides the default function"""
    if mini.label:
        handler = open_rpg.get_component("tree").get_handler_by_path("mini_left_dclick", ParserContext(mini.label))
        if handler != None:
            event = 0
            handler.on_use(event)

def my_prepare_mini_rclick_menu(self, evt):
    """overrides the default function"""
    self.build_menu()
    handler = open_rpg.get_component("tree").get_handler_by_path("mini_right_click", ParserContext(self.sel_rmin.label))
    if handler is None:
        return
    parent_id = handler.mytree_node
    if not parent_id:
        return
    self.min_menu.AppendSeparator()
    self.menuId_nodeId_map = {}
    self.recursiveAddMenuNode(parent_id, self.min_menu, self.menuid0, "Nodes")

def my_bind_100_ids(self):
    self.menuid0 = wx.NewId()
    self.canvas.Bind(wx.EVT_MENU, self.on_node_menu_item, id=self.menuid0)
    for i in range(1, 99):
        wx.NewId()
        self.canvas.Bind(wx.EVT_MENU, self.on_node_menu_item, id=self.menuid0+i)

def my_unbind_100_ids(self):
    for i in range(100):
        self.canvas.Unbind(wx.EVT_MENU, id=self.menuid0+i)
    self.build_menu()


class Plugin(PluginHandler):
    def __init__(self, plugindb, parent):
        PluginHandler.__init__(self, plugindb, parent)
        self.name = 'Mini Menu'
        self.author = 'David'
        self.help = """add context menu and double-click popup to map minis.
Requires OpenRPG 1.8.0+ development
EXPERIMENTAL! In the process of being updated."""

    def plugin_enabled(self):
        # all we do is add methods to the miniatures_handler object, and call bind
        m = open_rpg.get_component("map").layer_handlers[2]
        self.old_prepare_mini_rclick_menu = m.prepare_mini_rclick_menu
        self.old_on_mini_dclick = m.on_mini_dclick
        m.prepare_mini_rclick_menu = types.MethodType(my_prepare_mini_rclick_menu, m, m.__class__)
        m.on_mini_dclick           = types.MethodType(my_on_mini_dclick, m, m.__class__)
        m.recursiveAddMenuNode     = types.MethodType(my_recursiveAddMenuNode, m, m.__class__)
        m.selectAliasName          = types.MethodType(my_selectAliasName, m, m.__class__)
        m.on_node_menu_item        = types.MethodType(my_on_node_menu_item, m, m.__class__)
        m.bind_100_ids             = types.MethodType(my_bind_100_ids, m, m.__class__)
        m.unbind_100_ids           = types.MethodType(my_unbind_100_ids, m, m.__class__)
        m.bind_100_ids()

    def plugin_disabled(self):
        # call unbind, put back old methods, delete methods that didn't exist before
        m = open_rpg.get_component("map").layer_handlers[2]
        m.unbind_100_ids()
        m.prepare_mini_rclick_menu = self.old_prepare_mini_rclick_menu
        m.on_mini_dclick = self.old_on_mini_dclick
        del m.recursiveAddMenuNode
        del m.selectAliasName
        del m.on_node_menu_item
        del m.bind_100_ids
        del m.unbind_100_ids
        del m.menuid0





import types

import wx


#This is a bad import
from orpg.mapper.miniatures_handler import *

from orpg.pluginhandler import PluginHandler
from orpg.orpgCore import open_rpg, ParserContext
from orpg.mapper.base_handler import base_layer_handler
from orpg.chat.chatwnd import compiled_simple_arithmetic_regex
from orpg.chat.commands import command_args_parser
from orpg.gametree.nodehandlers.core import node_handler


class Bonus(object):
    def __init__(self, number, path, bonus_type):
        self.number = int(number) # amount added (can be negative)
        self.path = path # fullpath or namespace path (not leaf)
        self.type = bonus_type # arbitrary string or empty

class Effect(object):
    def __init__(self, name, desc, namespace_name, expire, bonus_list):
        self.name = name
        self.desc = desc
        self.namespace_name = namespace_name
        self.expire = expire
        self.bonus_list = bonus_list

# add these functions to the chat object
def add_effect(self, effect):
    self.effects.append(effect)
    for bonus in effect.bonus_list:
        self.add_bonus(effect.namespace_name, bonus)

def delete_effect(self, effect):
    for bonus in effect.bonus_list:
        self.delete_bonus(effect.namespace_name, bonus)
    self.effects.remove(effect)

def add_bonus(self, namespace_name, bonus):
    key = (namespace_name, bonus.path)
    if key in self.bonus_map:
        bonus_list = self.bonus_map[key]
        bonus_list.append(bonus)
    else:
        self.bonus_map[key]=[bonus]

def delete_bonus(self, namespace_name, bonus):
    key = (namespace_name, bonus.path)
    if key in self.bonus_map:
        bonus_list = self.bonus_map[key]
        bonus_list.remove(bonus)

# overrides the existing chat method
def my_handle_adding_bonuses(self, value, handler):
    if compiled_simple_arithmetic_regex.match(value):
        newint = eval(value)
        fullpath, v, namespace_name, namespace_path =\
            open_rpg.get_component('tree').get_path_data(handler.mytree_node)
        if namespace_name:
            key = (namespace_name, namespace_path)
        else:
            key = ("", fullpath)
        if key in self.bonus_map:
            typed_bonuses = {}
            for bonus in self.bonus_map[key]:
                if bonus.type:
                    if bonus.type in typed_bonuses:
                        typed_bonuses[bonus.type] =\
                                max(bonus.number, typed_bonuses[bonus.type])
                    else:
                        typed_bonuses[bonus.type] = bonus.number
                else:
                    newint += bonus.number
            for number in typed_bonuses.values():
                newint += number
            value = str(newint)
    return value


class Plugin(PluginHandler):
    def __init__(self, plugindb, parent):
        PluginHandler.__init__(self, plugindb, parent)
        self.name = 'Bonuses and Effects'
        self.author = 'David'
        self.help = """Allows on-the-fly temporary modification of character sheet values.
Requires OpenRPG 1.8.0+ development
EXPERIMENTAL! In the process of being updated."""

    def plugin_enabled(self):
        c = open_rpg.get_component("chat")
        c.bonus_map = {}
        c.effects = []
        self.old_handle_adding_bonuses = c.handle_adding_bonuses
        c.handle_adding_bonuses = types.MethodType(my_handle_adding_bonuses, c,
                                                   c.__class__)
        c.add_bonus = types.MethodType(add_bonus, c, c.__class__)
        c.delete_bonus = types.MethodType(delete_bonus, c, c.__class__)
        c.add_effect = types.MethodType(add_effect, c, c.__class__)
        c.delete_effect = types.MethodType(delete_effect, c, c.__class__)
        self.plugin_addcommand('/bonus', self.on_bonus,
                               '/bonus number target [type]')
        self.plugin_addcommand('/effect', self.on_effect,
                               '/effect name alias= [type=] number= path= [number= path=]')
        # was /effect name alias= [desc=] [expire=] [type=] number= path= [number= path=]
        self.plugin_commandalias('/b', '/bonus')
        self.plugin_addcommand('/view_effects', self.on_view_effects,
                               'bring up the effects view (use this to '\
                               'delete effects)')
        self.plugin_commandalias('/ve', '/view_effects')
        # load effects from last session
        db_effects_list = self.plugindb.GetList("xxbonuses", "effect_list", [])
        for db_effect_data in db_effects_list:
            # if the ordering of these data is messed with you will
            #break old saved data
            name = db_effect_data[0]
            desc = db_effect_data[1]
            namespace_name = db_effect_data[2]
            expire = db_effect_data[3]
            db_bonus_list = db_effect_data[4]
            bonus_list = []
            for b in db_bonus_list:
                number = b[0] # int
                path = b[1]
                bonus_type = b[2]
                bonus_list.append(Bonus(number, path, bonus_type))
            open_rpg.get_component("chat").add_effect(Effect(name, desc,
                                                             namespace_name,
                                                             expire,
                                                             bonus_list))

    def plugin_disabled(self):
        c = open_rpg.get_component("chat")
        #save list of effects
        db_effects_list = []
        for effect in c.effects:
            db_bonus_list = []
            for bonus in effect.bonus_list:
                db_bonus_list.append([bonus.number, bonus.path, bonus.type])
            db_effect_data = [effect.name, effect.desc, effect.namespace_name,
                              effect.expire, db_bonus_list]
            db_effects_list.append(db_effect_data)
        self.plugindb.SetList("xxbonuses", "effect_list", db_effects_list)
        c.handle_adding_bonuses = self.old_handle_adding_bonuses
        del c.add_bonus
        del c.delete_bonus
        del c.add_effect
        del c.delete_effect
        del c.bonus_map
        del c.effects
        self.plugin_removecmd('/bonus')
        self.plugin_removecmd('/b')
        self.plugin_removecmd('/effect')
        self.plugin_removecmd('/view_effects')
        self.plugin_removecmd('/ve')

    def on_bonus(self, cmdargs):
        values, key_values = command_args_parser(cmdargs)
        if len(values) < 2:
            open_rpg.get_component("chat").InfoPost("/effect: Not enough "\
                                                    "params passed.")
            return
        try:
            number = int(values[0])
        except:
            open_rpg.get_component("chat").InfoPost("First parameter must "\
                                                    "be an integer.")
            return
        path = values[1]
        handler = open_rpg.get_component('tree').get_handler_by_path(path,
                            open_rpg.get_component("chat").chat_cmds.context)
        if handler is None:
            open_rpg.get_component("chat").InfoPost("Second parameter must "\
                                                    "reference a node.")
            return
##        value = handler.get_value()
##        try:
##            value = int(value)
##        except:
##            open_rpg.get_component("chat").InfoPost("Value of node must be an integer.")
##            return
        if 'type' in key_values:
            bonus_type = key_values['type']
        else:
            bonus_type = ''
        fullpath, v, namespace_name, namespace_path =\
            open_rpg.get_component('tree').get_path_data(handler.mytree_node)
        if namespace_name:
            effect = Effect("", "", namespace_name, "",
                            [Bonus(number, namespace_path, bonus_type)])
        else:
            effect = Effect("", "", "", "", [Bonus(number, fullpath,
                                                   bonus_type)])
        open_rpg.get_component("chat").add_effect(effect)

    def on_effect(self, cmdargs):
        chat = open_rpg.get_component("chat")
        values, key_values = command_args_parser(cmdargs)
        if len(values) < 1:
            chat.InfoPost("/effect: Not enough params passed.")
            return
        if 'number' not in key_values or 'path' not in key_values:
            chat.InfoPost("/effect: No number/path params passed.")
            return
        name = values[0]
        if 'desc' in key_values:
            desc = key_values['desc']
        else:
            desc = ''
        # determine namespace_name from one of various sources
        if 'alias' in key_values:
            namespace_name = key_values['alias'].lower().strip()
            if namespace_name != "" and\
               open_rpg.get_component('tree').\
               get_namespace_by_name(namespace_name) is None:
                chat.InfoPost("/effect: the alias passed ('"+namespace_name+"') was not a namespace.")
                return
        else:
            context = chat.chat_cmds.context
            if isinstance(context.namespace_hint, node_handler):
                namespace_name = open_rpg.get_component('tree').\
                        get_namespace_name(context.namespace_hint.mytree_node)
            elif isinstance(context.namespace_hint, (str, unicode)):
                namespace_name = context.namespace_hint.lower().strip()
            else:
                namespace_name = ''
        if 'expire' in key_values:
            expire = key_values['expire']
        else:
            expire = ''
        if 'type' in key_values:
            effect_type = key_values['type']
        else:
            effect_type = ''
        numbers = key_values['number']
        if not isinstance(numbers, list):
            numbers = [numbers]
        paths = key_values['path']
        if not isinstance(paths, list):
            paths = [paths]
        if len(numbers) != len(paths):
            chat.InfoPost("/effect: number and path params must be paired.")
            return
        bonus_list = []
        for i in range(len(numbers)):
            # check validity of path / replace with fullpath (not leaf)
            handler = open_rpg.get_component('tree').\
                get_handler_by_path(paths[i], ParserContext(namespace_name))
            if handler is None:
                chat.InfoPost("Path parameters must be indexed nodes.")
                return
##            value = handler.get_value()
##            try:
##                value = int(value)
##            except:
##                chat.InfoPost("Value of node to receive bonus must be an integer.")
##                return
            fullpath, v, n, namespace_path = open_rpg.get_component('tree').\
                    get_path_data(handler.mytree_node)
            if namespace_name:
                path = namespace_path
            else:
                path = fullpath
            bonus_list.append(Bonus(numbers[i], path, effect_type))
        effect = Effect(name, desc, namespace_name, expire, bonus_list)
        chat.add_effect(effect)

    def on_view_effects(self, cmdargs):
        frame = BonusFrame()
        frame.Show()
        #frame.Destroy()


LIST_CTRL = wx.NewId()
BUT_DEL = wx.NewId()
BUT_DEL_ALL = wx.NewId()

class BonusFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1)
        self.SetTitle('List of bonuses and effects')
        self.SetSize((600,400))
        self.listbox = wx.ListCtrl(self, LIST_CTRL, style=wx.LC_REPORT)
        self.listbox.InsertColumn(0, 'Name')
        #self.listbox.InsertColumn(1, 'Description')
        self.listbox.InsertColumn(2, 'Alias')
        #self.listbox.InsertColumn(3, 'Expiration')
        self.listbox.InsertColumn(4, 'Bonuses')
        self.effects = []
        for effect in open_rpg.get_component("chat").effects:
            bonus_description = ', '.join([str(bonus.number)+\
                                           " "+bonus.path.split('::')[-1] \
                                           for bonus in effect.bonus_list])
            self.listbox.Append((effect.name, #effect.desc,
                                 effect.namespace_name, #effect.expire,
                                 bonus_description))
            self.effects.append(effect)
            
        self.listbox.SetColumnWidth(0, 180)
        #self.listbox.SetColumnWidth(0, 80)
        self.listbox.SetColumnWidth(1, 90)
        #self.listbox.SetColumnWidth(0, 40)
        self.listbox.SetColumnWidth(2, 270)
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.listbox, 1, wx.EXPAND)
        sizer.Add(wx.Button(self, BUT_DEL, "Delete"), 0, wx.EXPAND)
        sizer.Add(wx.Button(self, BUT_DEL_ALL, "Delete All"), 0, wx.EXPAND)
        self.SetSizer(sizer)
        #self.SetAutoLayout(True)
        self.Bind(wx.EVT_BUTTON, self.on_delete, id=BUT_DEL)
        self.Bind(wx.EVT_BUTTON, self.on_delete_all, id=BUT_DEL_ALL)

    def on_delete(self, evt):
        index = self.listbox.GetFocusedItem()
        if index >= 0:
            effect = self.effects[index]
            open_rpg.get_component("chat").delete_effect(effect)
            self.effects[index:index+1] = []
            self.listbox.DeleteItem(index)

    def on_delete_all(self, evt):
        dlg = wx.MessageDialog(self, "Are you sure you want to delete all "\
                               "bonuses and effects?", "Delete All",
                               wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION)
        if dlg.ShowModal() != wx.ID_YES:
            return
        open_rpg.get_component("chat").effects = []
        open_rpg.get_component("chat").bonus_map = {}
        self.effects = []
        self.listbox.DeleteAllItems()

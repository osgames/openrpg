import wx

#this is a bad import fix it
from orpg.mapper.miniatures_handler import *

from orpg.orpgCore import open_rpg
from orpg.pluginhandler import PluginHandler
from orpg.mapper.base_handler import base_layer_handler

class Plugin(PluginHandler):
    def __init__(self, plugindb, parent):
        PluginHandler.__init__(self, plugindb, parent)
        self.name = 'Text Completion'
        self.author = 'David'
        self.help = """Text completion will be based on selecting a node name from the tree (not other player's names).
Requires OpenRPG 1.8.0+ development
EXPERIMENTAL! In the process of being updated."""

    def plugin_enabled(self):
        open_rpg.get_component('chat').get_completion_word_set = self.get_node_names_set

    def plugin_disabled(self):
        open_rpg.get_component('chat').get_completion_word_set = open_rpg.get_component('chat').get_player_set

    def get_node_names_set(self):
        # could take into account the selected alias
        # could calculate once instead of re-calculating every time
        names = set()
        open_rpg.get_component('tree').traverse(open_rpg.get_component('tree').root, self.add_name, names)
        return names

    def add_name(self, item, data):
        if open_rpg.get_component('tree').is_referenceable(item):
            data.add(open_rpg.get_component('tree').GetItemText(item))

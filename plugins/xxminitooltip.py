import types

import wx

#This is a bad import fix it
from orpg.mapper.miniatures_handler import *

from orpg.pluginhandler import PluginHandler
from orpg.orpgCore import open_rpg
from orpg.mapper.base_handler import base_layer_handler

def my_get_mini_tooltip(self, mini_list):
    return ", ".join([mini.label for mini in mini_list])


class Plugin(PluginHandler):
    def __init__(self, plugindb, parent):
        PluginHandler.__init__(self, plugindb, parent)
        self.name = 'Mini Tooltip'
        self.author = 'David'
        self.help = """Add a tooltip displaying the name of the mini.  For OpenRPG 1.8.0+"""

    def plugin_enabled(self):
        m = open_rpg.get_component("map").layer_handlers[2]
        self.old_get_mini_tooltip = m.get_mini_tooltip
        m.get_mini_tooltip = types.MethodType(my_get_mini_tooltip, m, m.__class__)

    def plugin_disabled(self):
        m = open_rpg.get_component("map").layer_handlers[2]
        m.get_mini_tooltip = self.old_get_mini_tooltip

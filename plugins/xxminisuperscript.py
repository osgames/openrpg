import os
import orpg.pluginhandler
from orpg.mapper.miniatures import register_mini_draw_callback_function, mini_additional_attribute_list
from orpg.mapper.miniatures_handler import *
from orpg.orpgCore import *
import wx


class Plugin(orpg.pluginhandler.PluginHandler):
    def __init__(self, plugindb, parent):
        orpg.pluginhandler.PluginHandler.__init__(self, plugindb, parent)
        self.name = 'Mini Superscript'
        self.author = 'David'
        self.help = """Adds a temporary custom superscript to miniatures on the map.
Requires OpenRPG 1.8.0+ development
EXPERIMENTAL! In the process of being updated."""

    def plugin_enabled(self):
        m = open_rpg.get_component("map").layer_handlers[2]
        m.set_mini_rclick_menu_item("Superscript", self.on_superscript)
        register_mini_draw_callback_function(self.on_draw)
        mini_additional_attribute_list.append('superscript')

    def plugin_disabled(self):
        register_mini_draw_callback_function(self.on_draw)
        m = open_rpg.get_component("map").layer_handlers[2]
        m.set_mini_rclick_menu_item("Superscript", None)
        #mini_additional_attribute_list.remove('superscript')

    def on_draw(self, mini, dc):
        if 'superscript' in mini.__dict__:
            width, height = dc.GetTextExtent(mini.superscript)
            dc.SetPen(wx.Pen('#FFFF00'))#yellow
            dc.SetBrush(wx.Brush('#FFFF00'))#yellow
            dc.DrawRectangle(mini.pos.x-1, mini.pos.y, width+2, height) 
            dc.SetTextForeground(wx.BLACK)
            dc.DrawText(mini.superscript, mini.pos.x, mini.pos.y)

    def on_superscript(self, event):
        m = open_rpg.get_component("map").layer_handlers[2]
        # m.sel_rmin is the mini that was just right-clicked
        if 'superscript' not in m.sel_rmin.__dict__:
            m.sel_rmin.superscript = ''
        dlg = wx.TextEntryDialog(None, "", "Superscript")
        dlg.SetValue(m.sel_rmin.superscript)
        if dlg.ShowModal() == wx.ID_OK:
            m.sel_rmin.superscript = dlg.GetValue()
            m.sel_rmin.isUpdated = True
            open_rpg.get_component("map").canvas.Refresh(False)
            open_rpg.get_component("map").canvas.send_map_data()
        dlg.Destroy()


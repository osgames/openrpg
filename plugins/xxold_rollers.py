import os
import orpg.pluginhandler

class Plugin(orpg.pluginhandler.PluginHandler):
    def __init__(self, plugindb, parent):
        orpg.pluginhandler.PluginHandler.__init__(self, plugindb, parent)

        # The Following code should be edited to contain the proper information
        self.name = 'Old Rollers'
        self.author = 'Dj Gilcrease'
        self.help = 'A group of old style rollers that have or will not be converted to the new system'

    def plugin_enabled(self):
        import plugins.old_rollers

    def plugin_disabled(self): pass

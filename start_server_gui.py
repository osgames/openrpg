#!/usr/bin/env python

import os, os.path
import sys

import pyver
pyver.checkPyVersion()

try:
    import wx
    WXLOADED = True
except ImportError:
    WXLOADED = False

if __name__ == "__main__":
    os.chdir(os.path.abspath(os.path.dirname(sys.argv[0])))
    if WXLOADED:
        import orpg.networking.mplay_server_gui
        app = orpg.networking.mplay_server_gui.ServerGUIApp(0)
        app.MainLoop()

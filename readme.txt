How to use OpenRPG version 1.7.x
Make sure you have installed python 2.5+ and wxPython 2.8+!

Launching OpenRPG:
OpenRPG can be launch by executing the OpenRPG.pyw script
located in the openrpg folder.  On windows, Macs, and
Unix with a GUI, this can be accomplished by double
clicking OpenRPG.pyw. From a shell, type: python OpenRPG.pyw

Launching a OpenRPG game server:
You want to launch your own server execute the
Server.py.

For more info on how to use OpenRPG,
visit http://www.openrpg.com


-OpenRPG Team

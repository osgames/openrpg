# Copyright (C) 2000-2001 The OpenRPG Project
#
#    openrpg-dev@lists.sourceforge.net
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# --
#
# File: chat_msg.py
# Author: Ted Berg
# Maintainer:
# Version:
#   $Id: chat_msg.py,v 1.15 2006/11/04 21:24:19 digitalxero Exp $
#
# Description: Contains class definitions for manipulating <chat/> messages
#
#

__version__ = "$Id: chat_msg.py,v 1.15 2006/11/04 21:24:19 digitalxero Exp $"

from orpg.tools.orpg_log import logger

from orpg.external.etree.ElementTree import ElementTree, Element
from orpg.external.etree.ElementTree import fromstring, tostring

"""
from encodings.punycode import punycode_encode, punycode_decode
I would like to use this to allow unicode characters, but if it was not
encoded with it the decode returns giberish on any hyphenated word
"""

from chat_version import CHAT_VERSION

CHAT_MESSAGE = 1
WHISPER_MESSAGE = 2
EMOTE_MESSAGE = 3
INFO_MESSAGE = 4
SYSTEM_MESSAGE = 5
WHISPER_EMOTE_MESSAGE = 6

class chat_msg:
    def __init__(self):
        self.chat_dom = Element('chat')
        self.chat_dom.set('type', '1')
        self.chat_dom.set('version', CHAT_VERSION)
        self.chat_dom.set('alias', '')

    def toxml(self):
        return tostring(self.chat_dom)

    def takexml(self, xml_text):
        xml_dom = fromstring(xml_text)

        if xml_dom.iter_find('chat') is not None:
            chat_node = xml_dom.iter_find('chat')
        else:
            chat_node = xml_dom

        if chat_node.tag != 'chat':
            logger.general("Warning: no <chat/> elements found in DOM.")
        else:
            self.takedom(chat_node)

    def takedom(self, xml_dom):
        self.chat_dom = xml_dom

    def set_text(self, text):
        #self.chat_dom.text = punycode_encode(text)
        self.chat_dom.text = text

    def set_type(self, chat_type):
        self.chat_dom.set('type', str(chat_type))

    def get_type(self):
        return int(self.chat_dom.get('type'))

    def set_alias(self, alias):
        self.chat_dom.set('alias', alias)

    def get_alias(self):
        return self.chat_dom.get('alias')

    def get_text(self):
        #return punycode_decode(self.chat_dom.text)
        return self.chat_dom.text

    def get_version(self):
        return self.chat_dom.get('version')
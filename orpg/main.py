#!/usr/bin/env python
# Copyright (C) 2000-2001 The OpenRPG Project
#
#        openrpg-dev@lists.sourceforge.net
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# --
#
# File: main.py
# Author: Chris Davis
# Maintainer:
# Version:
#   $Id: main.py,v 1.153 2008/01/24 03:52:03 digitalxero Exp $
#
# Description: This is the main entry point of the oprg application
#
from __future__ import with_statement

import traceback
from xml.parsers.expat import ExpatError
import time
import wx
import os
import sys
import wx.py
import webbrowser
try:
    import wx.lib.agw.aui as AUI
except ImportError:
    import wx.aui as AUI

from orpg_version import *

#These should be fixed to from bla import stuff
import orpg.player_list
import orpg.tools.passtool
import orpg.tools.orpg_sound
import orpg.tools.rgbhex
import orpg.gametree.gametree
import orpg.chat.chatwnd
import orpg.networking.mplay_client
import orpg.mapper.map
import orpg.mapper.images

from orpg.orpgCore import open_rpg
from orpg.ui import AliasLib, SettingsWindow, BrowseServerWindow, PluginFrame
from orpg.ui.toolbars import StatusBar, DiceToolBar
from orpg.dieroller import roller_manager
from orpg.dirpath import dir_struct
from orpg.tools.settings import settings
from orpg.tools.orpg_log import logger
from orpg.tools.validate import validate
from orpg.tools.decorators import debugging, pending_deprecation
from orpg.external.metamenus import MenuBarEx
from orpg.external.etree.ElementTree import ElementTree, Element
from orpg.external.etree.ElementTree import fromstring, tostring

####################################
## Main Frame
####################################


class orpgFrame(wx.Frame):
    @debugging
    def __init__(self, parent, id, title):
        wx.Frame.__init__(self, parent, id, title, wx.Point(100, 100),
                          wx.Size(600,420), style=wx.DEFAULT_FRAME_STYLE)

        self.rgbcovert = orpg.tools.rgbhex.RGBHex()
        self._mgr = AUI.AuiManager(self)

        # Determine which icon format to use
        icon = None
        if wx.Platform == '__WXMSW__':
            icon = wx.Icon(dir_struct['icon'] + 'd20.ico', wx.BITMAP_TYPE_ICO)
        else:
            icon = wx.Icon(dir_struct['icon'] + 'd20.xpm', wx.BITMAP_TYPE_XPM)

        # Set it if we have it
        if icon != None:
            self.SetIcon( icon )

        # create session
        call_backs = {'on_receive': self.on_receive,
                      'on_mplay_event': self.on_mplay_event,
                      'on_group_event': self.on_group_event,
                      'on_player_event': self.on_player_event,
                      'on_status_event': self.on_status_event,
                      'on_password_signal': self.on_password_signal,
                      'orpgFrame': self}
        self.session = orpg.networking.mplay_client.mplay_client(
            settings.get('player'), call_backs)

        self.poll_timer = wx.Timer(self, wx.NewId())
        self.Bind(wx.EVT_TIMER, self.session.poll, self.poll_timer)
        self.poll_timer.Start(100)
        self.ping_timer = wx.Timer(self, wx.NewId())
        self.Bind(wx.EVT_TIMER, self.session.update, self.ping_timer)

        # create roller manager
        roller_manager.roller = settings.get('dieroller')

        #create password manager --SD 8/03
        self.password_manager = orpg.tools.passtool.PassTool()
        open_rpg.add_component('session', self.session)
        open_rpg.add_component('frame', self)
        open_rpg.add_component('DiceManager', roller_manager)
        open_rpg.add_component('password_manager', self.password_manager)

        # build frame windows
        self.build_menu()
        self.build_gui()
        self.build_hotkeys()
        logger.debug("GUI Built")
        open_rpg.add_component('chat', self.chat)
        open_rpg.add_component('map', self.map)
        open_rpg.add_component('alias', self.aliaslib)
        logger.debug("openrpg components all added")
        self.tree.load_tree(settings.get('gametree'))
        logger.debug("Tree Loaded")
        self.players.size_cols()
        logger.debug("player window cols sized")

        #Load the Plugins This has to be after the chat component has been added
        open_rpg.add_component('pluginmenu', self.pluginMenu)
        self.pluginsFrame.Start()
        logger.debug("plugins reloaded and startup plugins launched")
        self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)

        tipotday_start = settings.get('tipotday_start', 0)
        try:
            tipotday_start = int(tipotday_start)
        except TypeError:
            tipotday_start = 0

        self.TipOfTheDay = wx.CreateFileTipProvider(
            dir_struct['data']+'tips.txt', tipotday_start)

    @debugging
    def ShowTipOfTheDay(self):
        if wx.ShowTip(self, self.TipOfTheDay,
                      settings.get('tipotday_enabled') != '0'):
            settings.set('tipotday_enabled', '1')
        else:
            settings.set('tipotday_enabled', '0')

        settings.set('tipotday_start', str(self.TipOfTheDay.CurrentTip))

    @debugging
    def post_show_init(self):
        """Some Actions need to be done after the main fram is drawn"""
        self.players.size_cols()

        if settings.get('tipotday_enabled', '1').lower() != '0':
            self.ShowTipOfTheDay()

    @debugging
    def get_activeplugins(self):
        try:
            tmp = self.pluginsFrame.get_activeplugins()
        except:
            tmp = {}

        return tmp

    @debugging
    def get_startplugins(self):
        try:
            tmp = self.pluginsFrame.get_startplugins()
        except:
            tmp = {}

        return tmp

    @debugging
    def on_password_signal(self,signal,type,id,data):
        try:
            msg = ["DEBUG: password response= ",
                   str(signal),
                   " (T:",
                   str(type),
                   "  #",
                   str(id),
                   ")"]
            logger.debug("".join(msg))
            id = int(id)
            type = str(type)
            data = str(data)
            signal = str(signal)
            if signal == 'fail':
                if type == 'server':
                    self.password_manager.ClearPassword('server', 0)
                elif type == 'admin':
                    self.password_manager.ClearPassword('admin', int(id))
                elif type == 'room':
                    self.password_manager.ClearPassword('room', int(id))
                else:
                    pass
        except:
            traceback.print_exc()

    @debugging
    def build_menu(self):
        menu = \
             [[
                 ['&OpenRPG'],
                 ['  &Settings\tCtrl-S'],
                 ['  -'],
                 ['  Tab Styles'],
                 ['    Slanted'],
                 ['      Colorful', 'check'],
                 ['      Black and White', 'check'],
                 ['      Aqua', 'check'],
                 ['      Custom', 'check'],
                 ['    Flat'],
                 ['      Black and White', 'check'],
                 ['      Aqua', 'check'],
                 ['      Custom', 'check'],
                 ['  -'],
                 ['  &Exit']
                 ],
              [
                  ['&Game Server'],
                  ['  &Browse Servers\tCtrl-B'],
                  ['  -'],
                  ['  Server Heartbeat', 'check'],
                  ['  -'],
                  ['  &Start Server']
                  ],
              [
                  ['&Tools'],
                  ['  Logging Level'],
                  ['    Debug', 'check'],
                  ['    Note', 'check'],
                  ['    Info', 'check'],
                  ['    General', 'check'],
                  ['  -'],
                  ['  Password Manager', 'check'],
                  ['  -'],
                  ['  Sound Toolbar', 'check'],
                  ['  Dice Bar\tCtrl-D', 'check'],
                  ['  Map Bar', 'check'],
                  ['  Status Bar\tCtrl-T', 'check'],
                  ],
              [
                  ['&Help'],
                  ['  &About'],
                  ['  Online User Guide'],
                  ['  Change Log'],
                  ['  Report a Bug'],
                  ['  Tip of the Day']
              ]]

        self.mainmenu = MenuBarEx(self, menu)
        if settings.get('Heartbeat') == '1':
            self.mainmenu.SetMenuState('GameServerServerHeartbeat', True)
        tabtheme = settings.get('TabTheme')

        #This change is stable. TaS.
        self.mainmenu.SetMenuState('OpenRPGTabStylesSlantedColorful',
                                   tabtheme == 'slanted&colorful')
        self.mainmenu.SetMenuState('OpenRPGTabStylesSlantedBlackandWhite',
                                   tabtheme == 'slanted&bw')
        self.mainmenu.SetMenuState('OpenRPGTabStylesSlantedAqua',
                                   tabtheme == 'slanted&aqua')
        self.mainmenu.SetMenuState('OpenRPGTabStylesFlatBlackandWhite',
                                   tabtheme == 'flat&bw')
        self.mainmenu.SetMenuState('OpenRPGTabStylesFlatAqua',
                                   tabtheme == 'flat&aqua')
        self.mainmenu.SetMenuState('OpenRPGTabStylesSlantedCustom',
                                   tabtheme == 'customslant')
        self.mainmenu.SetMenuState('OpenRPGTabStylesFlatCustom',
                                   tabtheme == 'customflat')

        lvl = int(settings.get('LoggingLevel'))
        if lvl & logger.DEBUG:
            self.mainmenu.SetMenuState('ToolsLoggingLevelDebug', True)
        if lvl & logger.DEBUG:
            self.mainmenu.SetMenuState('ToolsLoggingLevelNote', True)
        if lvl & logger.INFO:
            self.mainmenu.SetMenuState('ToolsLoggingLevelInfo', True)
        if lvl & logger.GENERAL:
            self.mainmenu.SetMenuState('ToolsLoggingLevelGeneral', True)

        self.pluginMenu = wx.Menu()
        item = wx.MenuItem(self.pluginMenu, wx.ID_ANY, 'Control Panel',
                           'Control Panel')
        self.Bind(wx.EVT_MENU, self.OnMB_PluginControlPanel, item)
        self.pluginMenu.AppendItem(item)
        self.pluginMenu.AppendSeparator()
        self.mainmenu.Insert(2, self.pluginMenu, '&Plugins')

    #################################
    ## All Menu Events
    #################################
    #Tab Styles Menus
    @debugging
    def SetTabStyles(self, *args, **kwargs):
        if kwargs.has_key('style'):
            newstyle = kwargs['style']
        else:
            try:
                newstyle = args[1]
            except:
                logger.general('Invalid Syntax for orpgFrame->SetTabStyles'
                               '(self, *args, **kwargs)')
                return

        if kwargs.has_key('menu'):
            menu = kwargs['menu']
        else:
            try:
                menu = args[0]
            except:
                logger.general('Invalid Syntax for orpgFrame->SetTabStyles'
                               '(self, *args, **kwargs)')
                return

        if kwargs.has_key('graidentTo'):
            graidentTo = kwargs['graidentTo']
        else:
            graidentTo = None

        if kwargs.has_key('graidentFrom'):
            graidentFrom = kwargs['graidentFrom']
        else:
            graidentFrom = None

        if kwargs.has_key('textColor'):
            textColor = kwargs['textColor']
        else:
            textColor = None

        #Set all menu's to unchecked
        self.mainmenu.SetMenuState('OpenRPGTabStylesSlantedColorful', False)
        self.mainmenu.SetMenuState('OpenRPGTabStylesSlantedBlackandWhite',
                                   False)
        self.mainmenu.SetMenuState('OpenRPGTabStylesSlantedAqua', False)
        self.mainmenu.SetMenuState('OpenRPGTabStylesSlantedCustom', False)
        self.mainmenu.SetMenuState('OpenRPGTabStylesFlatBlackandWhite', False)
        self.mainmenu.SetMenuState('OpenRPGTabStylesFlatAqua', False)
        self.mainmenu.SetMenuState('OpenRPGTabStylesFlatCustom', False)

        #check the proper menu
        self.mainmenu.SetMenuState(menu, True)

        #Run though the current tabbed window list and remove
        #those that have been closed
        tabbedwindows = open_rpg.get_component('tabbedWindows')
        rgbc = orpg.tools.rgbhex.RGBHex()
        new = []
        for wnd in tabbedwindows:
            try:
                style = wnd.GetWindowStyleFlag()
                new.append(wnd)
            except:
                pass
        tabbedwindows = new
        open_rpg.add_component('tabbedWindows', tabbedwindows)

        #Run though the new list and set the proper styles
        tabbg = settings.get('TabBackgroundGradient')
        rgbc = orpg.tools.rgbhex.RGBHex()
        (red, green, blue) = rgbc.rgb_tuple(tabbg)

        for wnd in tabbedwindows:
            style = wnd.GetWindowStyleFlag()
            # remove old tabs style
            mirror = ~(FNB.FNB_VC71|FNB.FNB_VC8|FNB.FNB_FANCY_TABS|
                       FNB.FNB_COLORFUL_TABS)
            style &= mirror
            style |= newstyle
            wnd.SetWindowStyleFlag(style)
            wnd.SetTabAreaColour(wx.Color(red, green, blue))
            if graidentTo != None:
                wnd.SetGradientColourTo(graidentTo)
            if graidentFrom != None:
                wnd.SetGradientColourFrom(graidentFrom)
            if textColor != None:
                wnd.SetNonActiveTabTextColour(textColor)
            wnd.Refresh()

    @debugging
    def OnMB_OpenRPGTabStylesSlantedColorful(self):
        if self.mainmenu.GetMenuState('OpenRPGTabStylesSlantedColorful'):
            settings.set('TabTheme', 'slanted&colorful')
            self.SetTabStyles('OpenRPGTabStylesSlantedColorful',
                              FNB.FNB_VC8|FNB.FNB_COLORFUL_TABS)
        else:
            self.mainmenu.SetMenuState('OpenRPGTabStylesSlantedColorful', True)

    @debugging
    def OnMB_OpenRPGTabStylesSlantedBlackandWhite(self):
        if self.mainmenu.GetMenuState('OpenRPGTabStylesSlantedBlackandWhite'):
            settings.set('TabTheme', 'slanted&bw')
            self.SetTabStyles('OpenRPGTabStylesSlantedBlackandWhite',
                              FNB.FNB_VC8, graidentTo=wx.WHITE,
                              graidentFrom=wx.WHITE, textColor=wx.BLACK)
        else:
            self.mainmenu.SetMenuState('OpenRPGTabStylesSlantedBlackandWhite',
                                       True)

    @debugging
    def OnMB_OpenRPGTabStylesSlantedAqua(self):
        if self.mainmenu.GetMenuState('OpenRPGTabStylesSlantedAqua'):
            settings.set('TabTheme', 'slanted&aqua')
            self.SetTabStyles('OpenRPGTabStylesSlantedAqua', FNB.FNB_VC8,
                              graidentTo=wx.Color(0, 128, 255),
                              graidentFrom=wx.WHITE, textColor=wx.BLACK)
        else:
            self.mainmenu.SetMenuState('OpenRPGTabStylesSlantedAqua', True)

    @debugging
    def OnMB_OpenRPGTabStylesSlantedCustom(self):
        if self.mainmenu.GetMenuState('OpenRPGTabStylesSlantedCustom'):
            settings.set('TabTheme', 'customslant')
            rgbc = orpg.tools.rgbhex.RGBHex()
            gfrom = settings.get('TabGradientFrom')

            (fred, fgreen, fblue) = rgbc.rgb_tuple(gfrom)
            gto = settings.get('TabGradientTo')

            (tored, togreen, toblue) = rgbc.rgb_tuple(gto)
            tabtext = settings.get('TabTextColor')

            (tred, tgreen, tblue) = rgbc.rgb_tuple(tabtext)
            tabbg = settings.get('TabBackgroundGradient')

            (red, green, blue) = rgbc.rgb_tuple(tabbg)
            self.SetTabStyles('OpenRPGTabStylesSlantedCustom', FNB.FNB_VC8,
                              graidentTo=wx.Color(tored, togreen, toblue),
                              graidentFrom=wx.Color(fred, fgreen, fblue),
                              textColor=wx.Color(tred, tgreen, tblue))
        else:
            self.mainmenu.SetMenuState('OpenRPGTabStylesSlantedCustom', True)

    @debugging
    def OnMB_OpenRPGTabStylesFlatBlackandWhite(self):
        if self.mainmenu.GetMenuState('OpenRPGTabStylesFlatBlackandWhite'):
            settings.set('TabTheme', 'flat&bw')
            self.SetTabStyles('OpenRPGTabStylesFlatBlackandWhite',
                              FNB.FNB_FANCY_TABS, graidentTo=wx.WHITE,
                              graidentFrom=wx.WHITE, textColor=wx.BLACK)
        else:
            self.mainmenu.SetMenuState('OpenRPGTabStylesFlatBlackandWhite',
                                       True)

    @debugging
    def OnMB_OpenRPGTabStylesFlatAqua(self):
        if self.mainmenu.GetMenuState('OpenRPGTabStylesFlatAqua'):
            settings.set('TabTheme', 'flat&aqua')
            self.SetTabStyles('OpenRPGTabStylesFlatAqua', FNB.FNB_FANCY_TABS,
                              graidentTo=wx.Color(0, 128, 255),
                              graidentFrom=wx.WHITE, textColor=wx.BLACK)
        else:
            self.mainmenu.SetMenuState('OpenRPGTabStylesFlatAqua', True)

    @debugging
    def OnMB_OpenRPGTabStylesFlatCustom(self):
        if self.mainmenu.GetMenuState('OpenRPGTabStylesFlatCustom'):
            settings.set('TabTheme', 'customflat')

            rgbc = orpg.tools.rgbhex.RGBHex()
            gfrom = settings.get('TabGradientFrom')

            (fred, fgreen, fblue) = rgbc.rgb_tuple(gfrom)
            gto = settings.get('TabGradientTo')

            (tored, togreen, toblue) = rgbc.rgb_tuple(gto)
            tabtext = settings.get('TabTextColor')

            (tred, tgreen, tblue) = rgbc.rgb_tuple(tabtext)
            tabbg = settings.get('TabBackgroundGradient')

            (red, green, blue) = rgbc.rgb_tuple(tabbg)
            self.SetTabStyles('OpenRPGTabStylesFlatCustom', FNB.FNB_FANCY_TABS,
                              graidentTo=wx.Color(tored, togreen, toblue),
                              graidentFrom=wx.Color(fred, fgreen, fblue),
                              textColor=wx.Color(tred, tgreen, tblue))
        else:
            self.mainmenu.SetMenuState('OpenRPGTabStylesFlatCustom', True)

    #Window Menu
    @debugging
    def OnMB_WindowsMenu(self, event):
        menuid = event.GetId()
        name = self.mainwindows[menuid]
        if self._mgr.GetPane(name).IsShown() == True:
            self._mgr.GetPane(name).Hide()
        else:
            self._mgr.GetPane(name).Show()
        self._mgr.Update()

    #OpenRPG Menu
    @debugging
    def OnMB_OpenRPGSettings(self):
        dlg = SettingsWindow(self)
        dlg.Centre()
        dlg.ShowModal()

    @debugging
    def OnMB_OpenRPGExit(self):
        self.OnCloseWindow(0)

    #Game Server Menu
    @debugging
    def OnMB_GameServerBrowseServers(self):
        if self._mgr.GetPane('Browse Server Window').IsShown() == True:
            self._mgr.GetPane('Browse Server Window').Hide()
        else:
            self._mgr.GetPane('Browse Server Window').Show()
        self._mgr.Update()

    @debugging
    def OnMB_GameServerServerHeartbeat(self):
        if self.mainmenu.GetMenuState('GameServerServerHeartbeat'):
            settings.set('Heartbeat', '1')
        else:
            settings.set('Heartbeat', '0')

    @debugging
    def OnMB_GameServerStartServer(self):
        start_dialog = wx.ProgressDialog('Server Loading',
                                          "Server Loading, Please Wait...",
                                          1, self)
        # Spawn the new process and close the stdout handle from it
        start_dialog.Update( 0 )
        # Adjusted following code to work with win32, can't test for Unix
        # as per reported bug 586227
        if wx.Platform == '__WXMSW__':
            arg = ['\"',
                   os.path.abspath(dir_struct['home'] + 'start_server_gui.py'),
                   '\"']
            args = (sys.executable, "".join(arg))
        else:
            arg = dir_struct['home'] + 'start_server_gui.py'
            args = (arg,arg)
        os.spawnv(os.P_NOWAIT, sys.executable, args)
        start_dialog.Update(1)
        start_dialog.Show(False)
        start_dialog.Destroy()

    # Tools Menu
    @debugging
    def OnMB_PluginControlPanel(self, evt):
        if self.pluginsFrame.IsShown() == True:
            self.pluginsFrame.Hide()
        else:
            self.pluginsFrame.Show()

    @debugging
    def OnMB_ToolsLoggingLevelDebug(self):
        lvl = logger.log_level
        if self.mainmenu.GetMenuState('ToolsLoggingLevelDebug'):
            lvl |= logger.DEBUG
        else:
            lvl &= ~logger.DEBUG
        logger.log_level = lvl
        settings.set('LoggingLevel', lvl)

    @debugging
    def OnMB_ToolsLoggingLevelNote(self):
        lvl = logger.log_level
        if self.mainmenu.GetMenuState('ToolsLoggingLevelNote'):
            lvl |= logger.NOTE
        else:
            lvl &= ~logger.NOTE
        logger.log_level = lvl
        settings.set('LoggingLevel', lvl)

    @debugging
    def OnMB_ToolsLoggingLevelInfo(self):
        lvl = logger.log_level
        if self.mainmenu.GetMenuState('ToolsLoggingLevelInfo'):
            lvl |= logger.INFO
        else:
            lvl &= ~logger.INFO
        logger.log_level = lvl
        settings.set('LoggingLevel', lvl)

    @debugging
    def OnMB_ToolsLoggingLevelGeneral(self):
        lvl = logger.log_level
        if self.mainmenu.GetMenuState('ToolsLoggingLevelGeneral'):
            lvl |= logger.GENERAL
        else:
            lvl &= ~logger.GENERAL
        logger.log_level = lvl
        settings.set('LoggingLevel', lvl)

    @debugging
    def OnMB_ToolsPasswordManager(self):
        if self.mainmenu.GetMenuState('ToolsPasswordManager'):
            self.password_manager.Enable()
        else:
            self.password_manager.Disable()

    @debugging
    def OnMB_ToolsStatusBar(self):
        if self._mgr.GetPane('Status Window').IsShown() == True:
            self.mainmenu.SetMenuState('ToolsStatusBar', False)
            self._mgr.GetPane('Status Window').Hide()
        else:
            self.mainmenu.SetMenuState('ToolsStatusBar', True)
            self._mgr.GetPane('Status Window').Show()
        self._mgr.Update()

    @debugging
    def OnMB_ToolsSoundToolbar(self):
        if self._mgr.GetPane('Sound Control Toolbar').IsShown():
            self.mainmenu.SetMenuState('ToolsSoundToolbar', False)
            self._mgr.GetPane('Sound Control Toolbar').Hide()
        else:
            self.mainmenu.SetMenuState('ToolsSoundToolbar', True)
            self._mgr.GetPane('Sound Control Toolbar').Show()
        self._mgr.Update()

    @debugging
    def OnMB_ToolsDiceBar(self):
        if self._mgr.GetPane('Dice Tool Bar').IsShown() == True:
            self.mainmenu.SetMenuState('ToolsDiceBar', False)
            self._mgr.GetPane('Dice Tool Bar').Hide()
        else:
            self.mainmenu.SetMenuState('ToolsDiceBar', True)
            self._mgr.GetPane('Dice Tool Bar').Show()
        self._mgr.Update()

    @debugging
    def OnMB_ToolsMapBar(self):
        if self._mgr.GetPane('Map Tool Bar').IsShown() == True:
            self.mainmenu.SetMenuState('ToolsMapBar', False)
            self._mgr.GetPane('Map Tool Bar').Hide()
        else:
            self.mainmenu.SetMenuState('ToolsMapBar', True)
            self._mgr.GetPane('Map Tool Bar').Show()
        self._mgr.Update()

    #Help Menu
    @debugging
    def OnMB_HelpAbout(self):
        description = "OpenRPG is a Virtual Game Table that allows users to"
        description += " connect via a network and play table top games with"
        description += " friends. OpenRPG is originally designed by Chris Davis."

        license = "OpenRPG is free software; you can redistribute it and/or "
        license += "modify it under the terms of the GNU General Public License"
        license += "as published by the Free Software Foundation; either "
        license += "version 2 of the License, or (at your option) any later "
        license += "version."

        info = wx.AboutDialogInfo()
        info.SetIcon(wx.Icon(dir_struct['icon'] + 'splash.gif',
                             wx.BITMAP_TYPE_GIF))
        info.SetName('OpenRPG')
        info.SetVersion('OpenRPG ' + VERSION)
        info.SetDescription(description)

        info.SetWebSite('http://www.openrpg.com')
        info.SetLicence(license)
        orpg_devs = ['Thomas Baleno', 'Andrew Bennett', 'Lex Berezhny',
                     'Ted Berg', 'Bernhard Bergbauer', 'Chris Blocher',
                     'David Byron', 'Ben Collins-Sussman', 'Robin Cook',
                     'Greg Copeland', 'Chris Davis', 'Michael Edwards',
                     'Andrew Ettinger', 'Todd Faris', 'Dj Gilcrease',
                     'Christopher Hickman', 'Paul Hosking', 'Brian Manning',
                     'Scott Mackay', 'Jesse McConnell', 'Brian Osman',
                     'Rome Reginelli', 'Christopher Rouse', 'Dave Sanders',
                     'Tyler Starke', 'Mark Tarrabain']
        for dev in orpg_devs:
            info.AddDeveloper(dev)
        wx.AboutBox(info)

    @debugging
    def OnMB_HelpOnlineUserGuide(self):
        wb = webbrowser.get()
        wb.open("https://www.assembla.com/wiki/show/openrpg/User_Manual")

    @debugging
    def OnMB_HelpChangeLog(self):
        wb = webbrowser.get()
        wb.open("http://orpgmeta.appsspot.com/releasenotes?%s" % (VERSION))

    @debugging
    def OnMB_HelpReportaBug(self):
        wb = webbrowser.get()
        wb.open("http://www.assembla.com/spaces/openrpg/support/tickets")

    @debugging
    def OnMB_HelpTipoftheDay(self):
        self.ShowTipOfTheDay()


    #################################
    ##    Build the GUI
    #################################
    @debugging
    def build_gui(self):
        self.Freeze()
        validate.config_file('layout.xml', 'default_layout.xml')

        etree = ElementTree()
        with open(dir_struct['user'] + 'layout.xml') as f:
            etree.parse(f)

        base = etree.getroot()

        #Plugins Window
        self.pluginsFrame = PluginFrame(self)
        open_rpg.add_component('plugins', self.get_activeplugins())
        open_rpg.add_component('startplugs', self.get_startplugins())

        #Build Main Window
        self.windowsmenu = wx.Menu()
        self.mainwindows = {}
        logger.debug("Menu Created")
        h = int(base.get('height'))
        w = int(base.get('width'))
        posx = int(base.get('posx'))
        posy = int(base.get('posy'))
        maximized = int(base.get('maximized'))
        self.SetDimensions(posx, posy, w, h)
        logger.debug("Dimensions Set")

        # Sound Manager
        self.sound_player = orpg.tools.orpg_sound.orpgSound(self)
        open_rpg.add_component('sound', self.sound_player)
        wndinfo = AUI.AuiPaneInfo()
        wndinfo.DestroyOnClose(False)
        wndinfo.Name('Sound Control Toolbar')
        wndinfo.Caption('Sound Control Toolbar')
        wndinfo.Float()
        wndinfo.FloatingPosition((50,50))
        wndinfo.ToolbarPane()
        wndinfo.Hide()
        self._mgr.AddPane(self.sound_player, wndinfo)

        for c in base.getchildren():
            self.build_window(c, self)

        # status window
        self.status = StatusBar(self)
        wndinfo = AUI.AuiPaneInfo()
        wndinfo.DestroyOnClose(False)
        wndinfo.Name('Status Window')
        wndinfo.Caption('Status Window')
        wndinfo.Float()
        wndinfo.FloatingPosition((50,50))
        wndinfo.ToolbarPane()
        wndinfo.Hide()
        self._mgr.AddPane(self.status, wndinfo)
        logger.debug("Status Window Created")

        # Create and show the floating dice toolbar
        self.dieToolBar = DiceToolBar(self, callback=self.chat.ParsePost)
        wndinfo = AUI.AuiPaneInfo()
        wndinfo.DestroyOnClose(False)
        wndinfo.Name('Dice Tool Bar')
        wndinfo.Caption('Dice Tool Bar')
        wndinfo.Float()
        wndinfo.FloatingPosition((50,50))
        wndinfo.ToolbarPane()
        wndinfo.Hide()
        self._mgr.AddPane(self.dieToolBar, wndinfo)
        logger.debug("Dice Tool Bar Created")

        #Create the Browse Server Window
        self.gs = BrowseServerWindow(self)
        wndinfo = AUI.AuiPaneInfo()
        wndinfo.DestroyOnClose(False)
        wndinfo.Name('Browse Server Window')
        wndinfo.Caption('Game Server')
        wndinfo.Float()
        wndinfo.FloatingPosition((50,50))
        wndinfo.Dockable(False)
        wndinfo.MinSize(wx.Size(640,480))
        wndinfo.Hide()
        self._mgr.AddPane(self.gs, wndinfo)
        logger.debug("Game Server Window Created")

        #Create the Alias Lib Window
        self.aliaslib = AliasLib(self)
        wndinfo = AUI.AuiPaneInfo()
        wndinfo.DestroyOnClose(False)
        wndinfo.Name('Alias Lib')
        wndinfo.Caption('Alias Lib')
        wndinfo.Float()
        wndinfo.FloatingPosition((50,50))
        wndinfo.Dockable(True)
        wndinfo.MinSize(wx.Size(250,100))
        wndinfo.Hide()
        self._mgr.AddPane(self.aliaslib, wndinfo)
        logger.debug("Alias Window Created")
        menuid = wx.NewId()
        self.windowsmenu.Append(menuid, 'Alias Lib', kind=wx.ITEM_CHECK)
        self.windowsmenu.Check(menuid, False)
        self.Bind(wx.EVT_MENU, self.OnMB_WindowsMenu, id=menuid)
        self.mainwindows[menuid] = 'Alias Lib'
        self.mainmenu.Insert(3, self.windowsmenu, 'Windows')
        logger.debug("Windows Menu Done")
        self._mgr.Update()

        self.Bind(AUI.EVT_AUI_PANE_CLOSE, self.onPaneClose)

        logger.debug("AUI Bindings Done")

        #Load the layout if one exists
        layout = base.find('DockLayout')
        if layout is not None:
            self._mgr.LoadPerspective(layout.text)

        logger.debug("Perspective Loaded")
        self._mgr.GetPane('Browse Server Window').Hide()
        self._mgr.Update()
        self.Maximize(maximized)
        logger.debug("GUI is all created")
        self.Thaw()

    @debugging
    def do_tab_window(self, etreeEl, parent_wnd):
        # if container window loop through childern and do a recursive call
        temp_wnd = orpgTabberWnd(parent_wnd, style=FNB.FNB_ALLOW_FOREIGN_DND)

        for c in etreeEl.getchildren():
            wnd = self.build_window(c, temp_wnd)
            temp_wnd.AddPage(wnd, c.get('name'), False)

        return temp_wnd

    @debugging
    def build_window(self, etreeEl, parent_wnd):
        name = etreeEl.tag
        if name == 'DockLayout' or name == 'dock':
            return

        direction = etreeEl.get('direction')
        pos = etreeEl.get('pos')
        height = etreeEl.get('height')
        width = etreeEl.get('width')
        cap = etreeEl.get('caption')
        dockable = etreeEl.get('dockable')
        layer = etreeEl.get('layer')

        try:
            layer = int(layer)
            dockable = int(dockable)
        except:
            layer = 0
            dockable = 1

        if name == 'tab':
            temp_wnd = self.do_tab_window(etreeEl, parent_wnd)
        elif name == 'map':
            temp_wnd = orpg.mapper.map.map_wnd(parent_wnd, wx.ID_ANY)
            self.map = temp_wnd
        elif name == 'tree':
            temp_wnd = orpg.gametree.gametree.game_tree(parent_wnd, wx.ID_ANY)
            self.tree = temp_wnd

            if settings.get('ColorTree') == '1':
                self.tree.SetBackgroundColour(settings.get('bgcolor'))
                self.tree.SetForegroundColour(settings.get('textcolor'))
            else:
                self.tree.SetBackgroundColour('white')
                self.tree.SetForegroundColour('black')

        elif name == 'chat':
            temp_wnd = orpg.chat.chatwnd.chat_notebook(parent_wnd,
                                                       wx.DefaultSize)
            self.chattabs = temp_wnd
            self.chat = temp_wnd.MainChatPanel

        elif name == 'player':
            temp_wnd = orpg.player_list.player_list(parent_wnd)
            self.players = temp_wnd

            if settings.get('ColorTree') == '1':
                self.players.SetBackgroundColour(settings.get('bgcolor'))
                self.players.SetForegroundColour(settings.get('textcolor'))
            else:
                self.players.SetBackgroundColour('white')
                self.players.SetForegroundColour('black')

        if parent_wnd != self:
            #We dont need this if the window are beeing tabed
            return temp_wnd

        menuid = wx.NewId()
        self.windowsmenu.Append(menuid, cap, kind=wx.ITEM_CHECK)
        self.windowsmenu.Check(menuid, True)
        self.Bind(wx.EVT_MENU, self.OnMB_WindowsMenu, id=menuid)
        self.mainwindows[menuid] = cap

        wndinfo = AUI.AuiPaneInfo()
        wndinfo.DestroyOnClose(False)
        wndinfo.Name(cap)
        wndinfo.FloatingSize(wx.Size(int(width), int(height)))
        wndinfo.BestSize(wx.Size(int(width), int(height)))
        wndinfo.Layer(int(layer))
        wndinfo.Caption(cap)

        if direction.lower() == 'top':
            wndinfo.Top()
        elif direction.lower() == 'bottom':
            wndinfo.Bottom()
        elif direction.lower() == 'left':
            wndinfo.Left()
        elif direction.lower() == 'right':
            wndinfo.Right()
        elif direction.lower() == 'center':
            wndinfo.Center()

        if dockable != 1:
            wndinfo.Dockable(False)
            wndinfo.Floatable(False)

        if pos not in ['', '0', None]:
            wndinfo.Position(int(pos))

        wndinfo.Show()
        self._mgr.AddPane(temp_wnd, wndinfo)

        return temp_wnd

    @debugging
    def onPaneClose(self, evt):
        pane = evt.GetPane()

        if pane.name == 'Sound Control Toolbar':
            self.mainmenu.SetMenuState('ToolsSoundToolbar', False)
        elif pane.name == 'Status Window':
            self.mainmenu.SetMenuState('ToolsStatusBar', False)
        elif pane.name == 'Dice Tool Bar':
            self.mainmenu.SetMenuState('ToolsDiceBar', False)
        elif pane.name == 'Map Tool Bar':
            self.mainmenu.SetMenuState('ToolsMapBar', False)
        else:
            for wndid, wname in self.mainwindows.iteritems():
                if pane.name == wname:
                    self.windowsmenu.Check(wndid, False)
                    break

        evt.Skip()
        self._mgr.Update()

    @debugging
    def saveLayout(self):
        etree = ElementTree()
        with open(dir_struct['user'] + 'layout.xml') as f:
            etree.parse(f)

        base = etree.getroot()

        x_size, y_size = self.GetClientSize()
        x_pos, y_pos = self.GetPositionTuple()

        if self.IsMaximized():
            maximized = 1
        else:
            maximized = 0

        base.set('height', str(y_size))
        base.set('width', str(x_size))
        base.set('posx', str(x_pos))
        base.set('posy', str(y_pos))
        base.set('maximized', str(maximized))

        layout = base.find('DockLayout')
        if layout is None:
            layout = Element('DockLayout')
            layout.set('DO_NO_EDIT','True')
            base.append(layout)

        layout.text = str(self._mgr.SavePerspective())

        with open(dir_struct['user'] + 'layout.xml', 'w') as f:
            etree.write(f)

    @debugging
    def build_hotkeys(self):
        self.mainmenu.accel.xaccel.extend(self.chat.get_hot_keys())
        self.mainmenu.accel.xaccel.extend(self.map.get_hot_keys())

    @debugging
    def start_timer(self):
        self.poll_timer.Start(100)
        if settings.get('Heartbeat') == '1':
            self.ping_timer.Start(1000*60)
            logger.debug("starting heartbeat...", True)

    @debugging
    def kill_mplay_session(self):
        self.game_name = ''
        self.session.start_disconnect()

    @debugging
    def quit_game(self, evt):
        dlg = wx.MessageDialog(self, "Exit gaming session?", 'Game Session',
                               wx.YES_NO)
        if dlg.ShowModal() == wx.ID_YES:
            self.session.exitCondition.notifyAll()
            dlg.Destroy()
            self.kill_mplay_session()

    @debugging
    def on_status_event(self, evt):
        id = evt.get_id()
        status = evt.get_data()
        if id == orpg.networking.mplay_client.STATUS_SET_URL:
            self.status.url = status

    @debugging
    def on_player_event(self, evt):
        id = evt.get_id()
        player = evt.get_data()
        display_name = self.chat.chat_display_name(player)
        time_str = time.strftime('%H:%M', time.localtime())
        if id == orpg.networking.mplay_client.PLAYER_NEW:
            self.players.add_player(player)
            self.chat.InfoPost(display_name + " (enter): " + time_str)
        elif id == orpg.networking.mplay_client.PLAYER_DEL:
            self.players.del_player(player)
            self.chat.InfoPost(display_name + " (exit): " + time_str)
        elif id == orpg.networking.mplay_client.PLAYER_UPDATE:
            self.players.update_player(player)
        self.players.Refresh()

    @debugging
    def on_group_event(self, evt):
        id = evt.get_id()
        data = evt.get_data()

        if id == orpg.networking.mplay_client.GROUP_NEW:
            self.gs.add_room(data)
        elif id == orpg.networking.mplay_client.GROUP_DEL:
            self.password_manager.RemoveGroupData(data)
            self.gs.del_room(data)
        elif id == orpg.networking.mplay_client.GROUP_UPDATE:
            self.gs.update_room(data)


    @debugging
    def on_receive(self, etreeEl, player):
        #recvSound = "RecvSound"
        # this will be the default sound.  Whisper will change this below
        if player:
            display_name = self.chat.chat_display_name(player)
            display_name = '<b>' + display_name + '</b>: '
        else:
            display_name = "<b><i><u>Server Administrator</u>-></i></b> "

        if etreeEl.text:
            self.chat.Post(display_name + etreeEl.text)

        for child in etreeEl.getchildren():
            if child.tag == 'tree':
                #TODO: Fix game tree to accepts elements
                self.tree.on_receive_data(child, player)
                self.chat.InfoPost(display_name + " has sent you a tree node...")

            elif child.tag == 'map':
                #TODO: Fix map to accepts elements
                self.map.new_data(child)

            elif child.tag == 'chat':
                msg = orpg.chat.chat_msg.chat_msg()
                msg.takedom(child)
                self.chat.post_incoming_msg(msg, player)

    @debugging
    def on_mplay_event(self, evt):
        id = evt.get_id()
        if id == orpg.networking.mplay_client.MPLAY_CONNECTED:
            self.chat.InfoPost("Game connected!")
            self.gs.set_connected(1)
            self.password_manager.ClearPassword('ALL')

        elif id == orpg.networking.mplay_client.MPLAY_DISCONNECTED:
            self.poll_timer.Stop()
            self.ping_timer.Stop()
            self.chat.SystemPost("Game disconnected!")
            self.players.reset()
            self.gs.set_connected(0)
            self.status.connect_status = 'Not Connected'

        ####Begin changes for Custom Exit Message by mDuo13######
        elif id == orpg.networking.mplay_client.MPLAY_DISCONNECTING:
            custom_msg = settings.get('dcmsg')
            custom_msg=custom_msg[:80]
            if custom_msg[:3]=='/me':
                self.chat.send_chat_message(custom_msg[3:], 3)
            else:
                self.chat.system_message(custom_msg)
        #####End Changes for Custom Exit Message by mDuo13

        elif id== orpg.networking.mplay_client.MPLAY_GROUP_CHANGE:
            group = evt.get_data()
            self.chat.InfoPost("Moving to room '"+group[1]+"'..")
            if self.gs : self.gs.set_cur_room_text(group[1])
            self.players.reset()
        elif id== orpg.networking.mplay_client.MPLAY_GROUP_CHANGE_F:
            self.chat.SystemPost("Room access denied!")

    @debugging
    def OnCloseWindow(self, event):
        dlg = wx.MessageDialog(self, "Quit OpenRPG?", 'OpenRPG', wx.YES_NO)
        if dlg.ShowModal() == wx.ID_YES:
            dlg.Destroy()
            self.closed_confirmed()

    @debugging
    def closed_confirmed(self):
        self.activeplugins = open_rpg.get_component('plugins')
        self.aliaslib._on_file_save(None)

        #following lines added by mDuo13
        #########plugin_disabled()#########
        for plugin_fname in self.activeplugins.keys():
            plugin = self.activeplugins[plugin_fname]
            try:
                plugin.plugin_disabled()
            except Exception:
                traceback.print_exc()
        #end mDuo13 added code
        self.saveLayout()
        try:
            settings.save()
        except Exception:
            logger.general("[WARNING] Error saving 'settings' component", True)

        try:
            self.map.pre_exit_cleanup()
        except Exception:
            logger.general("[WARNING] Map error pre_exit_cleanup()", True)

        try:
            if settings.get('SaveGameTreeOnExit').lower() not in ['0',
                                                                  'false',
                                                                  'no']:
                self.tree.save_tree(settings.get('gametree'))
        except Exception:
            logger.exception(traceback.format_exc())
            logger.general("[WARNING] Error saving gametree", True)

        if self.session.get_status() == orpg.networking.mplay_client.MPLAY_CONNECTED:
            self.kill_mplay_session()

        try:
            #Kill all the damn timers
            self.sound_player.timer.Stop()
            del self.sound_player.timer
        except Exception:
            logger.general("sound didn't die properly.", True)

        try:
            self.poll_timer.Stop()
            self.ping_timer.Stop()
            self.chat.parent.chat_timer.Stop()
            self.map.canvas.zoom_display_timer.Stop()
            self.map.canvas.image_timer.Stop()
            self.status.timer.Stop()
            del self.ping_timer
            del self.poll_timer
            del self.chat.parent.chat_timer
            del self.map.canvas.zoom_display_timer
            del self.map.canvas.image_timer
            del self.status.timer
        except Exception:
            logger.general("some timer didn't die properly.", True)

        self._mgr.UnInit()
        mainapp = wx.GetApp()
        mainapp.ExitMainLoop()
        self.Destroy()

        try:
            if self.server_pipe != None:
                dlg = wx.ProgressDialog('Exit', "Stoping server", 2, self)
                dlg.Update(2)
                dlg.Show(True)
                self.server_pipe.write('\nkill\n')
                logger.general("Killing Server process:", True)
                time.sleep(5)
                self.server_stop()
                self.server_pipe.close()
                self.std_out.close()
                self.server_thread.exit()
                dlg.Destroy()
                logger.general("Server killed:", True)
        except Exception:
            pass


########################################
## Application class
########################################
class orpgSplashScreen(wx.SplashScreen):
    @debugging
    def __init__(self, parent, bitmapfile, duration, callback):
        wx.SplashScreen.__init__(self, wx.Bitmap(bitmapfile),
                                 wx.SPLASH_CENTRE_ON_SCREEN|wx.SPLASH_TIMEOUT,
                                 duration, None, -1)
        self.callback = callback
        self.closing = False
        self.Bind(wx.EVT_CLOSE, self.callback)

class orpgApp(wx.App):
    @debugging
    def OnInit(self):
        #Add the initial global components of the openrpg class
        #Every class should be passed openrpg
        open_rpg.add_component('log', logger)
        open_rpg.add_component('dir_struct', dir_struct)
        open_rpg.add_component('tabbedWindows', [])
        open_rpg.add_component('validate', validate)
        open_rpg.add_component('settings', settings)
        logger.log_level = int(settings.get('LoggingLevel'))
        self.called = False
        wx.InitAllImageHandlers()
        self.splash = orpgSplashScreen(None,
                            dir_struct['icon'] + 'splash13.jpg',
                            3000, self.AfterSplash)
        self.Bind(wx.EVT_KEY_DOWN, self.OnKeyPress)
        self._crust = None
        wx.Yield()
        return True

    @debugging
    def OnKeyPress(self, evt):
        #Event handler
        if evt.AltDown() and evt.CmdDown() and evt.KeyCode == ord('I'):
            self.ShowShell()
        else:
            evt.Skip()

    @debugging
    def ShowShell(self):
        #Show the PyCrust window.
        if not self._crust:
            self._crust = wx.py.crust.CrustFrame(self.GetTopWindow())
            self._crust.shell.interp.locals['app'] = self
        win = wx.FindWindowAtPointer()
        self._crust.shell.interp.locals['win'] = win
        self._crust.Show()

    @debugging
    def AfterSplash(self,evt):
        if not self.called:
            self.splash.Hide()
            self.called = True
            self.frame = orpgFrame(None, wx.ID_ANY, CLIENT_STRING)
            self.frame.Raise()
            self.frame.Refresh()
            self.frame.Show(True)
            self.SetTopWindow(self.frame)
            #self.frame.show_dlgs()
            self.frame.post_show_init()
            wx.CallAfter(self.splash.Close)
            return True

    @debugging
    def OnExit(self):
        pass

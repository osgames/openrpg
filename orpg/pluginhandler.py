import wx

from orpg.orpgCore import open_rpg
from orpg.dirpath import dir_struct
from orpg.tools.settings import settings
from orpg.tools.orpg_log import logger
from orpg.tools.validate import validate
from orpg.tools.decorators import debugging
from orpg.external.etree.ElementTree import ElementTree, Element
from orpg.external.etree.ElementTree import fromstring, tostring

class PluginHandler(object):
    @debugging
    def __init__(self, plugindb, parent):
        self.session = open_rpg.get_component("session")
        self.chat = open_rpg.get_component("chat")
        self.settings = settings
        self.gametree = open_rpg.get_component("tree")
        self.startplugs = open_rpg.get_component("startplugs")
        self.validate = validate
        self.topframe = open_rpg.get_component("frame")
        self.plugindb = plugindb
        self.parent = parent
        self.shortcmdlist = self.chat.chat_cmds.shortcmdlist
        self.cmdlist = self.chat.chat_cmds.cmdlist

    @debugging
    def plugin_enabled(self):
        pass

    @debugging
    def plugin_disabled(self):
        pass

    @debugging
    def plugin_menu(self):
        self.menu = wx.Menu()
        empty = wx.MenuItem(self.menu, wx.ID_ANY, 'Empty')
        self.menu.AppendItem(empty)

    @debugging
    def menu_start(self):
        rootMenu = open_rpg.get_component("pluginmenu")
        self.plugin_menu()
        self._basemenu = rootMenu.AppendMenu(wx.ID_ANY, self.name, self.menu)

    @debugging
    def menu_cleanup(self):
        settings.save()
        rootMenu = open_rpg.get_component("pluginmenu")
        menus = rootMenu.MenuItems
        rootMenu.RemoveItem(self._basemenu)

    @debugging
    def plugin_addcommand(self, cmd, function, helptext, show_msg=True):
        self.chat.chat_cmds.addcommand(cmd, function, helptext)
        if(show_msg):
            msg = '<br /><b>New Command added</b><br />'
            msg += '<b><font color="#000000">%s</font></b>  %s' % (cmd, helptext)
            self.chat.InfoPost(msg)

    @debugging
    def plugin_commandalias(self, shortcmd, longcmd, show_msg=True):
        self.chat.chat_cmds.addshortcmd(shortcmd, longcmd)
        if(show_msg):
            msg = '<br /><b>New Command Alias added:</b><br />'
            msg += '<b><font color="#0000CC">%s</font></b> is short for <font color="#000000">%s</font>' % (shortcmd, longcmd)
            self.chat.InfoPost(msg)

    @debugging
    def plugin_removecmd(self, cmd):
        self.chat.chat_cmds.removecmd(cmd)
        msg = '<br /><b>Command Removed:</b> %s' % (cmd)
        self.chat.InfoPost(msg)

    @debugging
    def plugin_add_nodehandler(self, nodehandler, nodeclass):
        self.gametree.add_nodehandler(nodehandler, nodeclass)

    @debugging
    def plugin_remove_nodehandler(self, nodehandler):
        self.gametree.remove_nodehandler(nodehandler)

    @debugging
    def plugin_add_msg_handler(self, tag, function):
        self.session.add_msg_handler(tag, function)

    @debugging
    def plugin_delete_msg_handler(self, tag):
        self.session.remove_msg_handler(tag)

    @debugging
    def plugin_add_setting(self, setting, value, options, help):
        settings.new_tab(self.name, 'grid', 'Plugins')
        settings.add(self.name, setting, value, options, help)

    @debugging
    def plugin_send_msg(self, to, plugin_msg):
        if isinstance(plugin_msg, basestring):
            plugin_msg = fromstring(plugin_msg)

        plugin_msg.set('from', str(self.session.id))
        plugin_msg.set('to', str(to))
        plugin_msg.set('group_id', str(self.session.group_id))

        if not plugin_msg.tag in self.session.core_msg_handlers:
            msg = Element('plugin')
            msg.set('to', str(to))
            msg.set('from', str(self.session.id))
            msg.set('group_id', str(self.session.group_id))
            msg.append(plugin_msg)

            self.session.outbox.put(msg)
        else:
            #Spoofing attempt
            pass

    @debugging
    def message(self, text):
        return text

    @debugging
    def pre_parse(self, text):
        return text

    @debugging
    def send_msg(self, text, send):
        return text, send

    @debugging
    def plugin_incoming_msg(self, text, type, name, player):
        return text, type, name

    @debugging
    def post_msg(self, text, myself):
        return text

    @debugging
    def refresh_counter(self):
        pass

    @debugging
    def close_module(self):
        #This is called when OpenRPG shuts down
        pass

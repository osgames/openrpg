from orpg.tools.orpg_log import logger

try:
    import wxversion
    wxversion.select(["2.7.2", "2.8"])
    import wx
    import wx.html
    import wx.lib.wxpTag
    import wx.grid
    import wx.media
    from wx.lib.filebrowsebutton import  *

    try:
        import wx.lib.agw.flatnotebook as FNB
    except ImportError:
        import orpg.external.agw.flatnotebook as FNB

    try:
        import wx.lib.agw.aui as AUI
    except ImportError:
        import wx.aui as AUI

    if wx.VERSION_STRING < '2.8':
        AUI.EVT_AUI_PANEMAXIMIZE = wx.PyEventBinder(AUI.wxEVT_AUI_PANEMAXIMIZE)
        AUI.EVT_AUI_PANERESTORE = wx.PyEventBinder(AUI.wxEVT_AUI_PANERESTORE)
        AUI.EVT_AUI_PANE_CLOSE = wx.PyEventBinder(AUI.wxEVT_AUI_PANECLOSE)

    WXLOADED = True
except ImportError:
    WXLOADED = False
    logger.general("*WARNING* failed to import wxPython.", True)
    logger.general("Download the correct version", True)
    logger.general("If you are running the server with no gui you can ignore this message", True)
except Exception, e:
    WXLOADED = False
    logger.general(e)
    logger.general("\nYou do not have the correct version of wxPython installed, please", True)
    logger.general("Download the correct version", True)
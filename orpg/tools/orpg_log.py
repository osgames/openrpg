# Copyright (C) 2000-2001 The OpenRPG Project
#
#   openrpg-dev@lists.sourceforge.net
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# --
#
# File: orpg_log.py
# Author: Dj Gilcrease
# Maintainer:
# Version:
#   $Id: orpg_log.py,v 1.9 2007/05/06 16:43:02 digitalxero Exp $
#
# Description: classes for orpg log messages
#
from __future__ import with_statement
import sys
import os, os.path
import time
import traceback

from orpg.external.terminalwriter import TerminalWriter
from orpg.tools.decorators import pending_deprecation
from orpg.dirpath import dir_struct

#########################
## Error Types
#########################

class orpgLog(object):
    _log_level = 7
    _log_name = None
    _log_to_console = False
    _io = TerminalWriter(sys.stderr)
    _lvl_args = None
    _CRITICAL = 1
    _GENERAL = 2
    _INFO = 4
    _NOTE = 8
    _DEBUG = 16

    def __new__(cls, *args, **kwargs):
        it = cls.__dict__.get("__it__")
        if it is not None:
            return it
        cls.__it__ = it = object.__new__(cls)

        try:
            it._io.line("Logger Started", green=True)
        except IOError:
            it._io = TerminalWriter(stringio=True)
        return it

    def __init__(self, home_dir, filename='orpgRunLog '):
        self._lvl_args = {16: {'colorizer': {'green': True},
                               'log_string': 'DEBUG'},
                          8: {'colorizer': {'bold': True, 'green': True},
                              'log_string':'NOTE'},
                          4: {'colorizer': {'blue': True, 'green': True},
                              'log_string': 'INFO'},
                          2: {'colorizer': {'red': True},
                             'log_string': 'ERROR'},
                          1: {'colorizer': {'bold': True, 'red': True},
                             'log_string': 'EXCEPTION'}}
        if not self.log_name:
            self.log_name = home_dir + filename + time.strftime('%m-%d-%Y.txt',
                                                    time.localtime(time.time()))

    def debug(self, msg, to_console=False):
        self.log(msg, self.DEBUG, to_console)

    def note(self, msg, to_console=False):
        self.log(msg, self.NOTE, to_console)

    def info(self, msg, to_console=False):
        self.log(msg, self.INFO, to_console)

    def general(self, msg, to_console=False):
        self.log(msg, self.GENERAL, to_console)

    def exception(self, msg, to_console=True):
        self.log(msg, self.CRITICAL, to_console)

    def log(self, msg, log_type, to_console=False):
        if self.log_to_console or to_console or log_type == self.CRITICAL or\
           log_type & self.log_level:
            self._io.line(str(msg), **self._lvl_args[log_type]['colorizer'])


        if log_type & self.log_level or to_console:
            atr = {'msg': msg, 'level': self._lvl_args[log_type]['log_string']}
            atr['time'] = time.strftime('[%x %X]', time.localtime(time.time()))
            logMsg = '%(time)s (%(level)s) - %(msg)s\n' % (atr)

            with open(self.log_name, 'a') as f:
                f.write(logMsg)

    def crash_report(self, _type, value, crash):
        report = ''.join(traceback.format_exception(_type, value, crash))
        with open(dir_struct["home"] + 'crash-report.txt', "w") as f:
            f.write(report)

        logger.exception(report)
        logger.exception("Crash Report Created!!")
        logger.info("Printed out crash-report.txt in your openrpg folder", True)


    @pending_deprecation("use logger.log_level = #")
    def setLogLevel(self, log_level):
        self.log_level = log_level

    @pending_deprecation("use logger.log_level")
    def getLogLevel(self):
        return self.log_level

    @pending_deprecation("use logger.log_name = bla")
    def setLogName(self, log_name):
        self.log_name = log_name

    @pending_deprecation("use logger.log_name")
    def getLogName(self):
        return self.log_name

    @pending_deprecation("use logger.log_to_console = True/False")
    def setLogToConsol(self, true_or_false):
        self.log_to_consol = true_or_false

    @pending_deprecation("use logger.log_to_console")
    def getLogToConsol(self):
        return self.log_to_consol

    """
    Property Methods
    """
    def _get_log_level(self):
        return self._log_level
    def _set_log_level(self, log_level):
        if not isinstance(log_level, int) or log_level < 1 or log_level > 31:
            raise TypeError("The loggers level must be an int between 1 and 31")

        self._log_level = log_level

    def _get_log_name(self):
        return self._log_name
    def _set_log_name(self, name):
        if not os.access(os.path.abspath(os.path.dirname(name)), os.W_OK):
            raise IOError("Could not write to the specified location")

        self._log_name = name

    def _get_log_to_console(self):
        return self._log_to_console
    def _set_log_to_console(self, true_or_false):
        if not isinstance(true_or_false, bool):
            raise TypeError("log_to_console must be a boolean value")

        self._log_to_console = true_or_false

    def _get_critical(self):
        return self._CRITICAL
    def _get_general(self):
        return self._GENERAL
    def _get_info(self):
        return self._INFO
    def _get_note(self):
        return self._NOTE
    def _get_debug(self):
        return self._DEBUG

    log_level = property(_get_log_level, _set_log_level)
    log_name = property(_get_log_name, _set_log_name)
    log_to_console = property(_get_log_to_console, _set_log_to_console)
    CRITICAL = property(_get_critical, None)
    GENERAL = property(_get_general, None)
    INFO = property(_get_info, None)
    NOTE = property(_get_note, None)
    DEBUG = property(_get_debug, None)

logger = orpgLog(dir_struct.get("user") + "runlogs/")
sys.excepthook = logger.crash_report
# file: config_files.py
#
# Author: Todd Faris (Snowdog)
# Date:   5/10/2005
#
# Misc. config file service methods
#

from __future__ import with_statement
import os
from orpg.dirpath import dir_struct

class Validate(object):
    _load_user_path = dir_struct["user"]

    def __new__(cls):
        it = cls.__dict__.get("__it__")
        if it is not None:
            return it
        cls.__it__ = it = object.__new__(cls)
        it._init()
        return it

    def __init__(self, path=None):
        if not path:
            path = dir_struct["user"]

        self._load_user_path = path

    def _init(self):
        self._load_user_path = dir_struct["user"]

    def set_base_path(self, path):
        self._load_user_path = path

    def config_file(self, user_file, template_file):
        #STEP 1: verify the template exists
        if not os.access(dir_struct["template"] + template_file, os.F_OK):
            return False

        #STEP 2: verify the user file exists. If it doesn't then create it from template
        if not os.access(self._load_user_path + user_file, os.F_OK):
            with open(dir_struct["template"] + template_file,"r") as default:
                with open(self._load_user_path + user_file,"w") as newfile:
                    newfile.write(default.read())

            return True

        #STEP 3: user file exists (is openable) return 1 indicating no-create operation required
        else: return None

    def ini_entry(self, entry_name, ini_file):
        pass

validate = Validate()
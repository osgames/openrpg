import wx
try:
    import wx.lib.agw.flatnotebook as FNB
except ImportError:
    import orpg.external.agw.flatnotebook as FNB

from orpg.orpgCore import open_rpg
from orpg.tools.rgbhex import RGBHex
from orpg.tools.settings import settings
from orpg.tools.decorators import debugging

class TabberWindow(FNB.FlatNotebook):
    @debugging
    def __init__(self, parent, closeable=False, size=wx.DefaultSize,
                 style=False):
        nbstyle = FNB.FNB_HIDE_ON_SINGLE_TAB|FNB.FNB_BACKGROUND_GRADIENT
        FNB.FlatNotebook.__init__(self, parent, -1, size=size, style=nbstyle)
        rgbcovert = RGBHex()
        tabtheme = settings.get('TabTheme')
        tabtext = settings.get('TabTextColor')
        (tred, tgreen, tblue) = rgbcovert.rgb_tuple(tabtext)
        tabbedwindows = open_rpg.get_component("tabbedWindows")
        tabbedwindows.append(self)
        open_rpg.add_component("tabbedWindows", tabbedwindows)

        theme_dict = {'slanted&aqua': FNB.FNB_VC8, 'slanted&bw': FNB.FNB_VC8,
                      'flat&aqua': FNB.FNB_FANCY_TABS,
                      'flat&bw': FNB.FNB_FANCY_TABS,
                      'customflat': FNB.FNB_FANCY_TABS,
                      'customslant': FNB.FNB_VC8,
                      'slanted&colorful': FNB.FNB_VC8|FNB.FNB_COLORFUL_TABS,
                      'slant&colorful': FNB.FNB_VC8|FNB.FNB_COLORFUL_TABS}
        nbstyle |= theme_dict[tabtheme]
        if style:
            nbstyle |= style
        self.SetWindowStyleFlag(nbstyle)

        #Tas - sirebral.  Planned changes to the huge statement below.
        if tabtheme == 'slanted&aqua':
            self.SetGradientColourTo(wx.Color(0, 128, 255))
            self.SetGradientColourFrom(wx.WHITE)

        elif tabtheme == 'slanted&bw':
            self.SetGradientColourTo(wx.WHITE)
            self.SetGradientColourFrom(wx.WHITE)

        elif tabtheme == 'flat&aqua':
            self.SetGradientColourFrom(wx.Color(0, 128, 255))
            self.SetGradientColourTo(wx.WHITE)
            self.SetNonActiveTabTextColour(wx.BLACK)

        elif tabtheme == 'flat&bw':
            self.SetGradientColourTo(wx.WHITE)
            self.SetGradientColourFrom(wx.WHITE)
            self.SetNonActiveTabTextColour(wx.BLACK)

        elif tabtheme == 'customflat':
            gfrom = settings.get('TabGradientFrom')
            (red, green, blue) = rgbcovert.rgb_tuple(gfrom)
            self.SetGradientColourFrom(wx.Color(red, green, blue))

            gto = settings.get('TabGradientTo')
            (red, green, blue) = rgbcovert.rgb_tuple(gto)
            self.SetGradientColourTo(wx.Color(red, green, blue))
            self.SetNonActiveTabTextColour(wx.Color(tred, tgreen, tblue))

        elif tabtheme == 'customslant':
            gfrom = settings.get('TabGradientFrom')
            (red, green, blue) = rgbcovert.rgb_tuple(gfrom)
            self.SetGradientColourFrom(wx.Color(red, green, blue))

            gto = settings.get('TabGradientTo')
            (red, green, blue) = rgbcovert.rgb_tuple(gto)
            self.SetGradientColourTo(wx.Color(red, green, blue))
            self.SetNonActiveTabTextColour(wx.Color(tred, tgreen, tblue))

        tabbg = settings.get('TabBackgroundGradient')
        (red, green, blue) = rgbcovert.rgb_tuple(tabbg)
        self.SetTabAreaColour(wx.Color(red, green, blue))
        self.Refresh()
import wx

from orpg.tools.decorators import debugging

class MultiTextEntry(wx.Dialog):
    """ a dialog that takes two lists (labels and values) and creates a
        'label: value' style text edit control for each node in the dict"""
    @debugging
    def __init__(self, parent, opts, caption, pos=wx.DefaultPosition):
        wx.Dialog.__init__(self, parent, wx.ID_ANY, caption, pos,
                           wx.DefaultSize)
        num = len(opts)
        sizers = {'ctrls': wx.FlexGridSizer(num, 2, 5, 0),
                  'buttons': wx.BoxSizer(wx.HORIZONTAL)}

        add_list = []
        ctrls = {}

        for name, value in opts.iteritems():
            add_list.append((wx.StaticText(self, wx.ID_ANY, name + ": "), 0,
                             wx.ALIGN_CENTER_VERTICAL))
            ctrls[name] = wx.TextCtrl(self, wx.ID_ANY, value)
            add_list.append((ctrls[name], 1, wx.EXPAND))

        self.ctrls = ctrls
        sizers['ctrls'].AddMany(add_list)
        sizers['ctrls'].AddGrowableCol(1)
        sizers['buttons'].Add(wx.Button(self, wx.ID_OK, "OK"), 1, wx.EXPAND)
        sizers['buttons'].Add(wx.Size(10,10))
        sizers['buttons'].Add(wx.Button(self, wx.ID_CANCEL, "Cancel"), 1,
                              wx.EXPAND)
        width = 300
        w, h = ctrls.values()[0].GetSizeTuple()
        h = h + 5
        height = ((num)*h)+35
        self.SetClientSizeWH(width,height)
        sizers['ctrls'].SetDimension(10, 5, width-20, num*30)
        sizers['buttons'].SetDimension(10, (num*h)+5, width-20, 25)
        self.Bind(wx.EVT_BUTTON, self.on_ok, id=wx.ID_OK)

    @debugging
    def on_ok(self,evt):
        self.EndModal(wx.ID_OK)

    @debugging
    def get_values(self):
        return self.ctrls

    @debugging
    def __exit__(self, type, value, traceback):
        return False

    @debugging
    def __enter__(self):
        return self

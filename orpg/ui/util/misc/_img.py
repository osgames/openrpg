import urllib

import wx

from orpg.tools.decorators import debugging

class ImageHelper(object):
    img_types = {"gif": wx.BITMAP_TYPE_GIF, "jpg": wx.BITMAP_TYPE_JPEG,
                 "jpeg": wx.BITMAP_TYPE_JPEG, "bmp": wx.BITMAP_TYPE_BMP,
                 "png": wx.BITMAP_TYPE_PNG}

    @debugging
    def __init__(self):
        pass

    @debugging
    def load_url(self, path):
        img_type = self.get_type(path)
        try:
            data = urllib.urlretrieve(path)
            if data:
                img = wx.Bitmap(data[0], img_type)
            else:
                raise IOError("Image refused to load!")
        except IOError, e:
            img = None
        return img

    @debugging
    def load_file(self, path):
        img_type = self.get_type(path)
        return wx.Bitmap(path, img_type)

    @debugging
    def get_type(self, file_name):
        ext = file_name.split('.')[-1].lower()
        img_type = 0
        img_type = self.img_types.get(ext)
        return img_type
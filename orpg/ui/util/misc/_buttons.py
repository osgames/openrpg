import wx

def create_masked_button(parent, image, tooltip, id, mask_color=wx.WHITE,
                       image_type=wx.BITMAP_TYPE_GIF):
    gif = wx.Image(image, image_type).ConvertToBitmap()
    mask = wx.Mask(gif, mask_color)
    gif.SetMask(mask)
    btn = wx.BitmapButton(parent, id, gif)
    btn.SetToolTip(wx.ToolTip(tooltip))
    return btn
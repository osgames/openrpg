import wx

from orpg.tools.decorators import debugging

class ProgressDlg(wx.Dialog):
    @debugging
    def __init__(self, parent,  title="", text="", range=10):
        wx.Dialog.__init__(self, parent, -1, title, size= wx.Size(200,75))
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.text = wx.StaticText(self, -1, text)
        self.gauge = wx.Gauge(self,-1,range)
        self.sizer.Add(self.text,1,wx.ALIGN_CENTER | wx.EXPAND)
        self.sizer.Add(self.gauge,1,wx.ALIGN_CENTER | wx.EXPAND)
        (w,h) = self.GetClientSizeTuple()
        self.sizer.SetDimension(10,10,w-20,h-20)

    @debugging
    def Update(self,pos,text=None):
        self.gauge.SetValue(pos)
        if text:
            self.text.SetLabel(text)

    def __enter__(self, *args, **kwargs):
        return self
    def __exit__(self, *args, **kwargs):
        self.Destroy()
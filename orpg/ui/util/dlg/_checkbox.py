import wx

from orpg.tools.decorators import debugging

class MultiCheckBoxDlg(wx.Dialog):
    """ notes """
    @debugging
    def __init__(self, parent, opts, text, caption, selected=[],
                 pos=wx.DefaultPosition):
        wx.Dialog.__init__(self, parent, wx.ID_ANY, caption, pos,
                           wx.DefaultSize)
        sizers = {'ctrls': wx.BoxSizer(wx.VERTICAL),
                   'buttons': wx.BoxSizer(wx.HORIZONTAL)}
        self.opts = opts
        self.list = wx.CheckListBox(self, wx.ID_ANY, wx.DefaultPosition,
                                    wx.DefaultSize, opts)
        for s in selected:
            self.list.Check(s, 1)

        sizers['ctrls'].Add(wx.StaticText(self, -1, text), 0, 0)
        sizers['ctrls'].Add(wx.Size(10,10))
        sizers['ctrls'].Add(self.list, 1, wx.EXPAND)
        sizers['buttons'].Add(wx.Button(self, wx.ID_OK, "OK"), 1, wx.EXPAND)
        sizers['buttons'].Add(wx.Size(10,10))
        sizers['buttons'].Add(wx.Button(self, wx.ID_CANCEL, "Cancel"), 1,
                              wx.EXPAND)
        sizers['ctrls'].Add(sizers['buttons'], 0, wx.EXPAND)
        self.SetSizer(sizers['ctrls'])
        self.SetAutoLayout(True)
        self.Fit()
        self.Bind(wx.EVT_BUTTON, self.on_ok, id=wx.ID_OK)

    @debugging
    def on_ok(self,evt):
        checked = []
        for i in xrange(len(self.opts)):
            if self.list.IsChecked(i):
                checked.append(i)
        self.checked = checked
        self.EndModal(wx.ID_OK)

    @debugging
    def get_selections(self):
        return self.checked

    def __enter__(self, *args, **kwargs):
        return self
    def __exit__(self, *args, **kwargs):
        self.Destroy()
import wx

from orpg.dirpath import dir_struct
from orpg.tools.settings import settings
from orpg.tools.rgbhex import RGBHex
from orpg.ui.util.misc import create_masked_button

class TextFormatToolBar(wx.Panel):
    """
    Toolbar that adds formating buttons
    """
    _text_field = None
    _rgb = RGBHex()

    def __init__(self, parent, text_field):
        wx.Panel.__init__(self, parent, wx.ID_ANY)

        self._text_field = text_field

        self.sizer = wx.BoxSizer(wx.HORIZONTAL)

        btn = create_masked_button(self, dir_struct['icon'] + 'bold.gif',
                                   'Make the selected text Bold', wx.ID_ANY,
                                   '#bdbdbd')
        self.sizer.Add(btn, 0, wx.ALIGN_CENTER)
        self.Bind(wx.EVT_BUTTON, self._bold_clicked, btn)

        btn = create_masked_button(self, dir_struct["icon"]+'italic.gif',
                                   'Italicize the selected text', wx.ID_ANY,
                                   '#bdbdbd')
        self.sizer.Add(btn, 0, wx.ALIGN_CENTER)
        self.Bind(wx.EVT_BUTTON, self._italics_clicked, btn)

        btn = create_masked_button(self, dir_struct['icon'] + 'underlined.gif',
                                   'Underline the selected text', wx.ID_ANY,
                                   '#bdbdbd')
        self.sizer.Add(btn, 0, wx.ALIGN_CENTER)
        self.Bind(wx.EVT_BUTTON, self._underline_clicked, btn)

        self.color_btn = create_masked_button(self, dir_struct['icon'] + 'textcolor.gif',
                                   'Set the Text Color for the selected text',
                                   wx.ID_ANY, '#bdbdbd')
        self.sizer.Add(self.color_btn, 0, wx.ALIGN_CENTER)
        self.Bind(wx.EVT_BUTTON, self._color_clicked, self.color_btn)

        # Now, attach the sizer to the panel and tell it to do it's magic
        self.SetSizer(self.sizer)
        self.SetAutoLayout(True)
        self.Fit()

        #Update btn
        self._update_color_button(settings.get('mytextcolor'))

    #Events
    def _bold_clicked(self, evt):
        self._do_replacemnt('b')


    def _italics_clicked(self, evt):
        self._do_replacemnt('i')

    def _underline_clicked(self, evt):
        self._do_replacemnt('u')

    def _color_clicked(self, evt):
        hexcolor = self._rgb.do_hex_color_dlg(self)

        start, end = self._text_field.GetSelection()

        if hexcolor != None:
            if start != end:
                self._do_replacemnt('font color="{hexcolor}"'.format(
                    hexcolor=hexcolor))
            else:
                self._update_color_button(hexcolor)
                settings.set('mytextcolor', hexcolor)

    def _do_replacemnt(self, format_type):
        txt = self._text_field.Value
        start, end = self._text_field.GetSelection()

        pre = txt[:start]
        selection = txt[start:end]
        post = txt[end:]

        txt = '{pre}<{format_type}>{selection}</{format_type}>{post}'.\
                                                format(pre=pre,
                                                       format_type=format_type,
                                                       selection=selection,
                                                       post=post)

        self._text_field.Value = txt
        self._text_field.SetInsertionPointEnd()
        self._text_field.SetFocus()

    def _update_color_button(self, color):
        """
        Dynamically Generates a properly colored background for the text color
        button to maintain uniform look on buttons
        """
        self.color_btn.BackgroundColour = color


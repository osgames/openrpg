import wx

from orpg.orpgCore import open_rpg
from orpg.dirpath import dir_struct
from orpg.ui.util.misc import create_masked_button
from orpg.tools.pubsub import Publisher

class AliasToolBar(wx.Panel):
    """
    The Alias Toolbar
    """

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.ID_ANY)

        self._colors = []
        self._rules = []

        self.sizer = wx.BoxSizer(wx.HORIZONTAL)

        btn = create_masked_button(self, dir_struct["icon"] + 'player.gif', '',
                                 wx.ID_ANY, '#bdbdbd')
        self.sizer.Add(btn, 0, wx.EXPAND)

        self._alias_list = wx.Choice(self, wx.ID_ANY, size=(100, 25),
                                    choices=[self.default_alias])
        self._alias_list.SetSelection(0)
        self._colors.append('Default')
        self.sizer.Add(self._alias_list, 0, wx.EXPAND)

        btn = create_masked_button(self, dir_struct["icon"] + 'add_filter.gif',
                                 '', wx.ID_ANY, '#bdbdbd')
        self.sizer.Add(btn, 0, wx.EXPAND)

        self._filter_list = wx.Choice(self, wx.ID_ANY, size=(100, 25),
                                    choices=[self.default_filter])
        self._rules = [[]]
        self._filter_list.SetSelection(0)
        self.sizer.Add(self._filter_list, 0, wx.EXPAND)

        self.SetSizer(self.sizer)
        self.SetAutoLayout(True)
        self.Fit()

        Publisher.subscribe(self._on_alias_changed, 'aliaslib.alias_list')
        Publisher.subscribe(self._on_filter_changed, 'aliaslib.filter_list')
        Publisher.subscribe(self._on_set_alias, 'aliaslib.set_alias')
        Publisher.subscribe(self._on_set_filter, 'aliaslib.set_filter')

    def select_alias(self, name):
        try:
            self._alias_list.StringSelection = name
        except Exception:
            self._alias_list.Selection = 0

    #Events
    def _on_alias_changed(self, evt):
        current = self._alias_list.StringSelection
        self._alias_list.Clear()
        self._alias_list.Append(self.default_alias)
        self._colors = ['Default']
        for name, color in evt.data:
            self._alias_list.Append(name)
            self._colors.append(color)

        self._alias_list.StringSelection = current

    def _on_filter_changed(self, evt):
        current = self._filter_list.StringSelection
        self._filter_list.Clear()
        self._filter_list.Append(self.default_filter)
        self._rules = [[]]
        for name, rules in evt.data:
            self._filter_list.Append(name)
            self._rules.append(rules)

        self._filter_list.StringSelection = current

    def _on_set_alias(self, evt):
        self._alias_list.StringSelection = evt.data or self.default_alias

    def _on_set_filter(self, evt):
        self._filter_list.StringSelection = evt.data or self.default_filter

    #Properties
    def _get_alias(self):
        return self._alias_list.StringSelection
    alias = property(_get_alias)

    def _get_alias_color(self):
        try:
            return self._colors[self._alias_list.CurrentSelection]
        except IndexError:
            return 'Default'
    alias_color = property(_get_alias_color)

    def _get_filter(self):
        return self._filter_list.StringSelection
    filter = property(_get_filter)

    def _get_filter_rules(self):
        try:
            return self._rules[self._filter_list.CurrentSelection]
        except IndexError:
            return []
    filter_rules = property(_get_filter_rules)

    def _get_default_alias(self):
        return 'Use Real Name'
    default_alias = property(_get_default_alias)

    def _get_default_filter(self):
        return 'No Filter'
    default_filter = property(_get_default_filter)
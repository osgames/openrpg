import time

import wx

from orpg.tools.decorators import debugging, deprecated

class StatusBar(wx.StatusBar):
    _connect_status = "Not Connected"
    _url = ""
    _window = 1

    @debugging
    def __init__(self, parent):
        wx.StatusBar.__init__(self, parent, wx.ID_ANY)

        self.menu = wx.Menu("Switch layout to...")
        item = wx.MenuItem(self.menu, wx.ID_ANY, "General", "General",
                           wx.ITEM_CHECK)
        self.Bind(wx.EVT_MENU, self.OnM_SwitchlayouttoGeneral, item)
        self.menu.AppendItem(item)
        item = wx.MenuItem(self.menu, wx.ID_ANY, "Url Display", "Url Display",
                           wx.ITEM_CHECK)
        self.Bind(wx.EVT_MENU, self.OnM_SwitchlayouttoUrlDisplay, item)
        self.menu.AppendItem(item)
        self.Bind(wx.EVT_RIGHT_DOWN, self.onPopup)
        self.SetFieldsCount(2)
        self.timer = wx.Timer(self, wx.NewId())
        self.Bind(wx.EVT_TIMER, self.Notify)
        self.timer.Start(3000)

    @debugging
    def onPopup(self, evt):
        self.PopupMenu(self.menu)

    @debugging
    def OnM_SwitchlayouttoUrlDisplay(self, evt):
        self._window = 2
        self.bar1()

    @debugging
    def OnM_SwitchlayouttoGeneral(self, evt):
        self._window = 1
        self.bar0()

    def Notify(self, event):
        if self._window == 1:
            self.bar0()
        elif self._window == 2:
            self.bar1()

    def bar1(self):
        self.SetFieldsCount(1)
        self.SetStatusWidths([-1])
        self.SetStatusText("URL: " + self.urlis, 0)

    def bar0(self):
        self.SetFieldsCount(2)
        t = time.gmtime(time.time())
        st = time.strftime("GMT: %d-%b-%Y  %I:%M:%S", t)
        (x,y) = self.GetTextExtent(st)
        self.SetStatusWidths([-1, x+10])
        self.SetStatusText(self.connect_status, 0)
        self.SetStatusText(st, 1)

    def __del__(self):
        self.timer.Stop()
        del self.timer

    #Depreciated Mthods
    @deprecated("set the url with the url property")
    def set_url(self, url):
        self.url = url

    @deprecated("set the connect_status with the connect_status property")
    def set_connect_status(self, connect):
        self.connect_status = connect

    #Property Methods
    def _get_url(self):
        return _url
    def _set_url(self, value):
        if not isinstance(value, basestring):
            raise TypeError("url must be a string")
        self._url = value
    url = property(_get_url, _set_url)

    def _get_connect_status(self):
        return self._connect_status
    def _set_connect_status(self, value):
        if not isinstance(value, basestring):
            raise TypeError("connect_status must be a string")
        self._connect_status = value
    connect_status = property(_get_connect_status, _set_connect_status)

@deprecated("Please use the StatusBar class")
def status_bar(parent):
    return StatusBar(parent)
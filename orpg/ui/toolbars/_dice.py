import wx

from orpg.dirpath import dir_struct
from orpg.tools.inputValidator import MathOnlyValidator
from orpg.ui.util.misc import create_masked_button

class DiceToolBar(wx.Panel):
    """
    This is where all of the dice related tools belong for quick reference.
    """
    _callback = None

    def __init__(self, parent, title="Dice Tool Bar",
                  size=wx.Size(300, 45), callback=None):
        wx.Panel.__init__(self, parent, wx.ID_ANY, size=size)
        # Save our post callback
        self._callback = callback

        # Make a sizer for everything to belong to
        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        # Build the toolbar now
        self.numDieText = wx.TextCtrl(self, wx.ID_ANY, "1",
                                      size= wx.Size(50, 25),
                                      validator=MathOnlyValidator())
        self.sizer.Add(self.numDieText, 1, wx.EXPAND | wx.ALIGN_LEFT)

        btns = ['d4', 'd6', 'd8', 'd10', 'd12', 'd20', 'd100']
        for die in btns:
            btn = create_masked_button(self,
                                       dir_struct['icon'] + 'b_{die}.gif'.\
                                       format(die=die),
                                       'Roll {die}'.format(die=die),
                                       wx.ID_ANY, wx.WHITE)
            self.sizer.Add(btn, 0, wx.ALIGN_CENTER)
            evt_method = getattr(self, '_{die}_clicked'.format(die=die))
            self.Bind(wx.EVT_BUTTON, evt_method, btn)

        # Add our other text control to the sizer
        self.dieModText = wx.TextCtrl(self, wx.ID_ANY, "+0",
                                      size=wx.Size(50, 25),
                                      validator=MathOnlyValidator())
        self.sizer.Add(self.dieModText, 1, wx.EXPAND | wx.ALIGN_RIGHT)

        # Now, attach the sizer to the panel and tell it to do it's magic
        self.SetSizer(self.sizer)
        self.SetAutoLayout(True)
        self.Fit()

    #Events
    def _d4_clicked(self, evt):
        self._do_roll('d4')

    def _d6_clicked(self, evt):
        self._do_roll('d6')

    def _d8_clicked(self, evt):
        self._do_roll('d8')

    def _d10_clicked(self, evt):
        self._do_roll('d10')

    def _d12_clicked(self, evt):
        self._do_roll('d12')

    def _d20_clicked(self, evt):
        self._do_roll('d20')

    def _d100_clicked(self, evt):
        self._do_roll('d100')

    def _do_roll(self, sides):
        die = self.numDieText.GetValue()
        mod = self.dieModText.GetValue()

        rollString = '[{die}{sides}{mod}]'.format(die=die,
                                                  sides=sides,
                                                  mod=mod)
        if self._callback != None:
            self._callback(rollString, 1, 1)
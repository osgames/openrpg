# Copyright (C) 2000-2001 The OpenRPG Project
#
#       openrpg-dev@lists.sourceforge.net
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# --
#
# File: gsclient.py
# Author: Chris Davis
# Maintainer:
# Version:
#   $Id: gsclient.py,v 1.53 2007/10/25 21:49:34 digitalxero Exp $
#
# Description: The file contains code for the game server browser
#
from __future__ import with_statement

import traceback

import wx
from wx.lib.mixins.listctrl import ListCtrlAutoWidthMixin, CheckListCtrlMixin

from orpg.networking.meta_server_lib import get_server_list
from orpg.chat.chat_util import strip_html, strip_unicode
from orpg.tools.rgbhex import RGBHex
from orpg.orpgCore import open_rpg
from orpg.dirpath import dir_struct
from orpg.tools.decorators import debugging
from orpg.tools.orpg_log import logger
from orpg.tools.settings import settings
from orpg.tools.validate import validate
from orpg.external.etree.ElementTree import ElementTree, Element
from orpg.external.etree.ElementTree import fromstring, tostring

class server_instance(object):
    def __init__(self, id, name="[Unknown]", users="0", address="127.0.0.1",
                 port="6774"):
        self.id = id
        self.name = name
        self.user = users
        self.addy = address
        self.port = port

@debugging
def server_instance_compare(x, y):
    """compares server insances for list sort"""
    DEV_SERVER = "OpenRPG DEV"
    xname = x.get('name', '')
    xuser = int(x.get('num_users', 0))
    yname = y.get('name', '')
    yuser = int(y.get('num_users', 0))

    who_name = cmp(yname, xname)
    who_user = cmp(yuser, xuser)

    if xname.startswith(DEV_SERVER):
        if yname.startswith(DEV_SERVER):
            if not who_name:
                return who_user
            else:
                return who_name
        else:
            return -1
    elif yname.startswith(DEV_SERVER):
        return 1
    elif not who_user:
        return who_name
    else:
        return who_user

@debugging
def roomCmp(room1, room2):
    if int(room1) > int(room2):
        return 1
    elif int(room1) < int(room2):
        return -1
    return 0

class BrowseServerWindow(wx.Panel):
    @debugging
    def __init__(self,parent):
        wx.Panel.__init__(self, parent, -1)
        self.parent = parent
        self.password_manager = open_rpg.get_component('password_manager')
        self.frame = open_rpg.get_component('frame')
        self.session = open_rpg.get_component('session')
        self.serverNameSet = 0
        self.last_motd = ""
        self.buttons = {}
        self.texts = {}
        self.svrList = []
        self.build_ctrls()
        self.build_bookmarks()
        self.refresh_server_list()
        self.refresh_room_list()

    @debugging
    def build_ctrls(self):
        ## Section Sizers (with frame edges and text captions)
        self.box_sizers = {}
        self.box_sizers["server"] = wx.StaticBox(self, -1, "Server")
        self.box_sizers["window"] = wx.StaticBox(self, -1, "Exit" )
        self.box_sizers["room"] = wx.StaticBox(self, -1, "Rooms")
        self.box_sizers["c_room"] = wx.StaticBox(self, -1, "Create Room")

        ## Layout Sizers
        self.sizers = {}
        self.sizers["main"] = wx.GridBagSizer(hgap=1, vgap=1)
        self.sizers["server"] = wx.StaticBoxSizer(self.box_sizers["server"],
                                                  wx.VERTICAL)
        self.sizers["rooms"] = wx.StaticBoxSizer(self.box_sizers["room"],
                                                 wx.VERTICAL)
        self.sizers["close"] = wx.StaticBoxSizer(self.box_sizers["window"],
                                                 wx.HORIZONTAL)
        self.sizers["c_room"] = wx.StaticBoxSizer(self.box_sizers["c_room"],
                                                  wx.VERTICAL)

        #Build Server Sizer
        adder = wx.StaticText(self, -1, "Address:")
        self.texts["address"] = wx.TextCtrl(self, wx.ID_ANY)
        servers = wx.StaticText(self, -1, "Servers:")
        self.server_list = ServerListCtrl(self, self.on_bookmark_check)
        #wx.ListCtrl(self, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.server_list.InsertColumn(0, "")
        self.server_list.InsertColumn(1, "Players", wx.LIST_FORMAT_LEFT, 0)
        self.server_list.InsertColumn(2, "Name", wx.LIST_FORMAT_LEFT, 0)
        self.buttons['gs_connect'] = wx.Button(self, wx.ID_ANY, "Connect")
        self.buttons['gs_refresh'] = wx.Button(self, wx.ID_ANY, "Refresh")
        self.buttons['gs_disconnect'] = wx.Button(self, wx.ID_ANY, "Disconnect")
        self.sizers["svrbtns"] = wx.BoxSizer(wx.HORIZONTAL)
        self.sizers["svrbtns"].Add(self.buttons['gs_connect'], 0, wx.EXPAND)
        self.sizers["svrbtns"].Add(self.buttons['gs_refresh'], 0, wx.EXPAND)
        self.sizers["svrbtns"].Add(self.buttons['gs_disconnect'], 0, wx.EXPAND)
        self.sizers["server"].Add(adder, 0, wx.EXPAND)
        self.sizers["server"].Add(self.texts["address"], 0, wx.EXPAND)
        self.sizers["server"].Add(servers, 0, wx.EXPAND)
        self.sizers["server"].Add(self.server_list, 1, wx.EXPAND)
        self.sizers["server"].Add(self.sizers["svrbtns"], 0, wx.EXPAND)

        #Build Rooms Sizer
        self.room_list = wx.ListCtrl(self, wx.ID_ANY,
                                     style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.room_list.InsertColumn(0,"Game", wx.LIST_FORMAT_LEFT,0)
        self.room_list.InsertColumn(1,"Players", wx.LIST_FORMAT_LEFT,0)
        self.room_list.InsertColumn(2,"PW", wx.LIST_FORMAT_LEFT,0)
        self.buttons['gs_join_room'] = wx.Button(self, wx.ID_ANY, "Join Room")
        self.buttons['gs_join_lobby'] = wx.Button(self, wx.ID_ANY, "Lobby")
        self.sizers["roombtns"] = wx.BoxSizer(wx.HORIZONTAL)
        self.sizers["roombtns"].Add(self.buttons['gs_join_room'], 0, wx.EXPAND)
        self.sizers["roombtns"].Add(self.buttons['gs_join_lobby'], 0, wx.EXPAND)
        self.sizers["rooms"].Add(self.room_list, 1, wx.EXPAND)
        self.sizers["rooms"].Add(self.sizers["roombtns"], 0, wx.EXPAND)

        #Build Close Sizer
        self.buttons['close_openrpg'] = wx.Button(self, wx.ID_ANY,
                                                  "Exit OpenRPG")
        self.buttons['gs_close'] = wx.Button(self, wx.ID_ANY,"Close Window")
        self.sizers["close"].Add(self.buttons['close_openrpg'], 1,
                                 wx.ALIGN_CENTER_VERTICAL)
        self.sizers["close"].Add(self.buttons['gs_close'], 1,
                                 wx.ALIGN_CENTER_VERTICAL)

        #Build Create Room Sizer
        rname = wx.StaticText(self,-1, "Room Name:")
        self.texts["room_name"] = wx.TextCtrl(self, -1)
        rpass = wx.StaticText(self,-1, "Password:")
        self.buttons['gs_pwd'] = wx.CheckBox(self, wx.ID_ANY, "")
        self.texts["room_pwd"] = wx.TextCtrl(self, -1)
        self.texts["room_pwd"].Enable(0)
        pwsizer = wx.BoxSizer(wx.HORIZONTAL)
        pwsizer.Add(self.buttons['gs_pwd'],0,0)
        pwsizer.Add(self.texts["room_pwd"], 1, wx.EXPAND)
        apass = wx.StaticText(self,-1, "Admin Password:")
        self.texts["room_boot_pwd"] = wx.TextCtrl(self, -1)
        minver = wx.StaticText(self,-1, "Minimum Version:")
        self.texts["room_min_version"] = wx.TextCtrl(self, -1)
        self.sizers["c_room_layout"] = wx.FlexGridSizer(rows=8, cols=2, hgap=1,
                                                        vgap=1)
        self.sizers["c_room_layout"].Add(rname, 0,
                                         wx.ALIGN_RIGHT|
                                         wx.ALIGN_CENTER_VERTICAL|wx.ALL)
        self.sizers["c_room_layout"].Add(self.texts["room_name"], 0, wx.EXPAND)
        self.sizers["c_room_layout"].Add(rpass, 0,
                                         wx.ALIGN_RIGHT|
                                         wx.ALIGN_CENTER_VERTICAL|wx.ALL)
        self.sizers["c_room_layout"].Add(pwsizer, 0, wx.EXPAND)
        self.sizers["c_room_layout"].Add(apass, 0,
                                         wx.ALIGN_RIGHT|
                                         wx.ALIGN_CENTER_VERTICAL|wx.ALL)
        self.sizers["c_room_layout"].Add(self.texts["room_boot_pwd"], 0,
                                         wx.EXPAND)
        self.sizers["c_room_layout"].Add(minver, 0,
                                         wx.ALIGN_RIGHT|
                                         wx.ALIGN_CENTER_VERTICAL|wx.ALL)
        self.sizers["c_room_layout"].Add(self.texts["room_min_version"], 0,
                                         wx.EXPAND)
        self.sizers["c_room_layout"].AddGrowableCol(1)
        self.buttons['gs_create_room'] = wx.Button(self, wx.ID_ANY,
                                                   "Create Room")
        self.sizers["c_room"].Add(self.sizers["c_room_layout"], 1,
                                  wx.EXPAND)
        self.sizers["c_room"].Add(self.buttons['gs_create_room'], 0,
                                  wx.EXPAND)

        #Build Main Sizer
        self.sizers["main"].Add(self.sizers["server"], (0,0), span=(2,1),
                                flag=wx.EXPAND)
        self.sizers["main"].Add(self.sizers["rooms"], (0,1), flag=wx.EXPAND)
        self.sizers["main"].Add(self.sizers["close"], (2,0), flag=wx.EXPAND)
        self.sizers["main"].Add(self.sizers["c_room"], (1,1), span=(2,1),
                                flag=wx.EXPAND)
        self.sizers["main"].AddGrowableCol(0)
        self.sizers["main"].AddGrowableCol(1)
        self.sizers["main"].AddGrowableRow(0)
        self.SetSizer(self.sizers["main"])
        self.SetAutoLayout(True)
        self.Fit()

        ## Event Handlers
        self.Bind(wx.EVT_BUTTON, self.on_connect_btn,
                  self.buttons['gs_connect'])
        self.Bind(wx.EVT_BUTTON, self.on_disconnect_btn,
                  self.buttons['gs_disconnect'])
        self.Bind(wx.EVT_BUTTON, self.on_create_room_btn,
                  self.buttons['gs_create_room'])
        self.Bind(wx.EVT_BUTTON, self.on_join_room_btn,
                  self.buttons['gs_join_room'])
        self.Bind(wx.EVT_BUTTON, self.on_join_lobby_btn,
                  self.buttons['gs_join_lobby'])
        self.Bind(wx.EVT_BUTTON, self.on_refresh_btn,
                  self.buttons['gs_refresh'])
        self.Bind(wx.EVT_BUTTON, self.on_close_btn, self.buttons['gs_close'])
        self.Bind(wx.EVT_BUTTON, self.on_close_openrpg_btn,
                  self.buttons['close_openrpg'])
        self.Bind(wx.EVT_CHECKBOX, self.on_pwd_check, self.buttons['gs_pwd'])

        # Added double click handlers 5/05 -- Snowdog
        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.on_server_dbclick,
                  self.server_list)
        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.on_room_dbclick,
                  self.room_list)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_room_select,
                  self.room_list)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_server_select,
                  self.server_list)
        self.texts['address'].Bind(wx.EVT_SET_FOCUS, self.on_text)
        self.set_connected(self.session.is_connected())
        self.cur_room_index = -1
        self.cur_server_index = -1
        self.rmList = {}

    @debugging
    def build_bookmarks(self):
        gsm = self.frame.mainmenu.GetMenu(
            self.frame.mainmenu.FindMenu('Game Server'))
        self.bookmarks_menu = wx.Menu()

        validate.config_file('server_bookmarks.xml',
                             'default_server_bookmarks.xml')
        self.bookmarks = ElementTree()
        self.bookmarks.parse(dir_struct['user'] + 'server_bookmarks.xml')

        for server in self.bookmarks.findall('server'):
            item = wx.MenuItem(self.bookmarks_menu, wx.ID_ANY,
                               server.get('name'), server.get('name'))
            open_rpg.get_component('frame').Bind(wx.EVT_MENU,
                                                 self.on_bookmarks_menu, item)
            self.bookmarks_menu.AppendItem(item)

        gsm.AppendSubMenu(self.bookmarks_menu, "Bookmarks")

    @debugging
    def on_bookmarks_menu(self, evt):
        id = evt.GetId()
        menu = self.bookmarks_menu.FindItemById(id)
        for server in self.bookmarks.findall('server'):
            if server.get('name') == menu.GetLabel():
                address = server.get('address')
                self.cur_server_index = 999
                self.name = server.get('name')

                if self.session.is_connected():
                    if self.session.host_server == address :
                        #currently connected to address. Do nothing.
                        return
                    else:
                        #address differs, disconnect.
                        self.frame.kill_mplay_session()

                self.do_connect(address)
                break

    @debugging
    def on_bookmark_check(self, item, flag):
        name = self.svrList[item].get('name')
        address = self.svrList[item].get('address')
        port = self.svrList[item].get('port')

        if not flag:
            for server in self.bookmarks.findall('server'):
                if server.get('name') == name:
                    self.bookmarks_menu.Remove(
                        self.bookmarks_menu.FindItem(server.get('name')))
                    self.bookmarks.getroot().remove(server)
                    break
        else:
            server = Element('server')
            server.set('name', name)
            server.set('address', address + ':' + port)
            self.bookmarks.getroot().append(server)
            item = wx.MenuItem(self.bookmarks_menu, wx.ID_ANY, name, name)
            open_rpg.get_component('frame').Bind(wx.EVT_MENU,
                                                 self.on_bookmarks_menu, item)
            self.bookmarks_menu.AppendItem(item)

        self.save_bookmarks()

    @debugging
    def save_bookmarks(self):
        with open(dir_struct['user'] + 'server_bookmarks.xml', 'w') as f:
            self.bookmarks.write(f)

    @debugging
    def on_server_dbclick(self, evt=None):
        #make sure address is updated just in case list select wasn't done
        try:
            self.on_select(evt)
        except:
            pass
        address = self.texts["address"].GetValue()
        if self.session.is_connected():
            if self.session.host_server == address :
                #currently connected to address. Do nothing.
                return
            else:
                #address differs, disconnect.
                self.frame.kill_mplay_session()
        self.do_connect(address)

    @debugging
    def on_room_dbclick(self, evt=None):
        #make sure address is updated just in case list select wasn't done
        try:
            self.on_select(evt)
        except:
            pass
        group_id = str(self.room_list.GetItemData(self.cur_room_index))

        if self.NoGroups:
            self.NoGroups = False
            self.session.group_id = group_id
            self.on_server_dbclick()
            return

        if self.cur_room_index >= 0:
            if self.cur_room_index != 0:
                self.set_lobbybutton(1);
            else:
                self.set_lobbybutton(0);
            group = self.session.get_group_info(group_id)
            pwd = ""
            if (group[2] == "True") or (group[2] == "1"):
                pwd = self.password_manager.GetPassword("room", group_id)
            else:
                pwd = ""
            self.session.send_join_group(group_id, pwd)

    @debugging
    def on_room_select(self,evt):
        self.cur_room_index = evt.m_itemIndex

    @debugging
    def on_server_select(self,evt):
        self.cur_server_index = evt.m_itemIndex
        self.name = self.svrList[self.cur_server_index].get('name')
        address = self.svrList[self.cur_server_index].get('address')
        port = self.svrList[self.cur_server_index].get('port')
        self.texts["address"].SetValue(address + ":" + str(port))
        self.refresh_room_list()

    @debugging
    def on_text(self,evt):
        if self.cur_server_index >= 0:
            self.cur_server_index = -1
        evt.Skip()

    @debugging
    def add_room(self,data):
        i = self.room_list.GetItemCount()
        if (data[2]=="1") or (data[2]=="True"): pwd="yes"
        else: pwd="no"
        self.room_list.InsertStringItem(i,data[1])
        self.room_list.SetStringItem(i,1,data[3])
        self.room_list.SetStringItem(i,2,pwd)
        self.room_list.SetItemData(i,int(data[0]))
        self.refresh_room_list()

    @debugging
    def del_room(self, data):
        i = self.room_list.FindItemData(-1, int(data[0]))
        self.room_list.DeleteItem(i)
        self.refresh_room_list()

    @debugging
    def update_room(self,data):
        i = self.room_list.FindItemData(-1,int(data[0]))
        if data[2]=="1" : pwd="yes"
        else: pwd="no"
        self.room_list.SetStringItem(i,0,data[1])
        self.room_list.SetStringItem(i,1,data[3])
        self.room_list.SetStringItem(i,2,pwd)
        self.refresh_room_list()

    @debugging
    def set_cur_room_text(self,name): pass

    @debugging
    def set_lobbybutton(self,allow):
        self.buttons['gs_join_lobby'].Enable(allow)

    @debugging
    def set_connected(self,connected):
        self.buttons['gs_connect'].Enable(not connected)
        self.buttons['gs_disconnect'].Enable(connected)
        self.buttons['gs_join_room'].Enable(connected)
        self.buttons['gs_create_room'].Enable(connected)

        if not connected:
            self.buttons['gs_join_lobby'].Enable(connected)
            self.room_list.DeleteAllItems()
            self.set_cur_room_text("Not Connected!")
            self.cur_room_index = -1
            self.frame.status.connect_status = "Not Connected"
        else:
            self.frame.status.connect_status = self.name
            self.set_cur_room_text("Lobby")

    @debugging
    def on_connect_btn(self, evt):
            address = self.texts["address"].GetValue()
            # check to see if this is a manual entry vs. list entry.
            try:
                dummy = self.name
            except:
                self.name = str(address)
            self.do_connect(address)

    @debugging
    def on_disconnect_btn(self, evt):
        self.frame.kill_mplay_session()

    @debugging
    def on_refresh_btn(self, evt):
        self.refresh_server_list()

    @debugging
    def on_join_room_btn(self, evt):
        self.do_join_group()

    @debugging
    def on_join_lobby_btn(self, evt):
        self.do_join_lobby()

    @debugging
    def on_create_room_btn(self, evt):
        self.do_create_group()

    @debugging
    def on_pwd_check(self, evt):
        self.texts["room_pwd"].Enable(evt.Checked())

    @debugging
    def on_close_btn(self, evt):
        self.parent.OnMB_GameServerBrowseServers()

    @debugging
    def on_close_openrpg_btn(self,evt):
        dlg = wx.MessageDialog(self,"Quit OpenRPG?","OpenRPG",wx.YES_NO)
        if dlg.ShowModal() == wx.ID_YES:
            dlg.Destroy()
            self.frame.kill_mplay_session()
            self.frame.closed_confirmed()

    @debugging
    def refresh_room_list(self):
        self.room_list.DeleteAllItems()
        address = self.texts["address"].GetValue()
        try:
            cadder = self.session.host_server
        except:
            cadder = ''
        if address in self.rmList and len(self.rmList[address]) > 0 and\
           cadder != address:
            groups = self.rmList[address]
            self.NoGroups = True
        else:
            self.NoGroups = False
            groups = self.session.get_groups()


        hex = RGBHex()
        color1 = settings.get("RoomColor_Active")
        color2 = settings.get("RoomColor_Locked")
        color3 = settings.get("RoomColor_Empty")
        color4 = settings.get("RoomColor_Lobby")

        for g in groups:
            i = self.room_list.GetItemCount()
            if isinstance(g, tuple):
                og = g
                g = Element('room')
                g.set('pwd', og[2])
                g.set('id', og[0])
                g.set('name', og[1])
                g.set('num_users', og[3])

            if g.get('pwd') in ["True","1"]:
                pwd="yes"
            else:
                pwd="no"

            self.room_list.InsertStringItem(i, g.get('name'))
            self.room_list.SetStringItem(i, 1, g.get('num_users'))
            self.room_list.SetStringItem(i, 2, pwd)
            self.room_list.SetItemData(i, int(g.get('id')))

            #Do colorization
            if g.get('id') == '0':
                r,g,b = hex.rgb_tuple(color4)
                rmcolor = wx.Colour(red=r,green=g,blue=b)
            elif g.get('num_users') != '0':
                if g.get('pwd') in ["True","1"]:
                    r,g,b = hex.rgb_tuple(color2)
                    rmcolor = wx.Colour(red=r,green=g,blue=b)
                else:
                    r,g,b = hex.rgb_tuple(color1)
                    rmcolor = wx.Colour(red=r,green=g,blue=b)
            else:
                r,g,b = hex.rgb_tuple(color3)
                rmcolor = wx.Colour(red=r,green=g,blue=b)

            item = self.room_list.GetItem(i)
            color = wx.Colour(red=r,green=g,blue=b)
            item.SetTextColour(rmcolor)
            self.room_list.SetItem(item)

        if self.room_list.GetItemCount() > 0:
            self.room_list.SortItems(roomCmp)
            wx.CallAfter(self.autosizeRooms)

    @debugging
    def autosizeRooms(self):
        self.room_list.SetColumnWidth(0, wx.LIST_AUTOSIZE)
        self.room_list.SetColumnWidth(1, wx.LIST_AUTOSIZE)
        self.room_list.SetColumnWidth(2, wx.LIST_AUTOSIZE)

    @debugging
    def refresh_server_list(self):
        try:
            self.server_list.DeleteAllItems()
            hex = RGBHex()
            color1 = settings.get("RoomColor_Active")
            color2 = settings.get("RoomColor_Locked")
            color3 = settings.get("RoomColor_Empty")
            color4 = settings.get("RoomColor_Lobby")

            etree = get_server_list(["2"]);
            self.svrList = sorted(etree.findall('server'),
                                  server_instance_compare)
            for server in self.svrList:
                address = server.get('address') + ':' + server.get('port')

                i = self.server_list.GetItemCount()
                self.server_list.InsertStringItem(i, "")
                self.server_list.SetStringItem(i, 1, server.get('num_users'))
                self.server_list.SetStringItem(i, 2, server.get('name'))

                for s in self.bookmarks.findall('server'):
                    if s.get('address') == address:
                        self.server_list.SetItemImage(i, 1)

                #Setup the server color
                if server.get('name').startswith("OpenRPG DEV"):
                    r,g,b = hex.rgb_tuple(color2)
                    svrcolor = wx.Colour(red=r,green=g,blue=b)
                elif server.get('num_users') == '0':
                    r,g,b = hex.rgb_tuple(color3)
                    svrcolor = wx.Colour(red=r,green=g,blue=b)
                else:
                    r,g,b = hex.rgb_tuple(color1)
                    svrcolor = wx.Colour(red=r,green=g,blue=b)

                item = self.server_list.GetItem(i)
                item.SetTextColour(svrcolor)
                self.server_list.SetItem(item)

                #Add the server rooms to the rmList
                self.rmList[address] = server.findall('room')

            # No server is currently selected!!!  Versus the broken and random 0!
            self.cur_server_index = -1
            self.server_list.SetColumnWidth(0, wx.LIST_AUTOSIZE)
            self.server_list.SetColumnWidth(1, wx.LIST_AUTOSIZE)
            self.server_list.SetColumnWidth(2, wx.LIST_AUTOSIZE)

            if self.serverNameSet == 0:
                self.texts["address"].SetValue("127.0.0.1:6774")
                self.serverNameSet = 1
            else:
                pass

        except Exception, e:
            logger.exception(traceback.print_exc())
            logger.general("Server List not available.")

    @debugging
    def failed_connection(self): pass

    @debugging
    def do_connect(self, address):
        chat = open_rpg.get_component('chat')
        chat.InfoPost("Locating server at " + address + "...")
        if self.session.connect(address):
            self.frame.start_timer()
        else:
            chat.SystemPost("Failed to connect to game server...")
            self.failed_connection()

    @debugging
    def do_join_lobby(self):
        self.cur_room_index = 0
        self.session.send_join_group("0","")
        self.set_lobbybutton(0);

    @debugging
    def do_join_group(self):
        if self.cur_room_index >= 0:
            if self.cur_room_index != 0:
                self.set_lobbybutton(1);
            else:
                self.set_lobbybutton(0);
            group_id = str(self.room_list.GetItemData(self.cur_room_index))
            group = self.session.get_group_info(group_id)
            pwd = ""
            if (group[2] == "True") or (group[2] == "1"):
                pwd = self.password_manager.GetPassword("room", group_id)
            else:
                pwd = ""
            if pwd != None:
                self.session.send_join_group(group_id, pwd)

    @debugging
    def do_create_group(self):
        name = self.texts["room_name"].GetValue()
        boot_pwd = self.texts["room_boot_pwd"].GetValue()
        minversion = self.texts["room_min_version"].GetValue()
        name = name.replace('"', '&quot;').replace("'", '&#39;')
        strip_html(strip_unicode(name))

        if self.buttons['gs_pwd'].GetValue():
            pwd = self.texts["room_pwd"].GetValue()
        else:
            pwd = ""
        if name == "":
            wx.MessageBox("Invalid Name","Error");
        else:
            msg = "%s is creating room \'%s.\'" % (self.session.name, name)
            self.session.send( msg )
            self.session.send_create_group(name,pwd,boot_pwd,minversion)
            self.set_lobbybutton(1) #enable the Lobby quickbutton

    @debugging
    def on_size(self,evt):
        evt.Skip()

    @debugging
    def colorize_group_list(self, groups): pass


class ServerListCtrl(wx.ListCtrl, ListCtrlAutoWidthMixin, CheckListCtrlMixin):
    def __init__(self, parent, check_callback):
        wx.ListCtrl.__init__(self, parent, wx.ID_ANY,
                                        style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        ListCtrlAutoWidthMixin.__init__(self)
        CheckListCtrlMixin.__init__(self)
        self._callback = check_callback

    def OnCheckItem(self, index, flag):
        self._callback(index, flag)

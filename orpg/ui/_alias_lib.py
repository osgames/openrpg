from __future__ import with_statement

import operator

import wx
from wx.lib.newevent import NewEvent

from orpg.orpgCore import open_rpg
from orpg.ui.util.misc import create_masked_button
from orpg.ui.util.dlg import MultiCheckBoxDlg
from orpg.ui.toolbars import TextFormatToolBar
from orpg.tools.rgbhex import RGBHex
from orpg.tools.orpg_log import logger
from orpg.dirpath import dir_struct
from orpg.tools.validate import validate
from orpg.tools.settings import settings
from orpg.tools.decorators import debugging
from orpg.tools.pubsub import Publisher
from orpg.external.etree.ElementTree import ElementTree, Element
from orpg.external.etree.ElementTree import fromstring, tostring


class AliasLib(wx.Panel):
    _filename = settings.get('aliasfile') + '.alias'
    _alist_list = []
    _filter_list = []

    @debugging
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.ID_ANY)

        validate.config_file(self._filename, "default_alias.alias")

        self.Freeze()
        self._sizer = wx.BoxSizer(wx.VERTICAL)
        self._build_gui()
        self._on_file_open(None, False)
        self.SetSizer(self._sizer)
        self.SetAutoLayout(True)
        self.Fit()
        self.Thaw()

    def refresh_aliases(self):
        self._trigger_alias_list_changed()
        self._trigger_filter_list_changed()

    #Private Methods
    def _build_gui(self):
        rightwnd = wx.SplitterWindow(self, wx.ID_ANY,
                        style=wx.SP_LIVE_UPDATE|wx.SP_NO_XP_THEME|wx.SP_3DSASH)
        leftwnd = wx.SplitterWindow(rightwnd, wx.ID_ANY,
                        style=wx.SP_LIVE_UPDATE|wx.SP_NO_XP_THEME|wx.SP_3DSASH)

        self._select_alias_wnd = wx.ListCtrl(leftwnd, wx.ID_ANY,
                            style=wx.LC_SINGLE_SEL|wx.LC_REPORT|wx.LC_HRULES)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self._on_alias_select,
                  self._select_alias_wnd)
        self.Bind(wx.EVT_LIST_ITEM_DESELECTED, self._on_alias_deselect,
                  self._select_alias_wnd)
        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self._on_alias_edit,
                  self._select_alias_wnd)

        self._select_filter_wnd = wx.ListCtrl(leftwnd, wx.ID_ANY,
                            style=wx.LC_SINGLE_SEL|wx.LC_REPORT|wx.LC_HRULES)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self._on_filter_select,
                  self._select_filter_wnd)
        self.Bind(wx.EVT_LIST_ITEM_DESELECTED, self._on_filter_deselect,
                  self._select_filter_wnd)
        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self._on_filter_edit,
                  self._select_filter_wnd)

        self._text_wnd = wx.TextCtrl(rightwnd, wx.ID_ANY,
                                   style=wx.TE_MULTILINE|wx.TE_BESTWRAP)

        leftwnd.SplitHorizontally(self._select_alias_wnd,
                                  self._select_filter_wnd)
        rightwnd.SplitVertically(leftwnd, self._text_wnd)

        #Create top buttons
        sizer = wx.BoxSizer(wx.HORIZONTAL)

        btn = create_masked_button(self, dir_struct["icon"] + 'book.gif',
                                   'New Alias Lib', wx.ID_ANY, '#C0C0C0')
        self.Bind(wx.EVT_BUTTON, self._on_file_new, btn)
        sizer.Add(btn, 0, wx.EXPAND)

        btn = create_masked_button(self, dir_struct["icon"] + 'open.gif',
                                   'Open an existing Alias Lib', wx.ID_ANY,
                                   '#C0C0C0')
        self.Bind(wx.EVT_BUTTON, self._on_file_open, btn)
        sizer.Add(btn, 0, wx.EXPAND)

        btn = create_masked_button(self, dir_struct["icon"] + 'save.gif',
                                   'Save Alias Lib', wx.ID_ANY, '#C0C0C0')
        self.Bind(wx.EVT_BUTTON, self._on_file_save, btn)
        sizer.Add(btn, 0, wx.EXPAND)
        sizer.Add(wx.StaticLine(self, wx.ID_ANY, style=wx.LI_VERTICAL), 0,
                  wx.EXPAND)

        btn = create_masked_button(self, dir_struct["icon"] + 'player.gif',
                                   'Add a new Alias', wx.ID_ANY)
        self.Bind(wx.EVT_BUTTON, self._on_alias_new, btn)
        sizer.Add(btn, 0, wx.EXPAND)

        btn = create_masked_button(self, dir_struct["icon"] + 'note.gif',
                                   'Edit selected Alias', wx.ID_ANY)
        self.Bind(wx.EVT_BUTTON, self._on_alias_edit, btn)
        sizer.Add(btn, 0, wx.EXPAND)

        btn = create_masked_button(self, dir_struct["icon"] + 'noplayer.gif',
                                   'Delete selected Alias', wx.ID_ANY)
        self.Bind(wx.EVT_BUTTON, self._on_alias_delete, btn)
        sizer.Add(btn, 0, wx.EXPAND)
        sizer.Add(wx.StaticLine(self, wx.ID_ANY, style=wx.LI_VERTICAL), 0,
                  wx.EXPAND)

        btn = create_masked_button(self, dir_struct["icon"] + 'add_filter.gif',
                                   'Add a new Filter', wx.ID_ANY, '#0000FF')
        self.Bind(wx.EVT_BUTTON, self._on_filter_new, btn)
        sizer.Add(btn, 0, wx.EXPAND)

        btn = create_masked_button(self, dir_struct["icon"] + 'edit_filter.gif',
                                   'Edit selected Filter', wx.ID_ANY,
                                   '#FF0000')
        self.Bind(wx.EVT_BUTTON, self._on_filter_edit, btn)
        sizer.Add(btn, 0, wx.EXPAND)

        btn = create_masked_button(self,
                                   dir_struct["icon"] + 'delete_filter.gif',
                                   'Delete selected Filter', wx.ID_ANY,
                                   '#0000FF')
        self.Bind(wx.EVT_BUTTON, self._on_filter_delete, btn)
        sizer.Add(btn, 0, wx.EXPAND)
        sizer.Add(wx.StaticLine(self, wx.ID_ANY, style=wx.LI_VERTICAL), 0,
                  wx.EXPAND)

        sizer.Add(TextFormatToolBar(self, self._text_wnd), 0, wx.EXPAND)

        #Add the Top and middle sections to the main sizer
        self._sizer.Add(sizer, 0, wx.EXPAND)
        self._sizer.Add(rightwnd, 1, wx.EXPAND)

        #Build the check boxes
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        self._auto_clear_text = wx.CheckBox(self, wx.ID_ANY, "Auto Clear Text")
        sizer.Add(self._auto_clear_text, 0, wx.EXPAND)

        #Add check boxes to the main sizer
        self._sizer.Add(sizer, 0, wx.EXPAND)

        #Build the Bottom Buttons
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        btn = wx.Button(self, wx.ID_ANY, "Say")
        self.Bind(wx.EVT_BUTTON, self._on_transmit_send, btn)
        sizer.Add(btn, 0, wx.EXPAND)

        btn = wx.Button(self, wx.ID_ANY, "Emote")
        self.Bind(wx.EVT_BUTTON, self._on_transmit_emote, btn)
        sizer.Add(btn, 0, wx.EXPAND)

        btn = wx.Button(self, wx.ID_ANY, "Whisper")
        self.Bind(wx.EVT_BUTTON, self._on_transmit_whisper, btn)
        sizer.Add(btn, 0, wx.EXPAND)

        #Add the final section to the main sizer
        self._sizer.Add(sizer, 0, wx.EXPAND)

    def _set_alias_list_wnd(self):
        self._select_alias_wnd.ClearAll()
        self._select_alias_wnd.InsertColumn(0, "Alias")
        self._select_alias_wnd.InsertColumn(1, "Chat Color")
        for name, color in self._alias_list:
            i = self._select_alias_wnd.InsertStringItem(
                self._select_alias_wnd.GetItemCount(), name)
            self._select_alias_wnd.SetStringItem(i, 1, color)
            if color != 'Default':
                self._select_alias_wnd.SetItemTextColour(i, color)
            self._select_alias_wnd.RefreshItem(i)

    def _set_filter_list_wnd(self):
        self._select_filter_wnd.ClearAll()
        self._select_filter_wnd.InsertColumn(0, "Filter")
        self._select_filter_wnd.InsertColumn(1, "# Rules")
        for name, rules in self._filter_list:
            i = self._select_filter_wnd.InsertStringItem(
                self._select_filter_wnd.GetItemCount(), name)
            self._select_filter_wnd.SetStringItem(i, 1, str(len(rules)))
            self._select_filter_wnd.RefreshItem(i)


    #Events
    def _on_file_new(self, evt):
        dlg = wx.TextEntryDialog(self, "Please Name This Alias Lib",
                                 "New Alias Lib")
        try:
            if dlg.ShowModal() == wx.ID_OK:
                new_filename = dlg.GetValue() + '.alias'
            else:
                return
        finally:
            dlg.Destroy()

        if self._filename == new_filename:
            return

        self._on_file_save(None)

        self._filename = new_filename
        self._alias_list = []
        self._filter_list = []

        self._on_file_save(None)
        settings.set('aliasfile', self._filename[:-6])
        self._set_alias_list_wnd()
        self._set_filter_list_wnd()

        self._trigger_alias_list_changed()
        self._trigger_filter_list_changed()

    def _on_file_open(self, evt, do_dlg=True):
        if do_dlg:
            dlg = wx.FileDialog(self, "Select an Alias Lib to Open",
                                dir_struct["user"], wildcard="*.alias",
                                style=wx.HIDE_READONLY|wx.OPEN)
            try:
                if dlg.ShowModal() == wx.ID_OK:
                    new_filename = dlg.GetFilename()
                else:
                    return
            finally:
                dlg.Destroy()

            if self._filename == new_filename:
                return

            self._on_file_save(None)

            self._filename = new_filename
            settings.set('aliasfile', self._filename[:-6])

        xml = ElementTree().parse(dir_struct["user"] + self._filename)
        self.alias = None
        self.filter = None
        self._alias_list = []
        for alias in xml.findall('alias'):
            if alias.get('color'):
                color = alias.get('color')
            else:
                color = 'Default'
            aname = alias.get("name", '')
            self._alias_list.append([aname, color])
        self._alias_list.sort(key=operator.itemgetter(0))
        self._set_alias_list_wnd()

        self._filter_list = []
        for filter in xml.findall('filter'):
            sub = []
            for rule in filter.findall("rule"):
                sub.append([rule.get("match", ''), rule.get("sub", '')])

            self._filter_list.append([filter.get("name"), sub])
        self._set_filter_list_wnd()

        self._trigger_alias_list_changed()
        self._trigger_filter_list_changed()

    def _on_file_save(self, evt):
        root = Element('aliaslib')
        for name, color in self._alias_list:
            alias = Element('alias')
            alias.set('color', color)
            alias.set('name', name)
            root.append(alias)

        for name, rules in self._filter_list:
            filter = Element('filter')
            filter.set('name', name)
            for r in rules:
                rule = Element('rule')
                rule.set('match', r[0])
                rule.set('sub', r[1])
                filter.append(rule)
            root.append(filter)

        with open(dir_struct["user"] + self._filename, "w") as f:
            f.write(tostring(root))

    def _on_alias_select(self, evt):
        alias = self._select_alias_wnd.GetItem(evt.GetIndex(),
                                                     0).GetText()
        Publisher.sendMessage('aliaslib.set_alias', alias)

    def _on_alias_deselect(self, evt):
        Publisher.sendMessage('aliaslib.set_alias', None)

    def _on_alias_new(self, evt):
        with AliasEditDlg('New') as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                alias = dlg.Result()
            else:
                return

        self._alias_list.append(alias)
        self._set_alias_list_wnd()
        self._trigger_alias_list_changed()

    def _on_alias_edit(self, evt):
        idx = self._select_alias_wnd.GetNextItem(-1, wx.LIST_NEXT_ALL,
                                                      wx.LIST_STATE_SELECTED)

        if idx == -1:
            return

        alias = self._alias_list[idx]

        with AliasEditDlg('Edit', alias) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                alias = dlg.Result()
            else:
                return

        self._alias_list[idx] = alias
        self._set_alias_list_wnd()
        self._trigger_alias_list_changed()

    def _on_alias_delete(self, evt):
        idx = self._select_alias_wnd.GetNextItem(-1, wx.LIST_NEXT_ALL,
                                                      wx.LIST_STATE_SELECTED)
        if idx == -1:
            return

        alias = self._alias_list[idx]
        self._alias_list.remove(alias)
        self._set_alias_list_wnd()

        self._trigger_alias_list_changed()

    def _on_filter_select(self, evt):
        filter = self._select_filter_wnd.GetItem(evt.GetIndex(),
                                                     0).GetText()
        Publisher.sendMessage('aliaslib.set_filter', filter)

    def _on_filter_deselect(self, evt):
        Publisher.sendMessage('aliaslib.set_filter', None)

    def _on_filter_new(self, evt):
        dlg = wx.TextEntryDialog(self, 'Filter Name: ',
                                 'Please name this filter')
        try:
            if dlg.ShowModal() != wx.ID_OK:
                return
            filterName = dlg.GetValue()
        finally:
            dlg.Destroy()

        i = self._select_filter_wnd.InsertStringItem(
            self._select_filter_wnd.GetItemCount(),
            filterName)
        self._select_filter_wnd.SetItemState(i, wx.LIST_STATE_SELECTED,
                                           wx.LIST_STATE_SELECTED)
        self._filter_list.append([filterName, []])
        self._on_filter_edit(None)

    def _on_filter_edit(self, evt):
        idx = self._select_filter_wnd.GetNextItem(-1, wx.LIST_NEXT_ALL,
                                                      wx.LIST_STATE_SELECTED)

        if idx == -1:
            return

        filter = self._filter_list[idx]

        with FilterEditDlg(filter) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                rules = dlg.Result()
            else:
                return

        self._filter_list[idx][1] = rules
        self._set_filter_list_wnd()
        self._trigger_filter_list_changed()

    def _on_filter_delete(self, evt):
        idx = self._select_filter_wnd.GetNextItem(-1, wx.LIST_NEXT_ALL,
                                                      wx.LIST_STATE_SELECTED)
        if idx == -1:
            return

        filter = self._filter_list[idx]
        self._filter_list.remove(filter)
        self._set_filter_list_wnd()
        self._trigger_filter_list_changed()

    def _on_transmit_send(self, evt):
        chat = open_rpg.get('chat')
        chat.submit_chat_text(self._text_wnd.Value)
        if self._auto_clear_text.IsChecked():
            self._text_wnd.Value = ''

    def _on_transmit_emote(self, evt):
        chat = open_rpg.get('chat')
        if not self._text_wnd.Value.startswith('/me '):
            self._text_wnd.Value = '/me ' + self._text_wnd.Value
        chat.submit_chat_text(self._text_wnd.Value)
        if self._auto_clear_text.IsChecked():
            self._text_wnd.Value = ''

    def _on_transmit_whisper(self, evt):
        session = open_rpg.get('session')
        chat = open_rpg.get('chat')
        players = session.get_players()
        opts = []
        myid = session.get_id()
        for p in players:
            if p[2] != myid:
                opts.append("(" + p[2] + ") " + chat.html_strip(p[0]))
        with MultiCheckBoxDlg(self, opts, "Select Players:", "Whisper To", []) as dlg:
            sendto = []
            if dlg.ShowModal() == wx.ID_OK:
                selections = dlg.get_selections()
                for s in selections:
                    sendto.append(players[s][2])
            else:
                return

        self._text_wnd.Value = '/w ' + ','.join(sendto) + '=' + self._text_wnd.Value
        chat.submit_chat_text(self._text_wnd.Value)
        if self._auto_clear_text.IsChecked():
            self._text_wnd.Value = ''

    def _trigger_alias_list_changed(self):
        Publisher.sendMessage('aliaslib.alias_list', self._alias_list)

    def _trigger_filter_list_changed(self):
        Publisher.sendMessage('aliaslib.filter_list', self._filter_list)

class AliasEditDlg(wx.Dialog):
    _color = 'Default'

    def __init__(self, type_, alias=None):
        wx.Dialog.__init__(self, None, wx.ID_ANY, type_ + " Alias",
                  style=wx.DEFAULT_DIALOG_STYLE|wx.STAY_ON_TOP)
        self.txt = wx.TextCtrl(self, wx.ID_ANY)
        if type_ == 'Edit':
            self.txt.SetValue(alias[0])

        self.colorbtn = wx.Button(self, wx.ID_ANY, "Default Color")
        self.Bind(wx.EVT_BUTTON, self._on_change_color, self.colorbtn)

        if alias and alias[1] != 'Default':
            self.colorbtn.SetLabel("Chat Color")
            self.colorbtn.SetForegroundColour(alias[1])

        okbtn = wx.Button(self, wx.ID_OK)
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(wx.StaticText(self, wx.ID_ANY, "Alias: "), 0, wx.EXPAND)
        sizer.Add(self.txt, 1, wx.EXPAND)
        sizer.Add(self.colorbtn, 0, wx.EXPAND)
        sizer.Add(okbtn, 0, wx.EXPAND)
        self.SetSizer(sizer)
        self.SetAutoLayout(True)
        self.Fit()

    def Result(self):
        return self.txt.Value, self._color

    def _on_change_color(self, evt):
        hexcolor = RGBHex().do_hex_color_dlg(self)
        self.colorbtn.SetLabel("Chat Color")
        self.colorbtn.SetForegroundColour(hexcolor)
        self._color = hexcolor

    def __enter__(self, *args, **kwargs):
        return self
    def __exit__(self, *args, **kwargs):
        self.Destroy()

class RuleEditDlg(wx.Dialog):
    def __init__(self, type_, rule=None):
        wx.Dialog.__init__(self, None, wx.ID_ANY, type_ + " Filter Rule",
                  style=wx.DEFAULT_DIALOG_STYLE|wx.STAY_ON_TOP)

        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(wx.StaticText(self, wx.ID_ANY, 'Replace: '), 0, wx.EXPAND)
        self.match = wx.TextCtrl(self, wx.ID_ANY)
        sizer.Add(self.match, 0, wx.EXPAND)
        sizer.Add(wx.StaticText(self, wx.ID_ANY, 'With: '), 0, wx.EXPAND)
        self.sub = wx.TextCtrl(self, wx.ID_ANY)
        sizer.Add(self.sub, 0, wx.EXPAND)
        sizer.Add(wx.Button(self, wx.ID_OK, 'Ok'), 0, wx.EXPAND)
        sizer.Add(wx.Button(self, wx.ID_CANCEL, 'Cancel'), 0, wx.EXPAND)

        self.SetSizer(sizer)
        self.SetAutoLayout(True)
        self.Fit()

        if rule:
            self.match.Value = rule[0]
            self.sub.Value = rule[1]

    def Result(self):
        return self.match.Value, self.sub.Value

    def __enter__(self, *args, **kwargs):
        return self
    def __exit__(self, *args, **kwargs):
        self.Destroy()

class FilterEditDlg(wx.Dialog):
    _rules = []
    def __init__(self, filter):
        wx.Dialog.__init__(self, None, wx.ID_ANY, "Edit Filter: " + filter[0],
                  style=wx.DEFAULT_DIALOG_STYLE|wx.STAY_ON_TOP)

        self._rules = filter[1]

        self.Freeze()
        self._build_gui()
        self._populate_rules()
        self.Thaw()

    def Result(self):
        return self._rules

    def _build_gui(self):

        bsizer = wx.BoxSizer(wx.VERTICAL)
        self.panel = wx.Panel(self, wx.ID_ANY)
        bsizer.Add(self.panel, 1, wx.EXPAND)
        self.SetSizer(bsizer)
        self.SetAutoLayout(True)

        self.grid = wx.ListCtrl(self.panel, wx.ID_ANY,
                            style=wx.LC_SINGLE_SEL|wx.LC_REPORT|wx.LC_HRULES)
        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self._on_rule_edit, self.grid)

        btsizer = wx.BoxSizer(wx.VERTICAL)

        btn = wx.Button(self.panel, wx.ID_ANY, 'Add')
        self.Bind(wx.EVT_BUTTON, self._on_rule_add, btn)
        btsizer.Add(btn, 0, wx.EXPAND)

        btn = wx.Button(self.panel, wx.ID_ANY, 'Edit')
        self.Bind(wx.EVT_BUTTON, self._on_rule_edit, btn)
        btsizer.Add(btn, 0, wx.EXPAND)

        btn = wx.Button(self.panel, wx.ID_ANY, 'Delete')
        self.Bind(wx.EVT_BUTTON, self._on_rule_delete, btn)
        btsizer.Add(btn, 0, wx.EXPAND)

        btn = wx.Button(self.panel, wx.ID_OK, 'Done')
        btsizer.Add(btn, 0, wx.EXPAND)

        sizer = wx.GridBagSizer(5,5)

        sizer.Add(self.grid, (0,0), flag=wx.EXPAND)
        sizer.Add(btsizer, (0,1), flag=wx.EXPAND)

        sizer.AddGrowableCol(0)
        sizer.AddGrowableRow(0)
        sizer.SetEmptyCellSize((0,0))

        self.panel.SetSizer(sizer)
        self.panel.SetAutoLayout(True)

    def _populate_rules(self):
        self.grid.ClearAll()
        self.grid.InsertColumn(0, "Replace")
        self.grid.InsertColumn(1, "With")
        for match, sub in self._rules:
            i = self.grid.InsertStringItem(self.grid.GetItemCount(), match)
            self.grid.SetStringItem(i, 1, sub)

        self.grid.SetColumnWidth(0, wx.LIST_AUTOSIZE)
        self.grid.SetColumnWidth(1, wx.LIST_AUTOSIZE)

    def _on_rule_add(self, evt):
        with RuleEditDlg('New') as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                rule = dlg.Result()
            else:
                return

        self._rules.append(rule)
        self._populate_rules()
        pass

    def _on_rule_edit(self, evt):
        idx = self.grid.GetNextItem(-1, wx.LIST_NEXT_ALL,
                                    wx.LIST_STATE_SELECTED)
        if idx == -1:
            return

        rule = self._rules[idx]
        with RuleEditDlg('Edit', rule) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                rule = dlg.Result()
            else:
                return

        self._rules[idx] = rule
        self._populate_rules()

    def _on_rule_delete(self, evt):
        idx = self.grid.GetNextItem(-1, wx.LIST_NEXT_ALL,
                                    wx.LIST_STATE_SELECTED)
        if idx == -1:
            return

        rule = self._rules[idx]
        self._rules.remove(rules)
        self._populate_rules()

    def __enter__(self, *args, **kwargs):
        return self
    def __exit__(self, *args, **kwargs):
        self.Destroy()

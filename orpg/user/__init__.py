import os, os.path

from orpg.dirpath import dir_struct

from pkgutil import extend_path
__path__ = extend_path([os.path.abspath(dir_struct['user'])], __name__)
print __path__
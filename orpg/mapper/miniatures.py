# Copyright (C) 2000-2001 The OpenRPG Project
#
#    openrpg-dev@lists.sourceforge.net
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# --
#
# File: mapper/miniatures.py
# Author: Chris Davis
# Maintainer:
# Version:
#   $Id: miniatures.py,v 1.46 2007/12/07 20:39:50 digitalxero Exp $
#
# Description: This file contains some of the basic definitions for the chat
# utilities in the orpg project.
#
from __future__ import with_statement

import thread
import time
import urllib
import os.path
import mimetypes

from orpg.mapper.base import *

from orpg.dirpath import dir_struct
from orpg.tools.orpg_log import logger
from orpg.tools.settings import settings

MIN_STICKY_BACK = -0XFFFFFF
MIN_STICKY_FRONT = 0xFFFFFF

##----------------------------------------
##  miniature object
##----------------------------------------

FACE_NONE = 0
FACE_NORTH = 1
FACE_NORTHEAST = 2
FACE_EAST = 3
FACE_SOUTHEAST = 4
FACE_SOUTH = 5
FACE_SOUTHWEST = 6
FACE_WEST = 7
FACE_NORTHWEST = 8
SNAPTO_ALIGN_CENTER = 0
SNAPTO_ALIGN_TL = 1

def cmp_zorder(first,second):
    f = first.zorder
    s = second.zorder
    if f == None:
        f = 0
    if s == None:
        s = 0
    if f == s:
        value = 0
    elif f < s:
        value = -1
    else:
        value = 1
    return value

mini_draw_callback_list = []
mini_additional_attribute_list = []

def register_mini_draw_callback_function(func):
    if func in mini_draw_callback_list:
        mini_draw_callback_list.remove(func)
    else:
        mini_draw_callback_list.append(func)

def handle_mini_draw_callback_functions(mini, dc):
    for func in mini_draw_callback_list:
        func(mini, dc)

##def assign_xml_attributes_to_object(obj, xml, attribute_map):
##    for attribute, attribute_type in attribute_map.items():
##        if attribute in xml.attrib:
##            value = xml.get(attribute)
##            if attribute_type == 'int':
##                value = int(xml.get(attribute))
##            elif attribute_type == 'bool':
##                if value == '1' or value == 'True':
##                    value = True
##                else:
##                    value = False
##            elif attribute_type == 'url':
##                value = urllib.unquote(value)
##            obj.__setattr__(attribute, value)
##
##def assign_obj_attributes_to_xml(obj, xml, attribute_map):
##    for attribute, attribute_type in attribute_map.items():
##        if attribute in obj.__dict__ and obj.__dict__[attribute] is not None:
##            if attribute_type == 'bool':
##                if obj.__dict__[attribute]:
##                    value = '1'
##                else:
##                    value = '0'
##            elif attribute_type == 'url':
##                value = urllib.quote(obj.__dict__[attribute]).replace('%3A', ':')
##            else:
##                value = str(obj.__dict__[attribute])
##            xml.set(attribute, value)
            
class BmpMiniature:
    def __init__(self, id, path, image, pos=cmpPoint(0,0), heading=FACE_NONE,
                 face=FACE_NONE, label="", locked=False, hide=False,
                 snap_to_align=SNAPTO_ALIGN_CENTER, zorder=0, width=0,
                 height=0, local=False, localPath='', localTime=-1):
        self.heading = heading
        self.face = face
        self.label = label
        self.path = path
        self.pos = pos
        self.selected = False
        self.locked = locked
        self.snap_to_align = snap_to_align
        self.hide = hide
        self.id = id
        self.zorder = zorder
        self.left = 0
        self.local = local
        self.localPath = localPath
        self.localTime = localTime
        if not width:
            self.width = 0
        else:
            self.width = width
        if not height:
            self.height = 0
        else:
            self.height = height
        self.right = image.GetWidth()
        self.top = 0
        self.bottom = image.GetHeight()
        self.isUpdated = False
        self.gray = False

        self.set_image(image)

    def set_image(self, image):
        self.image = image
        self.image.ConvertAlphaToMask()
        self.generate_bmps();

    def generate_bmps(self):
        if self.width:
            image = self.image.Copy()
            image.Rescale(int(self.width), int(self.height))
        else:
            image = self.image
        self.bmp = image.ConvertToBitmap()
        self.bmp_gray = image.ConvertToGreyscale().ConvertToBitmap()

    def set_min_props(self, heading=FACE_NONE, face=FACE_NONE, label="",
                      locked=False, hide=False, width=0, height=0):
        self.heading = heading
        self.face = face
        self.label = label
        if locked:
            self.locked = True
        else:
            self.locked = False
        if hide:
            self.hide = True
        else:
            self.hide = False
        self.width = int(width)
        self.height = int(height)
        self.isUpdated = True
        self.generate_bmps()

    def hit_test(self, pt):
        rect = self.get_rect()
        result = None
        result = rect.InsideXY(pt.x, pt.y)

        return result

    def get_rect(self):
        ret = wx.Rect(self.pos.x, self.pos.y, self.bmp.GetWidth(),
                      self.bmp.GetHeight())

        return ret

    def draw(self, dc, mini_layer, op=wx.COPY):
        # check if hidden and GM: we outline the mini in grey (little bit smaller than the actual size)
        # and write the label in the center of the mini
        if self.hide and mini_layer.canvas.frame.session.my_role() == mini_layer.canvas.frame.session.ROLE_GM:
            self.left = 0
            self.right = self.bmp.GetWidth()
            self.top = 0
            self.bottom = self.bmp.GetHeight()
            # grey outline
            graypen = wx.Pen("gray", 1, wx.DOT)
            dc.SetPen(graypen)
            dc.SetBrush(wx.TRANSPARENT_BRUSH)
            if self.bmp.GetWidth() <= 20:
                xoffset = 1
            else:
                xoffset = 5
            if self.bmp.GetHeight() <= 20:
                yoffset = 1
            else:
                yoffset = 5
            dc.DrawRectangle(self.pos.x + xoffset, self.pos.y + yoffset, self.bmp.GetWidth() - (xoffset * 2), self.bmp.GetHeight() - (yoffset * 2))
            dc.SetBrush(wx.NullBrush)
            dc.SetPen(wx.NullPen)

            ## draw label in the center of the mini
            label = mini_layer.get_mini_label(self)
            if len(label):
                dc.SetTextForeground(wx.RED)
                (textWidth,textHeight) = dc.GetTextExtent(label)
                x = self.pos.x +((self.bmp.GetWidth() - textWidth) /2) - 1
                y = self.pos.y + (self.bmp.GetHeight() / 2)
                dc.SetPen(wx.GREY_PEN)
                dc.SetBrush(wx.LIGHT_GREY_BRUSH)
                dc.DrawRectangle(x, y, textWidth+2, textHeight+2)
                if (textWidth+2 > self.right):
                    self.right += int((textWidth+2-self.right)/2)+1
                    self.left -= int((textWidth+2-self.right)/2)+1
                self.bottom = y+textHeight+2-self.pos.y
                dc.SetPen(wx.NullPen)
                dc.SetBrush(wx.NullBrush)
                dc.DrawText(label, x+1, y+1)

            #selected outline
            if self.selected:
                dc.SetPen(wx.RED_PEN)
                dc.SetBrush(wx.TRANSPARENT_BRUSH)
                dc.DrawRectangle(self.pos.x, self.pos.y, self.bmp.GetWidth(), self.bmp.GetHeight())
                dc.SetBrush(wx.NullBrush)
                dc.SetPen(wx.NullPen)
            return True

        elif not self.hide:
            # set the width and height of the image
            if self.gray:
                bmp = self.bmp_gray
            else:
                bmp = self.bmp
            dc.DrawBitmap(bmp, self.pos.x, self.pos.y, True)
            self.left = 0
            self.right = self.bmp.GetWidth()
            self.top = 0
            self.bottom = self.bmp.GetHeight()

            # Draw the facing marker if needed
            if self.face != 0:
                x_mid = self.pos.x + (self.bmp.GetWidth()/2)
                x_right = self.pos.x + self.bmp.GetWidth()
                y_mid = self.pos.y + (self.bmp.GetHeight()/2)
                y_bottom = self.pos.y + self.bmp.GetHeight()

                dc.SetPen(wx.WHITE_PEN)
                dc.SetBrush(wx.RED_BRUSH)
                triangle = []

                # Figure out which direction to draw the marker!!
                if self.face == FACE_WEST:
                    triangle.append(cmpPoint(self.pos.x,self.pos.y))
                    triangle.append(cmpPoint(self.pos.x - 5, y_mid))
                    triangle.append(cmpPoint(self.pos.x, y_bottom))
                elif self.face ==  FACE_EAST:
                    triangle.append(cmpPoint(x_right, self.pos.y))
                    triangle.append(cmpPoint(x_right + 5, y_mid))
                    triangle.append(cmpPoint(x_right, y_bottom))
                elif self.face ==  FACE_SOUTH:
                    triangle.append(cmpPoint(self.pos.x, y_bottom))
                    triangle.append(cmpPoint(x_mid, y_bottom + 5))
                    triangle.append(cmpPoint(x_right, y_bottom))
                elif self.face ==  FACE_NORTH:
                    triangle.append(cmpPoint(self.pos.x, self.pos.y))
                    triangle.append(cmpPoint(x_mid, self.pos.y - 5))
                    triangle.append(cmpPoint(x_right, self.pos.y))
                elif self.face == FACE_NORTHEAST:
                    triangle.append(cmpPoint(x_mid, self.pos.y))
                    triangle.append(cmpPoint(x_right + 5, self.pos.y - 5))
                    triangle.append(cmpPoint(x_right, y_mid))
                    triangle.append(cmpPoint(x_right, self.pos.y))
                elif self.face == FACE_SOUTHEAST:
                    triangle.append(cmpPoint(x_right, y_mid))
                    triangle.append(cmpPoint(x_right + 5, y_bottom + 5))
                    triangle.append(cmpPoint(x_mid, y_bottom))
                    triangle.append(cmpPoint(x_right, y_bottom))
                elif self.face == FACE_SOUTHWEST:
                    triangle.append(cmpPoint(x_mid, y_bottom))
                    triangle.append(cmpPoint(self.pos.x - 5, y_bottom + 5))
                    triangle.append(cmpPoint(self.pos.x, y_mid))
                    triangle.append(cmpPoint(self.pos.x, y_bottom))
                elif self.face == FACE_NORTHWEST:
                    triangle.append(cmpPoint(self.pos.x, y_mid))
                    triangle.append(cmpPoint(self.pos.x - 5, self.pos.y - 5))
                    triangle.append(cmpPoint(x_mid, self.pos.y))
                    triangle.append(cmpPoint(self.pos.x, self.pos.y))
                dc.DrawPolygon(triangle)
                dc.SetBrush(wx.NullBrush)
                dc.SetPen(wx.NullPen)

            # Draw the heading if needed
            if self.heading:
                x_adjust = 0
                y_adjust = 4
                x_half = self.bmp.GetWidth()/2
                y_half = self.bmp.GetHeight()/2
                x_quarter = self.bmp.GetWidth()/4
                y_quarter = self.bmp.GetHeight()/4
                x_3quarter = x_quarter*3
                y_3quarter = y_quarter*3
                x_full = self.bmp.GetWidth()
                y_full = self.bmp.GetHeight()
                x_center = self.pos.x + x_half
                y_center = self.pos.y + y_half

                # Remember, the pen/brush must be a different color than the
                # facing marker!!!!  We'll use black/cyan for starters.
                # Also notice that we will draw the heading on top of the
                # larger facing marker.
                dc.SetPen(wx.BLACK_PEN)
                dc.SetBrush(wx.CYAN_BRUSH)
                triangle = []

                # Figure out which direction to draw the marker!!
                if self.heading == FACE_NORTH:
                    triangle.append(cmpPoint(x_center - x_quarter, y_center - y_half ))
                    triangle.append(cmpPoint(x_center, y_center - y_3quarter ))
                    triangle.append(cmpPoint(x_center + x_quarter, y_center - y_half ))
                elif self.heading ==  FACE_SOUTH:
                    triangle.append(cmpPoint(x_center - x_quarter, y_center + y_half ))
                    triangle.append(cmpPoint(x_center, y_center + y_3quarter ))
                    triangle.append(cmpPoint(x_center + x_quarter, y_center + y_half ))
                elif self.heading == FACE_NORTHEAST:
                    triangle.append(cmpPoint(x_center + x_quarter, y_center - y_half ))
                    triangle.append(cmpPoint(x_center + x_3quarter, y_center - y_3quarter ))
                    triangle.append(cmpPoint(x_center + x_half, y_center - y_quarter ))
                elif self.heading == FACE_EAST:
                    triangle.append(cmpPoint(x_center + x_half, y_center - y_quarter ))
                    triangle.append(cmpPoint(x_center + x_3quarter, y_center ))
                    triangle.append(cmpPoint(x_center + x_half, y_center + y_quarter ))
                elif self.heading == FACE_SOUTHEAST:
                    triangle.append(cmpPoint(x_center + x_half, y_center + y_quarter ))
                    triangle.append(cmpPoint(x_center + x_3quarter, y_center + y_3quarter ))
                    triangle.append(cmpPoint(x_center + x_quarter, y_center + y_half ))
                elif self.heading == FACE_SOUTHWEST:
                    triangle.append(cmpPoint(x_center - x_quarter, y_center + y_half ))
                    triangle.append(cmpPoint(x_center - x_3quarter, y_center + y_3quarter ))
                    triangle.append(cmpPoint(x_center - x_half, y_center + y_quarter ))
                elif self.heading == FACE_WEST:
                    triangle.append(cmpPoint(x_center - x_half, y_center + y_quarter ))
                    triangle.append(cmpPoint(x_center - x_3quarter, y_center ))
                    triangle.append(cmpPoint(x_center - x_half, y_center - y_quarter ))
                elif self.heading == FACE_NORTHWEST:
                    triangle.append(cmpPoint(x_center - x_half, y_center - y_quarter ))
                    triangle.append(cmpPoint(x_center - x_3quarter, y_center - y_3quarter ))
                    triangle.append(cmpPoint(x_center - x_quarter, y_center - y_half ))
                dc.DrawPolygon(triangle)
                dc.SetBrush(wx.NullBrush)
                dc.SetPen(wx.NullPen)
            handle_mini_draw_callback_functions(self, dc)
            #selected outline
            if self.selected:
                dc.SetPen(wx.RED_PEN)
                dc.SetBrush(wx.TRANSPARENT_BRUSH)
                dc.DrawRectangle(self.pos.x, self.pos.y, self.bmp.GetWidth(), self.bmp.GetHeight())
                dc.SetBrush(wx.NullBrush)
                dc.SetPen(wx.NullPen)
            # draw label
            label = mini_layer.get_mini_label(self)
            if len(label):
                dc.SetTextForeground(wx.RED)
                (textWidth,textHeight) = dc.GetTextExtent(label)
                x = self.pos.x +((self.bmp.GetWidth() - textWidth) /2) - 1
                y = self.pos.y + self.bmp.GetHeight() + 6
                dc.SetPen(wx.WHITE_PEN)
                dc.SetBrush(wx.WHITE_BRUSH)
                dc.DrawRectangle(x,y,textWidth+2,textHeight+2)
                if (textWidth+2 > self.right):
                    self.right += int((textWidth+2-self.right)/2)+1
                    self.left -= int((textWidth+2-self.right)/2)+1
                self.bottom = y+textHeight+2-self.pos.y
                dc.SetPen(wx.NullPen)
                dc.SetBrush(wx.NullBrush)
                dc.DrawText(label,x+1,y+1)
            self.top-=5
            self.bottom+=5
            self.left-=5
            self.right+=5
        return True

    def toxml(self, action="update"):
        if action == "del":
            xml_str = "<miniature action='del' id='" + self.id + "'/>"

            return xml_str

        xml_str = "<miniature"
        xml_str += " action='" + action + "'"
        xml_str += " label='" + self.label + "'"
        xml_str+= " id='" + self.id + "'"
        if self.pos != None:
            xml_str += " posx='" + str(self.pos.x) + "'"
            xml_str += " posy='" + str(self.pos.y) + "'"
        if self.heading != None:
            xml_str += " heading='" + str(self.heading) + "'"
        if self.face != None:
            xml_str += " face='" + str(self.face) + "'"
        if self.path != None:
            xml_str += " path='" + urllib.quote(self.path).replace('%3A', ':') + "'"
        if self.locked:
            xml_str += "  locked='1'"
        else:
            xml_str += "  locked='0'"
        if self.hide:
            xml_str += " hide='1'"
        else:
            xml_str += " hide='0'"
        if self.snap_to_align != None:
            xml_str += " align='" + str(self.snap_to_align) + "'"
        if self.zorder != None:
            xml_str += " zorder='" + str(self.zorder) + "'"
        if self.width != None:
            xml_str += " width='" + str(self.width) + "'"
        if self.height != None:
            xml_str += " height='" + str(self.height) + "'"
        if self.local:
            xml_str += ' local="' + str(self.local) + '"'
            xml_str += ' localPath="' + str(urllib.quote(self.localPath).replace('%3A', ':')) + '"'
            xml_str += ' localTime="' + str(self.localTime) + '"'
        for attribute in mini_additional_attribute_list:
            if attribute in self.__dict__ and self.__dict__[attribute] is not None:
                xml_str += ' '+attribute+'="'+self.__dict__[attribute]+'"'        
        xml_str += " />"

        if (action == "update" and self.isUpdated) or action == "new":
            self.isUpdated = False
            return xml_str
        else:
            return ''

    def takedom(self, xml):
        self.id = xml.get("id")

        if 'posx' in xml.attrib:
            self.pos.x = int(xml.get("posx"))

        if 'posy' in xml.attrib:
            self.pos.y = int(xml.get("posy"))

        if 'heading' in xml.attrib:
            self.heading = int(xml.get("heading"))

        if 'face' in xml.attrib:
            self.face = int(xml.get("face"))

        if 'path' in xml.attrib:
            self.path = urllib.unquote(xml.get("path"))
            self.set_image(ImageHandler.load(self.path, 'miniature', self.id))

        if 'locked' in xml.attrib:
            if xml.get("locked") == '1' or xml.get("locked") == 'True':
                self.locked = True
            else:
                self.locked = False

        if 'hide' in xml.attrib:
            if xml.get("hide") == '1' or xml.get("hide") == 'True':
                self.hide = True
            else:
                self.hide = False

        if 'label' in xml.attrib:
            self.label = xml.get("label")

        if 'zorder' in xml.attrib:
            self.zorder = int(xml.get("zorder"))

        if 'align' in xml.attrib:
            if xml.get("align") == '1' or xml.get("align") == 'True':
                self.snap_to_align = 1
            else:
                self.snap_to_align = 0

        if 'width' in xml.attrib:
            self.width = int(xml.get("width"))

        if 'height' in xml.attrib:
            self.height = int(xml.get("height"))
        for attribute in mini_additional_attribute_list:
            if attribute in xml.attrib:
                self.__dict__[attribute] = xml.get(attribute)

##-----------------------------
## miniature layer
##-----------------------------
class miniature_layer(layer_base):
    def __init__(self, canvas):
        self.canvas = canvas
        layer_base.__init__(self)

        self.id = -1 #added.

        self.miniatures = []
        self.serial_number = 0
        self.show_labels = True

        # Set the font of the labels to be the same as the chat window
        # only smaller.
        font_size = int(settings.get('defaultfontsize'))
        if (font_size >= 10):
            font_size -= 2
        self.label_font = wx.Font(font_size, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL,
                                  False, settings.get('defaultfont'))

    def next_serial(self):
        self.serial_number += 1

        return self.serial_number

    def get_next_highest_z(self):
        z = len(self.miniatures)+1

        return z

    def cleanly_collapse_zorder(self):
        #  lock the zorder stuff
        sorted_miniatures = self.miniatures[:]
        sorted_miniatures.sort(cmp_zorder)
        i = 0
        for mini in sorted_miniatures:
            mini.zorder = i
            i = i + 1

    def collapse_zorder(self):
        #  lock the zorder stuff
        sorted_miniatures = self.miniatures[:]
        sorted_miniatures.sort(cmp_zorder)
        i = 0
        for mini in sorted_miniatures:
            if (mini.zorder != MIN_STICKY_BACK) and (mini.zorder != MIN_STICKY_FRONT):
                mini.zorder = i
            else:
                pass
            i = i + 1

    def rollback_serial(self):
        self.serial_number -= 1

    def add_miniature(self, id, path, pos=cmpPoint(0,0), label="", heading=FACE_NONE, face=FACE_NONE, width=0, height=0, local=False, localPath='', localTime=-1):
        image = ImageHandler.load(path, "miniature", id)
        if image:
            mini = BmpMiniature(id, path, image, pos, heading, face, label, zorder=self.get_next_highest_z(), width=width, height=height, local=local, localPath=localPath, localTime=localTime)
            self.miniatures.append(mini)

            xml_str = "<map><miniatures>"
            xml_str += mini.toxml("new")
            xml_str += "</miniatures></map>"
            self.canvas.frame.session.send(xml_str)

    def get_miniature_by_id(self, id):
        for mini in self.miniatures:
            if str(mini.id) == str(id):
                return mini

        return None

    def del_miniature(self, min):
        xml_str = "<map><miniatures>"
        xml_str += min.toxml("del")
        xml_str += "</miniatures></map>"
        self.canvas.frame.session.send(xml_str)
        self.miniatures.remove(min)
        del min
        self.collapse_zorder()

    def del_all_miniatures(self):
        while len(self.miniatures):
            min = self.miniatures.pop()
            del min

        self.collapse_zorder()

    def layerDraw(self, dc, topleft, size):
        dc.SetFont(self.label_font)

        sorted_miniatures = self.miniatures[:]
        sorted_miniatures.sort(cmp_zorder)
        for m in sorted_miniatures:
            if (m.pos.x>topleft[0]-m.right and
                m.pos.y>topleft[1]-m.bottom and
                m.pos.x<topleft[0]+size[0]-m.left and
                m.pos.y<topleft[1]+size[1]-m.top):
                m.draw(dc, self)

    def find_miniature(self, pt, only_unlocked=False):
        min_list = []
        for m in self.miniatures:
            if m.hit_test(pt):
                if m.hide and self.canvas.frame.session.my_role() != self.canvas.frame.session.ROLE_GM:
                    continue
                if only_unlocked and not m.locked:
                    min_list.append(m)
                elif not only_unlocked and m.locked:
                    min_list.append(m)
                else:
                    continue
        if len(min_list) > 0:
            return min_list
        else:
            return None

    def areAnySelected(self):
        for mini in self.miniatures:
            if mini.selected:
                return True
        return False

    def getSelected(self):
        selected = []
        minxy_pos = None
        maxxy_pos = None
        for mini in self.miniatures:
            if mini.selected:
                selected.append(mini)
                if minxy_pos is None:
                    minxy_pos = wx.Point(mini.pos.x, mini.pos.y)
                    maxxy_pos = wx.Point(mini.pos.x, mini.pos.y)
                else:
                    minxy_pos.x = min(minxy_pos.x, mini.pos.x)
                    minxy_pos.y = min(minxy_pos.y, mini.pos.y)
                    maxxy_pos.x = max(maxxy_pos.x, mini.pos.x)
                    maxxy_pos.y = max(maxxy_pos.y, mini.pos.y)
        return selected, minxy_pos, maxxy_pos

    def layerToXML(self, action="update"):
        """ format  """
        minis_string = ""
        if self.miniatures:
            for m in self.miniatures:
                minis_string += m.toxml(action)

        if minis_string != '':
            s = "<miniatures"
            s += " serial='" + str(self.serial_number) + "'"
            s += ">"
            s += minis_string
            s += "</miniatures>"
            return s
        else:
            return ""

    def layerTakeDOM(self, xml):
        if 'serial' in xml.attrib:
            self.serial_number = int(xml.get('serial'))
        for c in xml:
            action = c.get("action")
            id = c.get('id')
            if action == "del":
                mini = self.get_miniature_by_id(id)
                if mini:
                    self.miniatures.remove(mini)
                    del mini
            elif action == "new":
                pos = cmpPoint(int(c.get('posx')),int(c.get('posy')))
                path = urllib.unquote(c.get('path'))
                label = c.get('label')
                height = width = heading = face = snap_to_align = zorder = 0
                locked = hide = False
                if 'height' in c.attrib:
                    height = int(c.get('height'))
                if 'width' in c.attrib:
                    width = int(c.get('width'))
                if c.get('locked') == 'True' or c.get('locked') == '1':
                    locked = True
                if c.get('hide') == 'True' or c.get('hide') == '1':
                    hide = True
                if 'heading' in c.attrib:
                    heading = int(c.get('heading'))
                if 'face' in c.attrib:
                    face = int(c.get('face'))
                if 'align' in c.attrib:
                    snap_to_align = int(c.get('align'))
                if 'zorder' in c.attrib:
                    zorder = int(c.get('zorder'))

                mini = BmpMiniature(id, path, ImageHandler.load(path, 'miniature', id), pos, heading, face, label, locked, hide, snap_to_align, zorder, width, height)
                if 'selected' in c.attrib and c.get('selected')=='1':
                    mini.selected = True
                self.miniatures.append(mini)
                if 'local' in c.attrib and c.get('local') == 'True' and os.path.exists(urllib.unquote(c.get('localPath'))):
                    localPath = urllib.unquote(c.get('localPath'))
                    local = True
                    localTime = float(c.get('localTime'))
                    if localTime-time.time() <= 144000:
                        file = open(localPath, "rb")
                        imgdata = file.read()
                        file.close()
                        filename = os.path.split(localPath)
                        (imgtype,j) = mimetypes.guess_type(filename[1])
                        postdata = urllib.urlencode({'filename':filename[1], 'imgdata':imgdata, 'imgtype':imgtype})
                        thread.start_new_thread(self.upload, (postdata, localPath, True))
                #  collapse the zorder.  If the client behaved well, then nothing should change.
                #    Otherwise, this will ensure that there's some kind of z-order
                for attribute in mini_additional_attribute_list:
                    if attribute in c.attrib:
                        mini.__dict__[attribute] = c.get(attribute)
        
                self.collapse_zorder()
            else:
                mini = self.get_miniature_by_id(id)
                if mini:
                    mini.takedom(c)

    def upload(self, postdata, filename, modify=False, pos=cmpPoint(0,0)):
        with self.lock:
            url = settings.get('ImageServerBaseURL')
            f = urllib.urlopen(url, postdata)
            recvdata = f.read()
            f.close()
            try:
                xml = fromstring(recvdata)
                if xml.tag == 'path':
                    path = xml.get('url')
                    path = urllib.unquote(path)
                    if not modify:
                        start = path.rfind("/") + 1
                        if self.canvas.parent.layer_handlers[2].auto_label:
                            min_label = path[start:len(path)-4]
                        else:
                            min_label = ""
                        id = 'mini-' + self.canvas.frame.session.get_next_id()
                        self.add_miniature(id, path, pos=pos, label=min_label,
                                           local=True, localPath=filename,
                                           localTime=time.time())
                    else:
                        self.miniatures[len(self.miniatures)-1].local = True
                        self.miniatures[len(self.miniatures)-1].localPath = filename
                        self.miniatures[len(self.miniatures)-1].localTime = time.time()
                        self.miniatures[len(self.miniatures)-1].path = path
                else:
                    print xml.get('msg')
            except Exception, e:
                logger.general(e)
                logger.general(recvdata)
            finally:
            #urllib.urlcleanup()
            self.lock.release()

    def get_mini_label(self, mini):
        # override this to change the label displayed under each mini (and the label on hidden minis)
        if self.show_labels:
            return mini.label
        else:
            return ""

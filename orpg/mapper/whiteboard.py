# Copyright (C) 2000-2001 The OpenRPG Project
#
#    openrpg-dev@lists.sourceforge.net
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# --
#
# File: mapper/whiteboard.py
# Author: Chris Davis
# Maintainer:
# Version:
#   $Id: whiteboard.py,v 1.47 2007/03/09 14:11:55 digitalxero Exp $
#
# Description: This file contains some of the basic definitions for the chat
# utilities in the orpg project.
#
__version__ = "$Id: whiteboard.py,v 1.47 2007/03/09 14:11:55 digitalxero Exp $"

from base import *
from orpg.mapper.map_utils import *
from random import randint

from orpg.tools.settings import settings
from orpg.tools.orpg_log import logger

def cmp_zorder(first,second):
    f = first.zorder
    s = second.zorder
    if f == None:
        f = 0
    if s == None:
        s = 0
    if f == s:
        value = 0
    elif f < s:
        value = -1
    else:
        value = 1
    return value

class WhiteboardText:
    def __init__(self, id, text_string, pos, style, pointsize, weight, color="#000000"):
        self.scale = 1
        self.r_h = RGBHex()
        self.selected = False
        self.text_string = text_string
        self.id = id
        self.weight = int(weight)
        self.pointsize = int(pointsize)
        self.style = int(style)
        self.textcolor = color
        self.posx = pos.x
        self.posy = pos.y
        self.font = wx.Font(self.pointsize, wx.DEFAULT, self.style, self.weight,
                            False, settings.get('defaultfont'))
        self.highlighted = False
        r,g,b = self.r_h.rgb_tuple(self.textcolor)
        self.highlight_color = self.r_h.hexstring(r^255, g^255, b^255)
        self.isUpdated = False

    def highlight(self, highlight=True):
        self.highlighted = highlight

    def set_text_props(self, text_string, style, point, weight, color="#000000"):
        self.text_string = text_string
        self.textcolor = color
        self.style = int(style)
        self.font.SetStyle(self.style)
        self.pointsize = int(point)
        self.font.SetPointSize(self.pointsize)
        self.weight = int(weight)
        self.font.SetWeight(self.weight)
        self.isUpdated = True

    def hit_test(self, pt, dc):
        rect = self.get_rect(dc)
        result = rect.InsideXY(pt.x, pt.y)

        return result

    def get_rect(self, dc):
        dc.SetFont(self.font)
        (w,x,y,z) = dc.GetFullTextExtent(self.text_string)

        return wx.Rect(self.posx,self.posy,w,(x+y+z))

    def draw(self, parent, dc, op=wx.COPY):
        self.scale = parent.canvas.layers['grid'].mapscale
        if self.highlighted:
            textcolor = self.highlight_color
        else:
            textcolor = self.textcolor
        try:
            dc.SetTextForeground(textcolor)
        except Exception,e:
            dc.SetTextForeground('#000000')
        dc.SetUserScale(self.scale, self.scale)

        # Draw text
        (w,x,y,z) = self.get_rect(dc)
        dc.SetFont(self.font)
        dc.DrawText(self.text_string, self.posx, self.posy)
        dc.SetTextForeground(wx.Colour(0,0,0))

    def toxml(self, action="update"):
        if action == "del":
            xml_str = "<text action='del' id='" + str(self.id) + "'/>"

            return xml_str

        xml_str = "<text"
        xml_str += " action='" + action + "'"
        xml_str += " id='" + str(self.id) + "'"
        if self.pointsize != None:
            xml_str += " pointsize='" + str(self.pointsize) + "'"
        if self.style != None:
            xml_str += " style='" + str(self.style) + "'"
        if self.weight != None:
            xml_str += " weight='" + str(self.weight) + "'"
        if self.posx != None:
            xml_str+= " posx='" + str(self.posx) + "'"
        if not (self.posy is None):
            xml_str += " posy='" + str(self.posy) + "'"
        if self.text_string != None:
            xml_str+= " text_string='" + self.text_string + "'"
        if self.textcolor != None:
            xml_str += " color='" + self.textcolor + "'"
        xml_str += "/>"

        if (action == "update" and self.isUpdated) or action == "new":
            self.isUpdated = False
            return xml_str
        else:
            return ''

    def takedom(self, xml):
        self.text_string = xml.get("text_string")
        self.id = xml.get("id")

        if 'posy' in xml.attrib:
            self.posy = int(xml.get("posy"))

        if 'posx' in xml.attrib:
            self.posx = int(xml.get("posx"))

        if 'weight' in xml.attrib:
            self.weight = int(xml.get("weight"))
            self.font.SetWeight(self.weight)

        if 'style' in xml.attrib:
            self.style = int(xml.get("style"))
            self.font.SetStyle(self.style)

        if 'pointsize' in xml.attrib:
            self.pointsize = int(xml.get("pointsize"))
            self.font.SetPointSize(self.pointsize)

        if 'color' in xml.attrib and xml.get("color") != '':
            self.textcolor = xml.get("color")
            if self.textcolor == '#0000000':
                self.textcolor = '#000000'

class WhiteboardLine:
    def __init__(self, id, color="#000000", width=1):
        self.scale = 1
        self.r_h = RGBHex()
        if color == '':
            color = "#000000"
        self.linecolor = color
        self.linewidth = width
        self.points = []
        self.id = id
        self.highlighted = False
        r,g,b = self.r_h.rgb_tuple(self.linecolor)
        self.highlight_color = self.r_h.hexstring(r^255, g^255, b^255)

    def add_point(self, pos):
        self.points.append(pos)

    def approximate(self):
        """
        Approximate the line with fewer points.

        For every group of three adjacent points (A, B and C), point B
        is discarded if it's sufficiently close to AC.
        """
        new_points = []
        i = 0
        while i < len(self.points):
            a = self.points[i]
            new_points.append(a)
            for j in range(i+1, len(self.points)-1):
                b = self.points[j]
                c = self.points[j+1]
                if proximity_test(a, c, b, 0.5):
                    i += 1
                else:
                    break;
            i += 1
        self.points = new_points

    def points_from_string(self, line_string):
        self.points = []
        for p in line_string.split(";"):
            p = p.split(",")
            if len(p) == 2:
                self.add_point((int(p[0]), int(p[1])))

    def highlight(self, highlight=True):
        self.highlighted = highlight

    def hit_test(self, pt):
        if self.points != []:
            a = self.points[0]
            for b in self.points[1:]:
                if proximity_test(a, b, pt, 12):
                    return True
                a = b
        return False

    def draw(self, parent, dc, op=wx.COPY):
        self.scale = parent.canvas.layers['grid'].mapscale
        linecolor = self.linecolor
        if self.highlighted:
            linecolor = self.highlight_color
        pen = wx.BLACK_PEN
        try:
            pen.SetColour(linecolor)
        except Exception,e:
            pen.SetColour('#000000')

        pen.SetWidth( self.linewidth )
        dc.SetPen( pen )
        dc.SetBrush(wx.BLACK_BRUSH)
        # draw lines

        dc.SetUserScale(self.scale,self.scale)

        if self.points != []:
            a = self.points[0]
            for b in self.points[1:]:
                (xa, ya) = a
                (xb, yb) = b
                dc.DrawLine(xa, ya, xb, yb)
                a = b

        #pen.SetColour(wx.Colour(0,0,0))
        #dc.SetPen(pen)
        #dc.SetPen(wx.NullPen)
        #dc.SetBrush(wx.NullBrush)

    def toxml(self, action="update"):
        if action == "del":
            xml_str = "<line action='del' id='" + str(self.id) + "'/>"

            return xml_str

        #  if there are any changes, make sure id is one of them
        line_string = ';'.join([str(p[0]) + ',' + str(p[1])
                                for p in self.points])
        xml_str = "<line"
        xml_str += " action='" + action + "'"
        xml_str += " id='" + str(self.id) + "'"
        ########
        # the following lines may seem odd but are added to be backwards compatible with
        # OpenRPG 1.8.0.X and earlier.  The last point was always repeated and variables
        # upperleftx, upperlefty, lowerrightx, lowerrighty were mandatory (though unused).
        if len(self.points)==1 or self.points[-1] != self.points[-2]:
            line_string += ";"+str(self.points[-1][0])+","+str(self.points[-1][1])
        xml_str += " upperleftx='0' upperlefty='0' lowerrightx='0' lowerrighty='0'"
        ########
        xml_str+= " line_string='" + line_string + "'"

        if self.linecolor != None:
            xml_str += " color='" + str(self.linecolor) + "'"
        if self.linewidth != None:
            xml_str += " width='" + str(self.linewidth) + "'"
        xml_str += "/>"

        if action == "new":
            return xml_str
        return ''

    def takedom(self, xml):
        line_string = xml.get("line_string")
        self.points_from_line_string(line_string)
        self.id = xml.get("id")

        if 'upperleftx' in xml.attrib:
            self.upperleft.x = int(xml.get("upperleftx"))

        if 'upperlefty' in xml.attrib:
            self.upperleft.y = int(xml.get("upperlefty"))

        if 'lowerrightx' in xml.attrib:
            self.lowerright.x = int(xml.get("lowerrightx"))

        if 'lowerrighty' in xml.attrib:
            self.lowerright.y = int(xml.get("lowerrighty"))

        if 'color' in xml.attrib and xml.get("color") != '':
            self.linecolor = xml.get("color")
            if self.linecolor == '#0000000':
                self.linecolor = '#000000'

        if 'width' in xml.attrib:
            self.linewidth = int(xml.get("width"))

##-----------------------------
## whiteboard layer
##-----------------------------
class whiteboard_layer(layer_base):

    def __init__(self, canvas):
        self.canvas = canvas
        layer_base.__init__(self)

        self.r_h = RGBHex()
        self.lines = []
        self.texts = []
        self.color = "#000000"
        self.width = 1
        self.removedLines = []

    def get_next_highest_z(self):
        return len(self.lines)+1

    def cleanly_collapse_zorder(self):
        pass

    def collapse_zorder(self):
        pass

    def get_line_by_id(self, id):
        for line in self.lines:
            if str(line.id) == str(id):
                return line
        return None

    def get_text_by_id(self, id):
        for text in self.texts:
            if str(text.id) == str(id):
                return text

        return None

    def del_line(self, line):
        xml_str = "<map><whiteboard>"
        xml_str += line.toxml("del")
        xml_str += "</whiteboard></map>"
        self.canvas.frame.session.send(xml_str)
        if line:
            self.lines.remove(line)
            self.removedLines.append(line)
        self.canvas.Refresh()

    def redo_line(self):
        if len(self.removedLines)>0:
            line = self.removedLines.pop()
            self.lines.append(line)
            self.complete_line(line)
            self.canvas.Refresh(True)

    def del_all_lines(self):
        for i in xrange(len(self.lines)):
            self.del_line(self.lines[0])

    def del_text(self, text):
        xml_str = "<map><whiteboard>"
        xml_str += text.toxml("del")
        xml_str += "</whiteboard></map>"
        self.canvas.frame.session.send(xml_str)

        if text:
            self.texts.remove(text)
        self.canvas.Refresh(True)

    def layerDraw(self, dc):
        for m in self.lines:
            m.draw(self, dc)

        for m in self.texts:
            m.draw(self,dc)

    def hit_test_text(self, pos, dc):
        list_of_texts_matching = []
        if self.canvas.layers['fog'].use_fog == 1:
            if self.canvas.frame.session.role != "GM":
                return list_of_texts_matching

        for m in self.texts:
            if m.hit_test(pos,dc):
                list_of_texts_matching.append(m)

        return list_of_texts_matching

    def hit_test_lines(self, pos):
        list_of_lines_matching = []
        if self.canvas.layers['fog'].use_fog == 1:
            if self.canvas.frame.session.role != "GM":
                return list_of_lines_matching

        for m in self.lines:
            if m.hit_test(pos):
                list_of_lines_matching.append(m)

        return list_of_lines_matching

    def find_line(self, pt):
        line_list = self.hit_test_lines(pt)
        if line_list:
            return line_list[0]
        return None

    def setcolor(self, color):
        r,g,b = color.Get()
        self.color = self.r_h.hexstring(r,g,b)

    def sethexcolor(self, hexcolor):
        self.color = hexcolor

    def setwidth(self, width):
        self.width = int(width)

    def set_font(self, font):
        self.font = font

    def add_text(self, text_string, pos, style, pointsize, weight, color="#000000"):
        id = 'text-' + self.canvas.session.get_next_id()
        text = WhiteboardText(id, text_string, pos, style, pointsize, weight, color)
        self.texts.append(text)
        xml_str = "<map><whiteboard>"
        xml_str += text.toxml("new")
        xml_str += "</whiteboard></map>"
        self.canvas.frame.session.send(xml_str)
        self.canvas.Refresh(True)

    def layerToXML(self, action="update"):
        white_string = ""
        if self.lines:
            for l in self.lines:
                white_string += l.toxml(action)
        if self.texts:
            for l in self.texts:
                white_string += l.toxml(action)
        if len(white_string):
            s = "<whiteboard>"
            s += white_string
            s += "</whiteboard>"
            return s
        return ""

    def layerTakeDOM(self, xml):
        for l in xml:
            nodename = l.tag
            action = l.get("action")
            id = l.get('id')
            if action == "del":
                if nodename == 'line':
                    line = self.get_line_by_id(id)
                    if line != None:
                        self.lines.remove(line)
                elif nodename == 'text':
                    text = self.get_text_by_id(id)
                    if text != None:
                        self.texts.remove(text)
            elif action == "new":
                if nodename == "line":
                    try:
                        line_string = l.get('line_string')
                        color = l.get('color')
                        if color == '#0000000':
                            color = '#000000'
                        id = l.get('id')
                        width = int(l.get('width'))
                    except:
                        logger.general(traceback.format_exc())
                        continue
                    line = WhiteboardLine(id, color, width)
                    line.points_from_string(line_string)
                    self.lines.append(line)
                elif nodename == "text":
                    try:
                        text_string = l.get('text_string')
                        style = l.get('style')
                        pointsize = l.get('pointsize')
                        weight = l.get('weight')
                        color = l.get('color')
                        if color == '#0000000':
                            color = '#000000'
                        id = l.get('id')
                        posx = l.get('posx')
                        posy = l.get('posy')
                        pos = wx.Point(0,0)
                        pos.x = int(posx)
                        pos.y = int(posy)
                    except:
                        logger.general(traceback.format_exc())
                        continue
                    text = WhiteboardText(id, text_string, pos, style, pointsize, weight, color)
                    self.texts.append(text)
            else:
                if nodename == "line":
                    line = self.get_line_by_id(id)
                    if line:
                        line.takedom(l)

                if nodename == "text":
                    text = self.get_text_by_id(id)
                    if text:
                        text.takedom(l)

    def add_temp_line(self):
        id = 'line-' + self.canvas.session.get_next_id()
        line = WhiteboardLine(id, color=self.color, width=self.width)
        self.lines.append(line)
        return line

    def del_temp_line(self, line):
        if line:
            self.lines.remove(line)

    def complete_line(self, line):
        line.approximate()
        xml_str = "<map><whiteboard>"
        xml_str += line.toxml("new")
        xml_str += "</whiteboard></map>"
        self.canvas.frame.session.send(xml_str)


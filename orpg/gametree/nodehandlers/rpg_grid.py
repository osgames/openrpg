# Copyright (C) 2000-2001 The OpenRPG Project
#
#        openrpg-dev@lists.sourceforge.net
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# --
#
# File: rpg_grid.py
# Author: Chris Davis
# Maintainer:
# Version:
#   $Id: rpg_grid.py,v 1.20 2006/11/15 12:11:24 digitalxero Exp $
#
# Description: The file contains code for the grid nodehanlers
#

__version__ = "$Id: rpg_grid.py,v 1.20 2006/11/15 12:11:24 digitalxero Exp $"

from core import *
from forms import *
import string

M_HTML = 1
M_USE = 2
M_DESIGN = 3
class rpg_grid_handler(node_handler):
    """ Node handler for rpg grid tool
<nodehandler module='rpg_grid' class='rpg_grid_handler' name='sample'>
  <grid border='' autosize='1' >
    <row>
      <cell size='?'></cell>
      <cell></cell>
    </row>
    <row>
      <cell></cell>
      <cell></cell>
    </row>
  </grid>
  <macros>
    <macro name=''/>
  </macros>
</nodehandler>
    """
    def __init__(self,xml,tree_node):
        node_handler.__init__(self,xml,tree_node)
        self.grid = self.xml.find('grid')
        if self.grid.get("border") == "":
            self.grid.set("border","1")
        if self.grid.get("autosize") == "":
            self.grid.set("autosize","1")
        self.macros = self.xml.find('macros')
        self.myeditor = None
        self.refresh_rows()

    def refresh_die_macros(self):
        pass

    def refresh_rows(self):
        self.rows = {}
        tree = self.tree
        icons = self.tree.icons
        tree.CollapseAndReset(self.mytree_node)
        rows = self.grid.findall('row')
        for index in range(len(rows)):
            row = rows[index]
            first_cell = row.find('cell')
            name = getText(first_cell)
            if name == '':
                name = "Row"
            new_tree_node = tree.AppendItem(self.mytree_node,name,icons['gear'],icons['gear'])
            handler = grid_row_handler(row, new_tree_node, self, index)
            tree.SetPyData(new_tree_node,handler)

    def tohtml(self):
        border = self.grid.get("border", "")
        name = self.xml.get('name')
        rows = self.grid.findall('row')
        colspan = str(len(rows[0].findall('cell')))
        html_str = "<table border=\""+border+"\" align=center><tr bgcolor=\""+TH_BG+"\" ><th colspan="+colspan+">"+name+"</th></tr>"
        for index in range(len(rows)):
            html_str += "<tr><td >"
            html_str += "</td><td >".join(self.calculate_cell_values(rows[index], M_HTML, index)[0])
            html_str += "</td></tr>"
        html_str += "</table>"
        return html_str

    def calculate_cell_values(self, row, mode, row_index):
        """
        mode indicates whether this is for the use dialog, the design dialog or for pretty print/send to chat
        this is a complex function because each mode does slightly different calculations
        """
        cells = row.findall('cell')
        length = len(cells)
        values = ['']*length
        readonly = [False]*length
        valuesToBeSummed = []
        col_two_is_total = self.is_col_two_total() and row_index != 0
        
        for i in range(length):
            if i == 1 and col_two_is_total:
                readonly[i] = True
                continue
            raw = getText(cells[i])
            if raw == '':
                #cells[i].text = raw # not needed?
                if mode == M_HTML:
                    raw = '<br />'
            parsed = raw
            if self.chat.contains_reference(raw):
                parsed = self.chat.parse_multiline(raw, ParserContext(self), False)
                if mode == M_USE:
                    readonly[i] = True
            if mode == M_DESIGN:
                values[i] = raw
            else:
                values[i] = parsed
            if i > 1:
                valuesToBeSummed.append(parsed)
        if col_two_is_total and length > 1:
            value = 0
            for i in range(len(valuesToBeSummed)):
                if is_integer(valuesToBeSummed[i]):
                    value += int(valuesToBeSummed[i])
            values[1] = str(value)
        return values, readonly

    def get_design_panel(self,parent):
        return rpg_grid_edit_panel(parent,self)

    def get_use_panel(self,parent):
        return rpg_grid_panel(parent,self)

    def get_size_constraint(self):
        return 1

    def is_autosized(self):
        return self.grid.get("autosize") == '1'

    def set_autosize(self,autosize=1):
        self.grid.set("autosize",str(autosize))

    def is_col_two_total(self):
        return self.grid.get("coltwoistotal") == '1'

class grid_row_handler(node_handler):
    """ Node Handler grid row.
    """
    def __init__(self, xml, tree_node, grid, index):
        node_handler.__init__(self, xml, tree_node)
        self.grid = grid
        self.index = index

    def on_drop(self,evt):
        pass

    def can_clone(self):
        return 0;

    def tohtml(self):
        border = self.grid.grid.get("border", "")
        html_str = "<table border=\""+border+"\" align=center><tr ><td >"
        html_str += "</td><td >".join(self.grid.calculate_cell_values(self.xml, M_HTML, self.index)[0])
        html_str += "</td></tr></table>"
        return html_str

    def get_value(self):
        cells = self.xml.findall('cell')
        length = len(cells)
        if self.grid.is_col_two_total() and length > 1:
            return self.grid.calculate_cell_values(self.xml, M_USE, self.index)[0][1]
        elif length >= 2:
            return getText(cells[1])
        else:
            return None

    def set_value(self, new_value):
        cells = self.xml.findall('cell')
        if len(cells) >= 2 and not self.grid.is_col_two_total():
            cells[1].text = new_value

class MyCellEditor(wx.grid.PyGridCellEditor):
    """
    This is a sample GridCellEditor that shows you how to make your own custom
    grid editors.  All the methods that can be overridden are show here.  The
    ones that must be overridden are marked with "*Must Override*" in the
    docstring.

    Notice that in order to call the base class version of these special
    methods we use the method name preceded by "base_".  This is because these
    methods are "virtual" in C++ so if we try to call wxGridCellEditor.Create
    for example, then when the wxPython extension module tries to call
    ptr->Create(...) then it actually calls the derived class version which
    looks up the method in this class and calls it, causing a recursion loop.
    If you don't understand any of this, don't worry, just call the "base_"
    version instead.

    ----------------------------------------------------------------------------
    This class is copied from the wxPython examples directory and was written by
    Robin Dunn.

    I have pasted it directly in and removed all references to "log"

    -- Andrew

    """
    def __init__(self):
        wx.grid.PyGridCellEditor.__init__(self)

    def Create(self, parent, id, evtHandler):
        """
        Called to create the control, which must derive from wxControl.
        *Must Override*
        """
        self._tc = wx.TextCtrl(parent, id, "", style=wx.TE_PROCESS_ENTER | wx.TE_PROCESS_TAB)
        self._tc.SetInsertionPoint(0)
        self.SetControl(self._tc)
        if evtHandler:
            self._tc.PushEventHandler(evtHandler)

    def SetSize(self, rect):
        """
        Called to position/size the edit control within the cell rectangle.
        If you don't fill the cell (the rect) then be sure to override
        PaintBackground and do something meaningful there.
        """
        self._tc.SetDimensions(rect.x+1, rect.y+1, rect.width+2, rect.height+2)

    def BeginEdit(self, row, col, grid):
        """
        Fetch the value from the table and prepare the edit control
        to begin editing.  Set the focus to the edit control.
        *Must Override*
        """
        self.startValue = grid.GetTable().GetValue(row, col)
        self._tc.SetValue(self.startValue)
        self._tc.SetInsertionPointEnd()
        self._tc.SetFocus()

        # For this example, select the text
        self._tc.SetSelection(0, self._tc.GetLastPosition())

    def EndEdit(self, row, col, grid):
        """
        Complete the editing of the current cell. Returns True if the value
        has changed.  If necessary, the control may be destroyed.
        *Must Override*
        """
        changed = False

        val = self._tc.GetValue()
        if val != self.startValue:
            changed = True
            grid.GetTable().SetValue(row, col, val) # update the table

        self.startValue = ''
        self._tc.SetValue('')
        return changed

    def Reset(self):
        """
        Reset the value in the control back to its starting value.
        *Must Override*
        """
        self._tc.SetValue(self.startValue)
        self._tc.SetInsertionPointEnd()

    def IsAcceptedKey(self, evt):
        """
        Return True to allow the given key to start editing: the base class
        version only checks that the event has no modifiers.  F2 is special
        and will always start the editor.
        """

        ## Oops, there's a bug here, we'll have to do it ourself..
        ##return self.base_IsAcceptedKey(evt)

        return (not (evt.ControlDown() or evt.AltDown()) and
                evt.GetKeyCode() != wx.WXK_SHIFT)

    def StartingKey(self, evt):
        """
        If the editor is enabled by pressing keys on the grid, this will be
        called to let the editor do something about that first key if desired.
        """
        key = evt.GetKeyCode()
        ch = None
        if key in [wx.WXK_NUMPAD0, wx.WXK_NUMPAD1, wx.WXK_NUMPAD2, wx.WXK_NUMPAD3, wx.WXK_NUMPAD4,
                   wx.WXK_NUMPAD5, wx.WXK_NUMPAD6, wx.WXK_NUMPAD7, wx.WXK_NUMPAD8, wx.WXK_NUMPAD9]:
            ch = ch = chr(ord('0') + key - wx.WXK_NUMPAD0)

        elif key < 256 and key >= 0 and chr(key) in string.printable:
            ch = chr(key)
            if not evt.ShiftDown():
                ch = string.lower(ch)

        if ch is not None:
            # For this example, replace the text.  Normally we would append it.
            self._tc.AppendText(ch)
        else:
            evt.Skip()

    def Destroy(self):
        """final cleanup"""
        self.base_Destroy()

    def Clone(self):
        """
        Create a new object which is the copy of this one
        *Must Override*
        """
        return MyCellEditor()


class rpg_grid(wx.grid.Grid):
    """grid for attacks"""
    def __init__(self, parent, handler, mode):
        wx.grid.Grid.__init__(self, parent, -1, style=wx.SUNKEN_BORDER | wx.WANTS_CHARS)
        self.parent = parent
        self.handler = handler
        self.mode = mode

        #  Registers a "custom" cell editor (really the example from Robin Dunn with minor mods
        self.RegisterDataType(wx.grid.GRID_VALUE_STRING, wx.grid.GridCellStringRenderer(),MyCellEditor())

        self.rows = handler.grid.findall('row')
        rows = len(self.rows)
        cols = len(self.rows[0].findall('cell'))
        self.CreateGrid(rows,cols)
        self.SetRowLabelSize(0)
        self.SetColLabelSize(0)
        self.set_col_widths()

        for i in range(len(self.rows)):
            self.refresh_row(i)

        self.Bind(wx.grid.EVT_GRID_CELL_CHANGE, self.on_cell_change)
        self.Bind(wx.grid.EVT_GRID_COL_SIZE, self.on_col_size)
        self.Bind(wx.grid.EVT_GRID_CELL_LEFT_DCLICK, self.on_leftdclick)


    def on_leftdclick(self,evt):
        if self.CanEnableCellControl():
            self.EnableCellEditControl()

    def on_col_size(self, evt):
        col = evt.GetRowOrCol()
        cells = self.rows[0].findall('cell')
        size = self.GetColSize(col)
        cells[col].set('size',str(size))
        evt.Skip()

    def on_cell_change(self,evt):
        row = evt.GetRow()
        col = evt.GetCol()
        value = self.GetCellValue(row,col)
        cells = self.rows[row].findall('cell')
        cells[col].text = value
        if self.handler.is_col_two_total() and col > 1:
            self.refresh_row(row)
        elif col == 0:
            self.handler.refresh_rows()

    def set_col_widths(self):
        cells = self.rows[0].findall('cell')
        for i in range(0,len(cells)):
            try:
                size = int(cells[i].get('size'))
                self.SetColSize(i,size)
            except:
                continue

    def refresh_row(self, row_index):
        values, readonly = self.handler.calculate_cell_values(self.rows[row_index], self.mode, row_index)
        for i in range(len(values)):
            self.SetReadOnly(row_index, i, readonly[i])
            self.SetCellValue(row_index, i, values[i])
            
    def append_row(self,evt=None):
        self.insert_row(self.GetNumberRows())

    def add_row(self,evt=None):
        self.insert_row(self.GetGridCursorRow())

    def insert_row(self, index):
        cols = self.GetNumberCols()
        row = Element('row')
        for i in range(cols):
            cell = Element('cell')
            cell.text = ''
            row.append(cell) 
        self.handler.grid.insert(index, row)
        self.InsertRows(index, 1)
        self.rows = self.handler.grid.findall('row')
        self.handler.refresh_rows()
        if self.handler.is_col_two_total():
            self.refresh_row(index)

    def append_col(self,evt=None):
        self.insert_col(self.GetNumberCols())

    def add_col(self,evt=None):
        self.insert_col(self.GetGridCursorCol())

    def insert_col(self, index):
        for r in self.rows:
            cell = Element('cell')
            cell.text = ''
            r.insert(index, cell)
        self.InsertCols(index, 1)
        self.set_col_widths()
        if self.handler.is_col_two_total():
            for i in range(len(self.rows)):
                self.refresh_row(i)
        if index==0:
            self.handler.refresh_rows()

    def del_row(self,evt=None):
        if self.GetNumberRows() == 1:
            return
        cursor_row = self.GetGridCursorRow()
        self.handler.grid.remove(self.handler.grid[cursor_row])
        self.DeleteRows(cursor_row,1)
        self.rows = self.handler.grid.findall('row')
        self.handler.refresh_rows()

    def del_col(self,evt=None):
        if self.GetNumberCols() == 1:
            return
        cursor_col = self.GetGridCursorCol()
        for r in self.rows:
            cells = r.findall('cell')
            r.remove(r[cursor_col])
        self.DeleteCols(cursor_col,1)
        self.set_col_widths()
        if self.handler.is_col_two_total() and self.GetNumberCols() > 1:
            for i in range(len(self.rows)):
                self.refresh_row(i)
        if cursor_col==0:
            self.handler.refresh_rows()

G_TITLE = wx.NewId()
G_GRID_BOR = wx.NewId()
class rpg_grid_panel(wx.Panel):
    def __init__(self, parent, handler):
        wx.Panel.__init__(self, parent, -1)
        self.handler = handler
        self.grid = rpg_grid(self, handler, M_USE)
        label = handler.xml.get('name')
        self.main_sizer = wx.BoxSizer(wx.VERTICAL)
        self.main_sizer.Add(wx.StaticText(self, -1, label+": "), 0, wx.EXPAND)
        self.main_sizer.Add(self.grid,1,wx.EXPAND)
        self.SetSizer(self.main_sizer)
        self.SetAutoLayout(True)
        self.Fit()
        parent.SetSize(self.GetBestSize())


G_AUTO_SIZE = wx.NewId()
G_COL_2_IS_TOTAL = wx.NewId()
G_ADD_ROW = wx.NewId()
G_APP_ROW = wx.NewId()
G_ADD_COL = wx.NewId()
G_APP_COL = wx.NewId()
G_DEL_ROW = wx.NewId()
G_DEL_COL = wx.NewId()

class rpg_grid_edit_panel(wx.Panel):
    def __init__(self, parent, handler):
        wx.Panel.__init__(self, parent, -1)
        self.handler = handler
        self.grid = rpg_grid(self, handler, M_DESIGN)
        title_sizer = wx.BoxSizer(wx.HORIZONTAL)
        title_sizer.Add(wx.StaticText(self, -1, "Title:"), 0, wx.EXPAND)
        self.title = wx.TextCtrl(self, G_TITLE, handler.xml.get('name'))
        title_sizer.Add(self.title, 1, wx.EXPAND)

        self.border = wx.CheckBox(self, G_GRID_BOR, " Border (HTML)")
        if handler.grid.get("border") == '1':
            self.border.SetValue(True)
        else:
            self.border.SetValue(False)
        self.auto_size = wx.CheckBox(self, G_AUTO_SIZE, " Auto Size")
        if handler.is_autosized():
            self.auto_size.SetValue(True)
        else:
            self.auto_size.SetValue(False)
        self.colTwoIsTotal = wx.CheckBox(self, G_COL_2_IS_TOTAL, " 2nd Col Is Total")
        if handler.is_col_two_total():
            self.colTwoIsTotal.SetValue(True)
        else:
            self.colTwoIsTotal.SetValue(False)
        checkboxes_sizer = wx.BoxSizer(wx.HORIZONTAL)
        checkboxes_sizer.Add(self.border, 1, wx.EXPAND)
        checkboxes_sizer.Add(self.auto_size, 1, wx.EXPAND)
        checkboxes_sizer.Add(self.colTwoIsTotal, 1, wx.EXPAND)
        
        row_sizer = wx.BoxSizer(wx.HORIZONTAL)
        row_sizer.Add(wx.Button(self, G_APP_ROW, "Add Row To End"), 1, wx.EXPAND)
        row_sizer.Add(wx.Size(10,10))
        row_sizer.Add(wx.Button(self, G_ADD_ROW, "Insert Row"), 1, wx.EXPAND)
        row_sizer.Add(wx.Size(10,10))
        row_sizer.Add(wx.Button(self, G_DEL_ROW, "Remove Row"), 1, wx.EXPAND)

        col_sizer = wx.BoxSizer(wx.HORIZONTAL)
        col_sizer.Add(wx.Button(self, G_APP_COL, "Add Column To End"), 1, wx.EXPAND)
        col_sizer.Add(wx.Size(10,10))
        col_sizer.Add(wx.Button(self, G_ADD_COL, "Insert Column"), 1, wx.EXPAND)
        col_sizer.Add(wx.Size(10,10))
        col_sizer.Add(wx.Button(self, G_DEL_COL, "Remove Column"), 1, wx.EXPAND)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(title_sizer, 0, wx.EXPAND)
        sizer.Add(checkboxes_sizer, 0, wx.EXPAND)
        sizer.Add(self.grid,1,wx.EXPAND)
        sizer.Add(row_sizer,0,wx.EXPAND)
        sizer.Add(col_sizer,0,wx.EXPAND)

        self.SetSizer(sizer)
        self.SetAutoLayout(True)
        self.Fit()

        self.title.Bind(wx.EVT_KILL_FOCUS, self.on_text)
        self.Bind(wx.EVT_BUTTON, self.grid.add_row, id=G_ADD_ROW)
        self.Bind(wx.EVT_BUTTON, self.grid.append_row, id=G_APP_ROW)
        self.Bind(wx.EVT_BUTTON, self.grid.del_row, id=G_DEL_ROW)
        self.Bind(wx.EVT_BUTTON, self.grid.add_col, id=G_ADD_COL)
        self.Bind(wx.EVT_BUTTON, self.grid.append_col, id=G_APP_COL)
        self.Bind(wx.EVT_BUTTON, self.grid.del_col, id=G_DEL_COL)
        self.Bind(wx.EVT_CHECKBOX, self.on_border, id=G_GRID_BOR)
        self.Bind(wx.EVT_CHECKBOX, self.on_auto_size, id=G_AUTO_SIZE)
        self.Bind(wx.EVT_CHECKBOX, self.on_coltwoistotal, id=G_COL_2_IS_TOTAL)

    def on_auto_size(self,evt):
        self.handler.set_autosize(bool2int(evt.Checked()))

    def on_border(self,evt):
        self.handler.grid.set("border",str(bool2int(evt.Checked())))

    def on_coltwoistotal(self, evt):
        self.handler.grid.set("coltwoistotal",str(bool2int(evt.Checked())))
        for i in range(len(self.grid.rows)):
            self.grid.refresh_row(i)

    def on_text(self,evt):
        self.handler.rename(self.title.GetValue())

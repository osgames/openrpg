# Copyright (C) 2000-2001 The OpenRPG Project
#
#   openrpg-dev@lists.sourceforge.net
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# --
#
# File: voxchat.py
# Author: Ted Berg
# Maintainer:
# Version:
#   $Id: voxchat.py,v 1.37 2007/05/06 16:42:55 digitalxero Exp $
#
# Description: nodehandler for alias.
#

import re
import os
import string

from orpg.orpgCore import open_rpg
from orpg.tools.decorators import pending_deprecation

import core


class voxchat_handler(core.node_handler):
    def __init__(self, xml, tree_node):
        core.node_handler.__init__(self, xml, tree_node)

    @pending_deprecation("The ALias Lib no longer imports from the game tree")
    def get_use_panel(self, parent):
        chat = open_rpg.get_component('chat')
        chat.InfoPost("The ALias Lib no longer imports from the game tree")
        return None
    get_design_panel = get_use_panel

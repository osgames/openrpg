# Copyright (C) 2000-2001 The OpenRPG Project
#
#        openrpg-dev@lists.sourceforge.net
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# --
#
# File: gametree.py
# Author: Chris Davis
# Maintainer:
# Version:
#   $Id: gametree.py,v 1.68 2007/12/07 20:39:48 digitalxero Exp $
#
# Description: The file contains code fore the game tree shell
#

from __future__ import with_statement

import urllib
import time
import os
import re
import traceback
import wx
from wx.lib.filebrowsebutton import  *

from orpg.ui.util.dlg import MultiCheckBoxDlg
from orpg.ui.util.misc import ImageHelper
from orpg.orpgCore import open_rpg, ParserContext
from orpg.dirpath import dir_struct
from orpg.tools.settings import settings
from orpg.tools.orpg_log import logger
from orpg.tools.validate import validate
from orpg.tools.decorators import debugging

from orpg.gametree.nodehandlers import core
import orpg.gametree.nodehandlers.containers
import orpg.gametree.nodehandlers.forms
import orpg.gametree.nodehandlers.dnd3e
import orpg.gametree.nodehandlers.dnd35
import orpg.gametree.nodehandlers.chatmacro
import orpg.gametree.nodehandlers.map_miniature_nodehandler
import orpg.gametree.nodehandlers.minilib
import orpg.gametree.nodehandlers.rpg_grid
import orpg.gametree.nodehandlers.d20
import orpg.gametree.nodehandlers.voxchat
import orpg.gametree.nodehandlers.StarWarsd20
from orpg.gametree.gametree_version import GAMETREE_VERSION
import orpg.external.etree.ElementTree as ET
from xml.parsers.expat import ExpatError

compiled_illegal_node_name_regex = re.compile("(!@|@@|@!|::)")

STD_MENU_DELETE = wx.NewId()
STD_MENU_DESIGN = wx.NewId()
STD_MENU_USE = wx.NewId()
STD_MENU_PP = wx.NewId()
STD_MENU_RENAME = wx.NewId()
STD_MENU_SEND = wx.NewId()
STD_MENU_SAVE = wx.NewId()
STD_MENU_ICON = wx.NewId()
STD_MENU_RENAME = wx.NewId()
STD_MENU_CLONE = wx.NewId()
STD_MENU_REPLACE = wx.NewId()
##STD_MENU_ABOUT = wx.NewId()
STD_MENU_INDEX = wx.NewId()
STD_MENU_NAMESPACE = wx.NewId()
STD_MENU_HTML = wx.NewId()
STD_MENU_EMAIL = wx.NewId()
STD_MENU_CHAT = wx.NewId()
STD_MENU_WHISPER = wx.NewId()
STD_MENU_WIZARD = wx.NewId()
STD_MENU_NODE_SUBMENU = wx.NewId()
STD_MENU_NODE_USEFUL = wx.NewId()
STD_MENU_NODE_USELESS = wx.NewId()
STD_MENU_NODE_INDIFFERENT = wx.NewId()
STD_MENU_MAP = wx.NewId()
TOP_IFILE = wx.NewId()
TOP_INSERT_URL = wx.NewId()
TOP_NEW_TREE = wx.NewId()
TOP_SAVE_TREE = wx.NewId()
TOP_SAVE_TREE_AS = wx.NewId()
TOP_TREE_PROP = wx.NewId()
TOP_FEATURES = wx.NewId()

class Namespace(object):
    def __init__(self, name):
        self.name = name
        self.pathmap = {}
        self.leafmap = {}

    def get_handler(self, path):
        if path in self.pathmap:
            return self.pathmap[path]
        if path in self.leafmap:
            handler_list = self.leafmap[path]
            if len(handler_list)==1:
                return handler_list[0]
        return None

    def path_is_duplicate(self, path, handler):
        return (path in self.pathmap and self.pathmap[path] is not handler)

    def add_path(self, path, handler):
        self.pathmap[path] = handler

    def add_leaf(self, name, handler):
        if name in self.leafmap:
            handler_list = self.leafmap[name]
            #handler_list.remove(handler) # make sure we don't duplicate
            handler_list.append(handler)
        else:
            self.leafmap[name]=[handler] # create list of one item

    def delete_path(self, path, handler):
        if path in self.pathmap:
            del self.pathmap[path]

    def delete_leaf(self, name, handler):
        if name in self.leafmap:
            handler_list = self.leafmap[name]
            handler_list.remove(handler)


class MultiReplacer(wx.Dialog):
    def __init__(self, parent, id, title, findValue=''):
        wx.Dialog.__init__(self, parent, id, title, size=(250, 210))
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(wx.StaticText(self, -1, "Find this text:"), 0, wx.EXPAND)
        self.find = wx.TextCtrl(self, -1, "")
        self.find.SetValue(findValue)
        sizer.Add(self.find, 1, wx.EXPAND)
        sizer.Add(wx.Size(10,10))
        sizer.Add(wx.StaticText(self, -1, "Replace with the following lines.\nEach line creates a new clone."), 0, wx.EXPAND)
        self.replace = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE)
        sizer.Add(self.replace, 1, wx.EXPAND)
        sizer.Add(wx.Size(10,10))
        sizer.Add(self.CreateButtonSizer(wx.OK|wx.CANCEL))
        self.SetSizer(sizer)


class game_tree(wx.TreeCtrl):
    @debugging
    def __init__(self, parent, id):
        wx.TreeCtrl.__init__(self,parent,id,  wx.DefaultPosition, wx.DefaultSize,style=wx.TR_EDIT_LABELS | wx.TR_HAS_BUTTONS)
        self.session = open_rpg.get_component('session')
        self.mainframe = open_rpg.get_component('frame')
        self.build_img_list()
        self.build_std_menu()
        self.nodehandlers = {}
        self.nodes = {}
        self.init_nodehandlers()
        self.Bind(wx.EVT_LEFT_DCLICK, self.on_ldclick)
        self.Bind(wx.EVT_RIGHT_DOWN, self.on_rclick)
        self.Bind(wx.EVT_TREE_BEGIN_DRAG, self.on_drag, id=id)
        self.Bind(wx.EVT_LEFT_UP, self.on_left_up)
        self.Bind(wx.EVT_LEFT_DOWN, self.on_left_down)
        self.Bind(wx.EVT_TREE_END_LABEL_EDIT, self.on_label_change, id=self.GetId())
        self.Bind(wx.EVT_TREE_BEGIN_LABEL_EDIT, self.on_label_begin, id=self.GetId())
        self.Bind(wx.EVT_CHAR, self.on_char)
        self.Bind(wx.EVT_KEY_UP, self.on_key_up)
        self.id = 1
        self.dragging = False
        self.root_dir = dir_struct["home"]
        self.last_save_dir = dir_struct["user"]
        self.xml_root = None

        #Create tree from default if it does not exist
        validate.config_file("tree.xml","default_tree.xml")
        open_rpg.add_component("tree", self)
        #build tree
        self.root = self.AddRoot("Game Tree",self.icons['gear'])
        self.was_labeling = 0
        self.rename_flag = 0
        self.image_cache = {}

        self.initialize_indexes()
        self.infopost_validation_message = False

    @debugging
    def add_nodehandler(self, nodehandler, nodeclass):
        if not self.nodehandlers.has_key(nodehandler):
            self.nodehandlers[nodehandler] = nodeclass
        else:
            logger.general("Nodehandler for " + nodehandler + " already exists!")

    @debugging
    def remove_nodehandler(self, nodehandler):
        if self.nodehandlers.has_key(nodehandler):
            del self.nodehandlers[nodehandler]
        else:
            logger.general("No nodehandler for " + nodehandler + " exists!")

    @debugging
    def init_nodehandlers(self):
        self.add_nodehandler('group_handler', orpg.gametree.nodehandlers.containers.group_handler)
        self.add_nodehandler('tabber_handler', orpg.gametree.nodehandlers.containers.tabber_handler)
        self.add_nodehandler('splitter_handler', orpg.gametree.nodehandlers.containers.splitter_handler)
        self.add_nodehandler('form_handler', orpg.gametree.nodehandlers.forms.form_handler)
        self.add_nodehandler('textctrl_handler', orpg.gametree.nodehandlers.forms.textctrl_handler)
        self.add_nodehandler('listbox_handler', orpg.gametree.nodehandlers.forms.listbox_handler)
        self.add_nodehandler('resource_handler', orpg.gametree.nodehandlers.forms.resource_handler)
        self.add_nodehandler('link_handler', orpg.gametree.nodehandlers.forms.link_handler)
        self.add_nodehandler('webimg_handler', orpg.gametree.nodehandlers.forms.webimg_handler)
        self.add_nodehandler('dnd3echar_handler', orpg.gametree.nodehandlers.dnd3e.dnd3echar_handler)
        self.add_nodehandler('dnd35char_handler', orpg.gametree.nodehandlers.dnd35.dnd35char_handler)
        self.add_nodehandler('macro_handler', orpg.gametree.nodehandlers.chatmacro.macro_handler)
        self.add_nodehandler('map_miniature_handler', orpg.gametree.nodehandlers.map_miniature_nodehandler.map_miniature_handler)
        self.add_nodehandler('minilib_handler', orpg.gametree.nodehandlers.minilib.minilib_handler)
        self.add_nodehandler('rpg_grid_handler', orpg.gametree.nodehandlers.rpg_grid.rpg_grid_handler)
        self.add_nodehandler('d20char_handler', orpg.gametree.nodehandlers.d20.d20char_handler)
        self.add_nodehandler('SWd20char_handler', orpg.gametree.nodehandlers.StarWarsd20.SWd20char_handler)
        self.add_nodehandler('file_loader', core.file_loader)
        self.add_nodehandler('node_loader', core.node_loader)
        self.add_nodehandler('url_loader', core.url_loader)
        self.add_nodehandler('min_map', core.min_map)
        self.add_nodehandler('voxchat_handler', orpg.gametree.nodehandlers.voxchat.voxchat_handler)

    @debugging
    def on_key_up(self, evt):
        key_code = evt.GetKeyCode()
        if self.dragging and (key_code == wx.WXK_SHIFT):
            curSelection = self.GetSelection()
            cur = wx.StockCursor(wx.CURSOR_ARROW)
            self.SetCursor(cur)
            self.dragging = False
            obj = self.GetPyData(curSelection)
            self.SelectItem(curSelection)
            if(isinstance(obj,core.node_handler)):
                obj.on_drop(evt)
                self.drag_obj = None
        evt.Skip()

    @debugging
    def on_char(self, evt):
        key_code = evt.GetKeyCode()
        curSelection = self.GetSelection() #  Get the current selection
        if evt.ShiftDown() and ((key_code == wx.WXK_UP) or (key_code == wx.WXK_DOWN)) and not self.dragging:
            curSelection = self.GetSelection()
            obj = self.GetPyData(curSelection)
            self.SelectItem(curSelection)
            if(isinstance(obj,core.node_handler)):
                self.dragging = True
                cur = wx.StockCursor(wx.CURSOR_HAND)
                self.SetCursor(cur)
                self.drag_obj = obj
        elif key_code == wx.WXK_LEFT:
            self.Collapse(curSelection)

        elif key_code == wx.WXK_DELETE: #  Handle the delete key
            if curSelection:
                nextSelect = self.GetItemParent(curSelection)
                self.on_del(evt)
                try:
                    if self.GetItemText(nextSelect) != "":
                        self.SelectItem(nextSelect)
                except:
                    pass
        elif key_code == wx.WXK_F2:
            self.rename_flag = 1
            self.EditLabel(curSelection)
        evt.Skip()

    @debugging
    def locate_valid_tree(self, caption, message, failed_filename):
        """prompts the user to locate a new tree file or create a new one"""
        response = wx.MessageBox(message, caption, wx.YES|wx.NO|wx.ICON_ERROR)
        if response == wx.YES:
            file = None
            dlg = wx.FileDialog(self, "Locate Gametree file",
                                orpg.dirpath.dir_struct["user"],
                                failed_filename[ ((failed_filename.rfind(os.sep))+len(os.sep)):],
                                "Gametree (*.xml)|*.xml|All files (*.*)|*.*",
                                wx.OPEN | wx.CHANGE_DIR)
            if dlg.ShowModal() == wx.ID_OK: file = dlg.GetPath()
            dlg.Destroy()
            if not file:
                self.load_tree(error=1)
            else:
                self.load_tree(file)
            return
        else:
            validate.config_file("tree.xml","default_tree.xml")
            self.load_tree(error=1)
            return

    @debugging
    def load_tree(self, filename=dir_struct["user"]+'tree.xml', error=0):
        settings.set("gametree", filename)
        # check file exists
        if not os.path.exists(filename):
            emsg = "Gametree Missing!\n"+filename+" cannot be found.\n\n"\
                 "Would you like to locate it?\n"\
                 "(Selecting 'No' will cause a new default gametree to be generated)"
            self.locate_valid_tree("Gametree Error", emsg, filename)
            return
        # open and parse file
        try:
            f = open(filename, "rb")
            tree = ET.parse(f)
            self.xml_root = tree.getroot()
        except:
            f.close()
            self.xml_root = None
        # check we got something
        if not self.xml_root:
            os.rename(filename,filename+".corrupt")
            emsg = "Your gametree is being regenerated.\n\n"\
                 "To salvage a recent version of your gametree\n"\
                 "exit OpenRPG and copy the lastgood.xml file in\n"\
                 "your myfiles directory to "+filename+ "\n"\
                 "in your myfiles directory.\n\n"\
                 "lastgood.xml WILL BE OVERWRITTEN NEXT TIME YOU RUN OPENRPG.\n\n"\
                 "Would you like to select a different gametree file to use?\n"\
                 "(Selecting 'No' will cause a new default gametree to be generated)"
            self.locate_valid_tree("Corrupt Gametree!", emsg, filename)
            return
        # check we got a gametree
        if self.xml_root.tag != "gametree":
            emsg = filename+" does not appear to be a valid gametree file.\n\n"\
                 "Would you like to select a different gametree file to use?\n"\
                 "(Selecting 'No' will cause a new default gametree to be generated)"
            self.locate_valid_tree("Invalid Gametree!", emsg, filename)
            return
        try:
            # version = self.xml_root.get("version")
            # see if we should load the gametree
            loadfeatures = int(settings.get("LoadGameTreeFeatures"))
            if loadfeatures:
                features_tree = ET.parse(orpg.dirpath.dir_struct["template"]+"feature.xml")
                self.xml_root.append(features_tree.getroot())
                settings.set("LoadGameTreeFeatures","0")

            ## load tree
            logger.debug("Features loaded (if required)")
            self.CollapseAndReset(self.root)
            logger.note("Parsing Gametree Nodes ", True)
            for xml_child in self.xml_root:
                logger.note('.', True)
                self.load_xml(xml_child,self.root)
            logger.note("done", True)

            self.Expand(self.root)
            self.SetPyData(self.root,self.xml_root)
            if error != 1:
                with open(filename, "rb") as infile:
                    with open(dir_struct["user"]+"lastgood.xml", "wb") as outfile:
                        outfile.write(infile.read())
            else:
                logger.info("Not overwriting lastgood.xml file.", True)

        except Exception, e:
            logger.exception(traceback.format_exc())
            wx.MessageBox("Corrupt Tree!\nYour game tree is being regenerated. To\nsalvage a recent version of your gametree\nexit OpenRPG and copy the lastgood.xml\nfile in your myfiles directory\nto "+filename+ "\nin your myfiles directory.\nlastgood.xml WILL BE OVERWRITTEN NEXT TIME YOU RUN OPENRPG.")
            os.rename(filename,filename+".corrupt")
            validate.config_file("tree.xml","default_tree.xml")
            self.load_tree(error=1)

    @debugging
    def build_std_menu(self, obj=None):
        # build useful menu
        useful_menu = wx.Menu()
        useful_menu.Append(STD_MENU_NODE_USEFUL,"Use&ful")
        useful_menu.Append(STD_MENU_NODE_USELESS,"Use&less")
        useful_menu.Append(STD_MENU_NODE_INDIFFERENT,"&Indifferent")
        # build standard menu
        self.std_menu = wx.Menu()
        self.std_menu.SetTitle("game tree")
        self.std_menu.Append(STD_MENU_USE,"&Use")
        self.std_menu.Append(STD_MENU_DESIGN,"&Design")
        self.std_menu.Append(STD_MENU_PP,"&Pretty Print")
        self.std_menu.AppendSeparator()
        self.std_menu.Append(STD_MENU_SEND,"Send To Player")
        self.std_menu.Append(STD_MENU_MAP,"Send To Map")
        self.std_menu.Append(STD_MENU_CHAT,"Send To Chat")
        self.std_menu.Append(STD_MENU_WHISPER,"Whisper To Player")
        self.std_menu.AppendSeparator()
        self.std_menu.Append(STD_MENU_ICON,"Change &Icon")
        self.std_menu.Append(STD_MENU_RENAME,"Rename")
        self.std_menu.Append(STD_MENU_DELETE,"D&elete")
        self.std_menu.Append(STD_MENU_CLONE,"&Clone")
        self.std_menu.Append(STD_MENU_REPLACE,"Clone with Replace")
        self.std_menu.AppendMenu(STD_MENU_NODE_SUBMENU,"Node &Usefulness",useful_menu)
        self.std_menu.AppendSeparator()
        self.std_menu.Append(STD_MENU_SAVE,"&Save Node")
        self.std_menu.Append(STD_MENU_HTML,"E&xport as HTML")
        self.std_menu.AppendSeparator()
##        self.std_menu.Append(STD_MENU_ABOUT,"&About")
        self.std_menu.Append(STD_MENU_INDEX,"Index")
        self.std_menu.Append(STD_MENU_NAMESPACE,"Namespace")
        self.Bind(wx.EVT_MENU, self.on_send_to, id=STD_MENU_SEND)
        self.Bind(wx.EVT_MENU, self.indifferent, id=STD_MENU_NODE_INDIFFERENT)
        self.Bind(wx.EVT_MENU, self.useful, id=STD_MENU_NODE_USEFUL)
        self.Bind(wx.EVT_MENU, self.useless, id=STD_MENU_NODE_USELESS)
        self.Bind(wx.EVT_MENU, self.on_del, id=STD_MENU_DELETE)
        self.Bind(wx.EVT_MENU, self.on_send_to_map, id=STD_MENU_MAP)
        self.Bind(wx.EVT_MENU, self.on_node_design, id=STD_MENU_DESIGN)
        self.Bind(wx.EVT_MENU, self.on_node_use, id=STD_MENU_USE)
        self.Bind(wx.EVT_MENU, self.on_node_pp, id=STD_MENU_PP)
        self.Bind(wx.EVT_MENU, self.on_save, id=STD_MENU_SAVE)
        self.Bind(wx.EVT_MENU, self.on_icon, id=STD_MENU_ICON)
        self.Bind(wx.EVT_MENU, self.on_rename, id=STD_MENU_RENAME)
        self.Bind(wx.EVT_MENU, self.on_clone, id=STD_MENU_CLONE)
        self.Bind(wx.EVT_MENU, self.on_clone_and_replace, id=STD_MENU_REPLACE)
##        self.Bind(wx.EVT_MENU, self.on_about, id=STD_MENU_ABOUT)
        self.Bind(wx.EVT_MENU, self.on_index, id=STD_MENU_INDEX)
        self.Bind(wx.EVT_MENU, self.on_namespace, id=STD_MENU_NAMESPACE)
        self.Bind(wx.EVT_MENU, self.on_send_to_chat, id=STD_MENU_CHAT)
        self.Bind(wx.EVT_MENU, self.on_whisper_to, id=STD_MENU_WHISPER)
        self.Bind(wx.EVT_MENU, self.on_export_html, id=STD_MENU_HTML)
        self.top_menu = wx.Menu()
        self.top_menu.SetTitle("game tree")
        self.top_menu.Append(TOP_IFILE,"&Insert File")
        self.top_menu.Append(TOP_INSERT_URL,"Insert &URL")
        self.top_menu.Append(TOP_FEATURES, "Insert &Features Node")
        self.top_menu.Append(TOP_NEW_TREE, "&Load New Tree")
        self.top_menu.Append(TOP_SAVE_TREE,"&Save Tree")
        self.top_menu.Append(TOP_SAVE_TREE_AS,"Save Tree &As...")
        self.top_menu.Append(TOP_TREE_PROP,"&Tree Properties")
        self.Bind(wx.EVT_MENU, self.on_insert_file, id=TOP_IFILE)
        self.Bind(wx.EVT_MENU, self.on_insert_url, id=TOP_INSERT_URL)
        self.Bind(wx.EVT_MENU, self.on_save_tree_as, id=TOP_SAVE_TREE_AS)
        self.Bind(wx.EVT_MENU, self.on_save_tree, id=TOP_SAVE_TREE)
        self.Bind(wx.EVT_MENU, self.on_load_new_tree, id=TOP_NEW_TREE)
        self.Bind(wx.EVT_MENU, self.on_tree_prop, id=TOP_TREE_PROP)
        self.Bind(wx.EVT_MENU, self.on_insert_features, id=TOP_FEATURES)

    @debugging
    def do_std_menu(self, evt, obj):
        try:
            self.std_menu.Enable(STD_MENU_MAP, obj.checkToMapMenu())
        except:
            self.std_menu.Enable(STD_MENU_MAP, obj.map_aware())
        self.std_menu.Enable(STD_MENU_CLONE, obj.can_clone())
        self.PopupMenu(self.std_menu)

    @debugging
    def strip_html(self, player):
        ret_string = ""
        x = 0
        in_tag = 0
        for x in xrange(len(player[0])) :
            if player[0][x] == "<" or player[0][x] == ">" or in_tag == 1 :
                if player[0][x] == "<" :
                    in_tag = 1
                elif player[0][x] == ">" :
                    in_tag = 0
                else :
                    pass
            else :
                ret_string = ret_string + player[0][x]
        return ret_string

    @debugging
    def on_receive_data(self, data, player):
        if ET.iselement(data):
            data = ET.tostring(data)

        beg = data.find("<tree>")
        end = data.rfind("</tree>")
        data = data[6:end]
        self.insert_xml(data)

    @debugging
    def on_send_to_chat(self, evt):
        item = self.GetSelection()
        obj = self.GetPyData(item)
        obj.on_send_to_chat(evt)

    @debugging
    def on_whisper_to(self, evt):
        players = self.session.get_players()
        opts = []
        myid = self.session.get_id()
        me = None
        for p in players:
            if p[2] != myid:
                opts.append("("+p[2]+") " + self.strip_html(p))
            else:
                me = p
        if len(opts):
            players.remove(me)
        if len(opts):
            with MultiCheckBoxDlg(self, opts, "Select Players:", "Whisper To", []) as dlg:
                if dlg.ShowModal() == wx.ID_OK:
                    item = self.GetSelection()
                    obj = self.GetPyData(item)
                    selections = dlg.get_selections()
                    if len(selections) == len(opts):
                        open_rpg.get_component('chat').ParsePost(obj.tohtml(), True, True, ParserContext(obj))
                    else:
                        player_ids = []
                        for s in selections:
                            player_ids.append(players[s][2])
                        open_rpg.get_component('chat').whisper_to_players(obj.tohtml(), player_ids, ParserContext(obj))

    @debugging
    def on_export_html(self, evt):
        dlg = wx.FileDialog(self,"Select a file", self.last_save_dir,"","HTML (*.html)|*.html",wx.SAVE)
        if dlg.ShowModal() == wx.ID_OK:
            item = self.GetSelection()
            obj = self.GetPyData(item)
            type = dlg.GetFilterIndex()
            with open(dlg.GetPath(),"w") as f:
                data = "<html><head><title>"+obj.xml.get("name")+"</title></head>"
                html_str = obj.tohtml().replace("\n", "<br />")
                data += "<body bgcolor=\"#FFFFFF\" >"+html_str+"</body></html>"
                for tag in ("</tr>","</td>","</th>","</table>","</html>","</body>","<br />"):
                    data = data.replace(tag,tag+"\n")
                f.write(data)

            self.last_save_dir, throwaway = os.path.split(dlg.GetPath())
        dlg.Destroy()
        os.chdir(self.root_dir)

    @debugging
    def indifferent(self, evt):
        item = self.GetSelection()
        obj = self.GetPyData(item)
        obj.usefulness("indifferent")

    @debugging
    def useful(self, evt):
        item = self.GetSelection()
        obj = self.GetPyData(item)
        obj.usefulness("useful")

    @debugging
    def useless(self, evt):
        item = self.GetSelection()
        obj = self.GetPyData(item)
        obj.usefulness("useless")

    @debugging
    def on_email(self,evt):
        pass

    @debugging
    def on_send_to(self, evt):
        players = self.session.get_players()
        opts = []
        myid = self.session.get_id()
        me = None
        for p in players:
            if p[2] != myid:
                opts.append("("+p[2]+") " + self.strip_html(p))
            else:
                me = p
        if len(opts):
            players.remove(me)
            with MultiCheckBoxDlg(self, opts, "Select Players:", "Send To", []) as dlg:
                if dlg.ShowModal() == wx.ID_OK:
                    item = self.GetSelection()
                    obj = self.GetPyData(item)
                    xmldata = "<tree>" + ET.tostring(obj.xml) + "</tree>"
                    selections = dlg.get_selections()
                    if len(selections) == len(opts):
                        self.session.send(xmldata)
                    else:
                        for s in selections:
                            self.session.send(xmldata,players[s][2])

    @debugging
    def on_icon(self, evt):
        icons = self.icons.keys()
        icons.sort()
        dlg = wx.SingleChoiceDialog(self,"Choose Icon?","Change Icon",icons)
        if dlg.ShowModal() == wx.ID_OK:
            key = dlg.GetStringSelection()
            item = self.GetSelection()
            obj = self.GetPyData(item)
            obj.change_icon(key)
        dlg.Destroy()

    @debugging
    def on_rename(self, evt):
        item = self.GetSelection()
        dlg = wx.TextEntryDialog(self, "New name:", "Renaming tree item.")
        if dlg.ShowModal() == wx.ID_OK:
            new_name = dlg.GetValue()
            if new_name:
                self.SetItemText(item, dlg.GetValue())
            else:
                open_rpg.get_component('chat').InfoPost("New name cannot be blank.")

    @debugging
    def on_clone(self, evt, replace=False):
        item = self.GetSelection()
        obj = self.GetPyData(item)
        if obj.can_clone():
            parent_node = self.GetItemParent(item)
            clone_text_list = [ET.tostring(obj.xml)]
            if replace:
                clone_text_list = self.replace_for_clone(clone_text_list[0], self.GetItemText(item))
            if parent_node == self.root:
                parent_xml = self.GetPyData(parent_node)
            else:
                parent_xml = self.GetPyData(parent_node).xml
            for clone_text in clone_text_list:
                clone_xml = ET.XML(clone_text)
                # avoid the new item duplicating an existing name
                base_name = clone_xml.get("name")
                name_lc = base_name.lower()
                parts = base_name.rsplit("_", 1)
                if len(parts) == 2 and parts[1].isdigit():
                    base_name = parts[0]
                base_name += "_"
                base_name_lc = base_name.lower()
                pos = 0
                existing_suffixes = []
                name_already_exists = False
                for i in range(len(parent_xml)):
                    # optimise for many copies being added?
                    sibling_name_lc = parent_xml[i].get("name", "").lower()
                    if sibling_name_lc == name_lc:
                        name_already_exists = True
                    if sibling_name_lc.startswith(base_name_lc) and sibling_name_lc[len(base_name_lc):].isdigit():
                        existing_suffixes.append(int(sibling_name_lc[len(base_name_lc):]))
                    if parent_xml[i] is obj.xml:
                        pos = i
                if name_already_exists:
                    suffix = 2
                    while suffix in existing_suffixes:
                        suffix += 1
                    clone_xml.set("name", base_name+str(suffix))
                parent_xml.insert(pos+1, clone_xml) #copy placed below
                self.load_xml(clone_xml, parent_node, item)

    @debugging
    def on_clone_and_replace(self, evt):
        self.on_clone(evt, True)

    @debugging
    def replace_for_clone(self, xml_string, findValue=''):
        replaced_values = []
        dlg = MultiReplacer(None, -1, "Clone with Replace", findValue)
        result = dlg.ShowModal()
        dlg.Destroy()
        if result == wx.ID_OK:
            find = dlg.find.GetValue()
            multi_replace = dlg.replace.GetValue()
            if find and multi_replace:
                for replace in multi_replace.split("\n"):
                    # add in reverse order here so when added beneath original item, will be right order
                    replaced_values.insert(0, xml_string.replace(find, replace))# make it case insensitive?
            else:
                open_rpg.get_component('chat').InfoPost("Clone with Replace requires values for both find and replace.")
        return replaced_values

    @debugging
    def on_save(self, evt):
        """save node to a xml file"""
        item = self.GetSelection()
        obj = self.GetPyData(item)
        obj.on_save(evt)
        os.chdir(self.root_dir)

    @debugging
    def on_save_tree_as(self, evt):
        f = wx.FileDialog(self,"Select a file", self.last_save_dir,"","*.xml",wx.SAVE)
        if f.ShowModal() == wx.ID_OK:
            self.save_tree(f.GetPath())
            self.last_save_dir, throwaway = os.path.split(f.GetPath())
        f.Destroy()
        os.chdir(self.root_dir)

    @debugging
    def on_save_tree(self, evt=None):
        filename = settings.get("gametree")
        self.save_tree(filename)

    @debugging
    def save_tree(self, filename=dir_struct["user"]+'tree.xml'):
        self.xml_root.set("version",GAMETREE_VERSION)
        settings.set("gametree",filename)
        ET.ElementTree(self.xml_root).write(filename)

    @debugging
    def on_load_new_tree(self, evt):
        f = wx.FileDialog(self,"Select a file", self.last_save_dir,"","*.xml",wx.OPEN)
        if f.ShowModal() == wx.ID_OK:
            self.load_tree(f.GetPath())
            self.last_save_dir, throwaway = os.path.split(f.GetPath())
        f.Destroy()
        os.chdir(self.root_dir)

    @debugging
    def on_insert_file(self, evt):
        """loads xml file into the tree"""
        if self.last_save_dir == ".":
            self.last_save_dir = dir_struct["user"]
        f = wx.FileDialog(self,"Select a file", self.last_save_dir,"","*.xml",wx.OPEN)
        if f.ShowModal() == wx.ID_OK:
            self.insert_xml(open(f.GetPath(),"r").read())
            self.last_save_dir, throwaway = os.path.split(f.GetPath())
        f.Destroy()
        os.chdir(self.root_dir)

    @debugging
    def on_insert_url(self, evt):
        """loads xml url into the tree"""
        dlg = wx.TextEntryDialog(self,"URL?","Insert URL", "http://")
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetValue()
            file = urllib.urlopen(path)
            self.insert_xml(file.read())
        dlg.Destroy()

    @debugging
    def on_insert_features(self, evt):
        self.insert_xml(open(dir_struct["template"]+"feature.xml","r").read())

    @debugging
    def on_tree_prop(self, evt):
        dlg = gametree_prop_dlg(self, settings)
        if dlg.ShowModal() == wx.ID_OK:
            pass
        dlg.Destroy()

    @debugging
    def on_node_design(self, evt):
        item = self.GetSelection()
        obj = self.GetPyData(item)
        obj.on_design(evt)

    @debugging
    def on_node_use(self, evt):
        item = self.GetSelection()
        obj = self.GetPyData(item)
        obj.on_use(evt)

    @debugging
    def on_node_pp(self, evt):
        item = self.GetSelection()
        obj = self.GetPyData(item)
        obj.on_html_view(evt)

    @debugging
    def on_del(self, evt):
        status_value = "none"
        try:
            item = self.GetSelection()
            if item:
                handler = self.GetPyData(item)
                status_value = handler.xml.get('status')
                name = handler.xml.get('name')
                parent_item = self.GetItemParent(item)
                while parent_item.IsOk() and status_value!="useful" and status_value!="useless":
                    try:
                        parent_handler = self.GetPyData(parent_item)
                        status_value = parent_handler.get('status')
                        name = parent_handler.get('name')
                        if status_value == "useless":
                            break
                        elif status_value == "useful":
                            break
                    except:
                        status_value = "none"
                    parent_item = self.GetItemParent(parent_item)

                if status_value == "useful":
                    dlg = wx.MessageDialog(self, `name` + "  And everything beneath it are considered useful.  \n\nAre you sure you want to delete this item?",'Important Item',wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION)
                    if dlg.ShowModal() == wx.ID_YES:
                        handler.delete()
                else:
                    handler.delete()
        except:
            if self.GetSelection() == self.GetRootItem():
                msg = wx.MessageDialog(None,"You can't delete the root item.","Delete Error",wx.OK)
            else:
                msg = wx.MessageDialog(None,"Unknown error deleting node.","Delete Error",wx.OK)
            msg.ShowModal()
            msg.Destroy()

##    @debugging
##    def on_about(self, evt):
##        item = self.GetSelection()
##        obj = self.GetPyData(item)
##        about = MyAboutBox(self,obj.about())
##        about.ShowModal()
##        about.Destroy()

    @debugging
    def on_send_to_map(self, evt):
        item = self.GetSelection()
        obj = self.GetPyData(item)
        if hasattr(obj,"on_send_to_map"):
            obj.on_send_to_map(evt)

    @debugging
    def insert_xml(self, txt):
        if not txt:
            wx.MessageBox("Import Failed: Invalid or missing node data")
            logger.general("Import Failed: Invalid or missing node data")
            return

        try:
            new_xml = ET.XML(txt)
        except ExpatError:
            wx.MessageBox("Error Importing Node or Tree")
            logger.general("Error Importing Node or Tree")
            return

        if new_xml.tag == "gametree":
            for xml_child in new_xml:
                self.load_xml(xml_child, self.root)
            return

        self.xml_root.append(new_xml)
        self.load_xml(new_xml,self.root,self.root)

    @debugging
    def build_img_list(self):
        """make image list"""
        helper = ImageHelper()
        self.icons = { }
        self._imageList= wx.ImageList(16,16,False)
        icons_xml = ET.parse(orpg.dirpath.dir_struct["icon"]+"icons.xml")
        for icon in icons_xml.getroot():
            key = icon.get("name")
            path = orpg.dirpath.dir_struct["icon"] + icon.get("file")
            img = helper.load_file(path)
            self.icons[key] = self._imageList.Add(img)
        self.SetImageList(self._imageList)

    @debugging
    def load_xml(self, xml_element, parent_node, prev_node=None):

        #add the first tree node
        i = 0
        name = xml_element.get("name")
        icon = xml_element.get("icon")
        if self.icons.has_key(icon):
            i = self.icons[icon]

        if prev_node:
            if prev_node == parent_node:
                new_tree_node = self.PrependItem(parent_node, name, i, i)
            else:
                new_tree_node = self.InsertItem(parent_node,prev_node, name, i, i)
        else:
            new_tree_node = self.AppendItem(parent_node, name, i, i)

        logger.debug("Node Added to tree")
        #create a nodehandler or continue loading xml into tree
        if xml_element.tag == "nodehandler":
            try:
                py_class = xml_element.get("class")
                logger.debug("nodehandler class: " + py_class)
                if not self.nodehandlers.has_key(py_class):
                    raise Exception("Unknown Nodehandler for " + py_class)
                self.nodes[self.id] = self.nodehandlers[py_class](xml_element, new_tree_node)
                self.SetPyData(new_tree_node, self.nodes[self.id])
                logger.debug("Node Data set")
                bmp = self.nodes[self.id].get_scaled_bitmap(16,16)
                if bmp:
                    self.cached_load_of_image(bmp,new_tree_node,)
                logger.debug("Node Icon loaded")
                self.id = self.id + 1
            except Exception, er:
                logger.exception(traceback.format_exc())

                # was deleted -- should we delete non-nodehandler nodes then?
                #self.Delete(new_tree_node)
                #parent = xml_dom._get_parentNode()
                #parent.removeChild(xml_dom)

        return new_tree_node

    @debugging
    def cached_load_of_image(self, bmp_in, new_tree_node):
        image_list = self.GetImageList()
        img = wx.ImageFromBitmap(bmp_in)
        img_data = img.GetData()
        image_index = None

        for key in self.image_cache.keys():
            if self.image_cache[key] == str(img_data):
                image_index = key
                break

        if image_index is None:
            image_index = image_list.Add(bmp_in)
            self.image_cache[image_index] = img_data

        self.SetItemImage(new_tree_node,image_index)
        self.SetItemImage(new_tree_node,image_index, wx.TreeItemIcon_Selected)
        return image_index

    @debugging
    def on_rclick(self, evt):
        pt = evt.GetPosition()
        (item, flag) = self.HitTest(pt)
        if item.IsOk():
            obj = self.GetPyData(item)
            self.SelectItem(item)
            if(isinstance(obj,core.node_handler)):
                obj.on_rclick(evt)
            else:
                self.PopupMenu(self.top_menu)
        else:
            self.PopupMenu(self.top_menu,pt)

    @debugging
    def on_ldclick(self, evt):
        self.rename_flag = 0
        pt = evt.GetPosition()
        (item, flag) = self.HitTest(pt)
        if item.IsOk():
            obj = self.GetPyData(item)
            self.SelectItem(item)
            if(isinstance(obj,core.node_handler)):
                if not obj.on_ldclick(evt):
                    action = settings.get("treedclick")
                    if action == "use":
                        obj.on_use(evt)
                    elif action == "design":
                        obj.on_design(evt)
                    elif action == "print":
                        obj.on_html_view(evt)
                    elif action == "chat":
                        self.on_send_to_chat(evt)

    @debugging
    def on_left_down(self, evt):
        pt = evt.GetPosition()
        (item, flag) = self.HitTest(pt)
        if item.IsOk() and self.was_labeling:
            self.SelectItem(item)
            self.rename_flag = 0
            self.was_labeling = 0
        elif (flag & wx.TREE_HITTEST_ONITEMLABEL) == wx.TREE_HITTEST_ONITEMLABEL and self.IsSelected(item):
            #  this next if tests to ensure that the mouse up occurred over a label, and not the icon
            self.rename_flag = 1
        else:
            self.SelectItem(item)
        evt.Skip()

    @debugging
    def on_left_up(self, evt):
        if self.dragging:
            cur = wx.StockCursor(wx.CURSOR_ARROW)
            self.SetCursor(cur)
            self.dragging = False
            pt = evt.GetPosition()
            (item, flag) = self.HitTest(pt)
            if item.IsOk():
                obj = self.GetPyData(item)
                self.SelectItem(item)
                if(isinstance(obj,core.node_handler)):
                    obj.on_drop(evt)
                    self.drag_obj = None

    @debugging
    def get_handler_by_path(self, path, context):
        path = path.lower().strip()
        namespace = None
        # first, use the context to figure out the namespace
        if isinstance(context.namespace_hint, core.node_handler):
            namespace = self.get_namespace_by_name(self.get_namespace_name(context.namespace_hint.mytree_node))
        if isinstance(context.namespace_hint, (str, unicode)):
            namespace = self.get_namespace_by_name(context.namespace_hint.lower().strip())
        if namespace:
            handler = namespace.get_handler(path)
            if handler is not None:
                return handler
        # failing with that, check the global path
        handler = self.root_namespace.get_handler(path)
        if handler is not None:
            return handler
        # finally check for the namespace_name::namespace_path format
        path_bits = path.split("::")
        if len(path_bits) >= 2:
            namespace = self.get_namespace_by_name(path_bits[0])
            if namespace:
                handler = namespace.get_handler("::".join(path_bits[1:]))
                if handler is not None:
                    return handler
        return None

    @debugging
    def get_namespace_by_name(self, name):
        if name and name in self.namespacemap:
            return self.namespacemap[name]
        return None

    @debugging
    def validation_message(self, message):
        print message
        if self.infopost_validation_message:
            open_rpg.get_component('chat').InfoPost(message)

    @debugging
    def set_referenceable(self, item, value):
        if value:
            self.SetItemTextColour(item, wx.BLACK)
        else:
            self.SetItemTextColour(item, wx.RED)
        handler = self.GetPyData(item)
        if handler:
            handler.set_referenceable(value)

    @debugging
    def is_referenceable(self, item):
        return self.GetItemTextColour(item) == wx.BLACK;

    @debugging
    def set_namespace(self, item, value):
        if value:
            self.SetItemBold(item, True)
        else:
            self.SetItemBold(item, False)
        handler = self.GetPyData(item)
        if handler:
            handler.set_namespace(value)

    @debugging
    def is_namespace(self, item):
        return self.IsBold(item);

    @debugging
    def validate_name(self, name):
        # can't include !@ @@ @! or ::
        return compiled_illegal_node_name_regex.search(name) is None

    @debugging
    def get_path_data(self, item):
        namespace_name = None
        namespace_path = None
        path = self.GetItemText(item).lower().strip()
        valid_path = self.validate_name(path)
        parent = self.GetItemParent(item)
        while parent.IsOk() and parent != self.root:
            name = self.GetItemText(parent).lower().strip()
            if not self.validate_name(name):
                valid_path = False
            if self.is_namespace(parent):
                namespace_name = name
                namespace_path = path
            path = name+'::'+path
            parent = self.GetItemParent(parent)
        return path, valid_path, namespace_name, namespace_path

    @debugging
    def get_namespace_name(self, item):
        parent = self.GetItemParent(item)
        while parent.IsOk() and parent != self.root:
            if self.is_namespace(parent):
                return self.GetItemText(parent).lower().strip()
            parent = self.GetItemParent(parent)
        return None

    @debugging
    def traverse(self, root, function, data=None, recurse=True):
        child, cookie = self.GetFirstChild(root)
        while child.IsOk():
            function(child, data)
            if recurse:
                self.traverse(child, function, data)
            child, cookie = self.GetNextChild(root, cookie)

    @debugging
    def on_index(self, evt):
        self.infopost_validation_message = True
        item = self.GetSelection()
        if self.is_referenceable(item):
            self.on_delete_path(item)
            self.set_referenceable(item, False)
        else:
            self.on_new_path(item)
        self.infopost_validation_message = False

    @debugging
    def on_namespace(self, evt):
        self.infopost_validation_message = True
        item = self.GetSelection()
        if self.is_namespace(item):
            self.remove_namespace(item)
            self.set_namespace(item, False)
        else:
            self.on_new_namespace(item)
        self.infopost_validation_message = False

    @debugging
    def on_new_path(self, item):
        name = self.GetItemText(item).lower().strip()
        handler = self.GetPyData(item)
        path, valid_path, namespace_name, namespace_path = self.get_path_data(item)
        if not valid_path:
            self.set_referenceable(item, False)
            self.validation_message("Illegal node name: '"+path+"'.  Don't use !@ @@ @! or :: in node names.")
            return
        if self.root_namespace.path_is_duplicate(path, handler):
            self.set_referenceable(item, False)
            self.validation_message("Duplicate path: '"+path+"'.  Only first node is referenceable.")
            return
        namespace = self.get_namespace_by_name(namespace_name)
        if namespace:
            if namespace.path_is_duplicate(path, handler):
                self.set_referenceable(item, False)
                self.validation_message("Duplicate path: '"+namespace_path+"', within namespace: '"+namespace_name+"'.  Only first node is referenceable.")
                return
            namespace.add_path(namespace_path, handler)
        # add path only after checking namepace path uniqueness
        self.set_referenceable(item, True)
        self.root_namespace.add_path(path, handler)

        if not self.ItemHasChildren(item):
            self.root_namespace.add_leaf(name, handler)
            if namespace:
                namespace.add_leaf(name, handler)

    @debugging
    def on_new_namespace(self, item):
        name = self.GetItemText(item).lower().strip()
        # check for nested namspace
        parent_namespace_name = self.get_namespace_name(item)
        if parent_namespace_name is not None:
            self.validation_message("Cannot nest namespaces ('"+name+"' inside namespace '"+parent_namespace_name+"')")
            return
        errors = []
        self.traverse(item, self.check_no_namespaces_traverse, errors)
        if errors:
            list_of_names = ", ".join(errors)
            self.validation_message("Cannot nest namespaces (container '"+name+"' has child namespaces: "+list_of_names+")")
            return
        if name in self.namespacemap:
            self.set_namespace(item, False)
            self.validation_message("Duplicate namespace: '"+name+"'")
            return
        namespace = Namespace(name)
        self.set_namespace(item, True)
        self.namespacemap[name]=namespace
        self.traverse(item, self.on_new_namespace_traverse, namespace)

    @debugging
    def check_no_namespaces_traverse(self, item, errors):
        if self.is_namespace(item):
            errors.append(self.GetItemText(item))

    @debugging
    def on_new_namespace_traverse(self, item, namespace):
        # this item just became part of the new namepace
        # should we re-check its validity?
        if self.is_referenceable(item):
            name = self.GetItemText(item).lower().strip()
            handler = self.GetPyData(item)
            path, valid_path, namespace_name, namespace_path = self.get_path_data(item)
            if namespace.path_is_duplicate(namespace_path, handler):
                self.set_referenceable(item, False)
                self.validation_message("Duplicate node path: '"+namespace_path+"', within new namespace: '"+namespace_name+"'.  Only first node is referenceable.")
                return
            namespace.add_path(namespace_path, handler)
            if not self.ItemHasChildren(item):
                namespace.add_leaf(name, handler)

    @debugging
    def on_delete_path(self, item):
        name = self.GetItemText(item).lower().strip()
        handler = self.GetPyData(item)
        path, valid_path, namespace_name, namespace_path = self.get_path_data(item)
        self.root_namespace.delete_path(path, handler)
        namespace = self.get_namespace_by_name(namespace_name)
        if namespace:
            namespace.delete_path(namespace_path, handler)
        if not self.ItemHasChildren(item):
            self.root_namespace.delete_leaf(name, handler)
            if namespace:
                namespace.delete_leaf(name, handler)

    @debugging
    def remove_namespace(self, item):
        name = self.GetItemText(item).lower().strip()
        del self.namespacemap[name]

    @debugging
    def on_new_item(self, item, data=None):
        # the data is to make this function callable by traverse()
        if self.is_referenceable(item):
            self.on_new_path(item)
        if self.is_namespace(item):
            self.on_new_namespace(item)

    @debugging
    def on_delete_item(self, item, data=None):
        # the data is to make this function callable by traverse()
        if self.is_referenceable(item):
            self.on_delete_path(item)
        if self.is_namespace(item):
            self.remove_namespace(item)

    @debugging
    def initialize_indexes(self):
        self.root_namespace = Namespace("")
        self.namespacemap = {}

    @debugging
    def AppendItem(self, parent, text, image=-1, selectedImage=-1, data=None):
        newitem = wx.TreeCtrl.AppendItem(self, parent, text, image, selectedImage, None)
        if data:
            self.SetPyData(newitem, data)
        else:
            self.on_new_item(newitem)
        return newitem

    @debugging
    def PrependItem(self, parent, text, image=-1, selectedImage=-1, data=None):
        newitem = wx.TreeCtrl.PrependItem(self, parent, text, image, selectedImage, None)
        if data:
            self.SetPyData(newitem, data)
        else:
            self.on_new_item(newitem)
        return newitem

    @debugging
    def InsertItem(self, parent, sibling, text, image=-1, selectedImage=-1, data=None):
        newitem = wx.TreeCtrl.InsertItem(self, parent, sibling, text, image, selectedImage, None)
        if data:
            self.SetPyData(newitem, data)
        else:
            self.on_new_item(newitem)
        return newitem

    @debugging
    def SetPyData(self, item, handler):
        wx.TreeCtrl.SetPyData(self, item, handler)
        if item != self.GetRootItem():
            self.set_referenceable(item, handler.get_referenceable())
            self.set_namespace(item, handler.get_namespace())
            self.on_new_item(item)

    @debugging
    def Delete(self, item):
        self.on_delete_subtree(item)
        wx.TreeCtrl.Delete(self, item)

    @debugging
    def DeleteChildren(self, item):
        self.traverse(item, self.on_delete_item)
        wx.TreeCtrl.DeleteChildren(self, item)

    @debugging
    def CollapseAndReset(self, item):
        if item is self.GetRootItem():
            self.initialize_indexes()
        else:
            self.traverse(item, self.on_delete_item)
        wx.TreeCtrl.CollapseAndReset(self, item)

    @debugging
    def DeleteAllItems(self):
        self.initialize_indexes()
        wx.TreeCtrl.DeleteAllItems(self)

    @debugging
    def SetItemText(self, item, name):
        self.on_delete_subtree(item)
        wx.TreeCtrl.SetItemText(self, item, name)
        self.on_new_subtree(item)

    @debugging
    def on_delete_subtree(self, item):
        self.traverse(item, self.on_delete_item)
        self.on_delete_item(item)

    @debugging
    def on_new_subtree(self, item):
        self.traverse(item, self.on_new_item)
        self.on_new_item(item)

    @debugging
    def on_label_change(self, evt):
        item = evt.GetItem()
        txt = evt.GetLabel()
        self.was_labeling = 0
        self.rename_flag = 0
        if txt != "":
            handler = self.GetPyData(item)
            handler.xml.set('name',txt)
            self.on_delete_subtree(item)
            wx.CallAfter(self.on_new_subtree, item)
        else:
            evt.Veto()

    @debugging
    def on_label_begin(self, evt):
        if not self.rename_flag:
            evt.Veto()
        else:
            self.was_labeling = 1
            item = evt.GetItem()
            if item == self.GetRootItem():
                evt.Veto()

    @debugging
    def on_drag(self, evt):
        self.rename_flag = 0
        item = self.GetSelection()
        obj = self.GetPyData(item)
        self.SelectItem(item)
        if(isinstance(obj,core.node_handler) and obj.drag):
            self.dragging = True
            cur = wx.StockCursor(wx.CURSOR_HAND)
            self.SetCursor(cur)
            self.drag_obj = obj

    @debugging
    def is_parent_node(self, node, compare_node):
        parent_node = self.GetItemParent(node)
        if compare_node == parent_node:
            logger.debug("parent node")
            return 1
        elif parent_node == self.root:
            logger.debug("not parent")
            return 0
        else:
            return self.is_parent_node(parent_node, compare_node)

CTRL_TREE_FILE = wx.NewId()
CTRL_YES = wx.NewId()
CTRL_NO = wx.NewId()
CTRL_USE = wx.NewId()
CTRL_DESIGN = wx.NewId()
CTRL_CHAT = wx.NewId()
CTRL_PRINT = wx.NewId()

class gametree_prop_dlg(wx.Dialog):
    @debugging
    def __init__(self, parent, settings):
        wx.Dialog.__init__(self, parent, wx.ID_ANY, "Game Tree Properties")

        #sizers
        sizers = {}
        sizers['but'] = wx.BoxSizer(wx.HORIZONTAL)
        sizers['main'] = wx.BoxSizer(wx.VERTICAL)

        #box sizers
        box_sizers = {}
        box_sizers["save"] = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, "Save On Exit"), wx.HORIZONTAL)
        box_sizers["file"] = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, "Tree File"), wx.HORIZONTAL)
        box_sizers["dclick"] = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, "Double Click Action"), wx.HORIZONTAL)
        self.ctrls = {  CTRL_TREE_FILE : FileBrowseButtonWithHistory(self, wx.ID_ANY,  labelText="") ,
                        CTRL_YES : wx.RadioButton(self, CTRL_YES, "Yes", style=wx.RB_GROUP),
                        CTRL_NO : wx.RadioButton(self, CTRL_NO, "No"),
                        CTRL_USE : wx.RadioButton(self, CTRL_USE, "Use", style=wx.RB_GROUP),
                        CTRL_DESIGN : wx.RadioButton(self, CTRL_DESIGN, "Desgin"),
                        CTRL_CHAT : wx.RadioButton(self, CTRL_CHAT, "Chat"),
                        CTRL_PRINT : wx.RadioButton(self, CTRL_PRINT, "Pretty Print")
                        }
        self.ctrls[CTRL_TREE_FILE].SetValue(settings.get("gametree"))
        opt = settings.get("SaveGameTreeOnExit")
        self.ctrls[CTRL_YES].SetValue(opt=="1")
        self.ctrls[CTRL_NO].SetValue(opt=="0")
        opt = settings.get("treedclick")
        self.ctrls[CTRL_DESIGN].SetValue(opt=="design")
        self.ctrls[CTRL_USE].SetValue(opt=="use")
        self.ctrls[CTRL_CHAT].SetValue(opt=="chat")
        self.ctrls[CTRL_PRINT].SetValue(opt=="print")
        box_sizers['save'].Add(self.ctrls[CTRL_YES],0, wx.EXPAND)
        box_sizers['save'].Add(wx.Size(10,10))
        box_sizers['save'].Add(self.ctrls[CTRL_NO],0, wx.EXPAND)
        box_sizers['file'].Add(self.ctrls[CTRL_TREE_FILE], 0, wx.EXPAND)
        box_sizers['dclick'].Add(self.ctrls[CTRL_USE],0, wx.EXPAND)
        box_sizers['dclick'].Add(wx.Size(10,10))
        box_sizers['dclick'].Add(self.ctrls[CTRL_DESIGN],0, wx.EXPAND)
        box_sizers['dclick'].Add(wx.Size(10,10))
        box_sizers['dclick'].Add(self.ctrls[CTRL_CHAT],0, wx.EXPAND)
        box_sizers['dclick'].Add(wx.Size(10,10))
        box_sizers['dclick'].Add(self.ctrls[CTRL_PRINT],0, wx.EXPAND)

        # buttons
        sizers['but'].Add(wx.Button(self, wx.ID_OK, "Apply"), 1, wx.EXPAND)
        sizers['but'].Add(wx.Size(10,10))
        sizers['but'].Add(wx.Button(self, wx.ID_CANCEL, "Cancel"), 1, wx.EXPAND)
        sizers['main'].Add(box_sizers['save'], 1, wx.EXPAND)
        sizers['main'].Add(box_sizers['file'], 1, wx.EXPAND)
        sizers['main'].Add(box_sizers['dclick'], 1, wx.EXPAND)
        sizers['main'].Add(sizers['but'], 0, wx.EXPAND|wx.ALIGN_BOTTOM)

        #sizers['main'].SetDimension(10,10,csize[0]-20,csize[1]-20)
        self.SetSizer(sizers['main'])
        self.SetAutoLayout(True)
        self.Fit()
        self.Bind(wx.EVT_BUTTON, self.on_ok, id=wx.ID_OK)

    @debugging
    def on_ok(self,evt):
        settings.set("gametree",self.ctrls[CTRL_TREE_FILE].GetValue())
        settings.set("SaveGameTreeOnExit",str(self.ctrls[CTRL_YES].GetValue()))
        if self.ctrls[CTRL_USE].GetValue():
            settings.set("treedclick","use")
        elif self.ctrls[CTRL_DESIGN].GetValue():
            settings.set("treedclick","design")
        elif self.ctrls[CTRL_PRINT].GetValue():
            settings.set("treedclick","print")
        elif self.ctrls[CTRL_CHAT].GetValue():
            settings.set("treedclick","chat")
        self.EndModal(wx.ID_OK)

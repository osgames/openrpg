#!/usr/bin/python2.1
# Copyright (C) 2000-2001 The OpenRPG Project
#
#        openrpg-dev@lists.sourceforge.net
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# --
#
# File: mplay_server.py
#!/usr/bin/env python
# Author: Chris Davis
# Maintainer:
# Version:
#   $Id: mplay_server.py,v 1.155 2008/01/24 03:52:03 digitalxero Exp $
#
# Description: This file contains the code for the server of the multiplayer
# features in the orpg project.
#


# 04-15-2005 [Snowdog]: Added patch from Brandan Yares (xeriar). Reference: patch tracker id #1182076

from __future__ import with_statement

"""
<msg to='' from='' group_id='' />
<player id='' ip='' group_id='' name='' action='new,del,group,update' status="" version=""/>
<group id='' name='' pwd='' players='' action='new,del,update' />
<create_group from='' pwd='' name='' />
<join_group from='' pwd='' group_id='' />
<role action='set,get,display' player='' group_id='' boot_pwd='' role=''/>
"""


import gc
import cgi
import sys
import string
import time
import urllib

from threading import Lock, RLock
from struct import pack, unpack, calcsize
import traceback
import re

from mplay_client import *
from mplay_client import MPLAY_LENSIZE
from orpg.mapper.map_msg import *
from meta_server_lib import *
from orpg.dirpath import dir_struct
from orpg.tools.validate import validate
from orpg.tools.settings import settings
from orpg.tools.orpg_log import logger
from orpg.tools.decorators import debugging
from orpg.networking.mplay_messaging import messaging

from orpg.external import json

from orpg.external.etree.ElementTree import ElementTree, Element
from orpg.external.etree.ElementTree import fromstring, tostring, iselement
from xml.parsers.expat import ExpatError

# Import the minidom XML module
from xml.dom import minidom

# Snag the version number
from orpg.orpg_version import *

#Plugins
from server_plugins import ServerPlugins

def id_compare(a,b):
    "converts strings to intergers for list sort comparisons for group and player ids so they end up in numeric order"
    return cmp(int(a),int(b))


class game_group(object):
    def __init__(self, id, name, pwd, desc="", boot_pwd="", minVersion="",
                  mapFile=None, messageFile=None, persist=0):
        self.id = id
        self.name = name
        self.desc = desc
        self.minVersion = minVersion
        self.messageFile = messageFile
        self.players = []
        self.pwd = pwd
        self.boot_pwd = boot_pwd
        self.game_map = map_msg()
        self.lock = Lock()
        self.moderated = 0
        self.voice = {}
        self.persistant = persist
        self.mapFile = None

        if mapFile != None:
            self.mapFile = mapFile
            f = open( mapFile )
            tree = f.read()
            f.close()

        else:
            f = open(dir_struct["template"] + "default_map.xml")
            tree = f.read()
            f.close()

        self.game_map.init_from_xml(tree)

    def save_map(self):
        if self.mapFile is not None and self.persistant == 1 and self.mapFile.find("default_map.xml") == -1:
            f = open(self.mapFile, "w")
            f.write(self.game_map.get_all_xml())
            f.close()

    def add_player(self,id):
        self.players.append(id)

    def remove_player(self,id):
        if self.voice.has_key(id):
            del self.voice[id]
        self.players.remove(id)

    def get_num_players(self):
        num =  len(self.players)
        return num

    def get_player_ids(self):
        tmp = self.players
        return tmp

    def check_pwd(self,pwd):
        return (pwd==self.pwd)

    def check_boot_pwd(self,pwd):
        return (pwd==self.boot_pwd)

    def check_version(self,ver):
        if (self.minVersion == ""):
            return 1
        minVersion=self.minVersion.split('.')
        version=ver.split('.')
        for i in xrange(min(len(minVersion),len(version))):
            w=max(len(minVersion[i]),len(version[i]))
            v1=minVersion[i].rjust(w);
            v2=version[i].rjust(w);
            if v1<v2:
                return 1
            if v1>v2:
                return 0

        if len(minVersion)>len(version):
            return 0
        return 1

    def has_pwd(self):
        return self.pwd != ""

    #depreciated - see send_group_list()
    def toxml(self, act="new"):
        el = messaging.build('group', id=self.id, name=self.name,
                             pwd=self.has_pwd(), action=act,
                             players=self.get_num_players())
        return el



class client_stub(client_base):
    def __init__(self,inbox,sock,props,log):
        client_base.__init__(self)
        self.ip = props['ip']
        self.role = props['role']
        self.id = props['id']
        self.group_id = props['group_id']
        self.name = props['name']
        self.version = props['version']
        self.protocol_version = props['protocol_version']
        self.client_string = props['client_string']
        self.inbox = inbox
        self.sock = sock
        self.timeout_time = None
        self.log_console = log
        self.ignorelist = {}

    # implement from our base class
    def isServer( self ):
        return 1

    def clear_timeout(self):
        self.timeout_time = None

    def check_time_out(self):
        if self.timeout_time==None:
            self.timeout_time = time.time()
        curtime = time.time()
        diff = curtime - self.timeout_time
        if diff > 1800:
            return 1
        else:
            return 0

    def send(self, msg, player, group):
        if self.get_status() == MPLAY_CONNECTED:
            el = messaging.build('msg', to=player, _from='0',
                                         group_id=self.group_id)
            try:
                el1 = fromstring(msg)
                el.append(el1)
            except ExpatError:
                chatel = messaging.build('chat', _type='1', version='1.0')
                chatel.text = msg
                el.append(chatel)

            self.outbox.put(el)

    def change_group(self, group_id, groups):
        old_group_id = str(self.group_id)
        groups[group_id].add_player(self.id)
        groups[old_group_id].remove_player(self.id)
        self.group_id = group_id
        self.outbox.put(self.toxml('group'))
        msg = groups[group_id].game_map.get_all_xml()
        self.send(msg, self.id, group_id)
        return old_group_id

    def self_message(self, act):
        self.send(act, self.id, self.group_id)

    def take_dom(self, etreeEl):
        self.name = etreeEl.get("name")
        self.text_status = etreeEl.get("status")


######################################################################
######################################################################
##
##
##   MPLAY SERVER
##
##
######################################################################
######################################################################

class mplay_server:
    def __init__(self, log_console=None, name=None):
        self.log_to_console = 1
        self.log_console = log_console
        self.alive = 1
        self.players = {}
        self.listen_event = Event()
        self.incoming_event = Event()
        self.incoming = Queue.Queue(0)
        self.p_lock = RLock()
        self.next_player_id = 1
        self.plugin_player_id = -1
        self.next_group_id = 100
        self.metas = {}              #  This holds the registerThread objects for each meta
        self.be_registered = 0       #  Status flag for whether we want to be registered.
        self.serverName = name            #  Name of this server in the metas
        self.reg = None
        self.boot_pwd = ""
        self.server_address = None # IP or Name of server to post to the meta. None means the meta will auto-detect it.
        self.defaultMessageFile = None
        self.userPath = dir_struct["user"]
        self.lobbyMapFile = "Lobby_map.xml"
        self.lobbyMessageFile = "LobbyMessage.html"
        self.banFile = "ban_list.xml"
        self.show_meta_messages = 0
        self.log_network_messages = 0
        self.allow_room_passwords = 1
        self.silent_auto_kick = 0
        self.zombie_time = 480 #time in minutes before a client is considered a ZOMBIE
        self.minClientVersion = SERVER_MIN_CLIENT_VERSION
        self.maxSendSize = 1024
        self.server_port = OPENRPG_PORT
        self.allowRemoteKill = False
        self.allowRemoteAdmin = True
        self.sendLobbySound = False
        self.lobbySound = 'http://www.digitalxero.net/music/mus_tavern1.bmu'

    def initServer(self, **kwargs):
        for atter, value in kwargs.iteritems():
            setattr(self, atter, value)

        validate.config_file(self.lobbyMapFile, "default_Lobby_map.xml")
        validate.config_file(self.lobbyMessageFile,
                                   "default_LobbyMessage.html")
        self.server_start_time = time.time()

        # Since the server is just starting here, we read in the XML configuration
        # file.  Notice the lobby is still created here by default.
        self.groups = {'0': game_group('0','Lobby','','The game lobby', '',
                                        '', self.userPath + self.lobbyMapFile,
                                        self.userPath + self.lobbyMessageFile,
                                        1)}
        # Make sure the server's name gets set, in case we are being started from
        # elsewhere.  Basically, if it's passed in, we'll over ride what we were
        # prompted for.  This should never really happen at any rate.

        self.initServerConfig()
        self.listen_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.listen_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.listen_thread = thread.start_new_thread(self.listenAcceptThread,
                                                     (0,))
        self.in_thread = thread.start_new_thread(self.message_handler, (0,))

        #  Starts the player reaper thread.  See self.player_reaper_thread_func() for more explanation
        self.player_reaper_thread = thread.start_new_thread(
            self.player_reaper_thread_func, (0,))
        thread.start_new_thread(self.PluginThread, ())
        self.svrcmds = {}
        self.initsvrcmds()
        self.ban_list = {}
        self.initBanList()

    def addsvrcmd(self, cmd, function):
        if not self.svrcmds.has_key(cmd):
            self.svrcmds[cmd] = {}
            self.svrcmds[cmd]['function'] = function

    def initsvrcmds(self):
        self.addsvrcmd('msg', self.incoming_msg_handler)
        self.addsvrcmd('player', self.incoming_player_handler)
        self.addsvrcmd('admin', self.remote_admin_handler)
        self.addsvrcmd('alter', self.do_alter)
        self.addsvrcmd('role', self.do_role)
        self.addsvrcmd('ping', self.do_ping)
        self.addsvrcmd('system', self.do_system)
        self.addsvrcmd('join_group', self.join_group)
        self.addsvrcmd('create_group', self.create_group)
        self.addsvrcmd('moderate', self.moderate_group)
        self.addsvrcmd('plugin', self.plugin_msg_handler)
        self.addsvrcmd('sound', self.sound_msg_handler)


    # This method reads in the server's ban list added by Darren
    def initBanList( self ):
        self.log_msg("Processing Ban List File...")

        # make sure the server_ini.xml exists!
        validate.config_file(self.banFile, "default_ban_list.xml")

        # try to use it.
        try:
            with open(dir_struct['user'] + 'ban_list.xml') as f:
                bl = fromstring(f.read())

            for banned in bl.findall('banned'):
                ip = banned.get('ip')
                self.ban_list[ip] = banned.get('name')
        except Exception, e:
            self.log_msg("Exception in initBanList() " + str(e))

    # This method writes out the server's ban list added by Darren
    def saveBanList( self ):
        self.log_msg("Saving Ban List File...")

        # try to use it.
        try:
            root = messaging.build('server')
            for ip, name in self.ban_list.iteritems():
                banned = messaging.build('banned', name=name, ip=ip)
                root.append(banned)

            with open(dir_struct['user'] + self.banFile ,"w") as f:
                f.write(tostring(root))
        except Exception, e:
            self.log_msg("Exception in saveBanList() " + str(e))

    # This method reads in the server's configuration file and reconfigs the server
    # as needed, over-riding any default values as requested.
    def initServerConfig(self):
        self.log_msg("Processing Server Configuration File... " + self.userPath)
        # make sure the server_ini.xml exists!
        validate.config_file("server_ini.xml", "default_server_ini.xml")
        # try to use it.
        try:
            with open(dir_struct['user'] + 'server_ini.xml') as f:
                config = fromstring(f.read())

            if hasattr(self, 'bootPassword'):
                # if we are coming from the gui server just take the password that was passed in,
                # even if it's a blank string
                self.boot_pwd = self.bootPassword
            else:
                # Obtain the lobby/server password if it's been specified
                self.boot_pwd = config.get('admin') or config.get('boot', '')

                if len(self.boot_pwd) < 1:
                    self.boot_pwd = raw_input("Enter admin password:  ")

            self.reg = self.reg or config.get('register', '')

            if not len(self.reg) > 0 or self.reg[0].upper() not in ("Y", "N"):
                opt = raw_input("Do you want to post your server to the OpenRPG Meta Server list? (y,n) ")
                if len(opt) and (opt[0].upper() == 'Y'):
                    self.reg = 'Y'
                else:
                    self.reg = 'N'

            LobbyName = config.get('lobbyname', 'Lobby')

            map_node = config.iter_find('map')
            msg_node = config.iter_find('message')
            mapFile = map_node.get('file',
                                   'Lobby_map.xml').replace("myfiles/", "")
            msgFile = msg_node.get('file',
                                   'LobbyMessage.html').replace("myfiles/", "")

            # Update the lobby with the passwords if they've been specified
            if len(self.boot_pwd):
                self.groups = {'0': game_group('0', LobbyName, "",
                                               'The game lobby', self.boot_pwd,
                                               "", dir_struct['user'] + mapFile,
                                                dir_struct['user'] + msgFile,
                                                1)}

            # set ip or dns name to send to meta server
            service_node = config.iter_find('service')
            address = service_node.get('address')
            address = address.lower()
            if address in ["", "hostname/address", "localhost"]:
                self.server_address = None
            else:
                self.server_address = address
            self.server_port = int(service_node.get("port", OPENRPG_PORT))

            self.serverName = config.get('name')

            if self.reg[0].upper() == "Y":
                if not self.serverName:
                    self.serverName = raw_input("Server Name? ")
                self.register()


            # Get the minimum openrpg version from config if available
            # if it isn't set min version to internal default.
            #
            # server_ini.xml entry for version tag...
            # <version min="x.x.x">
            mver = config.iter_find('version')
            if mver is not None:
                self.minClientVersion = mver.get('min',
                                                 SERVER_MIN_CLIENT_VERSION)
            else:
                self.minClientVersion = SERVER_MIN_CLIENT_VERSION

            self.defaultMessageFile = ""
            #-----------[ START <AUTOKICK> TAG PROCESSING ]--------------
            # Auto-kick option defaults for silent booting and
            # setting the default zombie-client delay time --Snowdog 9/05
            #
            # server_ini.xml entry for autikick tag...
            # <autokick silent=["no","yes"] delay="(# of seconds)">
            ak = config.iter_find('autokick')
            self.silent_auto_kick = False
            self.zombie_time = 480
            if ak is not None:
                self.silent_auto_kick = False
                if ak.get('silent', 'no').lower() == 'yes':
                    self.silent_auto_kick = True
                self.zombie_time = int(ak.get('delay', 480))

            alk = ""
            if self.silent_auto_kick:
                alk = "(Silent Mode)"
            self.log_msg("Auto Kick:  Delay="+str(self.zombie_time) + " " + alk)
            #------------------[ END <AUTOKICK> TAG PROCESSING ]--------------



            #--------------[ START <ROOM_DEFAULT> TAG PROCESSING ]----------
            # New room_defaults configuration option used to set various defaults
            # for all user created rooms on the server. Incorporates akomans older
            # default room message code (from above)      --Snowdog 11/03
            #
            # option syntax
            # <room_defaults passwords="yes" map="myfiles/LobbyMap.xml"
            # message="myfiles/LobbyMessage.html" />

            #default settings for tag options...
            roomdefault_msg = str(self.defaultMessageFile)
            roomdefault_map = ""
            roomdefault_pass = True


            #pull information from config file DOM
            roomdefaults = config.iter_find('room_defaults')
            rs = roomdefaults.iter_find('passwords')
            if rs is not None and rs.get('allow', 'yes').lower() in ['no',
                                                                     '0']:
                roomdefault_pass = False

            rs = roomdefaults.iter_find('map')
            if rs is not None and rs.get('file'):
                new = rs.get('file').replace("myfiles/", "")
                roomdefault_map = dir_struct['user'] + new

            rs = roomdefaults.iter_find('message')
            if rs is not None and rs.get('file'):
                new = rs.get('file').replace("myfiles/", "")
                roomdefault_msg = dir_struct['user'] + new


            #set the defaults
            self.defaultMessageFile = roomdefault_msg or None
            self.defaultMapFile = roomdefault_map or None
            self.allow_room_passwords = roomdefault_pass or False
            #----------[ END <ROOM_DEFAULT> TAG PROCESSING ]----------

            ###Server Cheat message
            self.cheat_msg = "**FAKE ROLL**"
            cheat_node = config.iter_find('cheat')
            if cheat_node is not None:
                self.cheat_msg = cheat_node.get('text', '**FAKE ROLL**')

            # should validate protocal
            vp_node = config.iter_find('validate_protocol')

            self.validate_protocol = True

            if vp_node is not None:
                if vp_node.get('value').lower() not in ['true', 'yes', '1']:
                    self.validate_protocol = False

            self.makePersistentRooms(config)

            self.log_msg("Server Configuration File: Processing Completed.")

        except Exception, e:
            logger.exception(traceback.format_exc())
            self.log_msg("Exception in initServerConfig() " + str(e))


    def makePersistentRooms(self, config):
        """
        Creates rooms on the server as defined in the server config file.
        """
        for room in config.findall('room'):
            name = room.get('name')
            pwd = room.get('password')
            admin = room.get('admin') or room.get('boot')
            minVer = room.get('minVersion', '')
            mapFile = self.defaultMapFile
            msgFile = self.defaultMessageFile
            moderated = False

            map_node = room.iter_find('map')
            if map_node is not None:
                mapFile = dir_struct['user'] + map_node.get('file').\
                        replace("myfiles/", "")

            msg_node = room.iter_find('message')
            if msg_node is not None:
                msgFile = msg_node.get('file')

                if msgFile[:4] != 'http':
                    msgFile = dir_struct['user'] + msgFile.\
                                replace("myfiles/", "")

            if room.get('moderated', '').lower() in ['true', 'yes', '1']:
                moderated = True

            #create the new persistant group
            self.new_group(name, pwd, admin, minVer, mapFile, msgFile,
                           persist=True, moderated=moderated)

    def isPersistentRoom(self, id):
        """
        Returns True if the id is a persistent room (other than the lobby), otherwise, False.
        """
        try:
            id = str(id)
            if id not in self.groups:
                return False

            pr = (self.groups[id]).persistant
            return pr
        except:
            self.log_msg("Exception occured in isPersistentRoom(self,id)")
            return False

    #-----------------------------------------------------
    #  Toggle Meta Logging  -- Added by Snowdog 4/03
    #-----------------------------------------------------
    def toggleMetaLogging(self):
        if self.show_meta_messages != 0:
            self.log_msg("Meta Server Logging: OFF")
            self.show_meta_messages = 0
        else:
            self.log_msg("Meta Server Logging: ON")
            self.show_meta_messages = 1


    #-----------------------------------------------------
    #  Start/Stop Network Logging to File  -- Added by Snowdog 4/03
    #-----------------------------------------------------
    def NetworkLogging(self, mode=0):
        if mode == 0:
            self.log_msg("Network Logging: OFF")
            self.log_network_messages = 0
        elif mode == 1:
            self.log_msg("Network Logging: ON (composite logfile)")
            self.log_network_messages = 1
        elif mode == 2:
            self.log_msg("Network Logging: ON (split logfiles)")
            self.log_network_messages = 2
        else: return
        #when log mode changes update all connection stubs
        for n in self.players:
            try:
                self.players[n].EnableMessageLogging = mode
            except:
                self.log_msg("Error changing Message Logging Mode for client #" + str(self.players[n].id))

    def NetworkLoggingStatus(self):
        if self.log_network_messages == 0:
            return "Network Traffic Log: Off"
        elif self.log_network_messages == 1:
            return "Network Traffic Log: Logging (composite file)"
        elif self.log_network_messages == 2:
            return "Network Traffic Log: Logging (inbound/outbound files)"
        else:
            self.log_msg("Network Traffic Log: [Unknown]")

    def register_callback(instance, xml_dom=None, source=None):
        #FIXME#
        if xml_dom:    # if we get something
            if source == getMetaServerBaseURL():    # if the source of this DOM is the authoritative meta
                try:
                    metacache_lock.acquire()
                    curlist = getRawMetaList()      #  read the raw meta cache lines into a list
                    updateMetaCache(xml_dom)        #  update the cache from the xml
                    newlist = getRawMetaList()      #  read it into a second list
                finally:
                    metacache_lock.release()

                if newlist != curlist:          #  If the two lists aren't identical
                                                #  then something has changed.
                    instance.register()             #  Call self.register()
                                                #  which will force a re-read of the meta cache and
                                                #  redo the registerThreads
        else:
            instance.register()

                # Eventually, reset the MetaServerBaseURL here

    ## Added to help clean up parser errors in the XML on clients
    ## due to characters that break welformedness of the XML from
    ## the meta server.
    ## NOTE: this is a stopgap measure -SD
    def clean_published_servername(self, name):
        #clean name of all apostrophes and quotes
        badchars = "\"\\`><"
        for c in badchars:
            name = name.replace(c,"")
        return name

    def registerRooms(self, args=None):
        rooms = ''
        id = '0'
        time.sleep(500)
        data = {'rooms': {}}
        for rnum in self.groups.keys():
            room = {str(rnum): {
                'name': self.groups[rnum].name,
                'pwd': str(self.groups[rnum].pwd != ""),
                'players': {}
            }}

            for pid in self.groups[rnum].players:
                player = {str(pid): self.players[pid].name}
                room[str(rnum)]['players'].update(player)

            data['rooms'].update(room)


        for meta in self.metas.keys():
            while id == '0':
                id, cookie = self.metas[meta].getIdAndCookie()

            data['server_id'] = id

            out = urllib.urlencode({'act': 'registerrooms',
                                    'format': 'json',
                                    'data': json.dumps(data)})
            get_server_dom(out, self.metas[meta].path)


    def register(self,name_given=None):
        if name_given == None:
            name = self.serverName
        else:
            self.serverName = name = name_given

        name = self.clean_published_servername(name)

        #  Set up the value for num_users
        if self.players:
            num_players = len(self.players)
        else:
            num_players = 0

        #  request only Meta servers compatible with version 2
        metalist = getMetaServers(versions=["2"])
        if self.show_meta_messages != 0:
            self.log_msg("Found these valid metas:")
            for meta in metalist:
                self.log_msg("Meta:" + meta)

        #  Go through the list and see if there is already a running register
        #  thread for the meta.
        #  If so, call it's register() method
        #  If not, start one, implicitly calling the new thread's register() method


        #  iterate through the currently running metas and prune any
        #  not currently listed in the Meta Server list.
        if self.show_meta_messages != 0:
            self.log_msg( "Checking running register threads for outdated metas.")
        for meta in self.metas.keys():
            if self.show_meta_messages != 0:
                self.log_msg("meta:" + meta + ": ")

            if not meta in metalist:  # if the meta entry running is not in the list
                if self.show_meta_messages != 0:
                    self.log_msg( "Outdated.  Unregistering and removing")
                self.metas[meta].unregister()
                del self.metas[meta]
            else:
                if self.show_meta_messages != 0:
                    self.log_msg( "Found in current meta list.  Leaving intact.")

        #  Now call register() for alive metas or start one if we need one
        for meta in metalist:
            if self.metas.has_key(meta) and self.metas[meta] and self.metas[meta].isAlive():
                self.metas[meta].register(name=name, realHostName=self.server_address, num_users=num_players)
            else:
                self.metas[meta] = registerThread(name=name, realHostName=self.server_address, num_users=num_players, MetaPath=meta, port=self.server_port,register_callback=self.register_callback)
                self.metas[meta].start()

        #The register Rooms thread

        self.be_registered = 1
        thread.start_new_thread(self.registerRooms,(0,))

    def unregister(self):
        #  loop through all existing meta entries
        #  Don't rely on getMetaServers(), as a server may have been
        #  removed since it was started.  In that case, then the meta
        #  would never get unregistered.
        #
        #  Instead, loop through all existing meta threads and unregister them

        for meta in self.metas.values():
            if meta and meta.isAlive():
                meta.unregister()

        self.be_registered = 0

    #  This method runs as it's own thread and does the group_member_check every
    #    sixty seconds.  This should eliminate zombies that linger when no one is
    #    around to spook them.  GC: Frequency has been reduced as I question how valid
    #    the implementation is as it will only catch a very small segment of lingering
    #    connections.
    def player_reaper_thread_func(self, arg):
        while self.alive:
            time.sleep(60)

            with self.p_lock:
                for group in self.groups.keys():
                    self.check_group_members(group)

    #This thread runs ever 250 miliseconds, and checks various plugin stuff
    def PluginThread(self):
        while self.alive:
            with self.p_lock:
                players = ServerPlugins.getPlayer()

                for player in players:
                    if player is not None:
                        #Do something here so they can show up in the chat room for non web users'
                        pass

                data = ServerPlugins.preParseOutgoing()

                for msg in data:
                    try:
                        el = messaging.parse(msg)

                        if el.get('from') and int(el.get('from')) > -1:
                            el.set('from', '-1')

                        el.set('to', 'all')
                        self.incoming_msg_handler(el, msg)
                    except:
                        pass

            time.sleep(0.0250)

    def sendMsg(self, sock, msg, useCompression=False, cmpType=None):
        """Very simple function that will properly encode and send a message to te
        remote on the specified socket."""

        if iselement(msg):
            msg = tostring(msg)

        if useCompression and cmpType != None:
            msg = cmpType.compress(msg)

        lpacket = pack('!i', len(msg))
        sock.send(lpacket)

        offset = 0
        while offset < len(msg):
            slice = buffer(msg, offset, len(msg)-offset)
            sent = sock.send(slice)
            offset += sent

    def recvData(self, sock, readSize):
        """Simple socket receive method.  This method will only return when the exact
        byte count has been read from the connection, if remote terminates our
        connection or we get some other socket exception."""
        data = ""
        offset = 0
        try:
            while offset != readSize:
                frag = sock.recv(readSize - offset)
                # See if we've been disconnected
                rs = len( frag )
                if rs <= 0:
                    # Loudly raise an exception because we've been disconnected!
                    raise IOError, "Remote closed the connection!"
                else:
                    # Continue to build complete message
                    offset += rs
                    data += frag
        except socket.error, e:
            self.log_msg(e)
            data = ""
        return data

    def recvMsg(self, sock, useCompression=False, cmpType=None):
        """This method now expects to receive a message having a
        4-byte prefix length.  It will ONLY read completed messages.
        In the event that the remote's connection is terminated, it will
        throw an exception which should allow for the caller to more
        gracefully handles this exception event.

        Because we use strictly reading ONLY based on the length that is
        told to use, we no longer have to worry about partially adjusting
        for fragmented buffers starting somewhere within a buffer that we've
        read.  Rather, it will get ONLY a whole message and nothing more.
        Everything else will remain buffered with the OS until we attempt to
        read the next complete message."""

        msgData = ""
        try:
            lenData = self.recvData(sock, MPLAY_LENSIZE)

            # Now, convert to a usable form
            (length,) = unpack('!i', lenData)

            # Read exactly the remaining amount of data
            msgData = self.recvData(sock, length)

            try:
                if useCompression and cmpType != None:
                    msgData = cmpType.decompress(msgData)
            except Exception:
                logger.exception(traceback.format_exc())

        except Exception, e:
            self.log_msg("Exception: recvMsg(): " + str(e))

        return msgData

    def kill_server(self):
        #FIXME#
        self.alive = 0
        self.log_msg("Server stopping...")
        self.unregister()                    # unregister from the Meta
        for p in self.players.itervalues():
            p.disconnect()
            self.incoming.put("<system/>")

        for g in self.groups.itervalues():
            g.save_map()

        try:
            ip = socket.gethostbyname(socket.gethostname())
            kill = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            kill.connect((ip, self.server_port))

            # Now, send the "system" command using the correct protocol format
            self.sendMsg( kill, "<system/>" )
            kill.close()
        except:
            pass

        self.listen_sock.close()
        self.listen_event.wait(10)
        self.incoming_event.wait(10)
        self.log_msg("Server stopped!")

    def log_msg(self, msg):
        logger.note(msg)

    def print_help(self):
        #FIXME#
        logger.info('', True)
        logger.info("Commands: ", True)
        logger.info("'kill' or 'quit' - to stop the server", True)
        logger.info("'broadcast' - broadcast a message to all players", True)
        logger.info("'list' - list players and groups", True)
        logger.info("'dump' - to dump player data", True)
        logger.info("'dump groups' - to list the group names and ids only",
                    True)
        logger.info("'group n' - to list details about one group only", True)
        logger.info("'register' - To register the server as name.  "\
                    "Also used to change the server's name if registered.",
                    True)
        logger.info("'unregister' - To remove this server from the "\
                    "list of servers", True)
        logger.info("'get lobby boot password' - to show the Lobby's "\
                    "boot password", True)
        logger.info("'set lobby boot password' - to set the Lobby's "\
                    "boot password", True)
        logger.info("'log' - toggles logging to the console off or on", True)
        logger.info("'log meta' - toggles logging of meta server "\
                    "messages on or off", True)
        logger.info("'logfile [off|on|split]' - timestamped network "\
                    "traffic log", True)
        logger.info("'remove room' - to remove a room from the server", True)
        logger.info("'kick' - kick a player from the server", True)
        logger.info("'ban' - ban a player from the server", True)
        logger.info("'remotekill' - This will toggle the ability to kill "\
                    "the server via the /admin command", True)
        logger.info("'monitor (#)' - monitors raw network I/O stream "\
                    "to specific client", True)
        logger.info("'purge clients' - boots all connected clients "\
                    "off the server immediately", True)
        logger.info("'zombie [set [min]]' - view/set the auto-kick time "\
                    "for zombie clients", True)
        logger.info("'uptime' - reports how long server has been running",
                    True)
        logger.info("'roompasswords' - allow/disallow room passwords (toggle)",
                    True)
        logger.info("'search' - will prompt for pattern and display results",
                    True)
        logger.info("'sendsize' - will ajust the send size limit", True)
        logger.info("'remoteadmin' - will toggle remote admin commands", True)
        logger.info("'togglelobbysound' - Will turn on or off the "\
                    "Auto sending of a sound to all players who join the loby",
                    True)
        logger.info("'lobbysound' - Lets you specify which sound file "\
                    "to send to players joining the lobby", True)
        logger.info("'help' or '?' or 'h' - for this help message", True)
        logger.info('', True)

    def broadcast(self, msg):
        chatel = messaging.build('chat', _type='1', version='1.0')
        chatel.text = '<font color="#FF0000">' + msg + '</font>'
        el = messaging.build('msg', to='all', _from='0', group_id='1')
        el.append(chatel)
        self.send_to_all(0, el)

    def console_log(self):
        self.log_to_console = not self.log_to_console
        if not self.log_to_console:
            logger.info("console logging now off", True)
            logger.log_to_consol = False
        else:
            logger.info("console logging now on", True)
            logger.log_to_consol = True

    def groups_list(self):
        #CLEANUP#
        with self.p_lock:
            try:
                keys = self.groups.keys()
                for k in keys:
                    pw = "-"
                    pr = " -"
                    if self.groups[k].pwd != "":
                        pw = "P"
                    if self.isPersistentRoom( k ):
                        pr = " S" #using S for static (P for persistant conflicts with password)
                    logger.info("Group: " + k + pr + pw + '  Name: ' + self.groups[k].name, True)
                print

            except Exception, e:
                self.log_msg(str(e))

    def monitor(self, pid, mode=1 ):
        """
        allows monitoring of a specific user(s) network i/o
        """
        #if mode is not set to 1 then monitor adds toggles the state
        #of monitoring on the given user

        if (mode == 1):
            for p in self.players:
                try: p.monitor("off")
                except: pass
        try:
            r = (self.players[pid]).set_traffic_monitor("toggle")
            self.log_msg("Monitor: Mode=" + str(r) + " on Player #" + str(pid))
        except:
            self.log_msg("Monitor: Invalid Player ID")
            logger.exception(traceback.format_exc())

    def search(self,patern):
        keys = self.groups.keys()
        logger.info("Search results:", True)
        for k in keys:
            ids = self.groups[k].get_player_ids()
            for id in ids:
                if self.players[id].id.find(patern)>-1:
                    self.print_player_info(self.players[id])

                elif self.players[id].name.find(patern)>-1:
                    self.print_player_info(self.players[id])

                elif self.players[id].ip.find(patern)>-1:
                    self.print_player_info(self.players[id])

                elif self.players[id].group_id.find(patern)>-1:
                    self.print_player_info(self.players[id])

                elif self.players[id].role.find(patern)>-1:
                    self.print_player_info(self.players[id])

                elif self.players[id].version.find(patern)>-1:
                    self.print_player_info(self.players[id])

                elif self.players[id].protocol_version.find(patern)>-1:
                    self.print_player_info(self.players[id])

                elif self.players[id].client_string.find(patern)>-1:
                    self.print_player_info(self.players[id])

    def print_player_info(self, player):
        msg = '{player.id}, {player.name}, {player.ip}, {player.group_id}, '\
            '{player.role}, {player.version}, {player.protocol_version}, '\
            '{player.client_string}'.format(player=player)
        logger.info(msg, True)

    def uptime(self , mode = 0):
        """
        returns string containing how long server has been in operation
        """
        ut = time.time() - self.server_start_time
        d = int(ut/86400)
        h = int( (ut-(86400*d))/3600 )
        m = int( (ut-(86400*d)-(3600*h))/60)
        s = int( (ut-(86400*d)-(3600*h)-(60*m)) )
        uts =  str( "This server has been running for:\n " + str(d) + " days  " + str(h) + " hours  " + str(m) + " min. " + str(s) + " sec.  [" + str(int(ut)) + " seconds]")
        if mode == 0:
            logger.debug(uts, True)
        else:
            return uts

    def RoomPasswords(self):
        self.allow_room_passwords = not self.allow_room_passwords
        if not self.allow_room_passwords:
            return "Client Created Room Passwords: Disallowed"
        else:
            return "Client Created Room Passwords: Allowed"


    def group_dump(self, k):
        with self.p_lock:
            try:
                msg = ['Group: {group.id}']
                msg.append('    Name: {group.name}')
                msg.append('    Desc: {group.desc}')
                msg.append('    Pass: {group.pwd}')
                msg.append('    Boot: {group.boot_pwd}')
                msg.append('    Moderated: {group.moderated}')
                msg.append('    Map: {map_xml}\n')
                logger.info('\n'.join(msg).format(group=self.groups[k],
                        map_xml=self.groups[k].game_map.get_all_xml()), True)
            except Exception, e:
                self.log_msg(str(e))

    def player_list(self):
        #CLEANUP#
        """
        display a condensed list of players on the server
        """
        with self.p_lock:
            try:
                logger.info("------------[ PLAYER LIST ]------------", True)
                keys = self.groups.keys()
                keys.sort(id_compare)
                for k in keys:
                    groupstring = "Group " + str(k)  + ": " +  self.groups[k].name
                    if self.groups[k].pwd != "":
                        groupstring += " (Pass: \"" + self.groups[k].pwd + "\" )"
                    logger.info(groupstring, True)
                    ids = self.groups[k].get_player_ids()
                    ids.sort(id_compare)
                    for id in ids:
                        if self.players.has_key(id):
                            msg = "  ({player.id}){player.name} "\
                                "[IP: {player.ip}] {idle_status} "\
                                "({connected})".format(player=self.players[id],
                                                       idle_status=(self.players[id]).idle_status(),
                                                       connected=(self.players[id]).connected_time_string())
                            logger.info(msg, True)
                        else:
                            self.groups[k].remove_player(id)
                            msg = "Bad Player Ref (#{id}) in group".format(id=id)
                            logger.general(msg, True)
                    if len(ids) > 0:
                        logger.info('', True)
                logger.info("--------------------------------------", True)
                msg = "\nStatistics: groups: {num_groups} "\
                    "players: {num_players}".format(num_groups=len(self.groups),
                                                    num_players=len(self.players))
                logger.info(msg, True)
            except Exception, e:
                self.log_msg(str(e))

    def player_dump(self):
        #CLEANUP
        with self.p_lock:
            try:
                keys = self.groups.keys()
                for k in keys:
                    msg = 'Group: {group.id} {group.name} (pass: "{group.pwd}"'

                    logger.info(msg.format(group=self.groups[k]), True)

                    ids = self.groups[k].get_player_ids()
                    for id in ids:
                        if self.players.has_key(id):
                            logger.info(self.players[id], True)
                        else:
                            self.groups[k].remove_player(id)
                            msg = "Bad Player Ref (#{id}) in group"
                            logger.general(msg.format(id=id), True)
            except Exception, e:
                self.log_msg(str(e))

    def update_request(self, newsock, etreeEl):
        # handle reconnects
        self.log_msg("update_request() has been called.")

        # get player id
        id = etreeEl.get("id")
        group_id = etreeEl.get("group_id")

        with self.p_lock:
            if id in self.players:
                self.sendMsg(newsock, self.players[id].toxml("update"),
                             self.players[id].useCompression,
                             self.players[id].compressionType)

                self.players[id].reset(newsock)
                self.players[id].clear_timeout()
                need_new = 0
            else:
                need_new = 1

        if need_new:
            self.new_request(newsock,xml_dom)
        else:
            msg = self.groups[group_id].game_map.get_all_xml()
            self.send(msg, id, group_id)

    def new_request(self,newsock, etreeEl, LOBBY_ID='0'):
        #CLEANUP#
        #build client stub
        props = {}
        # Don't trust what the client tells us...trust what they connected as!
        props['ip'] = socket.gethostbyname(newsock.getpeername()[0])
        props['role'] = etreeEl.get('role', 'GM')
        props['name'] = etreeEl.get("name")
        props['group_id'] = LOBBY_ID
        props['id'] = str(self.next_player_id)
        props['version'] = etreeEl.get("version")
        props['protocol_version'] = etreeEl.get("protocol_version")
        props['client_string'] = etreeEl.get("client_string")
        self.next_player_id += 1
        new_stub = client_stub(self.incoming, newsock, props, self.log_console)
        new_stub.useCompression = bool(etreeEl.get('useCompression'))
        cmpType = etreeEl.get('cmpType')
        if cmpBZ2 and cmpType == 'bz2':
            new_stub.compressionType = bz2
        elif cmpZLIB and cmpType == 'zlib':
            new_stub.compressionType = zlib
        else:
            new_stub.compressionType = None

        #update newly create client stub with network logging state
        new_stub.EnableMessageLogging = self.log_network_messages

        self.sendMsg(newsock, new_stub.toxml("new"), False, None)

        # send confirmation
        data = self.recvMsg(newsock, new_stub.useCompression,
                            new_stub.compressionType)

        chatEl = messaging.build('chat', version='1.0', _type='1')
        try:
            incEl = messaging.parse(data)
        except ExpatError:
            (remote_host, remote_port) = newsock.getpeername()
            msg = ['Your client sent an illegal message to the server and',
                   'will be disconnected. Please report this bug to the',
                   'development Team at:<br />',
                   '<a href="http://sourceforge.net/tracker/?group_id=2237&atid=102237">OpenRPG bugs',
                   '(http://sourceforge.net/tracker/?group_id=2237&atid=102237)</a><br />',]

            chatEl.text = ' '.join(msg)
            el = messaging.build('msg', to=props['id'], _from=props['id'],
                                 group_id=LOBBY_ID)
            el.append(chatEl)
            self.sendMsg(newsock, el, new_stub.useCompression,
                         new_stub.compressionType)

            time.sleep(0.025)
            newsock.close()
            err_msg = ["Error in parse found from ", str(remote_host),
                       ". Disconnected.\n  Offending data(", str(len(data)),
                       "bytes)=", data, "\nException=", str(e)]
            logger.exception(''.join(err_msg))
            return

        #start threads and store player

        allowed = True
        version_string = ""

        if ((props['protocol_version'] != PROTOCOL_VERSION) and self.validate_protocol):
            version_string = "Sorry, this server can't handle your client version. (Protocol mismatch)<br />"
            allowed = False

        if not self.checkClientVersion(props['version']):
            version_string = "Sorry, your client is out of date. <br />"
            version_string += "This server requires your client be version " + self.minClientVersion + " or higher to connect.<br />"
            allowed = False

        if not allowed:
            version_string += '  Please go to <a href="http://openrpg.digitalxero.net">http://openrpg.digitalxero.net</a> to find a compatible client.<br />'
            version_string += "If you can't find a compatible client on the website, chances are that the server is running an unreleased development version for testing purposes.<br />"

            chatEl.text = version_string
            el = messaging.build('msg', to=props['id'], _from='0',
                                 group_id='0')
            el.append(chatEl)
            self.sendMsg(newsock, el, new_stub.useCompression,
                          new_stub.compressionType)
            # Give messages time to flow
            time.sleep(1)
            self.log_msg("Connection terminating due to version incompatibility with client (ver: " + props['version'] + "  protocol: " + props['protocol_version'] + ")" )
            newsock.close()
            return None

        ip = props['ip']
        if ip in self.ban_list:
            banmsg = "You have been banned from this server.<br />"
            cmsg = "Banned Client: (" + str(props['id']) + ") " + str(props['name']) + " [" + str(props['ip']) + "]"
            self.log_msg(cmsg)
            allowed = False

            chatEl.text = banmsg
            el = messaging.build('msg', to=props['id'], _from='0',
                                 group_id='0')
            el.append(chatEl)
            self.sendMsg(newsock, el, new_stub.useCompression,
                         new_stub.compressionType)
            # Give messages time to flow
            time.sleep(1)
            newsock.close()
            return None

        #---- Connection order changed by Snowdog 1/05
        #---- Attempt to register player and send group data
        #---- before displaying lobby message
        #---- Does not solve the Blackhole bug but under some conditions may
        #---- allow for a graceful server response. -SD

        #---- changed method of sending group names to user 8/05
        #---- black hole bug causes the group information to not be sent
        #---- to clients. Not sure why the group messages were being sent to the
        #---- incomming message queue, when they should be sent directly to user
        #---- Does not solve the black hole bug totally -SD

        try:
            if incEl.get("id") == props['id']:
                new_stub.initialize_threads()
                with self.p_lock:
                    self.players[props['id']] = new_stub
                    self.groups[LOBBY_ID].add_player(props['id']) #always add to lobby on connection.
                    self.send_group_list(props['id'])
                    self.send_player_list(props['id'], LOBBY_ID)

                msg = self.groups[LOBBY_ID].game_map.get_all_xml()
                self.send(msg, props['id'], LOBBY_ID)
                self.send_to_group(props['id'], LOBBY_ID, self.players[props['id']].toxml('new'))
                self.return_room_roles(props['id'], LOBBY_ID)

                # Re-initialize the role for this player incase they came from a different server
                self.handle_role("set", props['id'], "GM", self.groups[LOBBY_ID].boot_pwd, LOBBY_ID)

                cmsg = "Client Connect: (" + str(props['id']) + ") " + str(props['name']) + " [" + str(props['ip']) + "]"
                self.log_msg(cmsg)

                #  If already registered then re-register, thereby updating the Meta
                #    on the number of players
                if self.be_registered:
                    self.register()
        except:
            logger.exception(traceback.format_exc())

            #something didn't go right. Notify client and drop the connection
            err_string = "<center>"
            err_string +=  "<hr><b>The server has encountered an error while processing your connection request.</b><hr>"
            err_string += "<br /><i>You are being disconnected from the server.</i><br />"
            err_string += "This error may represent a problem with the server. If you continue to get this message "
            err_string += "please contact the servers administrator to correct the issue.</center> "

            chatEl.text = err_string
            el = messaging.build('msg', to=props['id'], _from=props['id'],
                                 group_id='0')
            el.append(chatEl)
            self.sendMsg(newsock, el, new_stub.useCompression,
                         new_stub.compressionType )
            time.sleep(1)
            newsock.close()
            return

        #  Display the lobby message
        self.SendLobbyMessage(newsock, props['id'])


    def checkClientVersion(self, clientversion):
        minv = self.minClientVersion.split('.')
        cver = clientversion.split('.')
        for i in xrange(min(len(minv),len(cver))):
            w = max(len(minv[i]),len(cver[i]))
            v1 = minv[i].rjust(w);
            v2 = cver[i].rjust(w);
            if v1 < v2:
                return True
            if v1 > v2:
                return False

        if len(minv) > len(cver):
            return False
        return True

    def SendLobbyMessage(self, socket, player_id):
        #######################################################################
        #  Display the lobby message
        #  prepend this server's version string to the the lobby message
        try:
            lobbyMsg = "You have connected to an <a href=\"http://www.openrpg.com\">OpenRPG</a> server, version '" + VERSION + "'"

            # See if we have a server name to report!

            if self.serverName:
                lobbyMsg += ", named '" + self.serverName + "'."

            else:
                lobbyMsg += "."

            # Add extra line spacing
            lobbyMsg += "\n\n"

            try:
                validate.config_file("LobbyMessage.html",
                                     "default_LobbyMessage.html")
            except:
                pass

            else:
                with open(self.userPath + "LobbyMessage.html", "r") as f:
                    lobbyMsg += f.read()

            chatel = messaging.build('chat', _type='1', version='1.0')
            chatel.text = lobbyMsg
            el = messaging.build('msg', to=player_id, _from='0', group_id='0')
            el.append(chatel)

            # Send the server's lobby message to the client no matter what
            self.sendMsg(socket, el, self.players[player_id].useCompression,
                         self.players[player_id].compressionType)
            if self.sendLobbySound:
                el = messaging.build('sound', url=self.lobbySound, loop=True,
                                     group_id=0, _from=0)
                self.sendMsg(socket, el,
                             self.players[player_id].useCompression,
                             self.players[player_id].compressionType)
            return
        except:
            logger.exception(traceback.format_exc())
        #  End of lobby message code
        #######################################################################


    def listenAcceptThread(self,arg):
        #  Set up the socket to listen on.
        try:
            self.log_msg("\nlisten thread running...")
            adder = ""
            if self.server_address is not None:
                adder = self.server_address
            self.listen_sock.bind(('', self.server_port))
            self.listen_sock.listen(5)
        except Exception, e:
            self.log_msg(("Error binding request socket!", e))
            self.alive = 0

        while self.alive:
            #  Block on the socket waiting for a new connection
            try:
                (newsock, addr) = self.listen_sock.accept()
                ## self.log_msg("New connection from " + str(addr)+ ". Interfacing with server...")

                # Now that we've accepted a new connection, we must immediately spawn a new
                # thread to handle it...otherwise we run the risk of having a DoS shoved into
                # our face!  :O  After words, this thread is dead ready for another connection
                # accept to come in.
                thread.start_new_thread(self.acceptedNewConnectionThread, ( newsock, addr ))
            except:
                logger.exception(traceback.format_exc())

        #  At this point, we're done and cleaning up.
        self.log_msg("server socket listening thread exiting...")
        self.listen_event.set()

    def acceptedNewConnectionThread(self, newsock, addr):
        """
        Once a new connection comes in and is accepted,
        this thread starts up to handle it.
        """
        data = None

        # get client info and send othe client info
        # If this receive fails, this thread should exit without even attempting to process it
        self.log_msg("Connection from " + str(addr) + " has been accepted.  Waiting for data...")

        data = self.recvMsg(newsock)

        if not data:
            self.log_msg("Connection from " + str(addr) + " failed. Closing connection.")
            try:
                newsock.close()
            except Exception, e:
                logger.exception(traceback.format_exc())
            return #returning causes connection thread instance to terminate


        if data == "<system/>":
            try:
                newsock.close()
            except:
                pass
            return #returning causes connection thread instance to terminate

        # Parse the XML received from the connecting client
        try:
            el = fromstring(data)
        except ExpatError:
            try:
                newsock.close()
            except:
                pass
            self.log_msg("Error in parse found from " + str(addr) + ".  Disconnected.")
            self.log_msg("  Offending data(" + str(len(data)) + "bytes)=" + data)
            logger.exception(traceback.format_exc())
            return

        # Determine the correct action and execute it
        try:
            # get action
            action = el.get("action")

            # Figure out what type of connection we have going on now
            if action == "new":
                self.new_request(newsock, el)
            elif action == "update":
                self.update_request(newsock, el)
            else:
                self.log_msg("Unknown Join Request!")

        except Exception, e:
            logger.exception(traceback.format_exc())
            logger.general("The following  message: " + str(data))
            logger.general("from " + str(addr) + " cause the above exception")
            return

    #========================================================
    #
    #   Message_handler
    #
    #========================================================
    #
    # Changed thread organization from one continuous parsing/handling thread
    # to multiple expiring parsing/handling threads to improve server performance
    # and player load capacity -- Snowdog 3/04
    def message_handler(self, arg):
        self.log_msg("message handler thread running...")
        while self.alive:
            data = None
            try:
                data = self.incoming.get(0)
            except Queue.Empty:
                time.sleep(0.025)
                continue

            bytes = len(data)
            if bytes <= 0:
                continue
            try:
                thread.start_new_thread(self.parse_incoming_dom,(str(data),))
                #data has been passed... unlink from the variable references
                #so data in passed objects doesn't change (python passes by reference)
                del data
                data = None
            except Exception, e:
                self.log_msg(str(e))
        self.log_msg("message handler thread exiting...")
        self.incoming_event.set()

    def parse_incoming_dom(self, data):
        try:
            el = fromstring(data)
        except ExpatError:
            end = data.find(">")
            head = data[:end+1]
            msg = data[end+1:]
            el = fromstring(head)
            try:
                el1 = fromstring(msg)
                el.append(el1)
            except ExpatError:
                el.text = msg
                logger.general("Bad Message: \n" + data)

        self.message_action(el, data)

    def message_action(self, etreeEL, data):
        if etreeEL.tag in self.svrcmds:
            self.svrcmds[etreeEL.tag]['function'](etreeEL, data)
        else:
            raise Exception, "Not a valid header!"
        #Message Action thread expires and closes here.
        return


    def do_alter(self, etreeEL, data):
        target = etreeEL.get("key")
        value = etreeEL.get("val")
        player = etreeEL.get("plr")
        group_id = etreeEL.get("gid")
        boot_pwd = etreeEL.get("bpw")
        actual_boot_pwd = self.groups[group_id].boot_pwd

        chatEl = messaging.build('chat', _type='1', version='1.0')

        if not self.allow_room_passwords:
            chatEl.text = 'Room passwords have been disabled by the server administrator.'
            el = messaging.build('msg', to=player, _from=0, group_id=0)
            el.append(chatEl)
            self.players[player].outbox.put(el)
            return
        elif boot_pwd == actual_boot_pwd:
            if target == "pwd":
                lmessage = "Room password changed to from \"" + self.groups[group_id].pwd + "\" to \"" + value  + "\" by " + player
                self.groups[group_id].pwd = value
                chatEl.text = 'Room password changed to {v}'.format(v=value)
                el = messaging.build('msg', to=player, _from=0, group_id=0)
                el.append(chatEl)
                self.players[player].outbox.put(el)
                self.log_msg(lmessage)
                self.send_to_all('0', self.groups[group_id].toxml('update'))
            elif target == "name":
                chatEl.text = self.change_group_name(group_id, value, player)
                el = messaging.build('msg', to=player, _from=0, group_id=0)
                el.append(chatEl)
                self.players[player].outbox.put(el)
        else:
            chatEl.text = "Invalid Administrator Password."
            el = messaging.build('msg', to=player, _from=0, group_id=0)
            self.players[player].outbox.put(el)


    def do_role(self, etreeEL, data):
        role = ""
        boot_pwd = ""
        act = etreeEL.get("action")
        player = etreeEL.get("player")
        group_id = etreeEL.get("group_id")
        if act == "set":
            role = etreeEL.get("role")
            boot_pwd = etreeEL.get("boot_pwd")

        if group_id != "0":
            self.handle_role(act, player, role, boot_pwd, group_id)
            self.log_msg(("role", (player, role)))

    def do_ping(self, xetreeEL, data):
        player = etreeEL.get("player")
        group_id = etreeEL.get("group_id")
        sent_time = etreeEL.get("time")

        if sent_time:
            #because a time was sent return a ping response
            el = messaging.build('ping', time=sent_time)
        else:
            chatel = messaging.build('chat', _type='1', version='1.0')
            chatel.text = '<font color="#FF0000">PONG!?!</font>',
            el = messaging.build('msg', to=player, _from=player,
                                 group_id=group_id)
            el.append(chatel)

        self.players[player].outbox.put(el)

    def do_system(self, xml_dom, data):
        pass

    def moderate_group(self, etreeEl, data):
        try:
            action = etreeEl.get("action")
            from_id = etreeEl.get("from")
            pwd = etreeEl.get("pwd", '')
            group_id = self.players[from_id].group_id

            if action == "list":
                if self.groups[group_id].moderated:
                    msg = ""
                    for i in self.groups[group_id].voice.keys():
                        if msg != "":
                            msg +=", "
                        if self.players.has_key(i):
                            msg += '('+i+') '+self.players[i].name
                        else:
                            del self.groups[group_id].voice[i]
                    if (msg != ""):
                        msg = "The following users may speak in this room: " + msg
                    else:
                        msg = "No people are currently in this room with the ability to chat"
                    self.players[from_id].self_message(msg)
                else:
                    self.players[from_id].self_message("This room is currently unmoderated")
            elif action == "enable":
                if not self.groups[group_id].check_boot_pwd(pwd):
                    self.players[from_id].self_message("Failed - incorrect admin password")
                    return
                self.groups[group_id].moderated = 1
                self.players[from_id].self_message("This channel is now moderated")
            elif action == "disable":
                if not self.groups[group_id].check_boot_pwd(pwd):
                    self.players[from_id].self_message("Failed - incorrect admin password")
                    return
                self.groups[group_id].moderated = 0
                self.players[from_id].self_message("This channel is now unmoderated")
            elif action == "addvoice":
                if not self.groups[group_id].check_boot_pwd(pwd):
                    self.players[from_id].self_message("Failed - incorrect admin password")
                    return
                users = etreeEl.get("users",'').split(',')
                for i in users:
                    self.groups[group_id].voice[i.strip()]=1
            elif action == "delvoice":
                if not self.groups[group_id].check_boot_pwd(pwd):
                    self.players[from_id].self_message("Failed - incorrect admin password")
                    return
                users = etreeEl.get("users",'').split(',')
                for i in users:
                    if self.groups[group_id].voice.has_key(i.strip()):
                        del self.groups[group_id].voice[i.strip()]
            else:
                logger.general("Bad input: " + data)
        except Exception,e:
            self.log_msg(str(e))

    def join_group(self, etreeEl, data):
        try:
            from_id = etreeEl.get("from")
            pwd = etreeEl.get("pwd")
            group_id = etreeEl.get("group_id")
            ver = self.players[from_id].version
            allowed = True

            if not self.groups[group_id].check_version(ver):
                allowed = False
                msg = 'failed - invalid client version ('+self.groups[group_id].minVersion+' or later required)'

            if not self.groups[group_id].check_pwd(pwd):
                allowed = False

                #tell the clients password manager the password failed -- SD 8/03
                pm = messaging.build('password', signal='fail', type='room',
                                     id=group_id, data='')
                self.players[from_id].outbox.put(pm)

                msg = 'failed - incorrect room password'

            if not allowed:
                self.players[from_id].self_message(msg)
                #the following line makes sure that their role is reset to normal,
                #since it is briefly set to lurker when they even TRY to change
                #rooms
                el = messaging.build('role', action='update', id=from_id,
                                     role=self.players[from_id].role)
                self.players[from_id].outbox.put(el)
                return

            #move the player into their new group.
            self.move_player(from_id, group_id)

        except Exception, e:
            self.log_msg(str(e))

    def move_player(self, from_id, group_id ):
        """
        move a player from one group to another
        """
        try:
            try:
                if group_id == "0":
                    self.players[from_id].role = "GM"
                else:
                    self.players[from_id].role = "Lurker"
            except Exception, e:
                logger.exception(traceback.format_exc())

            old_group_id = self.players[from_id].change_group(group_id,self.groups)
            self.send_to_group(from_id,old_group_id,self.players[from_id].toxml('del'))
            self.send_to_group(from_id,group_id,self.players[from_id].toxml('new'))
            self.check_group(from_id, old_group_id)

            # Here, if we have a group specific lobby message to send, push it on
            # out the door!  Make it put the message then announce the player...just
            # like in the lobby during a new connection.
            # -- only do this check if the room id is within range of known persistent id thresholds
            #also goes ahead if there is a defaultRoomMessage --akoman

            if self.isPersistentRoom(group_id) or self.defaultMessageFile != None:
                try:
                    if self.groups[group_id].messageFile[:4] == 'http':
                        data = urllib.urlretrieve(self.groups[group_id].messageFile)
                        with open(data[0]) as f:
                            roomMsg = f.read()

                        urllib.urlcleanup()
                    else:
                        with open(self.groups[group_id].messageFile, "r") as f:
                            roomMsg = f.read()

                except Exception, e:
                    roomMsg = ""
                    self.log_msg(str(e))

                # Spit that darn message out now!
                chatel = messaging.build('chat', _type='1', version='1.0')
                chatel.text = roomMsg
                el = messaging.build('msg', to=from_id, _from=0,
                                     group_id=group_id)
                el.append(chatel)
                self.players[from_id].outbox.put(el)

            if self.sendLobbySound and group_id == '0':
                el = messaging.build('sound', url=self.lobbySound, loop=True,
                                     _from=0, group_id=0)
                self.players[from_id].outbox.put(el)

            # Now, tell everyone that we've arrived
            self.send_to_all('0', self.groups[group_id].toxml('update'))

            # this line sends a handle role message to change the players role
            self.send_player_list(from_id, group_id)

            #notify user about others in the room
            self.return_room_roles(from_id, group_id)
            self.log_msg(("join_group", (from_id, group_id)))
            self.handle_role("set", from_id, self.players[from_id].role,
                             self.groups[group_id].boot_pwd, group_id)

        except Exception, e:
            self.log_msg(str(e))

        thread.start_new_thread(self.registerRooms, (0,))

    def return_room_roles(self, from_id, group_id):
        for m in self.players.keys():
            if self.players[m].group_id == group_id:
                el = messaging.build('role', action='update',
                                     id=self.players[m].id,
                                     role=self.players[m].role)
                self.players[from_id].outbox.put(el)

    # This is pretty much the same thing as the create_group method, however,
    # it's much more generic whereas the create_group method is tied to a specific
    # xml message.  Ack!  This version simply creates the groups, it does not
    # send them to players.  Also note, both these methods have race
    # conditions written all over them.  Ack! Ack!
    def new_group(self, name, pwd, boot, minVersion, mapFile, messageFile,
                   persist=False, moderated=False):
        group_id = str(self.next_group_id)
        self.next_group_id += 1

        self.groups[group_id] = game_group(group_id, name, pwd, "", boot,
                                           minVersion, mapFile, messageFile,
                                           persist)
        self.groups[group_id].moderated = moderated
        ins = ""
        if persist:
            ins = "Persistant "
        lmsg = "Creating " + ins + "Group... (" + str(group_id) + ") " + str(name)
        self.log_msg(lmsg)


    def change_group_name(self, gid, name, pid):
        """
        Change the name of a group
        """
        # Check for & in name.  We want to allow this because of its common
        # use in d&d games.
        name = name.replace('&', '&amp;').replace('"', '&quote;').replace("'",
                                                                        '&#39;')
        try:
            oldroomname = self.groups[gid].name
            self.groups[gid].name = str(name)
            lmessage = "Room name changed to from \"" + oldroomname + "\" to \"" + name + "\""
            self.log_msg(lmessage  + " by " + str(pid) )
            self.send_to_all('0', self.groups[gid].toxml('update'))
            return lmessage
        except:
            return "An error occured during rename of room!"

        thread.start_new_thread(self.registerRooms, (0,))

    def create_group(self, etreeEl, data):
        try:
            from_id = etreeEl.get("from")
            pwd = etreeEl.get("pwd")
            name = etreeEl.get("name")
            boot_pwd = etreeEl.get("boot_pwd")
            minVersion = etreeEl.get("min_version")
            #added var reassign -- akoman
            messageFile = self.defaultMessageFile

            # see if passwords are allowed on this server and null password if not
            if not self.allow_room_passwords:
                pwd = ""

            #
            # Check for & in name.  We want to allow this because of its common
            # use in d&d games.
            name = name.replace('&', '&amp;').replace('"', '&quote;').\
                 replace("'", '&#39;')

            group_id = str(self.next_group_id)
            self.next_group_id += 1
            self.groups[group_id] = game_group(group_id, name, pwd, "",
                                               boot_pwd, minVersion, None,
                                               messageFile)
            self.groups[group_id].voice[from_id] = True
            self.players[from_id].outbox.put(self.groups[group_id].toxml('new'))
            old_group_id = self.players[from_id].change_group(group_id,self.groups)
            self.send_to_group(from_id, old_group_id,
                               self.players[from_id].toxml('del'))
            self.check_group(from_id, old_group_id)
            self.send_to_all(from_id, self.groups[group_id].toxml('new'))
            self.send_to_all('0', self.groups[group_id].toxml('update'))
            self.handle_role("set", from_id, "GM", boot_pwd, group_id)
            lmsg = "Creating Group... (" + str(group_id) + ") " + str(name)
            self.log_msg(lmsg)
            jmsg = "moving to room " + str(group_id) + "."
            self.log_msg(jmsg)
            #even creators of the room should see the HTML --akoman
            #edit: jan10/03 - was placed in the except statement. Silly me.
            if self.defaultMessageFile != None:
                if self.defaultMessageFile[:4] == 'http':
                    data = urllib.urlretrieve(self.defaultMessageFile)
                    with open(data[0]) as f:
                        roomMsg = f.read()
                    urllib.urlcleanup()
                else:
                    with open(self.defaultMessageFile, "r") as f:
                        roomMsg = f.read()

                # Send the rooms message to the client no matter what
                chatel = messaging.build('chat', _type='1', version='1.0')
                chatel.text = roomMsg
                el = messaging.build('msg', to=from_id, _from=0,
                                     group_id=group_id)
                el.append(chatel)
                self.players[from_id].outbox.put(el)

        except Exception, e:
            self.log_msg( "Exception: create_group(): " + str(e))

        thread.start_new_thread(self.registerRooms, (0,))


    def check_group(self, from_id, group_id):
        try:
            if group_id not in self.groups: return
            if group_id == '0':
                self.send_to_all("0", self.groups[group_id].toxml('update'))
                return #never remove lobby *sanity check*
            if not self.isPersistentRoom(group_id)  and self.groups[group_id].get_num_players() == 0:
                self.send_to_all("0", self.groups[group_id].toxml('del'))
                del self.groups[group_id]
                self.log_msg(("delete_group", (from_id, group_id)))

            else:
                self.send_to_all("0", self.groups[group_id].toxml('update'))

            #The register Rooms thread
            thread.start_new_thread(self.registerRooms, (0,))

        except Exception, e:
            self.log_msg(str(e))

    def del_player(self, id, group_id):
        try:
            dmsg = "Client Disconnect: (" + str(id) + ") " + str(self.players[id].name)
            self.players[id].disconnect()
            self.groups[group_id].remove_player(id)
            del self.players[id]
            self.log_msg(dmsg)

            #  If already registered then re-register, thereby updating the Meta
            #    on the number of players
            #  Note:  Upon server shutdown, the server is first unregistered, so
            #           this code won't be repeated for each player being deleted.
            if self.be_registered:
                self.register()
        except Exception, e:
            self.log_msg(str(e))

    def incoming_player_handler(self, etreeEl, data):
        id = etreeEl.get("id")
        act = etreeEl.get("action")
        group_id = self.players[id].group_id
        ip = self.players[id].ip
        self.log_msg("Player with IP: " + str(ip) + " joined.")

        ServerPlugins.setPlayer(self.players[id])

        self.send_to_group(id, group_id, data)
        if act == "new":
            try:
                self.send_player_list(id, group_id)
                self.send_group_list(id)
            except Exception, e:
                logger.exception(traceback.format_exc())
        elif act == "del":
            self.del_player(id, group_id)
            self.check_group(id, group_id)
        elif act == "update":
            self.players[id].take_dom(etreeEl)
            self.log_msg(("update", {"id": id,
                                     "name": etreeEl.get("name"),
                                     "status": etreeEl.get("status"),
                                     "role": etreeEl.get("role"),
                                     "ip":  str(ip),
                                     "group": etreeEl.get("group_id"),
                                     "room": etreeEl.get("name"),
                                     "boot": etreeEl.get("rm_boot"),
                                     "version": etreeEl.get("version"),
                                     "ping": etreeEl.get("time") \
                                     }))

    def strip_cheat_roll(self, string):
        try:
            cheat_regex = re.compile('&amp;#91;(.*?)&amp;#93;')
            string = cheat_regex.sub( r'[ ' + self.cheat_msg + " \\1 " + self.cheat_msg + ' ]', string)
        except:
            pass
        return string

    def strip_body_tags(self, string):
        try:
            bodytag_regex = re.compile('&lt;\/?body(.*?)&gt;')
            string = bodytag_regex.sub('', string)
        except:
            pass
        return string

    def msgTooLong(self, length):
        if length > self.maxSendSize and not self.maxSendSize == 0:
            return True
        return False

    def incoming_msg_handler(self, etreeEl, data):
        etreeEl, data = ServerPlugins.preParseIncoming(etreeEl, data)

        to_id = etreeEl.get("to")
        from_id = etreeEl.get("from")
        group_id = etreeEl.get("group_id")
        end = data.find(">")
        msg = data[end+1:]

        if from_id == "0" or len(from_id) == 0:
            logger.general("WARNING!! Message received with "\
                           "an invalid from_id.  Message dropped.", True)
            return None

        if etreeEl.text:
            etreeEl.text = self.strip_body_tags(etreeEl.text)

        if self.players[from_id].role != "GM" and etreeEl.iter_find('chat'):
            el = etreeEl.find('chat')
            el.text = self.strip_cheat_roll(el.text)

        if group_id == '0' and etreeEl.iter_find('chat') and self.msgTooLong(len(etreeEl.iter_find('chat').text)):
            self.send("Your message was too long, break it up into smaller parts please", from_id, group_id)
            self.log_msg('Message Blocked from Player: ' + self.players[from_id].name + ' attempting to send a message longer then ' + str(self.maxSendSize))
            return

        if etreeEl.iter_find('map'):
            if group_id == '0':
                #attempt to change lobby map. Illegal operation.
                self.players[from_id].self_message('The lobby map may not be altered.')
            elif to_id.lower() == 'all':
                #valid map for all players that is not the lobby.
                self.send_to_group(from_id, group_id, etreeEl)
                self.groups[group_id].game_map.init_from_xml(etreeEl.iter_find('map'))
            else:
                #attempting to send map to specific individuals which is not supported.
                self.players[from_id].self_message('Invalid map message. Message not sent to others.')

        elif etreeEl.iter_find('boot'):
            self.handle_boot(from_id, to_id, group_id, etreeEl.iter_find('boot'))
        else:
            if to_id == 'all':
                if self.groups[group_id].moderated and\
                   not from_id in self.groups[group_id].voice:
                    self.players[from_id].self_message('This room is moderated - message not sent to others')
                else:
                    self.send_to_group(from_id, group_id, etreeEl)
            else:
                self.players[to_id].outbox.put(etreeEl)

        self.check_group_members(group_id)
        return

    def sound_msg_handler(self, etreeEl, data):
        from_id = etreeEl.get("from")
        group_id = etreeEl.get("group_id")
        if group_id != 0:
            self.send_to_group(from_id, group_id, etreeEl)

    def plugin_msg_handler(self, etreeEl, data):
        to_id = etreeEl.get("to")
        from_id = etreeEl.get("from")
        group_id = etreeEl.get("group_id")

        if from_id == "0" or len(from_id) == 0:
            logger.general("WARNING!! Message received with an "\
                           "invalid from_id.  Message dropped.")
            return None

        if to_id == 'all':
            if self.groups[group_id].moderated and not self.groups[group_id].voice.has_key(from_id):
                self.players[from_id].self_message('This room is moderated - message not sent to others')
            else:
                for child in etreeEl.getchildren():
                    self.send_to_group(from_id, group_id, child)
        else:
            for child in etreeEl.getchildren():
                self.players[to_id].outbox.put(child)

        self.check_group_members(group_id)
        return

    def handle_role(self, act, player, role, given_boot_pwd, group_id):
        if act == "display":
            msg = ["Displaying Roles<br /><br /><u>Role</u>",
                   "&nbsp&nbsp<u>Player</u><br />"]
            keys = self.players.keys()
            for m in keys:
                if self.players[m].group_id == group_id:
                    msg.append(self.players[m].role)
                    msg.append(self.players[m].name)
                    msg.append("<br />")
            chatel = messaging.build('chat', _type='1', version='1.0')
            chatel.text = ' '.join(msg)
            el = messaging.build('msg', to=player, _from=0, group_id=group_id)
            el.append(chatel)
            self.send(el, player, group_id)
        elif act == "set":
            try:
                actual_boot_pwd = self.groups[group_id].boot_pwd
                if self.players[player].group_id == group_id:
                    if actual_boot_pwd == given_boot_pwd:
                        self.log_msg("Administrator passwords match -- changing role")

                        #  Send update role event to all
                        el = messaging.build('role', action='update', id=player,
                                             role=role)
                        self.send_to_group("0", group_id, el)
                        self.players[player].role = role
                        if (role.lower() == "gm" or role.lower() == "player"):
                            self.groups[group_id].voice[player]=1
                    else:
                        el = messaging.build('password', signal='fail',
                                             type='admin', id=group_id, data='')
                        self.players[player].outbox.put(el)
                        self.log_msg( "Administrator passwords did not match")
            except Exception, e:
                logger.exception("Error executing the role change")
                logger.exception("due to the following exception:")
                logger.exception(traceback.format_exc())
                logger.exception("Ignoring boot message")

    def handle_boot(self, from_id, to_id, group_id, etreeEl):
        given_boot_pwd = etreeEl.get('boot_pwd')

        try:
            actual_boot_pwd = self.groups[group_id].boot_pwd
            server_admin_pwd = self.groups["0"].boot_pwd

            self.log_msg("Actual boot pwd = " + actual_boot_pwd)
            self.log_msg("Given boot pwd = " + given_boot_pwd)

            if self.players[to_id].group_id == group_id:

                ### ---CHANGES BY SNOWDOG 4/03 ---
                ### added boot to lobby code.
                ### if boot comes from lobby dump player from the server
                ### any user in-room boot will dump to lobby instead
                if given_boot_pwd == server_admin_pwd:
                    # Send a message to everyone in the room, letting them know someone has been booted
                    msg = ['<font color="#FF0000">Booting', '(%s)' % to_id,
                           self.players[to_id].name,
                           'from server...</font>']
                    chatel = messaging.build('chat', _type='1', version='1.0')
                    chatel.text = ' '.join(msg)
                    el = messaging.build('msg', to='all', _from=from_id,
                                         group_id=group_id)
                    el.append(chatel)

                    self.log_msg("boot_msg:" + ' '.join(msg))

                    self.send_to_group("0", group_id, el)
                    time.sleep(0.025)

                    self.log_msg("Booting player " + str(to_id) + " from server.")

                    #  Send delete player event to all
                    self.send_to_group("0", group_id, self.players[to_id].toxml("del"))

                    #  Remove the player from local data structures
                    self.del_player(to_id, group_id)

                    #  Refresh the group data
                    self.check_group(to_id, group_id)

                elif actual_boot_pwd == given_boot_pwd:
                    # Send a message to everyone in the room, letting them know someone has been booted
                    msg = ['<font color="#FF0000">Booting', '(%s)' % to_id,
                           self.players[to_id].name,
                           'from room...</font>']
                    chatel.text = ' '.join(msg)
                    el = messaging.build('msg', to='all', _from=from_id,
                                         group_id=group_id)
                    el.append(chatel)

                    self.log_msg("boot_msg:" + ' '.join(msg))

                    self.send_to_group("0", group_id, el)
                    time.sleep(0.0025)

                    #dump player into the lobby
                    self.move_player(to_id, "0")

                    #  Refresh the group data
                    self.check_group(to_id, group_id)
                else:
                    #tell the clients password manager the password failed -- SD 8/03
                    el = messaging.build('password', signal='fail', data='',
                                         id=group_id, type='admin')
                    self.players[from_id].outbox.put(el)
                    logger.general("boot passwords did not match")

        except Exception, e:
            logger.exception(traceback.format_exc())
            self.log_msg('Exception in handle_boot() ' + str(e))

    def admin_kick(self, id, message="", silent=0):
        """
        Kick a player from a server from the console
        """
        try:
            group_id = self.players[id].group_id
            # Send a message to everyone in the victim's room, letting them know someone has been booted
            msg = ['<font color="#FF0000">Kicking', '(%s)' % (id),
                   self.players[id].name, 'from server...', str(message)]
            chatel = messaging.build('chat', _type='1', version='1.0')
            chatel.text = ' '.join(msg)
            el = messaging.build('msg', to='all', _from=0, group_id=group_id)
            el.append(chatel)
            self.log_msg("boot_msg:" + ' '.join(msg))
            if (silent == 0):
                self.send_to_group("0", group_id, el)
            time.sleep(0.025)

            self.log_msg("kicking player " + str(id) + " from server.")
            #  Send delete player event to all
            self.send_to_group("0", group_id, self.players[id].toxml("del"))

            #  Remove the player from local data structures
            self.del_player(id, group_id)

            #  Refresh the group data
            self.check_group(id, group_id)

        except Exception, e:
            logger.exception(traceback.format_exc())
            self.log_msg('Exception in admin_kick() ' + str(e))


    def admin_banip(self, ip, name="", silent=0):
        """
        Ban a player from a server from the console
        """
        try:
            self.ban_list[ip] = {}
            self.ban_list[ip]['ip'] = ip
            self.ban_list[ip]['name'] = name
            self.saveBanList()

        except Exception, e:
            logger.exception(traceback.format_exc())
            self.log_msg('Exception in admin_banip() ' + str(e))

    def admin_ban(self, id, message="", silent=0):
        "Ban a player from a server from the console"
        try:
            id = str(id)
            group_id = self.players[id].group_id
            ip = self.players[id].ip
            self.ban_list[ip] = {}
            self.ban_list[ip]['ip'] = ip
            self.ban_list[ip]['name'] = self.players[id].name
            self.saveBanList()

            # Send a message to everyone in the victim's room, letting them know someone has been booted
            msg = ['<font color="#FF0000">Banning', '(%s)' % (id),
                   self.players[id].name, 'from server...', str(message)]
            chatel = messaging.build('chat', _type='1', version='1.0')
            chatel.text = ' '.join(msg)
            el = messaging.build('msg', to='all', _from=0, group_id=group_id)
            el.append(chatel)
            self.log_msg("ban_msg:" + ' '.join(msg))
            if (silent == 0):
                self.send_to_group("0", group_id, el)
            time.sleep(0.025)

            self.log_msg("baning player " + str(id) + " from server.")
            #  Send delete player event to all
            self.send_to_group("0", group_id, self.players[id].toxml("del"))

            #  Remove the player from local data structures
            self.del_player(id, group_id)

            #  Refresh the group data
            self.check_group(id, group_id)

        except Exception, e:
            logger.exception(traceback.format_exc())
            self.log_msg('Exception in admin_ban() ' + str(e))

    def admin_unban(self, ip):
        try:
            if ip in self.ban_list:
                del self.ban_list[ip]

            self.saveBanList()

        except Exception, e:
            logger.exception(traceback.format_exc())
            self.log_msg('Exception in admin_unban() ' + str(e))

    def admin_banlist(self):
        msg = []
        msg.append('<table border="1"><tr><td><b>Name</b></td><td><b>IP</b></td></tr>')
        for ip in self.ban_list.keys():
            msg.append("<tr><td>")
            msg.append(self.ban_list[ip]['name'])
            msg.append("</td><td>")
            msg.append(self.ban_list[ip]['ip'])
            msg.append("</td></tr>")
        msg.append("</table>")

        return "".join(msg)

    def admin_toggleSound(self):
        self.sendLobbySound = not self.sendLobbySound

        return self.sendLobbySound

    def admin_soundFile(self, _file):
        self.lobbySound = _file

    def admin_setSendSize(self, sendlen):
        self.maxSendSize = int(sendlen)
        self.log_msg('Max Send Size was set to ' + str(sendlen))

    def remove_room(self, group):
        """
        removes a group and boots all occupants
        """
        #check that group id exists
        if group not in self.groups:
            return "Invalid Room Id. Ignoring remove request."

        self.groups[group].persistant = 0
        try:
            keys = self.groups[group].get_player_ids()
            for k in keys:
                self.del_player(k, str(group))
            self.check_group("0", str(group))
        except:
            pass

    def send(self, msg, player, group):
        self.players[player].send(msg,player,group)

    def send_to_all(self, from_id, etreeEl):
        try:
            with self.p_lock:
                for player in self.players.itervalues():
                    if player.id != from_id:
                        player.outbox.put(etreeEl)
        except Exception, e:
            logger.exception(traceback.format_exc())
            self.log_msg("Exception: send_to_all(): " + str(e))

    def send_to_group(self, from_id, group_id, etreeEl):
        etreeEl = ServerPlugins.postParseIncoming(etreeEl)
        try:
            keys = self.groups[group_id].get_player_ids()
            for k in keys:
                if k != from_id:
                    self.players[k].outbox.put(etreeEl)
        except Exception, e:
            logger.exception(traceback.format_exc())
            self.log_msg("Exception: send_to_group(): " + str(e))

    def send_player_list(self, to_id, group_id):
        try:
            keys = self.groups[group_id].get_player_ids()
            for k in keys:
                if k != to_id:
                    data = self.players[k].toxml('new')
                    self.players[to_id].outbox.put(data)
        except Exception, e:
            logger.exception(traceback.format_exc())
            self.log_msg("Exception: send_player_list(): " + str(e))

    def send_group_list(self, to_id, action="new"):
        try:
            for key in self.groups:
                xml = self.groups[key].toxml(action)
                self.players[to_id].outbox.put(xml)
        except Exception, e:
            self.log_msg("Exception: send_group_list(): (client #"+to_id+") : " + str(e))
            logger.exception(traceback.format_exc())

    #--------------------------------------------------------------------------
    # KICK_ALL_CLIENTS()
    #
    # Convience method for booting all clients off the server at once.
    # used while troubleshooting mysterious "black hole" server bug
    # Added by Snowdog 11-19-04
    def kick_all_clients(self):
        try:
            keys = self.groups.keys()
            for k in keys:
                pl = self.groups[k].get_player_ids()
                for p in pl:
                    self.admin_kick(p, "Purged from server")
        except Exception, e:
            logger.exception(traceback.format_exc())
            self.log_msg("Exception: kick_all_clients(): " + str(e))


    # This really has little value as it will only catch people that are hung
    # on a disconnect which didn't complete.  Other idle connections which are
    # really dead go undeterred.
    #
    # UPDATED 11-29-04: Changed remove XML send to forced admin_kick for 'dead clients'
    #                   Dead clients now removed more effeciently as soon as they are detected
    #                        --Snowdog
    def check_group_members(self, group_id):
        try:
            keys = self.groups[group_id].get_player_ids()
            for k in keys:
                #drop any clients that are idle for more than 8 hours
                #as these are likely dead clients
                idlemins = self.players[k].idle_time()
                idlemins = idlemins/60
                if (idlemins > self.zombie_time):
                    self.admin_kick(k, "Removing zombie client",
                                    self.silent_auto_kick)
                elif self.players[k].get_status() != MPLAY_CONNECTED:
                    if self.players[k].check_time_out():
                        self.log_msg("Player #" + k + " Lost connection!")
                        self.admin_kick(k, "Removing dead client",
                                        self.silent_auto_kick)
        except Exception, e:
            self.log_msg("Exception: check_group_members(): " + str(e))


    def remote_admin_handler(self, etreeEl, data):
        # handle incoming remove server admin messages
        # (allows basic administration of server from a remote client)
        # base message format: <admin id="" pwd="" cmd="" [data for command]>

        if not self.allowRemoteAdmin:
            return

        try:
            pid = etreeEl.get("id")
            gid = ""
            given_pwd = etreeEl.get("pwd")
            cmd = etreeEl.get("cmd")
            server_admin_pwd = self.groups["0"].boot_pwd
            p_id = ""
            p_name= ""
            p_ip = ""


            #verify that the message came from the proper ID/Socket and get IP address for logging
            if pid in self.players:
                p_name = (self.players[pid]).name
                p_ip = (self.players[pid]).ip
                gid = (self.players[pid]).group_id
            else:
                #invalid ID.. report fraud and log
                m = "Invalid Remote Server Control Message (invalid id) #" + str(pid) + " does not exist."
                self.log_msg(m)
                return

            #log receipt of admin command   added by Darren
            m = "Remote Server Control Message ( "+ str(cmd) +" ) from #" + str(pid) + " (" + str(p_name) + ") " + str(p_ip)
            self.log_msg(m)

            #check the admin password(boot password) against the supplied one in message
            #dump and log any attempts to control server remotely with invalid password
            if server_admin_pwd != given_pwd:
                #tell the clients password manager the password failed -- SD 8/03
                pm = messaging.build('password', signal='fail', type='server',
                                     id=gid, data='')
                self.players[pid].outbox.put(pm)
                m = "Invalid Remote Server Control Message (bad password) from #" + str(pid) + " (" + str(p_name) + ") " + str(p_ip)
                self.log_msg(m)
                return

            #message now deemed 'authentic'
            #determine action to take based on command (cmd)
            chatel = messaging.build('chat', _type='1', version='1.0')

            if cmd == "list":
                #return player list to this user.
                chatel.text = self.player_list_remote()
                msg = messaging.build('msg', to=pid, _from=0, group_id=gid)
                msg.append(chatel)
                self.players[pid].outbox.put(msg)

            elif cmd == "banip":
                ip = etreeEl.get("bip")
                name = etreeEl.get("bname")
                chatel.text = 'Banned: {ip}'.format(ip=ip)
                msg = messaging.build('msg', to=pid, _from=0, group_id=gid)
                msg.append(chatel)
                self.admin_banip(ip, name)
                self.players[pid].outbox.put(msg)

            elif cmd == "ban":
                id = etreeEl.get("bid")
                chatel.text = 'Banned!'
                msg = messaging.build('msg', to=id, _from=0, group_id=gid)
                msg.append(chatel)
                self.players[pid].outbox.put(msg)
                self.admin_ban(id, "")

            elif cmd == "unban":
                ip = etreeEl.get("ip")
                self.admin_unban(ip)
                chatel.text = 'Unbanned {ip}'.format(ip=ip)
                msg = messaging.build('msg', to=pid, _from=0, group_id=gid)
                msg.append(chatel)
                self.players[pid].outbox.put(msg)

            elif cmd == "banlist":
                chatel.text = self.admin_banlist()
                msg = messaging.build('msg', to=pid, _from=0, group_id=gid)
                msg.append(chatel)
                self.players[pid].outbox.put(msg)

            elif cmd == "killgroup":
                ugid = etreeEl.get("gid")
                if ugid == "0":
                    chatel.text = 'Cannot Remove Lobby! Remote administrator request denied!'
                    msg = messaging.build('msg', to=pid, _from=0, group_id=gid)
                    msg.append(chatel)
                    self.players[pid].outbox.put(m)
                else:
                    result = self.prune_room(ugid)
                    chatel.text = str(result)
                    msg = messaging.build('msg', to=pid, _from=0, group_id=gid)
                    msg.append(chatel)
                    self.players[pid].outbox.put(msg)

            elif cmd == "message":
                tuid = etreeEl.get("to_id")
                msg = etreeEl.get("msg")
                chatel.text = msg
                pmsg = messaging.build('msg', to=tuid, _from=0,
                                       group_id=self.players[tuid].group_id)
                pmsg.append(chatel)
                try:
                    self.players[tuid].outbox.put(pmsg)
                except:
                    chatel.text = 'Unknown Player ID: No message sent.'
                    msg = messaging.build('msg',  to=pid, _from=0, group_id=gid)
                    msg.append(chatel)
                    self.players[pid].outbox.put(msg)

            elif cmd == "broadcast":
                bmsg = etreeEl.get("msg")
                self.broadcast(bmsg)

            elif cmd == "killserver" and self.allowRemoteKill:
                #dangerous command..once server stopped it must be restarted manually
                self.kill_server()

            elif cmd == "uptime":
                chatel.text = self.uptime(1)
                msg = messaging.build('msg', to=pid, _from=0, group_id=gid)
                msg.append(chatel)
                self.players[pid].outbox.put(msg)

            elif cmd == "help":
                chatel.text = self.AdminHelpMessage()
                msg = messaging.build('msg', to=pid, _from=0, group_id=gid)
                msg.append(chatel)
                self.players[pid].outbox.put(msg)

            elif cmd == "roompasswords":
                # Toggle if room passwords are allowed on this server
                chatel.text = self.RoomPasswords()
                msg = messaging.build('msg', to=pid, _from=0, group_id=gid)
                msg.append(chatel)
                self.players[pid].outbox.put( msg)

            elif cmd == "createroom":
                rm_name = xml_dom.getAttribute("name")
                rm_pass = xml_dom.getAttribute("pass")
                rm_boot = xml_dom.getAttribute("boot")
                result = self.create_temporary_persistant_room(rm_name,
                                                               rm_boot, rm_pass)

                chatel.text = result
                msg = messaging.build('msg', to=pid, _from=0, group_id=gid)
                msg.append(chatel)
                self.players[pid].outbox.put(msg)

            elif cmd == "nameroom":
                rm_id   = xml_dom.getAttribute("rmid")
                rm_name = xml_dom.getAttribute("name")
                result = self.change_group_name(rm_id,rm_name,pid)

                chatel.text = result
                msg = messaging.build('msg', to=pid, _from=0, group_id=gid)
                msg.append(chatel)
                self.players[pid].outbox.put(msg)

            elif cmd == "passwd":
                tgid = xml_dom.getAttribute("gid")
                npwd = xml_dom.getAttribute("pass")
                if tgid == "0":
                    chatel.text = 'Server password may not be changed remotely!'
                    msg = messaging.build('msg', to=pid, _from=0, group_id=gid)
                    msg.append(chatel)
                    self.players[pid].outbox.put(msg)
                else:
                    try:
                        self.groups[tgid].boot_pwd = npwd
                        chatel.text = 'Password changed for room ' + tgid
                        msg = messaging.build('msg', to=pid, _from=0,
                                              group_id=gid)
                        msg.append(chatel)
                        self.players[pid].outbox.put(msg)
                    except: pass

            elif cmd == "savemaps":
                for g in self.groups.itervalues():
                    g.save_map()

                chatel.text = 'Persistent room maps saved'
                msg = messaging.build('msg', to=pid, _from=0, group_id=gid)
                msg.append(chatel)
                self.players[pid].outbox.put(msg)


            else:
                chatel.text = '<i>[Unknown Remote Administration Command]</i>'
                msg = messaging.build('msg', to=pid, _from=0, group_id=gid)
                msg.append(chatel)
                self.players[pid].outbox.put(msg)


        except Exception, e:
            self.log_msg("Exception: Remote Admin Handler Error: " + str(e))
            logger.exception(traceback.format_exc())


    def toggleRemoteKill(self):
        self.allowRemoteKill = not self.allowRemoteKill

        return self.allowRemoteKill

    def toggleRemoteAdmin(self):
        self.allowRemoteAdmin = not self.allowRemoteAdmin

        return self.allowRemoteAdmin

#-----------------------------------------------------------------
# Remote Administrator Help (returns from server not client)
#-----------------------------------------------------------------
    def AdminHelpMessage(self):
        """
        returns a string to be sent as a message to a remote admin
        """

        #define the help command list information
        info = []
        info.append( ['list', '/admin list', 'Displays information about rooms and players on the server'] )
        info.append( ['uptime', '/admin uptime', 'Information on how long server has been running'] )
        info.append( ['help', '/admin help', 'This help message'])
        info.append( ['passwd', '/admin passwd &lt;group id&gt; &lt;new password&gt;', 'Changes a rooms bootpassword. Server(lobby) password may not be changed'])
        info.append( ['roompasswords', '/admin roompasswords', 'Allow/Disallow Room Passwords on the server (toggles)'])
        info.append( ['message', '/admin message &lt;user id&gt; &lt;message&gt;', 'Send a message to a specific user on the server'])
        info.append( ['broadcast', '/admin broadcast &lt;message&gt;', 'Broadcast message to all players on server'])
        info.append( ['createroom', '/admin createroom &lt;room name&gt; &lt;boot password&gt; [password]', 'Creates a temporary persistant room if possible.<i>Rooms created this way are lost on server restarts'])
        info.append( ['nameroom', '/admin nameroom &lt;group id&gt; &lt;new name&gt;', 'Rename a room'])
        info.append( ['killgroup', '/admin killgroup &lt;room id&gt;', 'Remove a room from the server and kick everyone in it.'])
        if self.allowRemoteKill:
            info.append( ['killserver', '/admin killserver', 'Shuts down the server. <b>WARNING: Server cannot be restarted remotely via OpenRPG</b>'])
        info.append( ['ban', '/admin ban {playerId}', 'Ban a player from the server.'])
        info.append( ['unban', '/admin unban {bannedIP}', 'UnBan a player from the server.'])
        info.append( ['banlist', '/admin banlist', 'List Banned IPs and the Names associated with them'])
        info.append( ['savemaps', '/admin savemaps', 'Save all persistent room maps that are not using the default map file.'])


        #define the HTML for the help display
        FS = "<font size='-1'>"
        FE = "<font>"

        help = "<hr><B>REMOTE ADMINISTRATOR COMMANDS SUPPORTED</b><br /><br />"
        help += "<table border='1' cellpadding='2'>"
        help += "<tr><td width='15%'><b>Command</b></td><td width='25%'><b>Format</b></td><td width='60%'><b>Description</b></td></tr>"
        for n in info:
            help += "<tr><td>" + FS + n[0] + FE + "</td><td><nobr>" + FS + n[1] + FE + "</nobr></td><td>" + FS + n[2] + FE + "</td></tr>"
        help += "</table>"
        return help

    #----------------------------------------------------------------
    # Create Persistant Group -- Added by Snowdog 6/03
    #
    # Allows persistant groups to be created on the fly.
    # These persistant groups are not added to the server.ini file
    # however and are lost on server restarts
    #
    # Updated function code to use per-group based persistance and
    # removed references to outdated persistRoomIdThreshold
    #----------------------------------------------------------------
    def create_temporary_persistant_room(self, roomname, bootpass, password=""):
        """
        create a temporary persistant room
        """

        group_id = str(self.next_group_id)
        self.next_group_id += 1
        self.groups[group_id] = game_group( group_id, roomname, password, "", bootpass, persist = 1 )
        cgmsg = "Create Temporary Persistant Group: (" + str(group_id) + ") " + str(roomname)
        self.log_msg( cgmsg )
        self.send_to_all('0',self.groups[group_id].toxml('new'))
        self.send_to_all('0',self.groups[group_id].toxml('update'))
        return str("Persistant room created (group " + group_id + ").")

    #----------------------------------------------------------------
    # Prune Room  -- Added by Snowdog 6/03
    #
    # similar to remove_room() except rooms are removed regardless
    # of them being persistant or not
    #
    # Added some error checking and updated room removal for per-room
    # based persistance -- Snowdog 4/04
    #----------------------------------------------------------------
    def prune_room(self,group):
        #don't allow lobby to be removed
        if group == '0':
            return "Lobby is required to exist and cannot be removed."

        #check that group id exists
        if group not in self.groups:
            return "Invalid Room Id. Ignoring remove request."

        try:
            keys = self.groups[group].get_player_ids()
            for k in keys:
                self.move_player(k,'0')

            ins = "Room"
            if self.isPersistentRoom(group):
                ins = "Persistant room"

            self.send_to_all("0", self.groups[group].toxml('del'))
            del self.groups[group]
            self.log_msg(("delete_group", ('0',group)))
            return ins + " removed."

        except:
            logger.exception(traceback.format_exc())
            return "An Error occured on the server during room removal!"


#----------------------------------------------------------------
#  Remote Player List  -- Added by snowdog 6/03
#
#  Similar to console listing except formated for web display
#  in chat window on remote client
#----------------------------------------------------------------
    def player_list_remote(self):
        """
        display a condensed list of players on the server
        """
        COLOR1 = "\"#004080\""  #header/footer background color
        COLOR2 = "\"#DDDDDD\""  #group line background color
        COLOR3 = "\"#FFFFFF\""  #player line background color
        COLOR4 = "\"#FFFFFF\""  #header/footer text color
        PCOLOR = "\"#000000\""  #Player text color
        LCOLOR = "\"#404040\""  #Lurker text color
        GCOLOR = "\"#FF0000\""  #GM text color
        SIZE   = "size=\"-1\""  #player info text size
        FG = PCOLOR

        with self.p_lock:
            pl = "<br /><table border=\"0\" cellpadding=\"1\" cellspacing=\"2\">"
            pl += "<tr><td colspan='4' bgcolor=" + COLOR1 + "><font color=" + COLOR4 + "><b>GROUP &amp; PLAYER LIST</b></font></td></tr>"
            try:

                keys = self.groups.keys()
                keys.sort(id_compare)
                for k in keys:
                    groupstring = "<tr><td bgcolor=" + COLOR2 + " colspan='2'><b>Group " + str(k)  + ": " +  self.groups[k].name  + "</b>"
                    groupstring += "</td><td bgcolor=" + COLOR2 + " > <i>Password: \"" + self.groups[k].pwd + "\"</td><td bgcolor=" + COLOR2 + " > Boot: \"" + self.groups[k].boot_pwd + "\"</i></td></tr>"
                    pl += groupstring
                    ids = self.groups[k].get_player_ids()
                    ids.sort(id_compare)
                    for id in ids:
                        if self.players.has_key(id):
                            if k != "0":
                                if (self.players[id]).role == "GM": FG = GCOLOR
                                elif (self.players[id]).role == "Player": FG = PCOLOR
                                else: FG = LCOLOR
                            else: FG = PCOLOR
                            pl += "<tr><td bgcolor=" + COLOR3 + ">"
                            pl += "<font color=" + FG + " " + SIZE + ">&nbsp;&nbsp;(" +  (self.players[id]).id  + ") "
                            pl += (self.players[id]).name
                            pl += "</font></td><td bgcolor=" + COLOR3 + " ><font color=" + FG + " " + SIZE + ">[IP: " + (self.players[id]).ip + "]</font></td><td  bgcolor=" + COLOR3 + " ><font color=" + FG + " " + SIZE + "> "
                            pl += (self.players[id]).idle_status()
                            pl += "</font></td><td><font color=" + FG + " " + SIZE + ">"
                            pl += (self.players[id]).connected_time_string()
                            pl += "</font>"

                        else:
                            self.groups[k].remove_player(id)
                            pl +="<tr><td colspan='4' bgcolor=" + COLOR3 + " >Bad Player Ref (#" + id + ") in group"
                    pl+="</td></tr>"
                pl += "<tr><td colspan='4' bgcolor=" + COLOR1 + "><font color=" + COLOR4 + "><b><i>Statistics: groups: " + str(len(self.groups)) + "  players: " +  str(len(self.players)) + "</i></b></font></td></tr></table>"
            except Exception, e:
                self.log_msg(str(e))

        return pl

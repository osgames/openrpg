from orpg.tools.orpg_log import logger
from orpg.external.etree.ElementTree import ElementTree, Element
from orpg.external.etree.ElementTree import fromstring, tostring, iselement
from xml.parsers.expat import ExpatError

class MessageingClass(object):
    """
    This is used by the Client and server to build outgoing messages
    It does not allow building of nested message as needed by the gametree
    or map, but that can be achive via a for or while loop is needed

    All systems that want to parse a message should use the parse method
    """
    def __new__(cls):
        it = cls.__dict__.get("__it__")
        if it is not None:
            return it
        cls.__it__ = it = object.__new__(cls)
        return it

    def build(self, tag, *args, **kwargs):
        """
        Builds a simple ElementTree Element
        args:
            tag - This is the elements tag
            *arg - used to build the text of the element
                These are combined via ' '.join(args)
            **kwargs - used to define the attributes
                kwargs that start with _ have the _ stripped
                this is to all attributes like from
                if you really need the _ you need to do __
        """
        el = Element(tag)
        for attr, value in kwargs.iteritems():
            if not isinstance(value, basestring):
                value = str(value)

            if attr.startswith('_'):
                attr = attr[1:]

            el.set(attr, value)

        if len(args):
            el.text = ' '.join(args)

        return el

    def parse(self, raw_msg):
        try:
            el = fromstring(raw_msg)
        except ExpatError:
            #Backwards compatibility crap
            end = raw_msg.find(">")
            head = raw_msg[:end+1]
            msg = raw_msg[end+1:]
            el = fromstring(head)
            try:
                el1 = fromstring(msg)
                if el1.tag == 'font':
                    raise ExpatError("")
                el.append(el1)
            except ExpatError:
                el.text = msg
                logger.general("Bad Message: \n" + raw_msg)
        return el

    def client_parse(self, raw_msg):
        try:
            el = fromstring(raw_msg)
        except ExpatError:
            #Backwards compatibility crap
            try:
                end =raw_msg.rfind("</")
                el = fromstring(raw_msg[:end])
            except ExpatError:
                return self.parse(raw_msg)
        return el

messaging = MessageingClass()

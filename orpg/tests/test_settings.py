import py

py.test.importorskip("orpg")
py.test.importorskip("orpg.tools.settings")

"""
This only tests get and set for settings since I dont want to test adding
tabs and settings since I have no removal functions at this time
"""
def test_get_known(settings):
    assert settings.get('player') is not None, "Unable to fetch "
    "a known to exist setting"

def test_get_unknown(settings):
    assert settings.get('TestUnknownSetting') is None, "Found a setting "
    "that should not exist!"

def test_set_known(settings):
    oldval = settings.get('F12')
    settings.set('F12', 'Test')
    assert oldval != settings.get('F12')
    settings.set('F12', oldval)
    assert oldval == settings.get('F12')

def test_set_unknown(settings):
    py.test.raises(AttributeError,
                   "settings.set('TestUnknownSetting', 'test')")
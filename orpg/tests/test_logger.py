from __future__ import with_statement

import os
import py

py.test.importorskip("orpg")
py.test.importorskip("orpg.tools.orpg_log")

def pytest_generate_tests(metafunc):
    # called once per each test function
    if metafunc.function.__name__ in metafunc.cls.params:
        for funcargs in metafunc.cls.params[metafunc.function.__name__]:
            # schedule a new test function run with applied **funcargs
            metafunc.addcall(funcargs=funcargs)
    else:
        metafunc.addcall()

class TestLogger:
    params = {'test_no_output': [{'log_type':'debug', 'lvl': 1},
                                 {'log_type':'note', 'lvl': 1},
                                 {'log_type':'info', 'lvl': 1},
                                 {'log_type':'general', 'lvl': 1}],
              'test_to_console': [{'log_type':'debug', 'lvl': 1},
                                 {'log_type':'note', 'lvl': 1},
                                 {'log_type':'info', 'lvl': 1},
                                 {'log_type':'general', 'lvl': 1}],
              'test_proper_level': [{'log_type':'debug', 'lvl': 16},
                                 {'log_type':'note', 'lvl': 8},
                                 {'log_type':'info', 'lvl': 4},
                                 {'log_type':'general', 'lvl': 2}]}

    def test_set_log_level(self, logger):
        oldlvl = logger.log_level
        logger.log_level = 1
        assert oldlvl != logger.log_level
        logger.log_level = oldlvl

    def test_set_log_level_invalid(self, logger):
        def test():
            logger.log_level = 0

        py.test.raises(TypeError, test)

    def test_set_log_name(self, logger, dir_struct):
        oldname = logger.log_name
        logger.log_name = dir_struct['user'] + 'runlogs/test.log'
        assert oldname != logger.log_name, "Unable to change the log name"
        logger.log_name = oldname

    def test_set_log_name_invalid(self, logger):
        def test():
            logger.log_name = 'this/is/an/invalid/log/name.log'

        py.test.raises(IOError, test)

    def test_set_to_console(self, logger):
        oldval = logger.log_to_console
        logger.log_to_console = not oldval
        assert oldval != logger.log_to_console
        logger.log_to_console = oldval

    def test_no_output(self, logger, dir_struct, log_type, lvl):
        oldlvl = logger.log_level
        oldname = logger.log_name

        logfile = dir_struct['user'] + 'runlogs/test_%s_no_output.log' % (log_type)

        logger.log_level = lvl
        logger.log_name = logfile

        logger.debug("Test %s" % (log_type))

        py.test.raises(IOError, open, (logfile))

        logger.log_level = oldlvl
        logger.log_name = oldname

    def test_to_console(self, logger, dir_struct, log_type, lvl):
        oldlvl = logger.log_level
        oldname = logger.log_name

        logfile = dir_struct['user'] + 'runlogs/test_%s_to_console.log' % (log_type)

        logger.log_level = lvl
        logger.log_name = logfile

        func = getattr(logger, log_type)
        func("Test %s" % (log_type), True)

        with open(logfile) as f:
            assert "Test %s" % (log_type) in f.read()

        try:
            os.remove(logfile)
        except IOError:
            pass
        finally:
            logger.log_level = oldlvl
            logger.log_name = oldname

    def test_proper_level(self, logger, dir_struct, log_type, lvl):
        oldlvl = logger.log_level
        oldname = logger.log_name

        logfile = dir_struct['user'] + 'runlogs/test_%s_proper_level.log' % (log_type)

        logger.log_level = lvl
        logger.log_name = logfile

        func = getattr(logger, log_type)
        func("Test %s" % (log_type))

        with open(logfile) as f:
            assert "Test %s" % (log_type) in f.read()

        try:
            os.remove(logfile)
        except IOError:
            pass
        finally:
            logger.log_level = oldlvl
            logger.log_name = oldname


    def test_exception(self, logger, dir_struct):
        oldname = logger.log_name
        logfile = dir_struct['user'] + 'runlogs/test_exception.log'
        logger.log_name = logfile

        logger.exception("Test exception")

        with open(logfile) as f:
            assert "Test exception" in f.read()

        try:
            os.remove(logfile)
        except IOError:
            pass
        finally:
            logger.log_name = oldname

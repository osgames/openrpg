import py

py.test.importorskip("orpg")
py.test.importorskip("orpg.dieroller.rollers")
from orpg.dieroller import roller_manager

def pytest_generate_tests(metafunc):
    # called once per each test function
    for tests in metafunc.cls.params[metafunc.function.__name__]:
        # schedule a new test function run with applied **funcargs
        for funcargs in tests:
            metafunc.addcall(funcargs=funcargs)

class TestDiceRolls:
    _tests = [
            [dict(roll='1d20', min_roll=1, max_roll=20) for x in xrange(100)],
            [dict(roll='1d20.minroll(5)', min_roll=5,
                 max_roll=20) for x in xrange(100)],
            [dict(roll='4d6.each(5)', min_roll=1, max_roll=6,
                 num_results=4, extra=5) for x in xrange(100)],
            [dict(roll='4d6.takeHighest(3)', min_roll=1, num_results=3,
                 max_roll=6) for x in xrange(100)],
            [dict(roll='(1+2+1)d6.takeHighest(3)', min_roll=1, num_results=3,
                 max_roll=6) for x in xrange(100)],
            [dict(roll='4d6.takeHighest(3).minroll(3)', min_roll=3,
                 num_results=3, max_roll=6) for x in xrange(100)]]
    params = {
        'test_roll': _tests,
        #'test_old_roll': _tests,
    }

    def test_roll(self, roll, min_roll, max_roll,
                  num_results=1, extra=None):
        r = roller_manager.process_roll(roll)
        r = r.split('-->')[1].split('=')[1][1:].strip()
        results = eval(r)

        assert isinstance(results, list)
        assert len(results) == num_results
        for r in results:
            if extra is not None:
                assert r[0] >= min_roll and r[0] <= max_roll
                assert r[1] == extra
            else:
                assert r >= min_roll and r <= max_roll

    #def test_old_roll(self, roll, min_roll, max_roll, num_results=1, extra=None):
        #from orpg.dieroller import roller_manager
        #roller_manager.roller = 'old_std'
        #r = roller_manager.process_roll(roll)
        #r = r[3].split('=')[0].strip()
        #results = eval(r)

        #assert isinstance(results, list)
        #assert len(results) == num_results
        #for r in results:
            #if extra is not None:
                #assert r[0] >= min_roll and r[0] <= max_roll
                #assert r[1] == extra
            #else:
                #assert r >= min_roll and r <= max_roll

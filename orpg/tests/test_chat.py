import py

py.test.importorskip("orpg")
py.test.importorskip("orpg.chat")

def test_create_null_tab(client, open_rpg):
    chat = open_rpg.get_component('chat')
    new_tab = chat.parent.create_null_tab('Test Null Tab')
    assert new_tab in chat.parent.null_tabs, "Failed To Create Null Tab"

def test_create_group_tab(client, open_rpg):
    chat = open_rpg.get_component('chat')
    new_tab = chat.parent.create_group_tab('Test Group Tab')
    assert new_tab in chat.parent.group_tabs, "Failed To Create Group Tab"

"""
We only test that the whisper tab create fails since you would need to be
connected to a server with someone else for the create to work
"""
def test_create_whisper_tab_fail(client, open_rpg):
    chat = open_rpg.get_component('chat')
    py.test.raises(ValueError, chat.parent.create_whisper_tab, ('0'))

def test_close_tab(client, open_rpg):
    chat = open_rpg.get_component('chat')
    new_tab = chat.parent.create_null_tab('Test Closeable Tab')
    chat.parent.SetSelection(chat.parent.GetPageCount()-1)
    assert new_tab in chat.parent.null_tabs, "Failed To Create Null Tab"
    chat.parent.onCloseTab(None)
    assert new_tab not in chat.parent.null_tabs, "Failed To Close Null Tab"

def test_new_msg_notify(client, open_rpg):
    chat = open_rpg.get_component('chat')
    new_tab = chat.parent.create_null_tab('Test New Message Tab')
    tabid = chat.parent.GetPageCount()-1
    chat.parent.newMsg(tabid)
    assert chat.parent.GetPageImage(tabid) == 0, "Failed to switch to the New Message Icon"
    chat.parent.SetSelection(tabid)
    # Fake the event since Progamatic calls to SetSelection dont trigger
    chat.parent.SetPageImage(tabid, 1)
    assert chat.parent.GetPageImage(tabid) == 1, "Failed to switch to the No New Messages Icon"

def test_set_chat_text(client, open_rpg):
    chat = open_rpg.get_component('chat')
    chat.set_chat_text("Testing")
    assert chat.chattxt.GetValue() == "Testing", "Failed to set chat text"
    chat.set_chat_text("")

def test_colorize(client, open_rpg):
    chat = open_rpg.get_component('chat')
    text = chat.colorize("#000000", "test")
    assert text == '<font color="#000000">test</font>'

def test_info_message(client, open_rpg):
    chat = open_rpg.get_component('chat')
    chat.info_message('test_info_message')
    assert 'test_info_message</font>' in chat.chatwnd.GetPageSource()

def test_emote_message(client, open_rpg):
    chat = open_rpg.get_component('chat')
    chat.emote_message('test_emote_message')
    assert 'test_emote_message</font>' in chat.chatwnd.GetPageSource()

def test_whisper_to_players(client, open_rpg):
    chat = open_rpg.get_component('chat')
    msg = 'test_whisper_to_players'
    chat.whisper_to_players(msg, ['0'])
    assert 'whispering to  Unknown!,  ' + msg in chat.chatwnd.GetPageSource()

def test_scroll_lock(client, open_rpg):
    chat = open_rpg.get_component('chat')
    chat.lock_scroll(None)
    chat.info_message('test_scroll_lock')
    assert 'test_scroll_lock</font>' not in chat.chatwnd.GetPageSource()
    assert len(chat.storedata) > 0
    chat.lock_scroll(None)
    assert 'test_scroll_lock</font>' in chat.chatwnd.GetPageSource()
    assert len(chat.storedata) == 0

def test_SystemPost(client, open_rpg):
    chat = open_rpg.get_component('chat')
    chat.SystemPost('test_SystemPost')
    assert 'test_SystemPost</font>' in chat.chatwnd.GetPageSource()

def test_ParseDice(client, open_rpg):
    chat = open_rpg.get_component('chat')
    assert '<!-- Official Roll' in chat.ParseDice('[1d20]')

def test_help_cmd(client, open_rpg):
    chat = open_rpg.get_component('chat')
    chat.lock_scroll(None)

    chat.chat_cmds.on_help('')
    assert 'Command Alias List:' in chat.storedata[0]

    chat.lock_scroll(None)

def test_version_cmd(client, open_rpg):
    chat = open_rpg.get_component('chat')
    chat.lock_scroll(None)

    chat.chat_cmds.on_version('')
    assert 'Version is OpenRPG ' in chat.storedata[0]

    chat.lock_scroll(None)

def test_font_cmd(client, open_rpg):
    chat = open_rpg.get_component('chat')
    chat.lock_scroll(None)

    chat.chat_cmds.on_font('times')
    assert 'Font is now times point size ' in chat.storedata[0]

    chat.lock_scroll(None)

def test_fontsize_cmd(client, open_rpg):
    chat = open_rpg.get_component('chat')
    chat.lock_scroll(None)

    chat.chat_cmds.on_fontsize('12')
    assert 'point size 12' in chat.storedata[0]

    chat.lock_scroll(None)

def test_set_cmd(client, open_rpg):
    chat = open_rpg.get_component('chat')
    chat.lock_scroll(None)

    chat.chat_cmds.on_set('')
    assert '<table border="2">' in chat.storedata[0]

    chat.lock_scroll(None)

def test_name_cmd(client, open_rpg, settings):
    chat = open_rpg.get_component('chat')
    chat.lock_scroll(None)

    oldval = settings.get('player')
    chat.chat_cmds.on_name('Test Player')
    assert settings.get('player') == 'Test Player'
    settings.set('player', oldval)

    chat.lock_scroll(None)

def test_time_cmd(client, open_rpg):
    chat = open_rpg.get_component('chat')
    chat.lock_scroll(None)

    chat.chat_cmds.on_time('')
    assert 'Local:' in chat.storedata[0]
    assert 'GMT:' in chat.storedata[0]

    chat.lock_scroll(None)

def test_dieroller_cmd(client, open_rpg):
    chat = open_rpg.get_component('chat')
    chat.lock_scroll(None)

    chat.chat_cmds.on_dieroller('')
    assert 'Available die rollers:' in chat.storedata[0]
    assert 'You are using the ' in chat.storedata[1]

    chat.lock_scroll(None)

def test_description_cmd(client, open_rpg):
    chat = open_rpg.get_component('chat')
    chat.lock_scroll(None)

    chat.chat_cmds.on_description('Test Description')
    assert '<table bgcolor="#c0c0c0" border="3" cellpadding="5" ' in\
           chat.storedata[0]
    assert 'Test Description' in chat.storedata[0]

    chat.lock_scroll(None)


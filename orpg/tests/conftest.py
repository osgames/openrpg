import sys
import os, os.path
sys.path.insert(0, os.path.abspath(os.curdir))


def pytest_funcarg__settings(request):
    """
    Any time a test function needs access to the settings module, just add
    settings as one of its args and it will be passed the correct one
    """
    from orpg.tools.settings import settings
    return settings

def pytest_funcarg__validate(request):
    """
    Any time a test function needs access to the validate module, just add
    validate as one of its args and it will be passed the correct one
    """
    from orpg.tools.validate import validate
    return validate

def pytest_funcarg__logger(request):
    """
    Any time a test function needs access to the logger module, just add
    logger as one of its args and it will be passed the correct one
    """
    from orpg.tools.orpg_log import logger
    return logger

def pytest_funcarg__dir_struct(request):
    """
    Any time a test function needs access to the dir_struct module, just add
    dir_struct as one of its args and it will be passed the correct one
    """
    from orpg.dirpath import dir_struct
    return dir_struct

def pytest_funcarg__open_rpg(request):
    """
    Any time a test function needs access to the open_rpg module, just add
    open_rpg as one of its args and it will be passed the correct one
    """
    from orpg.orpgCore import open_rpg
    return open_rpg

def pytest_funcarg__client(request):
    """
    Any time a test function needs access to the client, just add
    client as one of its args and it will be passed the correct one
    """

    def setup():
        from orpg.main import orpgApp, orpgFrame
        from orpg.orpg_version import CLIENT_STRING

        open_rpg = request.getfuncargvalue('open_rpg')
        logger = request.getfuncargvalue('logger')
        dir_struct = request.getfuncargvalue('dir_struct')
        validate = request.getfuncargvalue('validate')
        settings = request.getfuncargvalue('settings')

        mainapp = orpgApp(0)
        open_rpg.add_component('log', logger)
        open_rpg.add_component('dir_struct', dir_struct)
        open_rpg.add_component('tabbedWindows', [])
        open_rpg.add_component('validate', validate)
        open_rpg.add_component('settings', settings)

        frame = orpgFrame(None, -1, CLIENT_STRING)
        frame.Raise()
        frame.Refresh()
        frame.Show()
        mainapp.SetTopWindow(frame)

        return frame

    def teardown(client):
        client.kill_mplay_session()
        client.closed_confirmed()

    return request.cached_setup(setup=setup, teardown=teardown, scope="session")

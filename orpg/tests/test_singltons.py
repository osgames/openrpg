import py

py.test.importorskip("orpg")

def test_validate(validate):
    from orpg.tools.validate import Validate
    v1 = Validate()
    assert validate == v1, "Validate is not a singleton"

def test_settings(settings):
    from orpg.tools.settings import Settings
    s1 = Settings()
    assert settings == s1, "Settings is not a singleton"

def test_dir_struct(dir_struct):
    from orpg.dirpath import DirStruct
    d1 = DirStruct()
    assert dir_struct == d1, "dir_struct is not a singleton"

def test_log(logger, dir_struct):
    from orpg.tools.orpg_log import orpgLog
    l1 = orpgLog(dir_struct.get("user") + "runlogs/")
    assert logger == l1, "orpgLog is not a singleton"

def test_open_rpg(open_rpg):
    from orpg.orpgCore import ORPGStorage
    o1 = ORPGStorage()
    assert open_rpg == o1, "ORPGStroage is not a singleton"
import py

py.test.importorskip("orpg")

def test_chat_exists(client):
    assert hasattr(client, 'chat')

def test_map_exists(client):
    assert hasattr(client, 'map')

def test_tree_exists(client):
    assert hasattr(client, 'tree')

def test_players_exists(client):
    assert hasattr(client, 'players')

def test_session_exists(client):
    assert hasattr(client, 'session')

def test_poll_timer_exists(client):
    assert hasattr(client, 'poll_timer')

def test_ping_timer_exists(client):
    assert hasattr(client, 'ping_timer')

def test_password_manager_exists(client):
    assert hasattr(client, 'password_manager')

def test_pluginsFrame_exists(client):
    assert hasattr(client, 'pluginsFrame')

def test_mgr_exists(client):
    assert hasattr(client, '_mgr')

import os
import py

py.test.importorskip("orpg")
py.test.importorskip("orpg.tools.validate")

def test_config_file(validate, dir_struct):
    validate.config_file('test_config_file', 'about.html')
    assert os.access(dir_struct['user'] + 'test_config_file', os.R_OK),\
    "File was not created!"
    try:
        os.remove(dir_struct['user'] + 'test_config_file')
    except IOError:
        pass

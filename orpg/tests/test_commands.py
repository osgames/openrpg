import py
from orpg.chat.commands import command_args_parser

def pytest_generate_tests(metafunc):
    if metafunc.function.__name__ == "test_parser":
        for funcargs in test_parser_params:
            metafunc.addcall(funcargs=funcargs)
    else:
        metafunc.addcall()

test_parser_params = [
{'cmdargs':'', 'expected_list':[], 'expected_map':{}},
{'cmdargs':'one', 'expected_list':['one'], 'expected_map':{}},
{'cmdargs':'one two three', 'expected_list':['one','two','three'], 'expected_map':{}},
{'cmdargs':'"quoted1"', 'expected_list':['quoted1'], 'expected_map':{}},
{'cmdargs':"'quoted2'", 'expected_list':['quoted2'], 'expected_map':{}},
{'cmdargs':"'one' 'two'", 'expected_list':['one','two'], 'expected_map':{}},
{'cmdargs':'"quoted spaces and = sign"', 'expected_list':['quoted spaces and = sign'], 'expected_map':{}},
{'cmdargs':'"can\'t"', 'expected_list':["can't"], 'expected_map':{}},
{'cmdargs':' one ', 'expected_list':['one'], 'expected_map':{}},
{'cmdargs':' one two ', 'expected_list':['one','two'], 'expected_map':{}},
{'cmdargs':'x=one', 'expected_list':[], 'expected_map':{'x':'one'}},
{'cmdargs':'xyz_456=one', 'expected_list':[], 'expected_map':{'xyz_456':'one'}},
{'cmdargs':'x=', 'expected_list':[], 'expected_map':{'x':''}},
{'cmdargs':' x=one y="two" ', 'expected_list':[], 'expected_map':{'x':'one', 'y':'two'}},
{'cmdargs':' x= y="" z=', 'expected_list':[], 'expected_map':{'x':'', 'y':'', 'z':''}},
{'cmdargs':' one x=two "three" y="four"', 'expected_list':['one','three'], 'expected_map':{'x':'two', 'y':'four'}},
{'cmdargs':'x=1 y=one x=2 y=two x=3', 'expected_list':[], 'expected_map':{'x':['1','2','3'], 'y':['one','two']}},
]

def test_parser(cmdargs, expected_list, expected_map):
    result_list, result_map = command_args_parser(cmdargs)
    assert result_list == expected_list
    assert result_map == expected_map

                

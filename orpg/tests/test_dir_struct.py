import os, os.path
import py

py.test.importorskip("orpg")
py.test.importorskip("orpg.dirpath")

def test_path_exists(dir_struct):
    path = os.path.abspath(os.path.dirname(__file__) + '/../templates') + os.sep

    assert path == dir_struct['template'],\
           "Finding the incorect path!\n Looking for: %s\nFound: %s" %\
           (path, dir_struct['template'])
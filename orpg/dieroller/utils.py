#!/usr/bin/env python
# Copyright (C) 2000-2001 The OpenRPG Project
#
#       openrpg-dev@lists.sourceforge.net
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# --
#
# File: dieroller/utils.py
# Author: OpenRPG Team
# Maintainer:
# Version:
#   $Id: utils.py,v 1.22 2007/05/05 05:30:10 digitalxero Exp $
#
# Description: Classes to help manage the die roller
#

__version__ = "$Id: utils.py,v 1.22 2007/05/05 05:30:10 digitalxero Exp $"

import re

import orpg.dieroller.rollers
from orpg.dieroller._base import BaseRoller

from orpg.tools.decorators import deprecated

class RollerManager(object):
    _rollers = {}
    _roller = None

    def __new__(cls):
        it = cls.__dict__.get("__it__")
        if it is not None:
            return it
        cls.__it__ = it = object.__new__(cls)
        return it

    def register(self, roller):
        if not self._rollers.has_key(roller.name):
            self._rollers[roller.name] = roller

    def list(self):
        return self._rollers.keys()

    def _get_roller(self):
        return self._roller.name
    def _set_roller(self, value):
        if not isinstance(value, basestring):
            raise TypeError("roller must be a string")

        if value not in self._rollers:
            raise KeyError("UNKNOWN ROLLER")

        self._roller = self._rollers[value]
    roller = property(_get_roller, _set_roller)

    def process_roll(self, roll_string):
        if isinstance(self.roller, BaseRoller):
            return self.roller(roll_string)
        else:
            #Ok we are using an OLD roller, so have fun with eval and shit
            return self.proccessRoll(roll_string)

    #All these Methods are depreciated and will go away soon
    @deprecated("setRoller has been depreciated, use the roller property")
    def setRoller(self, name):
        self.roller = name

    @deprecated("getRoller has been depreciated, use the roller property")
    def getRoller(self):
        return self.roller

    @deprecated("listRollers has been depreciated, use the list method")
    def listRollers(self):
        return self.list()

    @deprecated("stdDieToDClass has been depreciated, convert your roller to the new style")
    def stdDieToDClass(self,match):
        s = match.group(0)
        (num,sides) = s.split('d')

        if sides.strip().upper() == 'F':
            sides = "'f'"

        try:
            if int(num) > 100 or int(sides) > 10000:
                return None
        except:
            pass

        ret = ['(', num.strip(), "**die_rollers['", self.getRoller(), "'](",
                sides.strip(), '))']
        return ''.join(ret)

    #  Use this to convert ndm-style (3d6) dice to d_base format
    @deprecated("convertTheDieString has been depreciated, convert your roller to the new style")
    def convertTheDieString(self,s):
        reg = re.compile("(?:\d+|\([0-9\*/\-\+]+\))\s*[a-zA-Z]+\s*[\dFf]+")
        (result, num_matches) = reg.subn(self.stdDieToDClass, s)
        if num_matches == 0 or result is None:
            try:
                s2 = self.roller_class + "(0)." + s
                test = eval(s2)
                return s2
            except:
                pass
        return result

    @deprecated("proccessRoll has been depreciated, convert your roller to the new style")
    def proccessRoll(self, s):
        return str(eval(self.convertTheDieString(s)))

roller_manager = RollerManager()
import string
import random
import traceback
import re
import time
from collections import deque

from orpg.external.pyparsing import (Word, alphas, Literal, CaselessLiteral,
                       Combine, Optional, nums, Or, Forward, ZeroOrMore,
                       StringEnd, alphanums, OneOrMore, Group, ParseException)

from orpg.tools.orpg_log import logger
from orpg.tools.decorators import pending_deprecation

__all__ = ['BaseRoller', 'die_rollers', 'roller_manager']

class BaseRoller(object):
    """
    This is the base roller class that all rollers should derive from
    In your custom roller you can
    1) set your own `format_string`
    2) change the fudge value by overriding the `fudge_roll` method
    3) Add additional info to the roll result by overriding `build_extra`

    It is possible to, but not reccomended
    1) override the `roll` method, that gets the #d# strig and turns it into
       a list and adds it to the parse tree.
    2) override the `evaluate_roll` method. This turns the parse tree into
       a numeric result. You really should not touch this. If you dont want
       your numeric result just change your `format_string` and dont include
       the {result} part. Then you will definitively need to override the
       `build_extra` method to change your result string to your liking.
    """
    format_string = "{roll_string}{rolls} = ({result}) {extra}"
    extra_string = ""

    def fudge_roll(self, *args):
        """
        This changes then f in #df to the return result, which then gets
        processed by the roll handler. By default it changes it to 15
        You can feel free to change this in your custom roller
        """
        return 15

    def build_extra(self):
        """
        You should override this method if you want to add extra info to a
        roll result string
        """
        self.extra_string = ""

    def roll(self, *args):
        """
        This processes the actual roll, you should not need to override this
        """
        roll_str, loc, tokens = args
        roll_string = tokens[0]
        num, sides = roll_string.split("d")
        roll = Roll(roll_string, num, sides, [])
        for i in range(roll.die):
            roll.result.append(random.randint(1, roll.sides))

        self.history.append(roll)

    def evaluate_roll(self):
        """
        This processes the parse tree, you should not override this
        unless you are SURE you know what you are doing
        """
        op = self.history.pop()
        if isinstance(op, Roll):
            self._rolls.append(op)
            return int(op)
        elif op in "+-*/^":
            op1 = self.evaluate_roll()
            op2 = self.evaluate_roll()
            return self._opn[op](op2, op1)
        else:
            a = int(op)
            b = float(op)
            if a == b:
                return a

            return b

    def __call__(self, roll_string):
        """
        This method takes a `roll_string` like [1d4] and does all the parsing
        it will then call `evaluate_roll` to get the results and finally it
        calls `format_result` to allow custom rollers to format their output
        how they see fit.

        This should be the last method you look at overriding, but if you need
        very non standard rollers
        """
        self._rolls = []
        self._quiet = False
        self.extra_string = ""

        self._bnf.die_pattern.parseString(roll_string)
        self.build_extra()
        self._result = self.evaluate_roll()
        total_rolls = Roll("", 0, 0)
        total_rolls.result.extend(
            [[r for r in roll.result] for roll in self._rolls])
        total_rolls.result.reverse()
        if len(total_rolls.result) == 1:
            total_rolls.result = total_rolls.result[0]
        official = self._official.format(roll_string=roll_string,
                                         rolls=total_rolls,
                                         result=self.result)
        if self.quiet:
            roll_string = ""
        else:
            roll_string = "[{roll_string}] => ".format(
                roll_string=roll_string)


        total_rolls = Roll("", 0, 0)
        total_rolls.result.extend(
            [[r for r in roll] for roll in self._rolls])
        total_rolls.result.reverse()
        if len(total_rolls.result) == 1:
            total_rolls.result = total_rolls.result[0]

        return official + self.format_string.format(roll_string=roll_string,
                                                    rolls=total_rolls,
                                                    result=self.result,
                                                    extra=self.extra_string)

    def extraroll(self, roll):
        """
        Utility function to return another roll
        """
        return random.randint(1, roll.sides)

    def extend_roll(self, roll, idx, value=None):
        roll.modified = roll.modified or roll.result
        c = roll.modified[idx]
        new = value or self.extraroll(roll)
        if isinstance(c, int):
            roll.modified[idx] = Roll(roll.string, roll.die, roll.sides,
                                      [c, new])
        else:
            roll.modified[idx].result.append(new)

    #private methods DO NOT OVERRIDE these
    _history = deque([])
    _rolls = None
    _result = None
    _official = "<!-- Official Roll {roll_string} => {rolls!s} = ({result}) -->"
    _opn = {"+": (lambda a,b: a + b),
           "-": (lambda a,b: a - b),
           "*": (lambda a,b: a * b),
           "/": (lambda a,b: float(a) / float(b)),
           "^": (lambda a,b: a ** b)}

    def __new__(cls):
        it = cls.__dict__.get("__it__")
        if it is not None:
            return it
        cls.__it__ = it = object.__new__(cls)
        it._bnf = BNF(it)
        return it

    def _do_function(self, *args):
        """
        This takes a method from the roller string and calls the
        corisponding method on the roller. It will throw an exception
        if the method does not exist.
        """
        roll_str, loc, tokens = args
        func = getattr(self, tokens.name)
        if callable(func):
            if tokens.args[0]:
                func(*tokens.args)
            else:
                func()

    def _push(self, *args):
        """
        Used to build the parse tree
        """
        roll_str, loc, tokens = args
        self.history.append(tokens[0])

    def _get_quiet(self):
        return self._quiet
    def _set_quiet(self, *args):
        self._quiet = True
    quiet = property(_get_quiet)

    def _get_rolls(self):
        return self._rolls
    rolls = property(_get_rolls)

    def _get_history(self):
        return self._history
    history = property(_get_history)

    def _get_result(self):
        return self._result
    result = property(_get_result)


class RollerManager(object):
    _rollers = {}
    _roller = None

    def __new__(cls):
        it = cls.__dict__.get("__it__")
        if it is not None:
            return it
        cls.__it__ = it = object.__new__(cls)
        return it

    def register(self, roller):
        if not self._rollers.has_key(roller.name):
            if not isinstance(roller, BaseRoller) and\
               isinstance(roller(), BaseRoller):
                roller = roller()
            self._rollers[roller.name] = roller

    def list(self):
        return self._rollers.keys()

    def process_roll(self, roll_string):
        st = time.time()
        try:
            if isinstance(self._roller, BaseRoller):
                return self._roller(roll_string)
            else:
                #Ok we are using an OLD roller,
                #so have fun with eval and shit
                return self.proccessRoll(roll_string)
        except Exception:
            logger.exception(traceback.format_exc())
            return roll_string
        finally:
            pass
            #print "Roll Time:", time.time()-st

    def _get_roller(self):
        if self._roller:
            self._roller = self._rollers['std']

        return self._roller.name
    def _set_roller(self, value):
        if not isinstance(value, basestring):
            raise TypeError("roller must be a string")

        if value not in self._rollers:
            logger.exception("Unknown Roller: {roller}.\n"\
                           "    Setting the roller to std".format(roller=value))
            value = 'std'

        self._roller = self._rollers[value]
    roller = property(_get_roller, _set_roller)

    #All these Methods are depreciated and will go away soon
    def __getitem__(self, name):
        return self._rollers[name]

    @pending_deprecation("setRoller has been depreciated, use the roller property")
    def setRoller(self, name):
        self.roller = name

    @pending_deprecation("getRoller has been depreciated, use the roller property")
    def getRoller(self):
        return self.roller

    @pending_deprecation("listRollers has been depreciated, use the list method")
    def listRollers(self):
        return self.list()

    @pending_deprecation("stdDieToDClass has been depreciated, convert your roller to the new style")
    def stdDieToDClass(self,match):
        s = match.group(0)
        (num,sides) = s.split('d')

        if sides.strip().upper() == 'F':
            sides = "'f'"

        try:
            if int(num) > 100 or int(sides) > 10000:
                return None
        except:
            pass

        ret = ['(', num.strip(), "**roller_manager['", self.roller, "'](",
                sides.strip(), '))']
        return ''.join(ret)

    #  Use this to convert ndm-style (3d6) dice to d_base format
    @pending_deprecation("convertTheDieString has been depreciated, convert your roller to the new style")
    def convertTheDieString(self,s):
        reg = re.compile("(?:\d+|\([0-9\*/\-\+]+\))\s*[a-zA-Z]+\s*[\dFf]+")
        (result, num_matches) = reg.subn(self.stdDieToDClass, s)
        if num_matches == 0 or result is None:
            try:
                s2 = self.roller_class + "(0)." + s
                test = eval(s2)
                return s2
            except:
                pass
        return result

    @pending_deprecation("proccessRoll has been depreciated, convert your roller to the new style")
    def proccessRoll(self, s):
        qmode = 0
        newstr1 = s
        if s[0].lower() == 'q':
            s = s[1:]
            qmode = 1
        try:
            s = str(eval(self.convertTheDieString(s)))
        except Exception:
            logger.exception(traceback.format_exc())

        if qmode == 1:
            off_roll = ["<!-- Official Roll [ ",
                        newstr1,
                        " ] => ",
                        s,
                        "-->",
                        s]
        else:
            off_roll = ["[",
                        newstr1,
                        "<!-- Official Roll -->] => ",
                        s]
        return off_roll

roller_manager = RollerManager()

class DieRollers(object):
    def __new__(cls):
        it = cls.__dict__.get("__it__")
        if it is not None:
            return it
        cls.__it__ = it = object.__new__(cls)
        return it

    @pending_deprecation("Use roller_manager.list() instead")
    def keys(self):
        return roller_manager.list()

    @pending_deprecation("Use roller_manager.register instead")
    def register(self, roller):
        roller_manager.register(roller)

    def __getitem__(self, roller_name):
        raise NotImplemented

    def __setitem__(self, *args):
        raise AttributeError

die_rollers = DieRollers()

class Roll(object):
    _string = ""
    _die = 0
    _sides = 0
    _result = None
    _modified = None

    def __init__(self, roll_string, num_die, sides, result=None):
        self.string = roll_string
        self.die = num_die
        self.sides = sides
        self.result = result or []

    def __int__(self):
        if self._modified:
            return sum(self._modified)

        return sum(self._result)

    def __float__(self):
        return float(int(self))

    def __str__(self):
        out = ['[']
        use = self._modified or self._result
        out.extend([str(r) + ', ' for r in use])
        out[-1] = out[-1].strip(', ')
        out.append(']')

        return ''.join(out)

    def __add__(self, other):
        return int(self) + other
    __radd__ = __add__

    def __mul__(self, by):
        return int(self) * by
    __rmul__ = __mul__

    def __div__(self, by):
        return int(self) / float(by)
    def __rdiv__(self, by):
        return float(by) / float(self)

    def __sub__(self, other):
        return int(self) - other
    def __rsub__(self, other):
        return other - int(self)

    def __iter__(self):
        loop = self._modified or self._result
        for i in loop:
            yield i

    def __contains__(self, item):
        check = self._modified or self._result
        return item in check

    def __len__(self):
        check = self._modified or self._result
        return len(check)

    def sort(self, cmp=None, key=None, reverse=False):
        self._modified = self._modified or self._result
        self._modified.sort(cmp, key, reverse)

    def _get_string(self):
        return self._string
    def _set_string(self, value):
        if not isinstance(value, basestring):
            raise TypeError("string must be a string")
        self._string = value
    string = property(_get_string, _set_string)

    def _get_die(self):
        return self._die
    def _set_die(self, value):
        if not isinstance(value, int):
            try:
                value = int(eval(value))
            except ValueError:
                raise TypeError("die must be an int")
        self._die = value
    die = property(_get_die, _set_die)

    def _get_sides(self):
        return self._sides
    def _set_sides(self, value):
        if not isinstance(value, int):
            try:
                value = int(eval(value))
            except ValueError:
                raise TypeError("die must be an int")
        self._sides = value
    sides = property(_get_sides, _set_sides)

    def _get_result(self):
        return self._result
    def _set_result(self, value):
        if not isinstance(value, (tuple, list)):
            raise TypeError("result must be a list")
        self._result = value
    result = property(_get_result, _set_result)

    def _get_modified(self):
        return self._modified
    def _set_modified(self, value):
        if not isinstance(value, (list, type(None))):
            raise TypeError("modified must be an instance or None")
        self._modified = value
    modified = property(_get_modified, _set_modified)

class BNF(object):
    def __init__(self, roller):
        point = Literal('.')
        plusorminus = Literal('+') | Literal('-')
        number = Word(nums)
        integer = Combine(Optional(
            plusorminus) + number)
        floatnumber = Combine(integer +\
                                       Optional(point +\
                                                Optional(number)))
        plus = Literal( "+" )
        minus = Literal( "-" )
        mult = Literal( "*" )
        div = Literal( "/" )
        lpar = Literal( "(" )
        rpar = Literal( ")" )
        addop = plus | minus
        multop = mult | div
        expop = Literal( "^" )

        die_expr = Forward()

        func_name = Word(alphas, alphanums + '_').setResultsName("name")
        func_arg = ZeroOrMore(Word(alphanums, nums))
        func_args = Combine(func_arg +\
                                     ZeroOrMore(ZeroOrMore(" ").suppress() +\
                                                Literal(",") +\
                                                ZeroOrMore(" ").suppress() +\
                                                  func_arg)).\
            setResultsName("args").setParseAction(self.build_args)
        func_expr = Combine(point + func_name +\
                                     lpar + func_args +\
                                     rpar).setParseAction(roller._do_function)

        roll_expr = Forward()

        die_expr << Combine(ZeroOrMore(CaselessLiteral("q").\
                                      setParseAction(roller._set_quiet).\
                                      suppress()) +\
                            ((lpar + roll_expr + rpar) | number)+\
                           CaselessLiteral("d") +\
                           (CaselessLiteral("f").\
                            setParseAction(roller.fudge_roll) |\
                            ((lpar + roll_expr + rpar) | number))).\
            setParseAction(roller.roll)

        atom = (
            OneOrMore(die_expr + ZeroOrMore(func_expr)) |
            (floatnumber | integer).setParseAction(roller._push) |
            (lpar + roll_expr.suppress() + rpar)
        )
        factor = Forward()
        factor << atom + ZeroOrMore((expop + factor).\
                            setParseAction(roller._push))
        term = factor + ZeroOrMore((multop + factor).\
                            setParseAction(roller._push))
        roll_expr << term + ZeroOrMore((addop + term).\
                                setParseAction(roller._push))

        self.die_pattern = roll_expr + StringEnd()

    def build_args(self, *args):
        roll_str, loc, tokens = args
        ret = []
        for arg in tokens[0].split(","):
            try:
                a = int(arg)
                b = float(arg)
                ret.append(a if a == b else b)
            except ValueError:
                ret.append(arg)
        return [ret]
from orpg.dieroller._base import roller_manager, BaseRoller

class StandardRoller(BaseRoller):
    """
    Basic roller designed to accept any combination of dice & sides,
    the results of each die are added up at the and providing a result
    total for the roll
    """
    name = "std"

    def ascending(self):
        """
        Sort the results of each die in ascending order
        """
        roll = self.history.pop()
        roll.sort()
        self.history.append(roll)

    def descending(self):
        """
        Sort the result of each die in descending order
        """
        roll = self.history.pop()
        roll.sort(reverse=True)
        self.history.append(roll)

    def takeHighest(self, num):
        """
        Take the highest `num` results of the roll
        eg [4d6.takeHighest(3)] => [4, 4, 6] = (14)
        """
        roll = self.history.pop()
        roll.sort(reverse=True)
        roll.modified = roll.result[:num] if not roll.modified else roll.modified[:num]
        self.history.append(roll)

    def takeLowest(self, num):
        """
        Take the lowest `num` results of the roll
        eg [5d6.takeLowest(3)] => [1, 2, 3] = (6)
        """
        roll = self.history.pop()
        roll.sort()
        roll.modified = roll.result[:num] if not roll.modified else roll.modified[:num]
        self.history.append(roll)

    def extra(self, num):
        """
        Add an extra die for each current roll larger then `num`
        eg [1d6.extra(2)] => [[3, 4]] = (7)
        """
        roll = self.history.pop()
        roll.modified = roll.modified or roll.result

        def do_extra(roll_):
            for i in xrange(len(roll_)):
                r = roll_.modified[i] if roll_.modified else roll_.result[i]
                if not isinstance(r, (int, type(None))):
                    do_extra(r)
                elif r >= num:
                    self.extend_roll(roll_, i)
        do_extra(roll)
        self.history.append(roll)

    def open(self, num):
        """
        Similar to extra, but it keeps looping over the roll and doing more
        rolls until no new rolls are larger then `num`
        eg [1d6.open(2)] => [[3, 4, 5, 3, 2]] = (17)
        """
        roll = self.history.pop()
        roll.modified = roll.modified or roll.result

        def do_open(roll_):
            for i in xrange(len(roll_)):
                r = roll_.modified[i] if roll_.modified else roll_.result[i]
                if not isinstance(r, (int, type(None))):
                    do_open(r)
                else:
                    while r >= num:
                        self.extend_roll(roll_, i)
                        r = roll_.modified[i].result[-1]

        do_open(roll)
        self.history.append(roll)

    def minroll(self, num):
        """
        Ensure that no die roll is less then `num`
        eg [3d6.minroll(2)] => [2, 5, 3] = (10)
        """
        roll = self.history.pop()
        roll.modified = roll.modified or roll.result
        for i in xrange(len(roll)):
            die = roll.modified[i]
            while die < num:
                die = self.extraroll(roll)
            roll.modified[i] = die

        self.history.append(roll)

    def each(self, num):
        """
        Add `num` to each die of the original roll,
        die added by extra or open are not included in this
        eg [4d6.each(2)] => [[3, 2], [2, 2], [5, 2], [4, 2]] = (22)
        eg [4d6.extra(4)] => [[3, 2], [2, 2], [5, 1, 2], [4, 6, 2]] = (29)
        """
        roll = self.history.pop()
        roll.modified = roll.modified or roll.result
        for i in xrange(len(roll)):
            self.extend_roll(roll, i, num)
        self.history.append(roll)

    def vs(self, target):
        """
        Checks each roll against `target` and set 1 for sucess 0 for failure,
        this results in the total being the number of successes.
        eg [5d10.vs(7)] => [1, 0, 0, 1, 0] = (2)
        eg [5d20.vs(10)] => [1, 1, 0, 1, 1] = (4)
        """
        roll = self.history.pop()
        roll.modified = roll.modified or roll.result
        for i in xrange(len(roll)):
            die = roll.modified[i]
            roll.modified[i] = 1 if die >= target else 0

roller_manager.register(StandardRoller)

from orpg.dieroller.base import die_base
class std(die_base):
    name = "old_std"

    def __init__(self,source=[]):
        die_base.__init__(self, source)

    #  Examples of adding member functions through inheritance.

    def ascending(self):
        result = self[:]
        result.sort()
        return result

    def descending(self):
        result = self[:]
        result.sort()
        result.reverse()
        return result

    def takeHighest(self,num_dice):
        return self.descending()[:num_dice]

    def takeLowest(self,num_dice):
        return self.ascending()[:num_dice]

    def extra(self,num):
        for i in range(len(self.data)):
            if self.data[i].lastroll() >= num:
                self.data[i].extraroll()
        return self

    def open(self,num):
        if num <= 1:
            self
        done = 1
        for i in range(len(self.data)):
            if self.data[i].lastroll() >= num:
                self.data[i].extraroll()
                done = 0
        if done:
            return self
        else:
            return self.open(num)

    def minroll(self,min):
        for i in range(len(self.data)):
            if self.data[i].lastroll() < min:
                self.data[i].roll(min)
        return self

    def each(self,mod):
        mod = int(mod)
        for i in range(len(self.data)):
            self.data[i].modify(mod)
        return self


    def vs(self, target):
        for dn in self.data:
            dn.target = target
        return self


    ## If we are testing against a saving throw, we check for
    ## greater than or equal to against the target value and
    ## we only return the number of successful saves.  A negative
    ## value will never be generated.
    def sum(self):
        retValue = 0
        for dn in self.data:
            setValue = reduce( lambda x, y : int(x)+int(y), dn.history )
            if dn.target:
                if setValue >= dn.target:
                    retValue += 1

            else:
                retValue += setValue

        return retValue
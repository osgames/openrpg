## a vs die roller as used by WOD games
#!/usr/bin/env python
# Copyright (C) 2000-2001 The OpenRPG Project
#
#       openrpg-dev@lists.sourceforge.net
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# --
#
# File: wod.py
# Author: OpenRPG Dev Team
# Maintainer:
# Version:
#   $Id: wod.py,v 1.14 2007/05/09 19:57:00 digitalxero Exp $
#
# Description: WOD die roller
#
# Targetthr is the Threshhold target
# for compatibility with Mage die rolls.
# Threshhold addition by robert t childers


from orpg.dieroller._base import BaseRoller, roller_manager

class WODRoller(BaseRoller):
    """
    World of Darkness die roller
    This roller is designed for d10's only and always expects a vs target
    use the .vs method to set the target, all future rolls will use that
    target, unless you use .vs again
    """
    name = "wod"
    format_string = "{roll_string}{rolls} {extra}"
    format_extra = "vs {target} result of {wod_result}"
    target = 5
    _thr = 0

    def build_extra(self):
        try:
            roll = self.history.pop()
        except Exception:
            print self.rolls
            raise

        thr = self._thr + 0
        success = 0
        botch = False
        for r in roll:
            if r >= self.target or r == 10:
                success += 1
                if thr > 0:
                    thr -= 1
                    success -= 1
                else:
                    botch = True
            elif r == 1:
                success -= 1

            if botch and success < 0:
                success = 0

        if success < 0:
            self.extra_string = self.format_extra.format(target=self.target,
                                                  wod_result='a botch')
        elif success > 0:
            self.extra_string = self.format_extra.format(target=self.target,
                                        wod_result='(' + str(success) + ')')
        else:
            self.extra_string = self.format_extra.format(target=self.target,
                                                  wod_result='a failure')

        self.history.append(roll)

    def vs(self, target=5):
        """
        set the target for all future rolls to use
        """
        self.target = target

    def thr(self, thr=0):
        """
        set the thr for all future rolls to use
        """
        self._thr = thr

roller_manager.register(WODRoller)

#Old
#from std import std
#class wod(std):
    #name = "old_wod"

    #def __init__(self,source=[],target=0,targetthr=0):
        #std.__init__(self,source)
        #self.target = target
        #self.targetthr = targetthr

    #def vs(self,target):
        #self.target = target
        #return self

    #def thr(self,targetthr):
        #self.targetthr = targetthr
        #return self

    #def sum(self):
        #rolls = []
        #s = 0
        #s1 = self.targetthr
        #botch = 0
        #for a in self.data:
            #rolls.extend(a.gethistory())
        #for r in rolls:
            #if r >= self.target or r == 10:
                #s += 1
                #if s1 >0:
                    #s1 -= 1
                    #s -= 1
                #else:
                    #botch = 1
            #elif r == 1:
                #s -= 1
            #if botch == 1 and s < 0:
                #s = 0
        #return s

    #def __str__(self):
        #if len(self.data) > 0:
            #myStr = "[" + str(self.data[0])
            #for a in self.data[1:]:
                #myStr += ","
                #myStr += str(a)
            #if self.sum() < 0:
                #myStr += "] vs " +str(self.target)+" result of a botch"
            #elif self.sum() == 0:
                #myStr += "] vs " +str(self.target)+" result of a failure"
            #else:
                #myStr += "] vs " +str(self.target)+" result of (" + str(self.sum()) + ")"


        #return myStr

#roller_manager.register(wod)
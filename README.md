# OpenRPG

Originally developed at https://sourceforge.net/projects/openrpg/ and https://app.assembla.com/spaces/openrpg/ by [posterboy](http://www.rpgobjects.com/index.php?c=orpg), [Digitalxero](https://app.assembla.com/user/popup_profile/Digitalxero), [sirebral](https://app.assembla.com/user/popup_profile/sirebral) and others. Published under GPL 2.0.

This Git repository combines the contents of https://sourceforge.net/p/openrpg/cvs/ (cvs), https://sourceforge.net/p/openrpg/svn/ (svn), https://app.assembla.com/spaces/openrpg/mercurial/source (hg) and https://app.assembla.com/spaces/traipse/mercurial/source (hg).
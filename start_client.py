#!/usr/bin/env python

import sys
import os, os.path
import runpy
from orpg.tools.orpg_log import logger

runpy.run_module('updater.gui', run_name='__main__', alter_sys=True)

import pyver
pyver.checkPyVersion()

try:
    import wx
    WXLOADED = True
except ImportError:
    WXLOADED = False

import orpg.main
from orpg.tools.orpg_log import logger

if __name__ == "__main__":
    os.chdir(os.path.abspath(os.path.dirname(sys.argv[0])))
    if WXLOADED:
        mainapp = orpg.main.orpgApp(0)
        mainapp.MainLoop()
    else:
        logger.exception("You really really need wx!")
